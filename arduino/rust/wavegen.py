# gensine.py
# Sine wave generation using integer math
import argparse
import math
import sys

# Currently hosted at https://sourceforge.net/projects/handy-cpif/
# If you have Mercurial installed, try:
# pip install --user hg+http://hg.code.sf.net/p/handy-cpif/code
from cpif import graphics


def draw_circle_using_sin_cos(dw):
    padding = 10
    size = min((dw.width - padding*4) // 3, (dw.height - padding*2))
    cx = padding + size / 2  # left third of window
    cy = padding + size / 2  # centered vertically
    r = size / 2.0

    freq = 1000.0  # Hz
    sample_rate = 16000  # samples / sec
    num_cycles = 1000
    duration = (1.0 / freq) * num_cycles  # sec
    dt = 1.0 / sample_rate
    w = 2.0 * math.pi * freq  # angular velocity

    t = 0.0
    while t < duration:
        x = r * math.cos(w * t)
        y = r * math.sin(w * t)
        dw.circle(cx + x, cy - y, r/100, 'black', fill='red')
        t += dt


def draw_circle_using_float_rotation(dw):
    padding = 10
    size = min((dw.width - padding*4) // 3, (dw.height - padding*2))
    cx = padding + size + padding + size/2  # middle third of window
    cy = padding + size / 2  # centered vertically
    r = size / 2.0

    freq = 1000.0  # Hz
    sample_rate = 16000  # samples / sec
    num_cycles = 1000
    duration = (1.0 / freq) * num_cycles  # sec
    dt = 1.0 / sample_rate
    w = 2.0 * math.pi * freq  # angular velocity
    theta = w * dt  # change in angle per sample
    ct = math.cos(theta)
    st = math.sin(theta)

    x = 1.0
    y = 0.0
    t = 0.0
    while t < duration:
        dw.circle(cx + (r * x), cy - (r * y), r/100, 'black', fill='green')
        # https://www.101computing.net/2d-rotation-matrix/
        next_x = x * ct - y * st
        next_y = x * st + y * ct
        x = next_x
        y = next_y
        t += dt


def draw_circle_using_integer_rotation(dw):
    padding = 10
    size = min((dw.width - padding*4) // 3, (dw.height - padding*2))
    cx = padding*2 + size*2 + padding + size/2  # right third of window
    cy = padding + size / 2  # centered vertically
    r = size / 2.0

    freq = 1000.0  # Hz
    sample_rate = 16000  # samples / sec
    rotator = SineGen(freq)
    num_cycles = 1000
    duration = (1.0 / freq) * num_cycles  # sec
    num_samples = int(duration * sample_rate)

    for i in range(num_samples):
        y = rotator.get_y_float()
        x = math.sqrt(1.0 - (y * y))
        dw.circle(cx + (r * x), cy - (r * y), r/100, 'black', fill='blue')
        rotator.rotate()


class SineGen:
    """
    Incrementally generate a series of sine wave values

    self.y is the sine value of the current phase angle.
    The phase angle starts at zero and is incremented on each call to rotate().
    """

    # 256-entry lookup table of sine values for angles from pi/(2*256) through
    # pi/2 inclusive, scaled by 127.
    LOOKUP = [
        int(math.sin(((i + 1) * math.pi) / (256.0 * 2.0)) * 127.0)
        for i in range(256)
    ]

    SAMP_RATE = 16000

    def __init__(self, freq):
        """
        freq: Frequency of rotations in cycles per second (Hz)
        """
        #
        # All angles are in units such that 65536 units = 1 complete circle
        #
        # The current phase angle:
        self.phase = 0
        # The current value of sine(self.phase), scaled by 127 and truncated to
        # an integer:
        self.y = 0
        # The change in phase per sample (1 sample == 1 call to .rotate()):
        self.theta = int((65536 * freq) / self.SAMP_RATE)

    def rotate(self):
        """
        Increase the phase angle by the per-sample phase velocity.
        """
        self.phase = (self.phase + self.theta) & 0xffff
        # Calculate the sine of the new phase angle, scaled by 127
        # For table lookup, 256 units = 1 quarter circle
        angle = (self.phase >> 6) & 0xff
        quadrant = self.phase >> 14
        negate = False
        if quadrant == 0:
            pass
        elif quadrant == 1:
            angle = 256 - angle
        elif quadrant == 2:
            negate = True
        else:
            negate = True
            angle = 256 - angle
        if angle == 0:
            self.y = 0
        else:
            self.y = self.LOOKUP[angle - 1]
        if negate:
            self.y *= -1

    def get_phase_float(self):
        """
        Return the current phase angle in radians
        """
        return (2.0 * math.pi * self.phase) / 65536.0

    def get_y_float(self):
        """
        Return the sine of the current phase angle
        """
        return self.y / 127


def try_integer_rotation(freq, num_samples: int):
    """
    Calculate the error bounds of rotation over many samples
    """
    if freq <= 0:
        raise ValueError("freq must be > 0")
    if num_samples <= 0:
        raise ValueError("num_samples must be > 0")
    rotator = SineGen(freq)
    max_err = None
    min_err = None
    for _ in range(num_samples):
        # Calculate sine of current phase angle of rotator
        angle = rotator.get_phase_float()
        y = math.sin(angle)
        # Compare with rotator sine value
        err = rotator.get_y_float() - y
        if max_err is None or err > max_err:
            max_err = err
        if min_err is None or err < min_err:
            min_err = err
        # Step to next sample
        rotator.rotate()
    expected_phase = 2.0 * math.pi * freq * num_samples / SineGen.SAMP_RATE
    expected_phase %= math.pi * 2.0
    phase_err = rotator.get_phase_float() - expected_phase
    while phase_err > math.pi:
        phase_err -= math.pi
    while phase_err < -math.pi:
        phase_err += math.pi
    return (min_err, max_err, phase_err)


def compute_rotation_accuracy_table():
    for freq in (261.63, 440.00, 1000, 2093):
        print(f"Frequency: {freq} Hz")
        print("num_samples min_err                  max_err        phase_err")
        for num_samples in (100, 1000, 10000, 100000, 1000000):
            min_err, max_err, phase_err = try_integer_rotation(freq, num_samples)
            print(f"{num_samples:10} {min_err:23} {max_err:23} {phase_err}")
        print()


def main():
    parser = argparse.ArgumentParser(description="Generate waveforms")
    parser.add_argument('-g', '--graphical', action='store_true')
    args = parser.parse_args()
    if args.graphical:
        dw = graphics.DrawingWindow(940, 320)
        draw_circle_using_sin_cos(dw)
        draw_circle_using_float_rotation(dw)
        draw_circle_using_integer_rotation(dw)
        dw.run()
        return 0
    compute_rotation_accuracy_table()
    return 0


if __name__ == '__main__':
    sys.exit(main())
