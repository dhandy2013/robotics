David H's Arduino Programs Written in Rust
==========================================

The Rust source code for each program is in a separate file under ``src/bin/``.
For example, ``src/bin/blink.rs`` is a simple program that blinks the on-board
LED on the Arduino.

The following instructions are for setting up, compiling, and running these
Rust programs from a Linux host computer.


One-time per-host-machine setup
-------------------------------

```
# Install compiler, library, and helper program.
# This command will be different depending on your version of Linux; this worked on Fedora.
sudo yum install avr-gcc avr-libc avrdude

cargo install ravedude  # enables `cargo run` to start program on Arduino
rustup toolchain install nightly-2021-01-07
rustup +nightly-2021-01-07 component add rust-src
```

*Note*: You must use this exact version of the Rust compiler.


Compiling the blink example
---------------------------

```
cargo build --bin blink --release
```

The resulting program is in: ``./target/atmega328p/release/blink.elf``


Running the blink example
-------------------------

Plug in your Arduino's USB cable into your computer, then run:
```
cargo run --bin blink --release
```

To do the same thing "manually" (without ``cargo run``):
```
avrdude -patmega328p -carduino -P/dev/ttyACM0 -D "-Uflash:w:target/atmega328p/release/blink.elf:e"
```

The appropriate lights will blink while the program is being loaded onto the
Arduino board. Then the main onboard LED will settle down to blinking in the
steady pattern dictated by the ``blink.rs`` program.


Connecting to Arduino TTY Output
--------------------------------

If the Ardunio is running a program and you just want to see the TTY output
from it in your Linux terminal window, run this command:
```
picocom -b 57600 /dev/ttyACM0
```
To disconnect from the Arduino and quit ``picocom``, press Ctrl-A then Ctrl-Q.
You cannot stop ``picocom`` by pressing Ctrl-C, that will send the Ctrl-C byte
(0x03) to the Arduino.


Showing the assembly code
--------------------------

Example of dumping the compiled blink program in assembly language format:
```
avr-objdump --file-headers --section-headers --disassemble target/atmega328p/release/blink.elf | less -FRX
```

Example of dumping the data section of a program:
```
avr-objdump --file-headers --section-headers --disassemble --section=.data target/atmega328p/release/uno-experiments.elf | less -FRX
```
