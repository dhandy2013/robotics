* Music file in GWBASIC PLAY format
* See https://hwiegman.home.xs4all.nl/gw-man/PLAY.html
*
* Changes from the old GWBASIC PLAY format:
* The input can contain multiple lines of ASCII text
* The "*" character means comment till end-of-line
* Octave numbers follow the international standard:
*   C in octave 4 is middle C (not octave 3 like GWBASIC)
*   Default is still octave 4 however
* N command not yet implemented
* MF (music foreground) and MB (music background) have no effect

* Example: Notes going up and down the scale
* c8 e8 g8 > c < g8 e8 c2

* "Death Theme" from https://github.com/DualBrain/gw-basic/blob/master/spacesc.bas
* MN T70 O2 B8 > D8 F#8 < B8 > D8 F#8 < B8 > D8 G8 < B8 > D8 G8 < B8 > D8
* G8 < B8 > D8 F#8 < A8 > D8 F#8 < A8 > D8 F#8 < A8 > C#8 E8 < A8 > C#8
* E8 < A8 > C#8

* The "Computer Programing is Fun!" song from https://www.handysoftware.com/cpif/
c
g c l8 ml g f e d mn
l4 f e d2
g c l8 ml g f e d mn
l4 e d c2
