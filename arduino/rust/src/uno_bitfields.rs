//! Define bit fields for ATmega328P not found in the avrd crate.
//! The names are taken from the datasheet. See section 30: Register Summary.

// Bitfields of TCCR0A - Timer/Counter0 Control Register A
pub const COM0A1: u8 = 0b10000000; // Compare Match Output A Mode
pub const COM0A0: u8 = 0b01000000; // Compare Match Output A Mode
pub const COM0B1: u8 = 0b00100000; // Compare Match Output B Mode
pub const COM0B0: u8 = 0b00010000; // Compare Match Output B Mode
pub const WGM01: u8 = 0b00000010; // Waveform Generation Mode
pub const WGM00: u8 = 0b00000001; // Waveform Generation Mode

// Bitfields of TCCR0B - TImer/Counter0 Control Register B
pub const FOC0A: u8 = 0b10000000; // Force Output Compare A
pub const FOC0B: u8 = 0b01000000; // Force Output Compare B
pub const WGM02: u8 = 0b00001000; // Waveform Generation Mode
pub const CS02: u8 = 0b00000100; // Clock Select
pub const CS01: u8 = 0b00000010; // Clock Select
pub const CS00: u8 = 0b00000001; // Clock Select

// Bitfields of TCCR1A - Timer/Counter1 Control Register A
pub const COM1A1: u8 = 0b10000000; // Compare Output Mode for Channel A
pub const COM1A0: u8 = 0b01000000; // Compare Output Mode for Channel A
pub const COM1B1: u8 = 0b00100000; // Compare Output Mode for Channel B
pub const COM1B0: u8 = 0b00010000; // Compare Output Mode for Channel B
pub const WGM11: u8 = 0b00000010; // Waveform Generation Mode
pub const WGM10: u8 = 0b00000001; // Waveform Generation Mode

// Bitfields of TCCR1B - Timer/Counter1 Control Register B
pub const ICNC1: u8 = 0b10000000; // Input Capture Noise Canceler
pub const ICES1: u8 = 0b01000000; // Input Capture Edge Select
pub const WGM13: u8 = 0b00010000; // Waveform Generation Mode
pub const WGM12: u8 = 0b00001000; // Waveform Generation Mode
pub const CS12: u8 = 0b00000100; // Clock Select
pub const CS11: u8 = 0b00000010; // Clock Select
pub const CS10: u8 = 0b00000001; // Clock Select

// Bitfields of TCCR2A - Timer/Counter2 Control Register A
pub const COM2A1: u8 = 0b10000000; // Compare Match Output A Mode
pub const COM2A0: u8 = 0b01000000; // Compare Match Output A Mode
pub const COM2B1: u8 = 0b00100000; // Compare Match Output B Mode
pub const COM2B0: u8 = 0b00010000; // Compare Match Output B Mode
pub const WGM21: u8 = 0b00000010; // Waveform Generation Mode
pub const WGM20: u8 = 0b00000001; // Waveform Generation Mode

// Bitfields of TCCR2B - Timer/Counter2 Control Register B
pub const FOC2A: u8 = 0b10000000; // Force Output Compare A
pub const FOC2B: u8 = 0b10000000; // Force Output Compare B
pub const WGM22: u8 = 0b00001000; // Waveform Generation Mode
pub const CS22: u8 = 0b00000100; // bit on register TCCR2B
pub const CS21: u8 = 0b00000010; // bit on register TCCR2B
pub const CS20: u8 = 0b00000001; // bit on register TCCR2B

// Bitfields of TIFR1 - Timer/Counter1 Interrupt Flag Register
pub const ICF1: u8 = 0b00100000; // Timer/Counter1, Input Capture Flag
pub const OCF1B: u8 = 0b00000100; // Timer/Counter1, Output Compare B Match Flag
pub const OCF1A: u8 = 0b00000010; // Timer/Counter1, Output Compare A Match Flag
pub const TOV1: u8 = 0b00000001; // Timer/Counter1, Overflow Flag

// Bitfields of ADCSRA - ADC Control and Status Register A
pub const ADSC: u8 = 0b01000000;
pub const ADIF: u8 = 0b00010000;

// Bitfields of register PORTD
pub const PORTD7: u8 = 0b10000000;
pub const PORTD6: u8 = 0b01000000;
pub const PORTD5: u8 = 0b00100000;
pub const PORTD4: u8 = 0b00010000;
pub const PORTD3: u8 = 0b00001000;
pub const PORTD2: u8 = 0b00000100;
pub const PORTD1: u8 = 0b00000010;
pub const PORTD0: u8 = 0b00000001;

// Bitfields of register DDRD
pub const DDD7: u8 = 0b10000000;
pub const DDD6: u8 = 0b01000000;
pub const DDD5: u8 = 0b00100000;
pub const DDD4: u8 = 0b00010000;
pub const DDD3: u8 = 0b00001000;
pub const DDD2: u8 = 0b00000100;
pub const DDD1: u8 = 0b00000010;
pub const DDD0: u8 = 0b00000001;

// Bitfields of register PIND
pub const PIND7: u8 = 0b10000000;
pub const PIND6: u8 = 0b01000000;
pub const PIND5: u8 = 0b00100000;
pub const PIND4: u8 = 0b00010000;
pub const PIND3: u8 = 0b00001000;
pub const PIND2: u8 = 0b00000100;
pub const PIND1: u8 = 0b00000010;
pub const PIND0: u8 = 0b00000001;

// Bitfields of register PORTB
pub const PORTB7: u8 = 0b10000000;
pub const PORTB6: u8 = 0b01000000;
pub const PORTB5: u8 = 0b00100000;
pub const PORTB4: u8 = 0b00010000;
pub const PORTB3: u8 = 0b00001000;
pub const PORTB2: u8 = 0b00000100;
pub const PORTB1: u8 = 0b00000010;
pub const PORTB0: u8 = 0b00000001;

// Bitfields of register DDRB
pub const DDB7: u8 = 0b10000000;
pub const DDB6: u8 = 0b01000000;
pub const DDB5: u8 = 0b00100000;
pub const DDB4: u8 = 0b00010000;
pub const DDB3: u8 = 0b00001000;
pub const DDB2: u8 = 0b00000100;
pub const DDB1: u8 = 0b00000010;
pub const DDB0: u8 = 0b00000001;

// Bitfields of register PINB
pub const PINB7: u8 = 0b10000000;
pub const PINB6: u8 = 0b01000000;
pub const PINB5: u8 = 0b00100000;
pub const PINB4: u8 = 0b00010000;
pub const PINB3: u8 = 0b00001000;
pub const PINB2: u8 = 0b00000100;
pub const PINB1: u8 = 0b00000010;
pub const PINB0: u8 = 0b00000001;
