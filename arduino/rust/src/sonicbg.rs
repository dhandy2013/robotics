//! Arduino Rust Sonic Background module
//! Uses TimerCounter/0 for PWM signals and Timer/Counter1 for sample timing.
//! D5: OC0B PWM output at 62.5 KHz: Channel 0 sound
//! D6: OC0A PWM output at 62.5 KHz: Channel 1 sound
//!
//! The timer frequency is 15.625KHz (exactly 1/4 of the PWM frequency),
//! so each timer tick is 64 microseconds.
//!
//! See: https://blog.rahix.de/005-avr-hal-millis/
// arduino_hal::pac == e.g. avr_device::atmega328p
//!
//! WARNING: This code does not work! Either due to compiler bugs or stack
//! overflow, the SonicBackgroundData::update() method returns essentially
//! random numbers for channel 0 when called inside the interrupt routine, but
//! not when called from main().
//!
//! My solution for now is to give up trying to use interrupt routines for
//! anything except for the simplest timer-tick counting (that part does work.)
use arduino_hal::pac::{TC0, TC1};
use avr_device::interrupt;
use core::cell::{RefCell, RefMut};
use ufmt::derive::uDebug;
use ufmt_write::uWrite;
use void::ResultVoidExt;
use wavegen::wavegen8::{WaveGenerator, Waveform};

pub const TICKS_PER_SECOND: u16 = 15625;

// OC1A timer frequency:
//  fOC1A = fCLK_IO / (2 * N * (1 + OCR1A))
// Where:
//  fCLK_IO = 16MHz
//  N = 1 (no prescaling)
//  OCR1A = 1023
// Therefore:
//  fOC1A = 7812.5KHz  # frequency of output square wave
// However OCF1A will be set on every transition, so double that rate, or
// 15.625KH (TICKS_PER_SECOND).
const TIMER_COUNTS: u16 = 1023;

const KEEP_SOUND_OFF: bool = false;

static SONIC_BG_DATA: interrupt::Mutex<RefCell<SonicBackgroundData>> =
    interrupt::Mutex::new(RefCell::new(SonicBackgroundData::new()));

#[derive(uDebug, Copy, Clone)]
pub enum Channel {
    Channel0,
    Channel1,
}

#[derive(Clone)]
pub struct SonicBackgroundData {
    /// Number of timer ticks till timer countdown is complete.
    /// Zero means timer is not running.
    /// 15.625 ticks per millisecond.
    countdown_ticks: u16,
    chan0_wavegen: WaveGenerator<TICKS_PER_SECOND>,
    chan1_wavegen: WaveGenerator<TICKS_PER_SECOND>,
    debug_index: usize,
    debug_array: [u8; 8],
}

impl SonicBackgroundData {
    const fn new() -> Self {
        Self {
            countdown_ticks: 0,
            chan0_wavegen: WaveGenerator::new(Waveform::Off, 440 * 16, 0),
            chan1_wavegen: WaveGenerator::new(Waveform::Off, 440 * 16, 0),
            debug_index: 0,
            debug_array: [0u8; 8],
        }
    }

    /// Update function called once per timer tick.
    /// Returns (pulsewidth0, pulsewidth1) for the channel 0 and channel 1
    /// speaker driver signals, respectively.
    pub fn update(&mut self) -> (u8, u8) {
        self.countdown_ticks = self.countdown_ticks.saturating_sub(1);
        let pulsewidth0 = self.chan0_wavegen.sample();
        let pulsewidth1 = self.chan1_wavegen.sample();
        (pulsewidth0, pulsewidth1)
    }

    /// Set the direction (input or output) of the PWM pins for the channels.
    /// WARNING: This sets all _other_ PORTD pins to inputs!
    /// TODO: Fix this; only modify the D5 and D6 PORTD pins.
    pub fn set_pwm_pin_direction(&self) {
        if KEEP_SOUND_OFF {
            return;
        }
        // Have to use steal() because take() was already called in main()
        let peripherals = unsafe { arduino_hal::Peripherals::steal() };
        peripherals.PORTD.ddrd.write(|w| {
            match self.chan0_wavegen.waveform() {
                Waveform::Off => {
                    // Set DDD5 = pin D5 (channel 0) direction = 0 (input)
                    w.pd5().clear_bit();
                }
                _ => {
                    // Set DDD5 = pin D5 (channel 0) direction = 1 (output)
                    w.pd5().set_bit();
                }
            };
            match self.chan1_wavegen.waveform() {
                Waveform::Off => {
                    // Set DDD6 = pin D6 (channel 1) direction = 0 (input)
                    w.pd6().clear_bit();
                }
                _ => {
                    // Set DDD6 = pin D6 (channel 1) direction = 1 (output)
                    w.pd6().set_bit();
                }
            };
            w
        });
    }

    /// Debug dump
    pub fn dump(&self, w: &mut dyn uWrite<Error = void::Void>) {
        ufmt::uwrite!(w, "SonicBackgroundData:",).void_unwrap();
        for i in 0..self.debug_array.len() {
            ufmt::uwrite!(w, " {}", self.debug_array[i]).void_unwrap();
        }
        ufmt::uwrite!(w, "\r\n").void_unwrap();
    }
}

/// Return the current tick countdown value.
#[inline]
pub fn get_countdown_ticks() -> u16 {
    interrupt::free(|cs| {
        let ref_cell = SONIC_BG_DATA.borrow(cs);
        let bgdata = ref_cell.borrow();
        bgdata.countdown_ticks
    })
}

/// Wait until the given number of timer interrupts have occurred.
/// Given the 15.625KHz interrupt rate and the size of the tick count,
/// the maximum wait time is 4.19424 seconds.
pub fn wait_ticks(ticks: u16) {
    interrupt::free(|cs| {
        let ref_cell = SONIC_BG_DATA.borrow(cs);
        let mut bgdata = ref_cell.borrow_mut();
        bgdata.countdown_ticks = ticks;
    });
    while get_countdown_ticks() > 0 {}
}

/// Timer interrupt! Called TICKS_PER_SECOND times per second.
#[allow(non_snake_case)]
#[avr_device::interrupt(atmega328p)]
fn TIMER1_COMPA() {
    interrupt::free(|cs| {
        let ref_cell = SONIC_BG_DATA.borrow(cs);
        let mut bgdata = ref_cell.borrow_mut();
        let (pulsewidth0, pulsewidth1) = bgdata.update();
        let i = bgdata.debug_index;
        bgdata.debug_array[i] = pulsewidth0;
        bgdata.debug_index = if i == bgdata.debug_array.len() - 1 {
            0
        } else {
            i + 1
        };
        // Have to use steal() because take() was already called in main()
        // let peripherals = unsafe { arduino_hal::Peripherals::steal() };
        // let tc0 = peripherals.TC0;
        // Set PWM pulse widths to generate current tone on each channel
        // Signal OC0B == pin D5 == channel 0 == left channel
        // tc0.ocr0b.write(|w| unsafe { w.bits(pulsewidth0) });
        crate::sonic::write_channel0(pulsewidth0);
        // Signal OC0A == pin D6 == channel 1 == right channel
        // tc0.ocr0a.write(|w| unsafe { w.bits(pulsewidth1) });
        crate::sonic::write_channel1(pulsewidth1);
    });
}

/// Setup timers and interrupts.
/// Call this function only once near the beginning of the program,
/// and not inside of an avr_device::interrupt::free critical section.
pub fn setup_timers_and_interrupts(tc0: TC0, tc1: TC1) {
    // Clock configuration assumes TCCRnx is all zeros at startup,
    // which I have confirmed by experiment.

    // Configure 8-bit Timer/Counter0 for fast PWM
    // Outputs are on OC0A (pin D6) and OC0B (pin D5).
    //
    // COM == Compare Output Mode
    // For PWM modes the COM bits control whether the PWM output should
    // be inverted. For non-PWM modes they control what happens to the
    // output at compare match: set, clear, or toggle.
    // Setting COM0A1 and leaving COM0A0 clear while in Fast PWM Mode
    // means clear OC0A on compare match, set OC0A at BOTTOM. In other
    // words, non-inverting mode. Setting COM0B1 and leaving COM0B0 clear
    // while in Fast PWM Mode means the same thing, but for OC0B.
    //
    // WGM == Waveform Generation Mode
    // Setting WGM00 and WGM01 in the TCCR0A register and leaving
    // WGM02 clear in the TCCR0B register sets Timer/Counter0 in Fast PWM
    // Mode, which affects the behavior of both OC0A and OC0B. By the way,
    // the TCCR0A and TCCR0B names are confusing because in this case there
    // are bits in both registers that control both the OC0A and OC0B output
    // behavior.
    tc0.tccr0a.write(|w| {
        w.com0a().match_clear();
        w.com0b().match_clear();
        w.wgm0().pwm_fast();
        w
    });

    // There is a 10bit prescaler common to both Timer/Counter0 and
    // Timer/Counter1. The prescaler is not being used, so the input
    // frequency is 16.000MHz.
    tc0.tccr0b.write(|w| w.cs0().direct());

    // Start both PWM outputs with 50% duty cycle
    tc0.ocr0b.write(|w| unsafe { w.bits(128u8) });
    tc0.ocr0a.write(|w| unsafe { w.bits(128u8) });

    // Configure 16-bit Timer/Counter1 to count 15.625KHz ticks.
    // Output would be on OC1A but we aren't configuring it to drive output;
    // instead we will trigger interrupts on every compare match in CTC mode.

    // Setting COM1A0 and leaving COM1A1 clear toggles OC1A on compare match
    // and causes OC1B to be disconnected (normal port operation).
    tc1.tccr1a.write(|w| w.com1a().match_toggle());

    // CS == Clock Select. According to the ATmega328P datasheet,
    // setting CS10 and leaving CS11 and CS12 cleared selects the
    // system clock with frequency fCLK_I/O which is 16.000MHz for
    // my Arduino.
    // WGM == Waveform Generation Mode. Setting WGM12 and leaving the
    // other WGM bits clear sets the timer/counter to CTC mode.
    tc1.tccr1b.write(|w| {
        // No prescaling
        w.cs1().direct();
        // Set WGM12. Honest.
        // The PAC doesn't give us clear guidance because the WGM bits
        // for Timer/Counter1 are split across two registers:
        // TCCR1A and TCCR1B. The WGM bits in TCCR1B are WGM13:WGM12.
        w.wgm1().bits(0b01);
        w
    });

    // set timer period
    tc1.ocr1a.write(|w| unsafe { w.bits(TIMER_COUNTS) });
    // enable timer interrupt
    tc1.timsk1.write(|w| w.ocie1a().set_bit());
}

/// If you own an instance of this you can change sonicbg parameters.
/// You get an instance by calling change_params() with a closure.
pub struct SonicBGToken<'a> {
    // private fields to prevent instantiation outside of this module
    bgdata: RefMut<'a, SonicBackgroundData>,
}

/// Change the sonic background parameters.
/// callback: Closure that takes as a parameter a token that can be used call
/// set_countdown_ticks(), set_channel_sound(), etc. The token must only be used
/// inside the callback closure.
pub fn change_params<F: FnOnce(SonicBGToken)>(callback: F) {
    interrupt::free(|cs| {
        let ref_cell = SONIC_BG_DATA.borrow(cs);
        let bgdata = ref_cell.borrow_mut();
        let token = SonicBGToken { bgdata };
        callback(token);
    })
}

impl SonicBGToken<'_> {
    pub fn set_countdown_ticks(&mut self, ticks: u16) {
        self.bgdata.countdown_ticks = ticks;
    }

    /// Set the sound parameters for a channel.
    /// channel: Which channel the parameters will be applied to.
    /// waveform: the type of waveform to generate, or Off for silence.
    /// phase_vel: frequency, in phase units per timeslice
    /// volume: 0 for silence, 128 for maximum.
    pub fn set_channel_sound(
        &mut self,
        channel: Channel,
        waveform: Waveform,
        phase_vel: u16,
        volume: u8,
    ) {
        let wavegen = match channel {
            Channel::Channel0 => &mut self.bgdata.chan0_wavegen,
            Channel::Channel1 => &mut self.bgdata.chan1_wavegen,
        };
        wavegen.set_waveform(waveform);
        wavegen.set_vel(phase_vel);
        wavegen.set_volume(volume);
        self.bgdata.set_pwm_pin_direction();
    }

    /// Return a copy of the current WaveGenerator for the given channel.
    pub fn get_channel_gen(
        &self,
        channel: Channel,
    ) -> WaveGenerator<TICKS_PER_SECOND> {
        match channel {
            Channel::Channel0 => self.bgdata.chan0_wavegen.clone(),
            Channel::Channel1 => self.bgdata.chan1_wavegen.clone(),
        }
    }

    /// Run update on the waveform generators just like timer would.
    /// Returns (pulsewidth0, pulsewidth1) for the channel 0 and channel 1
    /// speaker driver signals, respectively.
    pub fn update(&mut self) -> (u8, u8) {
        self.bgdata.update()
    }

    /// Debug dump of SonicBackgroundData
    pub fn dump(&self, w: &mut dyn uWrite<Error = void::Void>) {
        self.bgdata.dump(w);
    }
}
