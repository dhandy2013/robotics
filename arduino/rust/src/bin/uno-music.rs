//! Music player for Arduino UNO R3
//! Uses TimerCounter/0 for PWM signals and Timer/Counter1 for sample timing.
//! D6: OC0A PWM output at 62.5 KHz: Channel 1 sound
//! D5: OC0B PWM output at 62.5 KHz: Channel 2 sound
#![no_std]
#![no_main]
use arduino_hal::prelude::*;
use arduino_rust::sonic::{
    get_t0_config, get_t1_config, setup_timer_counter_0, setup_timer_counter_1,
    sound_off, sound_on, wait_for_timer_tick, write_channel0, write_channel1,
    TICKS_PER_SECOND,
};
use panic_halt as _;
use wavegen::wavegen8::{WaveGenerator, Waveform};

const SAMPLE_RATE: u16 = TICKS_PER_SECOND;
type WaveGen = WaveGenerator<SAMPLE_RATE>;

// Defines the SONG array
include!("../../gen/song.rs");

#[arduino_hal::entry]
fn main() -> ! {
    let dp = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(dp);
    let mut serial = arduino_hal::default_serial!(dp, pins, 57600);
    ufmt::uwriteln!(&mut serial, "Hello from uno-music.rs").void_unwrap();

    let mut wavegen = WaveGen::new(Waveform::SineWave, 440 * 16, 64);

    setup_timer_counter_0();
    setup_timer_counter_1();
    let (tccr0a, tccr0b) = get_t0_config();
    ufmt::uwriteln!(&mut serial, "TCCR0A={} TCCR0B={}\r", tccr0a, tccr0b)
        .void_unwrap();
    let (tccr1a, tccr1b) = get_t1_config();
    ufmt::uwriteln!(&mut serial, "TCCR1A={} TCCR1B={}\r", tccr1a, tccr1b)
        .void_unwrap();

    sound_on();
    for (duration_ticks, phase_vel) in SONG.iter() {
        if *phase_vel == 0xffff {
            wavegen.set_waveform(Waveform::Off);
        } else {
            wavegen.set_waveform(Waveform::SineWave);
            wavegen.set_vel(*phase_vel);
        }
        let mut tick_count = *duration_ticks;
        while tick_count > 0 {
            let pulse_width = wavegen.sample();
            // ufmt::uwrite!(&mut serial, "{} ", pulse_width).void_unwrap();
            write_channel0(pulse_width);
            write_channel1(pulse_width);
            wait_for_timer_tick();
            tick_count -= 1;
        }
    }
    sound_off();

    ufmt::uwriteln!(&mut serial, "\r\nDone.\r").void_unwrap();

    loop {}
}
