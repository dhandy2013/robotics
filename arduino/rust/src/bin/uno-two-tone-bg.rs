//! Ardunio UNO code to generate tones on speaker in background.
//! Uses TimerCounter/0 for PWM signals and Timer/Counter1 for sample timing.
//! Pin D4: Input with internal pullup, connected to pushbutton
//! Pin D5: OC0B PWM output at 62.5 KHz: Channel 0 sound
//! Pin D6: OC0A PWM output at 62.5 KHz: Channel 1 sound
//!
//! WARNING: This code does not work! Either due to compiler bugs or stack
//! overflow, the SonicBackgroundData::update() method returns essentially
//! random numbers for channel 0 when called inside the interrupt routine, but
//! not when called from main().
//!
//! My solution for now is to give up trying to use interrupt routines for
//! anything except for the simplest timer-tick counting (that part does work.)
#![no_std]
#![no_main]
use arduino_hal::prelude::*;
use arduino_rust::buttons::{portd_button_is_pressed, setup_portd_buttons};
use arduino_rust::sonicbg::{
    change_params, get_countdown_ticks, setup_timers_and_interrupts,
    Channel::Channel0, Channel::Channel1, TICKS_PER_SECOND,
};
use avr_device::interrupt;
use panic_halt as _;
use wavegen::wavegen8::{WaveGenerator, Waveform};

const SAMPLE_RATE: u16 = TICKS_PER_SECOND;
type WaveGen = WaveGenerator<SAMPLE_RATE>;
const MIN_PHASE_VEL: u16 = WaveGen::MIN_PHASE_VEL;

#[arduino_hal::entry]
fn main() -> ! {
    let peripherals = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(peripherals);
    let mut serial = arduino_hal::default_serial!(peripherals, pins, 57600);
    ufmt::uwriteln!(&mut serial, "Hello from uno-two-tone-bg.rs\r")
        .void_unwrap();

    setup_portd_buttons(1 << 4); // button on pin D4

    // Set up initial sound parameters on both channels
    // Start out with a sine wave, low frequency, silenced (volume 0)
    change_params(|mut token| {
        token.set_channel_sound(Channel0, Waveform::SineWave, MIN_PHASE_VEL, 0);
        token.set_channel_sound(Channel1, Waveform::SineWave, MIN_PHASE_VEL, 0);
    });

    // Start the PWM oscillators and timer
    setup_timers_and_interrupts(peripherals.TC0, peripherals.TC1);
    let (tccr0a, tccr0b) = arduino_rust::sonic::get_t0_config();
    ufmt::uwriteln!(&mut serial, "TCCR0A={} TCCR0B={}\r", tccr0a, tccr0b)
        .void_unwrap();
    let (tccr1a, tccr1b) = arduino_rust::sonic::get_t1_config();
    ufmt::uwriteln!(&mut serial, "TCCR1A={} TCCR1B={}\r", tccr1a, tccr1b)
        .void_unwrap();

    // Set tone length
    let duration_ticks = TICKS_PER_SECOND * 2; // 2 seconds

    // Calculate phase velocity for desired frequency
    let f220_phase_vel = WaveGen::freq_x16_to_phase_vel(220 * 16);
    let f440_phase_vel = WaveGen::freq_x16_to_phase_vel(440 * 16);

    // Max volume is 128
    let left_volume = 0;
    let right_volume = 32;

    // Start the tone on both channels
    change_params(|mut token| {
        token.set_countdown_ticks(duration_ticks);
        token.set_channel_sound(
            Channel0,
            Waveform::SineWave,
            f440_phase_vel,
            left_volume,
        );
        token.set_channel_sound(
            Channel1,
            Waveform::SineWave,
            f220_phase_vel,
            right_volume,
        );
    });

    ufmt::uwriteln!(
        &mut serial,
        "Checking waveform generators before enabling interrupts\n"
    )
    .void_unwrap();
    change_params(|mut token| {
        for i in 0..8 {
            let (pulsewidth0, pulsewidth1) = token.update();
            ufmt::uwriteln!(
                &mut serial,
                "{}: {} {}\r",
                i,
                pulsewidth0,
                pulsewidth1
            )
            .void_unwrap();
        }
    });

    change_params(|token| {
        token.dump(&mut serial);
        ufmt::uwrite!(&mut serial, "\r\n").void_unwrap();
    });

    // Enable interrupts globally
    unsafe { interrupt::enable() };

    loop {
        if portd_button_is_pressed(4) {
            ufmt::uwriteln!(&mut serial, "Button on D4 pressed\r")
                .void_unwrap();
            break;
        }

        if get_countdown_ticks() == 1 {
            ufmt::uwriteln!(&mut serial, "2-second timer expired\r")
                .void_unwrap();
        }
    }

    // Turn off sound output, then disable global interrupts, in that order.
    arduino_rust::sonic::sound_off();
    interrupt::disable();

    ufmt::uwriteln!(&mut serial, "Done.\r").void_unwrap();

    // Try to figure out what is going on with the generators

    change_params(|token| {
        token.dump(&mut serial);
        ufmt::uwrite!(&mut serial, "\r\n").void_unwrap();
    });

    let mut gen0 = WaveGenerator::default();
    let mut gen1 = WaveGenerator::default();
    change_params(|token| {
        gen0 = token.get_channel_gen(Channel0);
        gen1 = token.get_channel_gen(Channel1);
    });
    ufmt::uwriteln!(&mut serial, "gen0: {:?}", gen0).void_unwrap();
    ufmt::uwriteln!(&mut serial, "gen1: {:?}", gen1).void_unwrap();
    for i in 0..8 {
        let pulsewidth0 = gen0.sample();
        let pulsewidth1 = gen1.sample();
        ufmt::uwriteln!(
            &mut serial,
            "{}: {} {}\r",
            i,
            pulsewidth0,
            pulsewidth1
        )
        .void_unwrap();
    }

    loop {}
}
