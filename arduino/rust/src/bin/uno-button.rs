//! Button program using very low-level I/O, should work on Arduino UNO
//!
//! Arduino pin D2 should be connected to a button or similar input.
//! When D2 goes high, this program turns on the onboard LED (pin D13),
//! and when D2 goes low, it turns off the LED. Simple.
#![no_std]
#![no_main]

use arduino_hal::prelude::*;
use avrd::current::{DDRB, PIND, PORTB};
use core::ptr::{read_volatile, write_volatile};
use panic_halt as _;

fn setup() {
    // PORTB5 = Arduino pin D13
    // Famous digital pin 13 is the onboard LED
    unsafe {
        write_volatile(DDRB, read_volatile(DDRB) | 0b00100000);
    }

    // Leaving the PORTD / DDRD / PIND configuration at power-on default.
}

#[inline]
fn turn_led_on() {
    unsafe {
        write_volatile(PORTB, 0b00100000);
    }
}

#[inline]
fn turn_led_off() {
    unsafe {
        write_volatile(PORTB, 0b00000000);
    }
}

/// Read button == state of Arduino pin D2 == ATmega328P signal PORTD2
#[inline]
fn read_button() -> bool {
    (unsafe { read_volatile(PIND) } & 0b00000100) != 0
}

#[arduino_hal::entry]
fn main() -> ! {
    let peripherals = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(peripherals);
    let mut serial = arduino_hal::default_serial!(peripherals, pins, 57600);

    setup();
    ufmt::uwriteln!(&mut serial, "Hello from uno-button.rs").void_unwrap();

    loop {
        if read_button() {
            turn_led_on();
        } else {
            turn_led_off();
        }
    }
}
