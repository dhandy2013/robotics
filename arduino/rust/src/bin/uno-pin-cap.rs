//! Measure the digital I/O pin capacitance on my Arduino UNO R3.
//!
//! D8 (PORTB0):
//!     Input with internal pullup resistor, button connects it to ground
//! D10 (PORTB2):
//!     Output, connect to oscilloscope channel 1, use as trigger
//! D12 (PORTB4):
//!     Connect to 47Kohm resistor pulling up to +5V and to
//!     oscilloscope channel 2.
//! 
//! This program will first configure D10 and D12 as outputs driving low.
//! When it detects D8 low (when the button is pressed) it will rapidly drive
//! D10 high and turn D12 into an input to let it get pulled high by the external
//! resistor. It will then wait 100 milliseconds and start the cycle again.
#![no_std]
#![no_main]

use avrd::current::{DDRB, PINB, PORTB};
use core::ptr::{read_volatile, write_volatile};
use panic_halt as _;

fn setup() {
    // Configure D10 (PORTB0), D12 (PORTB4), and D13 (PORTB5) as outputs,
    // everything else in PORTB as inputs.
    // Famous digital pin 13 is the onboard LED.
    unsafe {
        write_volatile(DDRB, 0b00110100);
    }

    // Turn on pullup resistor for D8 (PORTB0)
    // and coincidentally drive D10, D12, and D13 low.
    unsafe {
        write_volatile(PORTB, 0b00000001);
    }
}

#[inline]
fn turn_led_on() {
    unsafe {
        write_volatile(PORTB, read_volatile(PORTB) | 0b00100000);
    }
}

#[inline]
fn turn_led_off() {
    unsafe {
        write_volatile(PORTB, read_volatile(PORTB) & 0b11011111);
    }
}

/// Read button == state of Arduino pin D8 == ATmega328P signal PORTB0.
/// Normally returns true, returns false iff button is pressed.
#[inline]
fn read_button() -> bool {
    (unsafe { read_volatile(PINB) } & 0b00000001) != 0
}

#[arduino_hal::entry]
fn main() -> ! {
    setup();

    // Blink LED to let people know the program has started
    turn_led_on();
    arduino_hal::delay_ms(250);
    turn_led_off();
    arduino_hal::delay_ms(250);
    turn_led_on();
    arduino_hal::delay_ms(500);
    turn_led_off();

    loop {
        while read_button() {}

        unsafe {
            // Set D10 (PORTB2) high
            write_volatile(PORTB, read_volatile(PORTB) | 0b00000100);
            // Turn D12 (PORTB4) into an input
            write_volatile(DDRB, 0b00100100);
        }

        // Give D12 time be pulled up by the resistor.
        // Wait until the button has been released for at least 100ms.
        let mut debounce_count: u8 = 0;
        loop {
            if read_button() {
                debounce_count += 1;
                if debounce_count > 10 {
                    break;
                }
                arduino_hal::delay_ms(10);
            } else {
                debounce_count = 0;
            }
        }

        // Reset I/O pin state and start over
        setup();
    }
}
