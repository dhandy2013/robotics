//! Control an SG90 MicroServo 9g
//! Datasheet: https://components101.com/sites/default/files/component_datasheet/SG90%20Servo%20Motor%20Datasheet.pdf
//! Programming the Ardunio UNO R3 by directly writing to registers.
//!
//! Configured pins:
//! - A0 (PORTC0/ADC0):
//!     Connected to potentiometer on the left.
//!     Used to control time to wait for servo arm to reach its destination.
//! - A1 (PORTC1/ADC1):
//!     Connected to potentiometer on the right. Used to control how far the
//!     servo moves.  More voltage means longer PWM pulse.
//! - D3 (PORTD3/OC2B):
//!     PWM output to servo input.
//! - D4 (PORTD4)
//!     Connected to pushbutton: Move servo from "home" position
//!     to position determined by right servo, and back.
//!     Pin is configured with internal pullup resister.
//!
//! Test results for my Micro Servo device:
//! The spec says 50 Hz PWM signal but 61.035 Hz seems to work Ok,
//! except the range of motion is about 90 degrees not 180 degrees.
//!
//! The servo starts from "home" position of ocr=15 (about 0.96ms pulse).  I
//! turned the potentiometer knobs to get various delay and target OCR values. I
//! got the servo behavior closest to what I want for my candy ejector machine
//! with target ocr=29 (about 1.9 ms pulse) and delay=154 ms. With these
//! settings the servo arm moves rapidly about 45 degrees then quickly reverses
//! direction and goes back to home position.
#![no_std]
#![no_main]

use arduino_hal::prelude::*;
use arduino_rust::{
    adc::{blocking_read_adc, setup_adc},
    buttons::{portd_button_is_pressed, setup_portd_buttons},
    uno_bitfields::*,
};
use avrd::current::{DDRD, OCR2B, TCCR2A, TCCR2B};
use core::ptr::{read_volatile, write_volatile};
use panic_halt as _;

fn setup_timer_counter_2() {
    unsafe {
        // Using Timer/Counter2 for PWM:
        // In Fast PWM Mode, the PWM frequency for the output is:
        //      fOCnxPWM = fclk_IO / (N * 256)
        // where:
        //      fclk_IO is the main clock frequency (16.000 MHz)
        //      N is the prescale factor, one of 1, 8, 32, 64, 128, 256, or 1024
        //
        // For N=1024, the PWM frequency is 61.035 Hz. The MicroServer wants 50Hz.
        // We will see if this is close enough.
        //
        // We need to generate pulse widths between 1ms and 2ms.
        // Pulse width calculation:
        //      pw = (OCR + 1) / (f * 256)
        // where:
        //      OCR is the Output Compare Register value (0 through 255)
        //      f is the PWM frequency as calculated above
        //
        // For pw = 0.001 (1 ms), OCR = 14.625 ~= 15, actual pw = 1.024 ms
        // For pw = 0.0015 (1.5 ms), OCR = 22.4375 ~= 22, actual pw = 1.472 ms
        // For pw = 0.002 (2 ms), OCR = 30.25 ~= 30, actual pw = 1.984 ms
        //
        // Experiments will show whether this is good enough.

        // TCCR2A - Timer/Counter2 Control Register A
        //
        //                       ++------   COM2A1:0 Compare Match Output A Mode
        //                       ||         00 == OC2A disconnected
        //                       ||         When in fast PWM mode:
        //                       ||         10 == Clear OC2A on compare match,
        //                       ||               set OC2A at BOTTOM
        //                       ||               (non-inverting mode)
        //                       ||         When in phase-correct PWM mode:
        //                       ||         10 == Clear OC2A when up-counting,
        //                       ||               set OC2A when down-counting.
        //                       ||
        //                       ||++----   COM2B1:0 Compare Match Output B Mode
        //                       ||||       00 == OC2B disconnected
        //                       ||||       When in fast PWM mode:
        //                       ||||       10 == Clear OC2B on compare match,
        //                       ||||             set OC2B at BOTTOM
        //                       ||||             (non-inverting mode)
        //                       ||||       When in phase-correct PWM mode:
        //                       ||||       10 == Clear OC2B when up-counting,
        //                       ||||             set OC2B when down-counting.
        //                       ||||
        //                       ||||++--   Reserved 1:0
        //                       ||||||
        //                       ||||||++   WGM21:0 Waveform Generation Mode
        //                       ||||||||   Combined with WGM22 in TCCR2B
        //                       ||||||||   WGM2:0 == 001 PWM, phase correct
        //                       ||||||||   WGM2:0 == 011 Fast PWM
        write_volatile(TCCR2A, 0b00100011);

        // TCCR2B - Timer/Counter2 Control Register B
        //
        //                       ++------   FOC2A, FOC2B Force Output Compare A/B
        //                       ||
        //                       ||++----   Reserved 1:0
        //                       ||||
        //                       ||||+---   WGM22 Waveform Generation Mode bit 2
        //                       |||||
        //                       |||||+++   CS22:0 Clock Select
        //                       ||||||||   000 == Timer/Counter stopped
        //                       ||||||||   001 == No prescaling
        //                       ||||||||   010 == /8 prescaling
        //                       ||||||||   011 == /32 prescaling
        //                       ||||||||   100 == /64 prescaling
        //                       ||||||||   101 == /128 prescaling
        //                       ||||||||   110 == /256 prescaling
        //                       ||||||||   111 == /1024 prescaling
        write_volatile(TCCR2B, 0b00000111);
    }

    servo_off();
}

fn servo_on() {
    // Configure pin D3 (signal PORTD3/OC2B) as an output.
    // This enables the servo control signal output.

    unsafe {
        write_volatile(DDRD, read_volatile(DDRD) | DDD3);
    }
}

fn servo_off() {
    // Configure pin D3 (signal PORTD3/OC2B) as an input.
    // This disables the servo control signal output.

    unsafe {
        write_volatile(DDRD, read_volatile(DDRD) & !DDD3);
    }
}

/// Map ADC input value in range 0 through 1023 (0x3ff) inclusive
/// to a delay in milliseconds.
fn map_adc_to_delay(adc_val: u16) -> u16 {
    let adjusted_adc = adc_val & 0x3ff;
    ((adjusted_adc as u32 * MAX_SERVO_DELAY_MS as u32) / 1024) as u16
}

/// Map ADC input value in range 0 through 1023 (0x3ff) inclusive
/// to a PWM Output Compare Register value. Lower OCR values are
/// lower duty cycles.
fn map_adc_to_ocr(adc_val: u16) -> u8 {
    let adjusted_adc = adc_val & 0x3ff;
    // Convert adjusted ADC value to OCR value
    const OCR_LOWER_BOUND: u8 = 15;
    const OCR_UPPER_BOUND: u8 = 30;
    OCR_LOWER_BOUND
        + ((adjusted_adc as u32 * (OCR_UPPER_BOUND - OCR_LOWER_BOUND) as u32)
            / 1024) as u8
}

/// The PWM duty cycle to take the servo arm to its home position.
const OCR_HOME_POSITION: u8 = 15;

// The spec for the Micro Servo says "Operating speed: 0.1 s/60 degree".
// The spec says the servo has 180 degrees of motion, so to get all the way
// over from left to right should take about 0.3 seconds = 300ms.
const MAX_SERVO_DELAY_MS: u16 = 300;

#[arduino_hal::entry]
fn main() -> ! {
    let dp = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(dp);
    let mut serial = arduino_hal::default_serial!(dp, pins, 57600);
    ufmt::uwriteln!(&mut serial, "Hello from uno-microservo.rs\r")
        .void_unwrap();
    ufmt::uwriteln!(&mut serial, "Left knob: servo arm travel delay\r")
        .void_unwrap();
    ufmt::uwriteln!(&mut serial, "Right knob: servo PWM pulse width\r")
        .void_unwrap();
    ufmt::uwriteln!(&mut serial, "Button: run servo\r").void_unwrap();

    setup_timer_counter_2();
    setup_adc();
    setup_portd_buttons(1 << 4); // Button attached to pin D4

    loop {
        if portd_button_is_pressed(4) {
            // Read the left potentiometer
            let adc0 = blocking_read_adc(0);

            // Read the right potentiometer
            let adc1 = blocking_read_adc(1);

            let delay = map_adc_to_delay(adc0);
            let ocr = map_adc_to_ocr(adc1);
            ufmt::uwriteln!(
                &mut serial,
                "ADC0={} ADC1={} delay={}, ocr={}\r",
                adc0,
                adc1,
                delay,
                ocr
            )
            .void_unwrap();

            // Set the PWM duty cycle to the value indicated by the right
            // potentiometer. This sets the range of motion of the servo arm.
            unsafe {
                write_volatile(OCR2B, ocr);
            }

            // Turn the servo on to start motion
            servo_on();

            // Delay to wait for servo arm to reach desired postion
            arduino_hal::delay_ms(delay);

            // Set the PWM duty cycle back to home position
            unsafe {
                write_volatile(OCR2B, OCR_HOME_POSITION);
            }

            // Delay to wait for servo arm to get back to home postion
            arduino_hal::delay_ms(MAX_SERVO_DELAY_MS);

            // Turn servo motor off
            servo_off();

            // Wait for button to be released for 100ms straight
            let mut button_released_ms: u8 = 0;
            while button_released_ms < 100 {
                arduino_hal::delay_ms(1);
                button_released_ms = if portd_button_is_pressed(4) {
                    0
                } else {
                    button_released_ms + 1
                }
            }
        }
    }
}
