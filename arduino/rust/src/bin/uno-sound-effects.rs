/*!
 * Generate sound effects on the Ardunio UNO R3 using PWM on two channels.
 *
 * Configured Pins:
 * D4: Input with internal pullup resistor, connected to pushbutton
 * D6: OC0A PWM output at 62.5 KHz, duty cycle varying at audio freqency
 * D5: OC0B PWM output at 62.5 KHz, duty cycle varying at audio freqency
 *
 * The program starts with sound off. Press the button to cycle between:
 * - square wave
 * - triangle wave
 * - sine wave
 * - off again
 *
 * Examples and inspiration from:
 * [m328p_fastpwm.c](https://gist.github.com/Wollw/2425784)
 */
#![no_std]
#![no_main]

use arduino_hal::prelude::*;
use arduino_rust::{
    buttons::{portd_button_is_pressed, setup_portd_buttons},
    sonic::{
        setup_timer_counter_0, setup_timer_counter_1, sound_off, sound_on,
        wait_for_timer_tick, write_channel0, write_channel1, TICKS_PER_SECOND,
    },
};
use panic_halt as _;
use ufmt::derive::uDebug;
use wavegen::wavegen8::{WaveGenerator, Waveform};

const SAMPLE_RATE: u16 = TICKS_PER_SECOND;

struct EffectController {
    wavegen: WaveGenerator<SAMPLE_RATE>,
    ticks: u16,
    effect: SoundEffect,
}

impl EffectController {
    const BASE_FREQ_X16: u16 = 440 * 16; // 440 Hz
    const BASE_VOLUME: u8 = 32; // range is 0..=128

    fn new() -> Self {
        Self {
            wavegen: WaveGenerator::new(
                Waveform::Off,
                Self::BASE_FREQ_X16,
                Self::BASE_VOLUME,
            ),
            ticks: 0,
            effect: SoundEffect::Off,
        }
    }

    fn sample(&mut self) -> u8 {
        match self.effect {
            SoundEffect::SwoopUp => {
                if self.ticks != 0 {
                    self.ticks -= 1;
                    self.wavegen.set_vel(self.wavegen.vel() + 1);
                } else {
                    self.wavegen.set_waveform(Waveform::Off);
                }
            }
            _ => {}
        }
        self.wavegen.sample()
    }

    fn set_effect(&mut self, effect: SoundEffect) {
        self.effect = effect;
        match effect {
            SoundEffect::Off => {
                self.wavegen.set_waveform(Waveform::Off);
                self.set_steady_tone();
            }
            SoundEffect::SquareWave => {
                self.wavegen.set_waveform(Waveform::SquareWave);
                self.set_steady_tone();
            }
            SoundEffect::TriangleWave => {
                self.wavegen.set_waveform(Waveform::TriangleWave);
                self.set_steady_tone();
            }
            SoundEffect::SineWave => {
                self.wavegen.set_waveform(Waveform::SineWave);
                self.set_steady_tone();
            }
            SoundEffect::SwoopUp => {
                // Sweep frequency from ~0 to ~3725Hz in 1 second
                self.wavegen.set_waveform(Waveform::SineWave);
                self.wavegen
                    .set_vel(WaveGenerator::<SAMPLE_RATE>::MIN_PHASE_VEL);
                self.wavegen.set_volume(Self::BASE_VOLUME);
                self.ticks = SAMPLE_RATE; // 1 second
            }
        }
    }

    fn set_next_effect(&mut self) {
        match self.effect {
            SoundEffect::Off => {
                self.set_effect(SoundEffect::SquareWave);
            }
            SoundEffect::SquareWave => {
                self.set_effect(SoundEffect::TriangleWave);
            }
            SoundEffect::TriangleWave => {
                self.set_effect(SoundEffect::SineWave);
            }
            SoundEffect::SineWave => {
                self.set_effect(SoundEffect::SwoopUp);
            }
            SoundEffect::SwoopUp => {
                if self.ticks == 0 {
                    self.set_effect(SoundEffect::SquareWave);
                } else {
                    self.set_effect(SoundEffect::Off);
                }
            }
        }
    }

    fn set_steady_tone(&mut self) {
        self.wavegen.set_pitch(Self::BASE_FREQ_X16);
        self.wavegen.set_volume(Self::BASE_VOLUME);
        self.ticks = 0xffff;
    }

    fn is_off(&self) -> bool {
        self.wavegen.is_off()
    }
}

#[derive(uDebug, Copy, Clone)]
enum SoundEffect {
    Off,
    SquareWave,
    TriangleWave,
    SineWave,
    SwoopUp,
}

#[arduino_hal::entry]
fn main() -> ! {
    let dp = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(dp);
    let mut serial = arduino_hal::default_serial!(dp, pins, 57600);
    ufmt::uwriteln!(&mut serial, "Hello from uno-pwm-raw.rs\r").void_unwrap();
    ufmt::uwriteln!(
        &mut serial,
        "Press button to cycle through sound effects\r"
    )
    .void_unwrap();

    setup_timer_counter_0();
    setup_timer_counter_1();
    setup_portd_buttons(1 << 4);

    let mut button_release_countdown: u8 = 255;
    let mut ec = EffectController::new();
    let mut pulse_width = ec.sample();

    loop {
        // Write to sound output channels at very beginning of timeslice
        // in order to reduce jitter.
        write_channel0(pulse_width);
        write_channel1(pulse_width);

        // Turn sound on or off as applicable
        if ec.is_off() {
            sound_off();
        } else {
            sound_on();
        }

        let pressed = portd_button_is_pressed(4);
        if button_release_countdown != 0 {
            // Debounce the button
            button_release_countdown = if pressed {
                255
            } else {
                button_release_countdown - 1
            };
        } else if pressed {
            button_release_countdown = 255;

            // Switch to next sound effect
            ec.set_next_effect();
        }

        // Calculate next pulse width
        pulse_width = ec.sample();
        wait_for_timer_tick();
    }
}
