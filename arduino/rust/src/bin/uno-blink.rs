//! Blink program using very low-level I/O, should work on Arduino UNO
#![no_std]
#![no_main]

use arduino_hal::prelude::*;
use avrd::current::{DDRB, PORTB};
use core::ptr::{read_volatile, write_volatile};
use panic_halt as _;

#[arduino_hal::entry]
fn main() -> ! {
    let peripherals = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(peripherals);
    let mut serial = arduino_hal::default_serial!(peripherals, pins, 57600);

    ufmt::uwriteln!(&mut serial, "Hello from uno-blink.rs").void_unwrap();

    // PORTB5 = Arduino pin D13
    // Famous digital pin 13 is the onboard LED
    unsafe { write_volatile(DDRB, read_volatile(DDRB) | 0b00100000); }

    loop {
        unsafe { write_volatile(PORTB, 0b00100000); }
        arduino_hal::delay_ms(500);
        unsafe { write_volatile(PORTB, 0b00000000); }
        arduino_hal::delay_ms(500);
    }
}
