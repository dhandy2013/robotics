//! Arduino Rust blink demo using interrupts set up by the sonicbg module.
#![no_std]
#![no_main]

use arduino_hal::prelude::*;
use arduino_rust::sonicbg;
use panic_halt as _;

const DELAY_TICKS_500MS: u16 = sonicbg::TICKS_PER_SECOND / 2;

#[arduino_hal::entry]
fn main() -> ! {
    let peripherals = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(peripherals);
    let mut serial = arduino_hal::default_serial!(peripherals, pins, 57600);
    ufmt::uwriteln!(&mut serial, "Hello from uno-blinkint.rs").void_unwrap();

    // Famous digital pin 13 is the onboard LED
    let mut led = pins.d13.into_output();

    let tc0 = peripherals.TC0;
    let tc1 = peripherals.TC1;
    sonicbg::setup_timers_and_interrupts(tc0, tc1);

    loop {
        led.toggle();
        sonicbg::wait_ticks(DELAY_TICKS_500MS);
    }
}
