//! Experiments to see what can be done with the Arduino UNO
#![no_std]
#![no_main]

use arduino_hal::prelude::*;
use panic_halt as _;

// Defines the NUMBERS array
include!("../../gen/numbers.rs");

#[arduino_hal::entry]
fn main() -> ! {
    let peripherals = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(peripherals);
    let mut serial = arduino_hal::default_serial!(peripherals, pins, 57600);
    ufmt::uwriteln!(&mut serial, "Hello from uno-experiments.rs").void_unwrap();

    for i in 0..NUMBERS_LEN {
        let (d1, d2) = NUMBERS.load_at(i);
        ufmt::uwrite!(&mut serial, "{}, {}\r\n", d1, d2).void_unwrap();
    }
    loop {}
}
