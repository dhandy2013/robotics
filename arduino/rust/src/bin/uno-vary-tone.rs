/*!
 * Read two potentiometers and use them to control audio pitch and volume.
 *
 * Programming the Ardunio UNO R3 by directly writing to registers.
 *
 * Configured Pins:
 * A0 (PORTC0/ADC0): Left potentiometer: Pitch control input
 * A1 (PORTC1/ADC1): Right potentiometer: Volume control input
 * D4: Input with internal pullup resistor, connected to pushbutton.
 *     Press button to switch between sound off/square/triangle/sine wave
 * D6: OC0A PWM output at 62.5 KHz, duty cycle varies
 * D5: OC0B PWM output at 62.5 KHz, duty cycle varies (same as D6 for now)
 *
 */
#![no_std]
#![no_main]

use arduino_hal::prelude::*;
use arduino_rust::{
    adc::{read_adc, setup_adc, start_adc},
    buttons::{portd_button_is_pressed, setup_portd_buttons},
    sonic::{
        setup_timer_counter_0, setup_timer_counter_1, sound_off, sound_on,
        wait_for_timer_tick, write_channel0, write_channel1, TICKS_PER_SECOND,
    },
};
use panic_halt as _;
use wavegen::wavegen8::{WaveGenerator, Waveform};

const SAMPLE_RATE: u16 = TICKS_PER_SECOND;
type WaveGen = WaveGenerator<SAMPLE_RATE>;

#[arduino_hal::entry]
fn main() -> ! {
    let dp = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(dp);
    let mut serial = arduino_hal::default_serial!(dp, pins, 57600);
    ufmt::uwriteln!(&mut serial, "Hello from uno-vary-tone.rs\r").void_unwrap();
    ufmt::uwriteln!(&mut serial, "Left knob: pitch control\r").void_unwrap();
    ufmt::uwriteln!(&mut serial, "Right knob: volume control\r").void_unwrap();
    ufmt::uwriteln!(
        &mut serial,
        "Button: cycle between square wave/triangle wave/sine wave/off\r"
    )
    .void_unwrap();

    setup_timer_counter_0();
    setup_timer_counter_1();
    setup_adc();
    setup_portd_buttons(1 << 4);

    let mut time_slice: u8 = 0;
    const DEBOUNCE_COUNT: u8 = 4;
    let mut button_release_countdown: u8 = DEBOUNCE_COUNT;
    let start_freq_x16 = 440 * 16; // start at 440 Hz
    let mut volume = 32; // volume range is 0..=128
    let mut wavegen = WaveGen::new(Waveform::Off, start_freq_x16, volume);
    let enable_sound = true;
    let enable_adc0 = true;
    let enable_adc1 = true;
    let print_adc_change = false;
    let mut glitch_count: u16 = 0;
    let mut clear_glitch_count = false;

    loop {
        let pulse_width = wavegen.sample();
        write_channel0(pulse_width);
        write_channel1(pulse_width);

        // Time slicing to avoid doing too much during one sample
        match time_slice {
            0x00 => {
                // read button
                let pressed = portd_button_is_pressed(4);
                if button_release_countdown != 0 {
                    // Debounce the button
                    button_release_countdown = if pressed {
                        DEBOUNCE_COUNT
                    } else {
                        button_release_countdown - 1
                    };
                } else if pressed {
                    button_release_countdown = DEBOUNCE_COUNT;
                    // Switch waveform type
                    wavegen.set_next_waveform();
                    if enable_sound && !wavegen.is_off() {
                        sound_on();
                    } else {
                        sound_off();
                    }
                    // Print current info
                    ufmt::uwrite!(
                        &mut serial,
                        "Button press:  \
                        vel: {} \
                        volume: {}  \
                        glitches: {}  \
                        waveform: {}\r\n",
                        wavegen.vel(),
                        volume,
                        glitch_count,
                        wavegen.name(),
                    )
                    .void_unwrap();
                    clear_glitch_count = true;
                }
            }
            0x01 if clear_glitch_count => {
                clear_glitch_count = false;
                glitch_count = 0;
            }
            0x10 if enable_adc0 => {
                // Start reading ADC0
                start_adc(0);
            }
            0x20 if enable_adc0 => {
                if let Some(_) = read_adc() {
                    // First ADC0 read: throw it away
                } else {
                    // Pseudo-panic: Conversion not complete
                    ufmt::uwriteln!(
                        &mut serial,
                        "ADC0 conversion not complete"
                    )
                    .void_unwrap();
                    break;
                }
            }
            0x30 if enable_adc0 => {
                // Start reading ADC0 again
                start_adc(0);
            }
            0x40 if enable_adc0 => {
                if let Some(adc0_val) = read_adc() {
                    // Convert ADC0 value in range 0..=1023
                    // to phase velocity in range 0..=16368
                    // which is frequency from 0 through 3902.4 Hz.
                    let new_vel = adc0_val << 4;
                    wavegen.set_vel(new_vel);
                } else {
                    // Pseudo-panic: Conversion not complete
                    ufmt::uwriteln!(
                        &mut serial,
                        "ADC0 conversion not complete"
                    )
                    .void_unwrap();
                    break;
                }
            }
            0x50 if enable_adc1 => {
                // Start reading ADC1
                start_adc(1);
            }
            0x60 if enable_adc1 => {
                if let Some(_) = read_adc() {
                    // First ADC1 read: throw it away
                } else {
                    // Pseudo-panic: Conversion not complete
                    ufmt::uwriteln!(
                        &mut serial,
                        "ADC1 conversion not complete"
                    )
                    .void_unwrap();
                    break;
                }
            }
            0x70 if enable_adc1 => {
                // Start reading ADC1 again
                start_adc(1);
            }
            0x80 if enable_adc1 => {
                if let Some(adc1_val) = read_adc() {
                    // Adjust volume
                    let new_volume = adc1_val >> 2;
                    let new_volume = if new_volume > 128 {
                        128 as u8
                    } else {
                        new_volume as u8
                    };
                    if new_volume != volume {
                        volume = new_volume;
                        if print_adc_change {
                            ufmt::uwriteln!(
                                &mut serial,
                                "ADC1 change: volume: {}",
                                volume,
                            )
                            .void_unwrap();
                        }
                        wavegen.set_volume(volume);
                    }
                } else {
                    // Pseudo-panic: Conversion not complete
                    ufmt::uwriteln!(
                        &mut serial,
                        "ADC1 conversion not complete"
                    )
                    .void_unwrap();
                    break;
                }
            }
            _ => {
                // pass
            }
        }
        time_slice = time_slice.wrapping_add(1);

        if !wait_for_timer_tick() {
            glitch_count = glitch_count.saturating_add(1);
        }
    }

    // If we reach this point there was an error of some kind
    sound_off();
    loop {}
}
