//! Pulse Generator
//! ===============
//! 
//! Press the button to generate a very short pulse on the output pin.
//! 
//! Connections
//! -----------
//! - ``D2``: Configured as floating input, connected to button with external
//!             10Kohm pulldown resistor, high when pressed.
//! - ``D3``: Configured as output, connected to oscilloscope to measure timing.
#![no_std]
#![no_main]

use arduino_rust::millis;
use avrd::current::{DDRB, DDRD, PIND, PORTB, PORTD};
use core::ptr::{read_volatile, write_volatile};
use panic_halt as _;

const DEBOUNCE_WAIT_MS: u32 = 10;

fn setup() {
    // PORTB5 = Arduino pin D13
    // Famous digital pin 13 is the onboard LED
    unsafe {
        write_volatile(DDRB, read_volatile(DDRB) | 0b00100000);
    }

    // PORTD3 = Arduino pin D3
    // Pulse output pin
    unsafe {
        write_volatile(DDRD, read_volatile(DDRD) | 0b00001000);
    }
}

#[inline]
fn turn_led_on() {
    unsafe {
        write_volatile(PORTB, 0b00100000);
    }
}

#[inline]
fn turn_led_off() {
    unsafe {
        write_volatile(PORTB, 0b00000000);
    }
}

#[inline]
fn pulse_on() {
    unsafe {
        write_volatile(PORTD, 0b00001000);
    }
}

#[inline]
fn pulse_off() {
    unsafe {
        write_volatile(PORTD, 0b00000000);
    }
}

/// Read button == state of Arduino pin D2 == ATmega328P signal PORTD2
#[inline]
fn read_button() -> bool {
    (unsafe { read_volatile(PIND) } & 0b00000100) != 0
}

#[arduino_hal::entry]
fn main() -> ! {
    let peripherals = arduino_hal::Peripherals::take().unwrap();
    setup();
    // Make sure pulse output is low
    unsafe {
        write_volatile(PORTD, 0b00000000);
    }

    millis::millis_init(peripherals.TC0);

    // Blink LED to let people know the program has started
    turn_led_on();
    arduino_hal::delay_ms(250);
    turn_led_off();
    arduino_hal::delay_ms(500);
    turn_led_on();
    arduino_hal::delay_ms(500);
    turn_led_off();

    loop {
        // Wait for button signal to go high
        while !read_button() {}

        // Generate a short pulse
        pulse_on();
        pulse_off();

        // Enable interrupts globally (otherwise millis() won't work)
        unsafe { avr_device::interrupt::enable() };

        // Wait for button to stay low for a while
        'debounce: loop {
            let t0: u32 = millis::millis();
            while !read_button() {
                if millis::millis() - t0 >= DEBOUNCE_WAIT_MS {
                    break 'debounce;
                }
            }
        }

        // Disable interrupts again so we get predictable timing
        avr_device::interrupt::disable();
    }
}
