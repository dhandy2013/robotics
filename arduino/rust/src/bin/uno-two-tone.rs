//! Ardunio UNO code to generate tones on speaker in the foreground.
//! Uses TimerCounter/0 for PWM signals and Timer/Counter1 for sample timing.
//! Pin D4: Input with internal pullup, connected to pushbutton
//! Pin D5: OC0B PWM output at 62.5 KHz: Channel 0 sound
//! Pin D6: OC0A PWM output at 62.5 KHz: Channel 1 sound
#![no_std]
#![no_main]
use arduino_hal::prelude::*;
use arduino_rust::buttons::{portd_button_is_pressed, setup_portd_buttons};
use arduino_rust::sonic::{
    get_t0_config, get_t1_config, setup_timer_counter_0, setup_timer_counter_1,
    sound_off, sound_on, wait_for_timer_tick, write_channel0, write_channel1,
    TICKS_PER_SECOND,
};
use panic_halt as _;
use wavegen::wavegen8::{WaveGenerator, Waveform};

const SAMPLE_RATE: u16 = TICKS_PER_SECOND;
type WaveGen = WaveGenerator<SAMPLE_RATE>;

#[arduino_hal::entry]
fn main() -> ! {
    let peripherals = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(peripherals);
    let mut serial = arduino_hal::default_serial!(peripherals, pins, 57600);
    ufmt::uwriteln!(&mut serial, "Hello from uno-two-tone.rs\r").void_unwrap();

    setup_portd_buttons(1 << 4); // button on pin D4

    // Set up initial sound parameters on both channels
    let mut gen0 = WaveGen::default();
    let mut gen1 = WaveGen::default();

    // Start the PWM oscillators and timer
    setup_timer_counter_0();
    let (tccr0a, tccr0b) = get_t0_config();
    ufmt::uwriteln!(&mut serial, "TCCR0A={} TCCR0B={}\r", tccr0a, tccr0b)
        .void_unwrap();
    setup_timer_counter_1();
    let (tccr1a, tccr1b) = get_t1_config();
    ufmt::uwriteln!(&mut serial, "TCCR1A={} TCCR1B={}\r", tccr1a, tccr1b)
        .void_unwrap();

    // Set tone length
    let duration_ticks = TICKS_PER_SECOND * 2; // 2 seconds

    // Calculate phase velocity for desired frequency
    let f220_phase_vel = WaveGen::freq_x16_to_phase_vel(220 * 16);
    let f440_phase_vel = WaveGen::freq_x16_to_phase_vel(440 * 16);

    // Max volume is 128
    let left_volume = 32;
    let right_volume = 32;

    // Start the tone on both channels
    gen0.set_waveform(Waveform::SineWave);
    gen0.set_vel(f440_phase_vel);
    gen0.set_volume(left_volume);
    gen1.set_waveform(Waveform::SineWave);
    gen1.set_vel(f220_phase_vel);
    gen1.set_volume(right_volume);

    let mut countdown_ticks = duration_ticks;

    sound_on();
    loop {
        if portd_button_is_pressed(4) {
            ufmt::uwriteln!(&mut serial, "Button on D4 pressed\r")
                .void_unwrap();
            break;
        }

        countdown_ticks = countdown_ticks.saturating_sub(1);
        if countdown_ticks == 1 {
            ufmt::uwriteln!(&mut serial, "2-second timer expired\r")
                .void_unwrap();
        }

        let pulsewidth0 = gen0.sample();
        let pulsewidth1 = gen1.sample();
        // Signal OC0B == pin D5 == channel 0 == left channel
        // tc0.ocr0b.write(|w| unsafe { w.bits(pulsewidth0) });
        write_channel0(pulsewidth0);
        // Signal OC0A == pin D6 == channel 1 == right channel
        // tc0.ocr0a.write(|w| unsafe { w.bits(pulsewidth1) });
        write_channel1(pulsewidth1);

        wait_for_timer_tick();
    }
    sound_off();

    ufmt::uwriteln!(&mut serial, "Done.\r").void_unwrap();

    // Try to figure out what is going on with the generators
    ufmt::uwriteln!(&mut serial, "gen0: {:?}", gen0).void_unwrap();
    ufmt::uwriteln!(&mut serial, "gen1: {:?}", gen1).void_unwrap();
    for i in 0..30 {
        let pulsewidth0 = gen0.sample();
        let pulsewidth1 = gen1.sample();
        ufmt::uwriteln!(
            &mut serial,
            "{}: {} {}\r",
            i,
            pulsewidth0,
            pulsewidth1
        )
        .void_unwrap();
    }

    loop {}
}
