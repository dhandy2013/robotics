/*!
 * PWM sweep program - change frequency and/or duty cycle over time.
 * 
 * Originally inspired by:
 * https://github.com/Rahix/avr-hal/blob/main/examples/arduino-uno/src/bin/uno-manual-servo.rs
 *
 * Connections
 * -----------
 *  - `D9`: PWM signal output
 *  - Uses OC1A which is connected to D9 of the Arduino Uno.
 */
#![no_std]
#![no_main]

use panic_halt as _;

#[arduino_hal::entry]
fn main() -> ! {
    let dp = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(dp);

    // Important because this sets the bit in the DDR register!
    pins.d9.into_output();

    // We will set up TC1 to run off a 250kHz clock
    // with `pwm_freq_count` counts per overflow.
    // pwm_freq = tc1_clk_freq / pwm_freq_count
    let start_pwm_freq_count: u16 = 2500;

    let tc1 = dp.TC1;
    tc1.icr1.write(|w| unsafe { w.bits(start_pwm_freq_count - 1) });
    tc1.tccr1a
        .write(|w| w.wgm1().bits(0b10).com1a().match_clear());
    tc1.tccr1b
        .write(|w| w.wgm1().bits(0b11).cs1().prescale_64());

    loop {
        // Correspondence of duty_cycle_count to pulse width:
        // pulse_width = duty_cycle_count / tc1_clk_freq
        // 100 counts => 0.4ms
        // 700 counts => 2.8ms

        let mut pwm_freq_count = start_pwm_freq_count;

        for duty_cycle_count in 100..=700 {
            tc1.icr1.write(|w| unsafe { w.bits(pwm_freq_count - 1) });
            tc1.ocr1a.write(|w| unsafe { w.bits(duty_cycle_count) });
            arduino_hal::delay_ms(10);
            pwm_freq_count -= 2;
        }
    }
}
