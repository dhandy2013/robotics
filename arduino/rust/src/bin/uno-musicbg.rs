//! Music player for Arduino UNO R3 - can play music in the background
//! Uses TimerCounter/0 for PWM signals and Timer/Counter1 for sample timing.
//! D5: OC0B PWM output at 62.5 KHz: Channel 0 sound
//! D6: OC0A PWM output at 62.5 KHz: Channel 1 sound
//!
//! WARNING: This code does not work! Either due to compiler bugs or stack
//! overflow, the SonicBackgroundData::update() method returns essentially
//! random numbers for channel 0 when called inside the interrupt routine, but
//! not when called from main().
//!
//! My solution for now is to give up trying to use interrupt routines for
//! anything except for the simplest timer-tick counting (that part does work.)
#![no_std]
#![no_main]
use arduino_hal::prelude::*;
use arduino_rust::sonicbg::{
    change_params, get_countdown_ticks, setup_timers_and_interrupts,
    Channel::Channel0, Channel::Channel1, TICKS_PER_SECOND,
};
use avr_device::interrupt;
use panic_halt as _;
use wavegen::wavegen8::{WaveGenerator, Waveform};

const SAMPLE_RATE: u16 = TICKS_PER_SECOND;
const MIN_PHASE_VEL: u16 = WaveGenerator::<SAMPLE_RATE>::MIN_PHASE_VEL;

// Defines the SONG array
include!("../../gen/song.rs");

#[arduino_hal::entry]
fn main() -> ! {
    let peripherals = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(peripherals);
    let mut serial = arduino_hal::default_serial!(peripherals, pins, 57600);
    ufmt::uwriteln!(&mut serial, "Hello from uno-musicbg.rs").void_unwrap();

    // Set up initial sound parameters on both channels
    // Start out with a sine wave, low frequency, silenced (volume 0)
    change_params(|mut token| {
        token.set_channel_sound(Channel0, Waveform::SineWave, MIN_PHASE_VEL, 0);
        token.set_channel_sound(Channel1, Waveform::SineWave, MIN_PHASE_VEL, 0);
    });

    // Start the PWM oscillators and timer
    setup_timers_and_interrupts(peripherals.TC0, peripherals.TC1);

    // Set the main volume = 1/4 of max range which usually is what we want
    let main_volume = 32;

    // Enable interrupts globally
    unsafe { interrupt::enable() };

    // Play all the notes in the song - in the background!
    for (duration_ticks, phase_vel) in SONG.iter() {
        let (volume, phase_vel) = if *phase_vel == 0xffff {
            // rest
            (0, MIN_PHASE_VEL)
        } else {
            (main_volume, *phase_vel)
        };
        let tick_count = *duration_ticks;
        change_params(|mut token| {
            token.set_countdown_ticks(tick_count);
            token.set_channel_sound(
                Channel0,
                Waveform::SineWave,
                phase_vel,
                volume,
            );
            token.set_channel_sound(
                Channel0,
                Waveform::SineWave,
                phase_vel,
                volume,
            );
        });
        while get_countdown_ticks() > 0 {
            // TODO: Check buttons, flash lights, etc.
        }
    }

    // Turn sound back off for both channels
    change_params(|mut token| {
        token.set_channel_sound(Channel0, Waveform::Off, MIN_PHASE_VEL, 0);
        token.set_channel_sound(Channel1, Waveform::Off, MIN_PHASE_VEL, 0);
    });

    ufmt::uwriteln!(&mut serial, "Done.\r").void_unwrap();
    loop {}
}
