//! Analog to Decimal Conversion
//! Signals used:
//! * A0 (PORTC0/ADC0): Left potentiometer
//! * A1 (PORTC1/ADC1): Right potentiometer
use avrd::current::{ADCH, ADCL, ADCSRA, ADMUX, DIDR0};
use core::ptr::{read_volatile, write_volatile};
use crate::uno_bitfields::*;

/// Configure Analog-Decimal Conversion
pub fn setup_adc() {
    unsafe {
        // Select ADC0 as input
        // MUX3:0 ------------------++++
        //                          ||||
        // Reserved ---------------+||||
        //                         |||||
        // Left-adjust result? no  |||||
        // ADLAR -----------------+|||||
        //                        ||||||
        // Select +5V voltage ref ||||||
        // REFS1:0 -------------++||||||
        //                      ||||||||
        write_volatile(ADMUX, 0b01000000);

        // Select ADC1 as input
        // MUX3:0 ------------------++++
        //                          ||||
        // Reserved ---------------+||||
        //                         |||||
        // Left-adjust result? no  |||||
        // ADLAR -----------------+|||||
        //                        ||||||
        // Select +5V voltage ref ||||||
        // REFS1:0 -------------++||||||
        //                      ||||||||
        write_volatile(ADMUX, 0b01000001);

        // ADC Prescaler Select
        // 000 == division factor 2
        // 001 == division factor 2
        // 010 == division factor 4
        // 011 == division factor 8
        // 100 == division factor 16
        // 101 == division factor 32
        // 110 == division factor 64
        // 111 == division factor 128
        // ADPS2:0 -------------------+++
        //                            |||
        // ADC Interrupt Enable off   |||
        // ADIE ---------------------+|||
        //                           ||||
        // ADC Interrupt Flag        ||||
        // write 1 to manually clear ||||
        // ADIF --------------------+||||
        //                          |||||
        // Auto Trigger Enable? no  |||||
        // ADATE ------------------+|||||
        //                         ||||||
        // Start conversion? no    ||||||
        // ADSC ------------------+||||||
        //                        |||||||
        // Enable ADC? yes        |||||||
        // ADEN -----------------+|||||||
        //                       ||||||||
        write_volatile(ADCSRA, 0b10010100);

        // Digital Input Disable Register 0
        // Setting to 0b11 turns off the digital input buffer for for
        // ADC0 and ADC1.
        write_volatile(DIDR0, 0b00000011);
    }
}

/// Start Analog to Decimal conversion.
/// adc_select: 0 through 5 to read ADC0 through ADC5 respectively.
/// Note: For best results, do two reads in a row. Throw the first away.
pub fn start_adc(adc_select: u8) {
    unsafe {
        // Select which input to read
        write_volatile(ADMUX, (read_volatile(ADMUX) & 0b11111000) | adc_select);
        // Set ADSC to start analog-to-digital conversion
        write_volatile(ADCSRA, read_volatile(ADCSRA) | ADSC);
    }
}

/// Return the result of the previously-started Analog to Decimal conversion.
/// Returns a value in range 0x000 through 0x3ff, where 0x000 is ground
/// and 0x3ff is the maximum voltage (+5V.)
///
/// Return Some(adc_value) if conversion is complete,
/// None if it is not yet finished.
pub fn read_adc() -> Option<u16> {
    unsafe {
        if read_volatile(ADCSRA) & ADIF == 0 {
            return None;
        }

        let adcl = read_volatile(ADCL); // ADCL must be read first
        let adch = read_volatile(ADCH);
        Some((adch as u16) << 8 | adcl as u16)
    }
}

/// adc_select: 0 through 5 to read ADC0 through ADC5 respectively.
/// Does 2 reads, throws the first one away, for accuracy.
/// This can take a while, don't do it in a short timeslice.
pub fn blocking_read_adc(adc_select: u8) -> u16 {
    start_adc(adc_select);
    loop {
        if let Some(_) = read_adc() {
            break;
        }
    }
    start_adc(adc_select);
    loop {
        if let Some(adc_val) = read_adc() {
            return adc_val;
        }
    }
}
