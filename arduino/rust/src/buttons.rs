//! Common code for handling buttons connected to Arduino UNO
use avrd::current::{DDRD, PIND, PORTD};
use core::ptr::{read_volatile, write_volatile};

/// Setup signals in the PORTD bank as inputs with internal pullup resistor.
/// buttons: Button bitmask, set bit 0 for D0, bit1 for D1, etc.
pub fn setup_portd_buttons(buttons: u8) {
    unsafe {
        // Explicitly configure PORTD signals as inputs
        write_volatile(DDRD, read_volatile(DDRD) & !buttons);

        // Set PORTD signals to logical 1 while the pins are configured as
        // inputs to turn on the internal pullup resistors.
        write_volatile(PORTD, read_volatile(PORTD) | buttons);
    }
}

/// Return false if a PORTD input pin is floating high,
/// true if a button press is pulling the pin to ground.
/// button_num: Button number 0 through 7, corresponding to pin D0 through D7
pub fn portd_button_is_pressed(button_num: u8) -> bool {
    unsafe { (read_volatile(PIND) & (1 << (button_num & 0x07))) == 0 }
}
