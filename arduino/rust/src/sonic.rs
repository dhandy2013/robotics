/*! Arduino UNU audio hardware setup and manipulation
 * Uses TimerCounter/0 for PWM signals and Timer/Counter1 for sample timing.
 * D5: OC0B PWM output at 62.5 KHz: Channel 0 sound (left channel)
 * D6: OC0A PWM output at 62.5 KHz: Channel 1 sound (right channel)
 */
use crate::uno_bitfields::*;
use avrd::current::{
    DDRD, OCR0A, OCR0B, OCR1A, TCCR0A, TCCR0B, TCCR1A, TCCR1B, TIFR1,
};
use core::ptr::{read_volatile, write_volatile};

pub const TICKS_PER_SECOND: u16 = 15625;

// OC1A timer frequency:
//  fOC1A = fCLK_IO / (2 * N * (1 + OCR1A))
// Where:
//  fCLK_IO = 16MHz
//  N = 1 (no prescaling)
//  OCR1A = 1023
// Therefore:
//  fOC1A = 7812.5KHz  # frequency of output square wave
// However OCF1A will be set on every transition, so double that rate, or
// 15.625KH (TICKS_PER_SECOND).
const TIMER_COUNTS: u16 = 1023;

pub fn setup_timer_counter_0() {
    unsafe {
        // Clock configuration assumes TCCRnx is all zeros at startup.
        // Which I confirmed by experiment.

        // There is a 10bit prescaler common to both Timer/Counter0 and
        // Timer/Counter1. The prescaler is not being used, so the input
        // frequency is 16.000MHz. Since we are doing 8-bit PWM, the expected
        // PWM frequency is the input frequency divided by 256, or 62.500KHz.
        // The measured frequency was between 62.4KHz and 62.8KHz.

        // Configure 8-bit Timer/Counter0 for fast PWM
        // Outputs are on OC0A (pin D6) and OC0B (pin D5).
        //
        // COM == Compare Output Mode
        // For PWM modes the COM bits control whether the PWM output should
        // be inverted. For non-PWM modes they control what happens to the
        // output at compare match: set, clear, or toggle.
        // Setting COM0A1 and leaving COM0A0 clear while in Fast PWM Mode
        // means clear OC0A on compare match, set OC0A at BOTTOM. In other
        // words, non-inverting mode. Setting COM0B1 and leaving COM0B0 clear
        // while in Fast PWM Mode means the same thing, but for OC0B.
        //
        // WGM == Waveform Generation Mode
        // Setting WGM00 and WGM01 in the TCCR0A register and leaving
        // WGM02 clear in the TCCR0B register sets Timer/Counter0 in Fast PWM
        // Mode, which affects the behavior of both OC0A and OC0B. By the way,
        // the TCCR0A and TCCR0B names are confusing because in this case there
        // are bits in both registers that control both the OC0A and OC0B output
        // behavior.
        write_volatile(
            TCCR0A,
            read_volatile(TCCR0A) | COM0A1 | COM0B1 | WGM00 | WGM01,
        );
        // CS == Clock Select. According to the ATmega328P datasheet,
        // setting CS00 and leaving CS01 and CS02 cleared selects the
        // system clock with frequency fCLK_I/O which is 16.000MHz for
        // my Arduino.
        write_volatile(TCCR0B, read_volatile(TCCR0B) | CS00);

        // Set initial duty cycle to 50% for both OCR0A and OCR0B
        write_volatile(OCR0A, 128);
        write_volatile(OCR0B, 128);

        // Pins D5 and D6 are still configured as inputs, no output yet.
    }
}

/// Return (TCCR0A, TCCR0B)
pub fn get_t0_config() -> (u8, u8) {
    unsafe { (read_volatile(TCCR0A), read_volatile(TCCR0B)) }
}

pub fn setup_timer_counter_1() {
    unsafe {
        // Clock configuration assumes TCCRnx is all zeros at startup.
        // Which I confirmed by experiment.

        // There is a 10bit prescaler common to both Timer/Counter0 and
        // Timer/Counter1. The prescaler is not being used, so the input
        // frequency is 16.000MHz.

        // Configure 16-bit Timer/Counter1 to count TICKS_PER_SECOND Hz ticks.
        // Output would be on OC1A but we aren't configuring it to drive output,
        // we are just checking/clearing the OCF1A flag.
        //
        // Setting COM1A0 and leaving COM1A1 clear toggles OC1A on compare match
        // and causes OC1B to be disconnected (normal port operation).
        write_volatile(TCCR1A, read_volatile(TCCR1A) | COM1A0);
        // CS == Clock Select. According to the ATmega328P datasheet,
        // setting CS10 and leaving CS11 and CS12 cleared selects the
        // system clock with frequency fCLK_I/O which is 16.000MHz for
        // my Arduino.
        // WGM == Waveform Generation Mode. Setting WGM12 and leaving the
        // other WGM bits clear sets the timer/counter to CTC mode.
        write_volatile(TCCR1B, read_volatile(TCCR1B) | CS10 | WGM12);
        // OC1A timer frequency:
        //  fOC1A = fCLK_IO / (2 * N * (1 + OCR1A))
        // Where:
        //  fCLK_IO = 16MHz
        //  N = 1 (no prescaling)
        //  OCR1A = 1023
        // Therefore:
        //  fOC1A = 7812.5KHz
        // And OCF1A will be set at a rate of 15.625KHz (every half cycle)
        write_volatile(OCR1A, TIMER_COUNTS);

        // For debug:
        // Enable the OC1A (pin D9) output so we can see the timer signal.
        // It should be 8KHz. (Output toggles at 15.625KHz frequency.)
        // write_volatile(DDRB, read_volatile(DDRB) | PORTB1);
    }
}

/// Return (TCCR1A, TCCR1B)
pub fn get_t1_config() -> (u8, u8) {
    unsafe { (read_volatile(TCCR1A), read_volatile(TCCR1B)) }
}

pub fn sound_on() {
    unsafe {
        // Configure pins D6 and D5 (signals OC0A and OC0B) as outputs.
        write_volatile(DDRD, read_volatile(DDRD) | 0b01100000);
    }
}

pub fn sound_off() {
    unsafe {
        // Configure pins D6 and D5 (signals OC0A and OC0B) as inputs.
        write_volatile(DDRD, read_volatile(DDRD) & 0b10011111);
    }
}

/// Wait for Timer/Counter1 to roll over.
/// Return true if we reached this point before the timer flag was set,
/// false if it was already set (and therefore we missed our timeslice).
#[inline]
pub fn wait_for_timer_tick() -> bool {
    let result;
    if unsafe { (read_volatile(TIFR1) & OCF1A) != 0 } {
        result = false;
    } else {
        result = true;
        while unsafe { (read_volatile(TIFR1) & OCF1A) == 0 } {}
    }
    // Writing a 1 to the OCF1A flag clears it (see the ATmega328P datasheet).
    unsafe {
        write_volatile(TIFR1, read_volatile(TIFR1) | OCF1A);
    }
    result
}

/// Set the output pulse with for sound channel 0 (left channel = D5)
#[inline]
pub fn write_channel0(pulse_width: u8) {
    unsafe {
        write_volatile(OCR0B, pulse_width);
    }
}

/// Set the output pulse width for sound channel 1 (right channel = D6)
#[inline]
pub fn write_channel1(pulse_width: u8) {
    unsafe {
        write_volatile(OCR0A, pulse_width);
    }
}
