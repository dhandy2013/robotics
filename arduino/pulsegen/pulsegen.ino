const int BUTTON_PIN = 2;
const int PULSE_OUT_PIN = 3;
const unsigned long DEBOUNCE_WAIT_MS = 10;

void setup() {
  pinMode(PULSE_OUT_PIN, OUTPUT);
  digitalWrite(PULSE_OUT_PIN, LOW); // initialize output value

  pinMode(BUTTON_PIN, INPUT);
}

void loop() {
  // Wait for button signal to go high
  while (digitalRead(BUTTON_PIN) == LOW) {
    // spin
  }

  // Generate a short pulse
  digitalWrite(PULSE_OUT_PIN, HIGH);
  digitalWrite(PULSE_OUT_PIN, LOW);

  // Wait for button to stay low for a while
  while (true)
  {
    while (digitalRead(BUTTON_PIN) == HIGH) {
      // spin
    }
    unsigned long t0 = millis();
    while (digitalRead(BUTTON_PIN) == LOW) {
      if (millis() - t0 >= DEBOUNCE_WAIT_MS) {
        return;
      }
    }
  }
}
