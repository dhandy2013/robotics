//! Template project for Flipper Zero.
//! This app prints "Hello, Rust!" to the console then exits.

#![no_main]
#![no_std]

extern crate alloc;
extern crate flipperzero_alloc;

// Required for panic handler
extern crate flipperzero_rt;

use flipperzero::{dialogs, print};
use flipperzero_rt::{entry, manifest};

// Define the FAP Manifest for this application
manifest!(
    name = "Flipper Zero Rust",
    app_version = 1,
    has_icon = true,
    // See https://github.com/flipperzero-rs/flipperzero/blob/v0.7.2/docs/icons.md for icon format
    icon = "rustacean-10x10.icon",
);

// Define the entry function
entry!(main);

// Entry point
fn main(_arngs: *mut u8) -> i32 {
    print!("Hello, Rust!\r\n");
    dialogs::alert("Thanks Nate for the cool device!");
    0
}
