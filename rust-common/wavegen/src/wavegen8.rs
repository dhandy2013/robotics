//! Waveform generation for 8-bit processors (like ATmega328p/Arduino)
//! Algorithmically generate wave functions with these constraints:
//! - Output is discrete sample values in the range 0..=255
//! - No floating-point arithmetic
//! - Try to stick to 8 or 16-bit integers in the critical path
//! - Try to avoid multiplying or dividing except by a power of 2
#[cfg(feature = "ufmt")]
use ufmt::derive::uDebug;

#[derive(Clone)]
#[cfg_attr(feature = "ufmt", derive(uDebug))]
pub struct WaveGenerator<const SAMPLE_RATE: u16> {
    /// The type of waveform to generate
    waveform: Waveform,

    /// The current phase in units such that 65536 units makes one cycle.
    phase: u16,

    /// Phase velocity - change in phase per sample
    vel: u16,

    /// Volume scaler - in range 0..=128
    volume: u8,
}

impl<const SAMPLE_RATE: u16> Default for WaveGenerator<SAMPLE_RATE> {
    fn default() -> Self {
        Self {
            waveform: Waveform::Off,
            phase: 0,
            vel: Self::MIN_PHASE_VEL,
            volume: 0,
        }
    }
}

impl<const SAMPLE_RATE: u16> WaveGenerator<SAMPLE_RATE> {
    pub const MAX_VOLUME: u8 = 128;

    /// Number of phase units for one complete waveform cycle
    pub const PHASE_UNITS_PER_CYCLE: u32 = 65536;

    /// Minimum allowed phase velocity (0 frequency could mean DC current)
    pub const MIN_PHASE_VEL: u16 = Self::freq_x16_to_phase_vel(32 * 16);

    /// Convert from frequency * 16 to phase units per timeslice,
    /// where there are 65536 phase units per cycle
    /// and SAMPLE_RATE timeslices per second.
    #[inline]
    pub const fn freq_x16_to_phase_vel(freq_x16: u16) -> u16 {
        let freq_x16 = if freq_x16 < 32 * 16 {
            32 * 16
        } else {
            freq_x16
        };
        let phase_x16 = (freq_x16 as u32 * Self::PHASE_UNITS_PER_CYCLE)
            / SAMPLE_RATE as u32;
        (phase_x16 >> 4) as u16
    }

    /// Create a new wave generator set to the given frequency.
    /// ``waveform`` is the waveform type.
    /// ``freq_x16`` is the frequency multiplied by 16 (4 fractional bits)
    /// with a lower limit of 32 Hz.
    /// ``volume`` is a number in range 0..=128
    pub const fn new(waveform: Waveform, freq_x16: u16, volume: u8) -> Self {
        let vel = Self::freq_x16_to_phase_vel(freq_x16);
        Self {
            waveform,
            phase: 0,
            vel,
            volume: if volume <= Self::MAX_VOLUME {
                volume
            } else {
                Self::MAX_VOLUME
            },
        }
    }

    /// Update the phase - called once per sample!
    #[inline]
    fn update(&mut self) {
        self.phase += self.vel;
    }

    /// Return the current waveform sample and update the generator.
    /// The sample value is an integer in the range 0..=255.
    /// Performance is critical because this is called once every 1000 clock
    /// cycles, so some shortcuts are taken.
    pub fn sample(&mut self) -> u8 {
        let phase = self.phase;
        let volume = self.volume;

        // The 2 most significant bits of phase are the quadrant.
        // The next 7 bits are the index (sample number inside the quadrant).
        let quadrant = ((phase & 0b1100000000000000) >> 14) as u8;
        let index = ((phase & 0b0011111110000000) >> 7) as i8;

        let y: i8 = match self.waveform {
            Waveform::Off => 0,
            Waveform::SquareWave => {
                if (quadrant & 0b10) == 0 {
                    127
                } else {
                    -127
                }
            }
            Waveform::TriangleWave => {
                // Calculate sample value y based on quadrant and index.
                // Yes I have noticed that due to binary math this isn't perfect:
                // A couple of phase values result in the same sample value. And
                // the final sample value can only go down to 1, not zero. But
                // performance here is more important than perfection.
                match quadrant {
                    0 => index,
                    1 => 127 - index,
                    2 => -index,
                    _ => -(127 - index),
                }
            }
            Waveform::SineWave => {
                let index = index as u8;
                let (angle, negate) = match quadrant {
                    0 => (index, false),
                    1 => (128 - index, false),
                    2 => (index, true),
                    _ => (128 - index, true),
                };
                if angle == 0 {
                    0
                } else {
                    let y = SINE_LOOKUP[(angle - 1) as usize];
                    if negate {
                        -y
                    } else {
                        y
                    }
                }
            }
        };

        self.update();

        (((y as i16 * volume as i16) >> 7) + 128) as u8
    }

    pub fn set_next_waveform(&mut self) {
        self.waveform = self.waveform.next();
    }

    pub fn set_waveform(&mut self, waveform: Waveform) {
        self.waveform = waveform;
    }

    pub fn waveform(&self) -> Waveform {
        self.waveform
    }

    /// Set frequency of wave generator in Hz.
    /// freq_x16: frequency in Hz * 16 (12.4 bit fixed point)
    pub fn set_pitch(&mut self, freq_x16: u16) {
        self.vel = Self::freq_x16_to_phase_vel(freq_x16);
    }

    /// Set the phase velocity (frequency) in phase units per timeslice.
    /// There are 65536 phase units per cycle
    /// and SAMPLE_RATE timeslices per second.
    ///
    /// To calculate phase velocity from frequency in cycles per second,
    /// see ``freq_x16_to_phase_vel()``. But beware, it can be a time-
    /// consuming calculation for a small embedded device.
    pub fn set_vel(&mut self, vel: u16) {
        self.vel = vel;
    }

    pub fn vel(&self) -> u16 {
        self.vel
    }

    /// volume: range is 0..=128
    pub fn set_volume(&mut self, volume: u8) {
        self.volume = if volume <= Self::MAX_VOLUME {
            volume
        } else {
            Self::MAX_VOLUME
        };
    }

    pub fn volume(&self) -> u8 {
        self.volume
    }

    pub fn name(&self) -> &'static str {
        self.waveform.name()
    }

    pub fn is_off(&self) -> bool {
        self.waveform.is_off()
    }
}

/// 128-entry lookup table of sine values for angles from pi/(2*128) through
/// pi/2 inclusive, scaled by 127.
static SINE_LOOKUP: [i8; 128] = [
    1, 3, 4, 6, 7, 9, 10, 12, 13, 15, 17, 18, 20, 21, 23, 24, 26, 27, 29, 30,
    32, 33, 35, 36, 38, 39, 41, 42, 44, 45, 47, 48, 50, 51, 52, 54, 55, 57, 58,
    59, 61, 62, 63, 65, 66, 67, 69, 70, 71, 73, 74, 75, 76, 78, 79, 80, 81, 82,
    84, 85, 86, 87, 88, 89, 90, 91, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102,
    102, 103, 104, 105, 106, 107, 108, 108, 109, 110, 111, 112, 112, 113, 114,
    114, 115, 116, 116, 117, 117, 118, 119, 119, 120, 120, 121, 121, 121, 122,
    122, 123, 123, 123, 124, 124, 124, 125, 125, 125, 125, 126, 126, 126, 126,
    126, 126, 126, 126, 126, 126, 127,
];

#[derive(Copy, Clone)]
#[cfg_attr(feature = "ufmt", derive(uDebug))]
pub enum Waveform {
    Off,
    SquareWave,
    TriangleWave,
    SineWave,
}

impl Waveform {
    pub fn name(&self) -> &'static str {
        match self {
            Self::Off => "Off",
            Self::SquareWave => "SquareWave",
            Self::TriangleWave => "TriangleWave",
            Self::SineWave => "SineWave",
        }
    }

    /// Cycle through modes
    pub fn next(&self) -> Self {
        match self {
            Self::Off => Self::SquareWave,
            Self::SquareWave => Self::TriangleWave,
            Self::TriangleWave => Self::SineWave,
            Self::SineWave => Self::Off,
        }
    }

    pub fn is_off(&self) -> bool {
        match self {
            Self::Off => true,
            _ => false,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    const SAMPLE_RATE: u16 = 10000;
    #[test]
    fn construct_sinegen() {
        let gen =
            WaveGenerator::<SAMPLE_RATE>::new(Waveform::SineWave, 220 * 16, 64);
        assert!(!gen.is_off());
    }

    #[test]
    fn construct_default() {
        let gen = WaveGenerator::<SAMPLE_RATE>::default();
        assert!(gen.is_off());
    }
}
