//! Waveform generation of 16-bit signed sample values
//!
//! Algorithmically generate wave functions with these constraints:
//! - Output is discrete sample values in the range -32767..=32767
//! - No floating-point calculations except at compile time.
//! - Avoid multiply and divide, stick to bit shifts and add/subtract
//! - Avoid exceeding 32-bit values

#[derive(Copy, Clone, Debug)]
pub struct WaveParams<const SAMPLE_RATE: u16> {
    /// The type of waveform to generate
    waveform: Waveform,

    /// Phase velocity - pitch in units of change in phase per sample
    phase_vel: u16,

    /// Waveform amplitude - in range 0..=16384 (AKA "volume")
    amp: u16,
}

impl<const SAMPLE_RATE: u16> WaveParams<SAMPLE_RATE> {
    /// Maximum amplitude scaling value; must be an even power of two.
    pub const MAX_AMPLITUDE: u16 = 16384;

    /// Number of phase units for one complete waveform cycle
    pub const PHASE_UNITS_PER_CYCLE: u32 = 65536;

    /// Minimum allowed frequency for waveform generator.
    /// Generating a frequency of zero could cause problems including
    /// DC currents in circuits that should not have them.
    pub const MIN_FREQ: u16 = 32;

    /// Minimum allowed phase velocity (based on MIN_FREQ)
    pub const MIN_PHASE_VEL: u16 =
        Self::freq_x16_to_phase_vel(Self::MIN_FREQ * 16);

    /// The sample rate of this wave generator in samples per second.
    pub const fn sample_rate() -> u16 {
        SAMPLE_RATE
    }

    pub const fn new() -> Self {
        Self {
            waveform: Waveform::Off,
            phase_vel: Self::MIN_PHASE_VEL,
            amp: 0,
        }
    }

    /// Convert from frequency * 16 to phase units per sample,
    /// where there are PHASE_UNITS_PER_CYCLE phase units per cycle
    /// and SAMPLE_RATE samples per second.
    ///
    /// Because the ``freq_x16`` parameter is type u16 with a maximum value of
    /// 65535, the highest frequency that can be represented is 4095.9375 Hz.
    ///
    /// For comparison:
    /// Lowest note on piano        : A0     =   27.5 Hz
    /// Middle C                    : C4     =  261.6 Hz
    /// Standard frequency reference: A4     =  440.0 Hz
    /// Second highest note on piano: B7     = 3951.1 Hz
    /// Largest freq_x16 value      : C8-37% = 4095.9375 Hz
    /// Highest note on piano       : C8     = 4186.0 Hz
    ///
    /// [Note Names, MIDI Numbers and frequencies](https://newt.phys.unsw.edu.au/jw/notes.html)
    ///
    /// Warning: This function does a potentially expensive division operation.
    /// Do not call it from an interrupt routine with a non-constant parameter.
    #[inline]
    pub const fn freq_x16_to_phase_vel(freq_x16: u16) -> u16 {
        let phase_x16 = (freq_x16 as u32 * Self::PHASE_UNITS_PER_CYCLE)
            / SAMPLE_RATE as u32;
        (phase_x16 >> 4) as u16
    }

    /// Convert from integer frequency to phase units per sample,
    /// where there are PHASE_UNITS_PER_CYCLE phase units per cycle
    /// and SAMPLE_RATE samples per second.
    ///
    /// It is impractical to call this function with a value greater than
    /// SAMPLE_RATE / 2 , due to the well-known Nyquist limit.
    ///
    /// Warning: This function does a potentially expensive division operation.
    /// Do not call it from an interrupt routine with a non-constant parameter.
    #[inline]
    pub const fn freq_to_phase_vel(freq: u16) -> u16 {
        let phase_vel =
            (freq as u32 * Self::PHASE_UNITS_PER_CYCLE) / SAMPLE_RATE as u32;
        phase_vel as u16
    }

    pub fn set_waveform(&mut self, waveform: Waveform) {
        self.waveform = waveform;
    }

    pub fn waveform(&self) -> Waveform {
        self.waveform
    }

    /// Set frequency of wave generator in Hz.
    /// freq_x16: frequency in Hz * 16 (fixed point with 4 fractional bits)
    /// Does not allow setting a pitch lower than MIN_FREQ.
    /// Warning: Calls freq_x16_to_phase_vel() which does a division operation.
    pub fn set_pitch(&mut self, freq_x16: u16) {
        let freq_x16 = if freq_x16 < Self::MIN_FREQ * 16 {
            Self::MIN_FREQ * 16
        } else {
            freq_x16
        };
        self.phase_vel = Self::freq_x16_to_phase_vel(freq_x16);
    }

    /// Set the phase velocity (frequency) in phase units per sample.
    /// There are PHASE_UNITS_PER_CYCLE (65536) phase units per cycle
    /// and SAMPLE_RATE samples per second.
    ///
    /// If you attempt to set a value lower than MIN_PHASE_VEL, it is clamped
    /// to MIN_PHASE_VEL.
    ///
    /// To calculate phase velocity from frequency in cycles per second,
    /// see ``freq_x16_to_phase_vel()``.
    pub fn set_phase_vel(&mut self, vel: u16) {
        self.phase_vel = if vel < Self::MIN_PHASE_VEL {
            Self::MIN_PHASE_VEL
        } else {
            vel
        };
    }

    pub fn phase_vel(&self) -> u16 {
        self.phase_vel
    }

    /// amp: waveform amplitude in range 0..=MAX_AMPLITUDE (16384)
    /// If you attempt to set a value higher than MAX_AMPLITUDE it is clamped to
    /// MAX_AMPLITUDE.
    pub fn set_amp(&mut self, amp: u16) {
        self.amp = if amp <= Self::MAX_AMPLITUDE {
            amp
        } else {
            Self::MAX_AMPLITUDE
        };
    }

    pub fn amp(&self) -> u16 {
        self.amp
    }

    /// Scale the audio sample by the waveform amplitude AKA volume.
    #[inline]
    pub fn scale_sample(&self, sample: i16) -> i16 {
        // MAX_AMPLITUDE must be an even power of 2 or this takes too long!
        ((sample as i32 * self.amp as i32) / (Self::MAX_AMPLITUDE as i32))
            as i16
    }
}

#[derive(Clone, Debug)]
pub struct WaveGenerator<const SAMPLE_RATE: u16> {
    wave_params: WaveParams<SAMPLE_RATE>,

    /// The current phase in units such that 65536 units makes one cycle.
    phase: u16,
}

impl<const SAMPLE_RATE: u16> Default for WaveGenerator<SAMPLE_RATE> {
    fn default() -> Self {
        Self::new()
    }
}

/// Waveform Generator
/// Generic const parameters:
/// SAMPLE_RATE: Number of samples per second being generated.
impl<const SAMPLE_RATE: u16> WaveGenerator<SAMPLE_RATE> {
    pub const MAX_AMPLITUDE: u16 = WaveParams::<SAMPLE_RATE>::MAX_AMPLITUDE;

    /// Number of phase units for one complete waveform cycle
    pub const PHASE_UNITS_PER_CYCLE: u32 =
        WaveParams::<SAMPLE_RATE>::PHASE_UNITS_PER_CYCLE;

    /// Minimum allowed frequency for waveform generator.
    /// Generating a frequency of zero could cause problems including
    /// DC currents in circuits that should not have them.
    pub const MIN_FREQ: u16 = WaveParams::<SAMPLE_RATE>::MIN_FREQ;

    /// Minimum allowed phase velocity (based on MIN_FREQ)
    pub const MIN_PHASE_VEL: u16 =
        Self::freq_x16_to_phase_vel(Self::MIN_FREQ * 16);

    /// The sample rate of this wave generator in samples per second.
    pub const fn sample_rate() -> u16 {
        SAMPLE_RATE
    }

    /// Convert from frequency * 16 to phase units per sample,
    /// where there are PHASE_UNITS_PER_CYCLE phase units per cycle
    /// and SAMPLE_RATE samples per second.
    ///
    /// Because the ``freq_x16`` parameter is type u16 with a maximum value of
    /// 65535, the highest frequency that can be represented is 4095.9375 Hz.
    ///
    /// For comparison:
    /// Lowest note on piano        : A0     =   27.5 Hz
    /// Middle C                    : C4     =  261.6 Hz
    /// Standard frequency reference: A4     =  440.0 Hz
    /// Second highest note on piano: B7     = 3951.1 Hz
    /// Largest freq_x16 value      : C8-37% = 4095.9375 Hz
    /// Highest note on piano       : C8     = 4186.0 Hz
    ///
    /// [Note Names, MIDI Numbers and frequencies](https://newt.phys.unsw.edu.au/jw/notes.html)
    ///
    /// Warning: This function does a potentially expensive division operation.
    /// Do not call it from an interrupt routine.
    #[inline]
    pub const fn freq_x16_to_phase_vel(freq_x16: u16) -> u16 {
        WaveParams::<SAMPLE_RATE>::freq_x16_to_phase_vel(freq_x16)
    }

    /// Create a new wave generator turned off, minimum frequency, zero amplitude.
    pub const fn new() -> Self {
        Self {
            wave_params: WaveParams::<SAMPLE_RATE>::new(),
            phase: 0,
        }
    }

    /// Copy the waveform parameters from wave_params to this wave generator.
    pub fn set_wave_params(&mut self, wave_params: &WaveParams<SAMPLE_RATE>) {
        self.wave_params = *wave_params;
    }

    /// Create a new wave generator set to the given frequency and amplitude.
    /// ``waveform`` is the waveform type.
    /// ``freq_x16`` is the frequency multiplied by 16 (4 fractional bits)
    /// with a lower limit of 32 Hz.
    /// ``amp`` is the waveform amplitude in range 0..=16384
    pub const fn from_freq_vol(
        waveform: Waveform,
        freq_x16: u16,
        amp: u16,
    ) -> Self {
        let freq_x16 = if freq_x16 < Self::MIN_FREQ * 16 {
            Self::MIN_FREQ * 16
        } else {
            freq_x16
        };
        let phase_vel = Self::freq_x16_to_phase_vel(freq_x16);
        let amp = if amp <= Self::MAX_AMPLITUDE {
            amp
        } else {
            Self::MAX_AMPLITUDE
        };
        Self {
            wave_params: WaveParams::<SAMPLE_RATE> {
                waveform,
                phase_vel,
                amp,
            },
            phase: 0,
        }
    }

    /// Update the phase - called once per sample!
    #[inline]
    fn update(&mut self) {
        self.phase = self.phase.wrapping_add(self.wave_params.phase_vel);
    }

    /// Return the current waveform sample and update the generator.
    /// The returned sample value is an integer in the range: -32767..=32767
    pub fn next_sample(&mut self) -> i16 {
        let phase = self.phase;

        // The 2 most significant bits of phase are the quadrant.
        // The next 14 bits are the index (sample number inside the quadrant).
        let quadrant = ((phase & 0b1100000000000000) >> 14) as u8;
        let index = phase & 0b0011111111111111;

        let y: i16 = match self.wave_params.waveform {
            Waveform::Off => 0,
            Waveform::SquareWave => {
                if (quadrant & 0b10) == 0 {
                    32767
                } else {
                    -32767
                }
            }
            Waveform::TriangleWave => {
                // Calculate sample value y based on quadrant and index.
                // Yes I have noticed that due to binary math this isn't perfect:
                // The highest value is 32766, not quite making it to 32767,
                // and the lowest value is -32766, not quite making it to -32767.
                match quadrant {
                    0 => (index as i16) << 1,
                    1 => ((16383 - index) as i16) << 1,
                    2 => -((index as i16) << 1),
                    _ => -(((16383 - index) as i16) << 1),
                }
            }
            Waveform::SineWave => {
                // ``angle`` is in range 0..=16384, representing 0..=90 degrees.
                // ``negate`` is true iff the sine of the angle should be
                // multiplied by -1 to get the true final value.
                let (angle, negate) = match quadrant {
                    0 => (index, false),
                    1 => (16384 - index, false),
                    2 => (index, true),
                    _ => (16384 - index, true),
                };
                // The lookup table only has 2**TABLE_BITS entries, so the
                // angle must be divided by 2**(14 - TABLE_BITS) to get the
                // lookup table index.
                let table_index = angle >> (14 - TABLE_BITS);
                if table_index == 0 {
                    0
                } else {
                    // The sine lookup table omits the trivial value zero,
                    // so the index must be adjusted down by one before use.
                    let y = SINE_LOOKUP[(table_index - 1) as usize];
                    if negate {
                        -y
                    } else {
                        y
                    }
                }
            }
            Waveform::HyperSquare => {
                // If the phase velocity is an even number, output zeros.
                // Otherwise, output the maximum possible square wave frequency.
                if self.wave_params.phase_vel & 0x1 == 0 {
                    0
                } else {
                    if self.phase & 0x1 == 1 {
                        32767
                    } else {
                        -32767
                    }
                }
            }
        };

        self.update();

        // Return the sample value scaled by the current amplitude.
        self.wave_params.scale_sample(y)
    }

    pub fn set_waveform(&mut self, waveform: Waveform) {
        self.wave_params.waveform = waveform;
    }

    pub fn waveform(&self) -> Waveform {
        self.wave_params.waveform
    }

    pub fn set_phase(&mut self, phase: u16) {
        self.phase = phase;
    }

    pub fn phase(&self) -> u16 {
        self.phase
    }

    /// Set frequency of wave generator in Hz.
    /// freq_x16: frequency in Hz * 16 (fixed point with 4 fractional bits)
    /// Does not allow setting a pitch lower than MIN_FREQ.
    /// Warning: Calls freq_x16_to_phase_vel() which does a division operation.
    pub fn set_pitch(&mut self, freq_x16: u16) {
        let freq_x16 = if freq_x16 < Self::MIN_FREQ * 16 {
            Self::MIN_FREQ * 16
        } else {
            freq_x16
        };
        self.wave_params.phase_vel = Self::freq_x16_to_phase_vel(freq_x16);
    }

    /// Set the phase velocity (frequency) in phase units per sample.
    /// There are PHASE_UNITS_PER_CYCLE (65536) phase units per cycle
    /// and SAMPLE_RATE samples per second.
    ///
    /// If you attempt to set a value lower than MIN_PHASE_VEL, it is clamped
    /// to MIN_PHASE_VEL.
    ///
    /// To calculate phase velocity from frequency in cycles per second,
    /// see ``freq_x16_to_phase_vel()``.
    pub fn set_phase_vel(&mut self, vel: u16) {
        self.wave_params.set_phase_vel(vel)
    }

    pub fn phase_vel(&self) -> u16 {
        self.wave_params.phase_vel()
    }

    /// amp: waveform amplitude in range 0..=MAX_AMPLITUDE (16384)
    /// If you attempt to set a value higher than MAX_AMPLITUDE it is clamped to
    /// MAX_AMPLITUDE.
    pub fn set_amp(&mut self, amp: u16) {
        self.wave_params.set_amp(amp)
    }

    pub fn amp(&self) -> u16 {
        self.wave_params.amp()
    }

    /// Return the name of the currently activated waveform
    pub fn name(&self) -> &'static str {
        self.wave_params.waveform().name()
    }

    /// Return true iff the waveform is Off (outputs are all zeros)
    pub fn is_off(&self) -> bool {
        self.wave_params.waveform().is_off()
    }
}

/// Lookup table of sine values for angles from pi/(2**(TABLE_BITS+1)) through
/// pi/2 inclusive, scaled by 32767.
const TABLE_BITS: usize = 8;
const TABLE_SIZE: usize = 1 << TABLE_BITS;
#[rustfmt::skip]
static SINE_LOOKUP: [i16; TABLE_SIZE] = [
      201,   402,   603,   804,  1005,  1206,  1407,  1608,
     1809,  2009,  2210,  2410,  2611,  2811,  3012,  3212,
     3412,  3612,  3811,  4011,  4210,  4410,  4609,  4808,
     5007,  5205,  5404,  5602,  5800,  5998,  6195,  6393,
     6590,  6786,  6983,  7179,  7375,  7571,  7767,  7962,
     8157,  8351,  8545,  8739,  8933,  9126,  9319,  9512,
     9704,  9896, 10087, 10278, 10469, 10659, 10849, 11039,
    11228, 11417, 11605, 11793, 11980, 12167, 12353, 12539,
    12725, 12910, 13094, 13279, 13462, 13645, 13828, 14010,
    14191, 14372, 14553, 14732, 14912, 15090, 15269, 15446,
    15623, 15800, 15976, 16151, 16325, 16499, 16673, 16846,
    17018, 17189, 17360, 17530, 17700, 17869, 18037, 18204,
    18371, 18537, 18703, 18868, 19032, 19195, 19357, 19519,
    19680, 19841, 20000, 20159, 20317, 20475, 20631, 20787,
    20942, 21096, 21250, 21403, 21554, 21705, 21856, 22005,
    22154, 22301, 22448, 22594, 22739, 22884, 23027, 23170,
    23311, 23452, 23592, 23731, 23870, 24007, 24143, 24279,
    24413, 24547, 24680, 24811, 24942, 25072, 25201, 25329,
    25456, 25582, 25708, 25832, 25955, 26077, 26198, 26319,
    26438, 26556, 26674, 26790, 26905, 27019, 27133, 27245,
    27356, 27466, 27575, 27683, 27790, 27896, 28001, 28105,
    28208, 28310, 28411, 28510, 28609, 28706, 28803, 28898,
    28992, 29085, 29177, 29268, 29358, 29447, 29534, 29621,
    29706, 29791, 29874, 29956, 30037, 30117, 30195, 30273,
    30349, 30424, 30498, 30571, 30643, 30714, 30783, 30852,
    30919, 30985, 31050, 31113, 31176, 31237, 31297, 31356,
    31414, 31470, 31526, 31580, 31633, 31685, 31736, 31785,
    31833, 31880, 31926, 31971, 32014, 32057, 32098, 32137,
    32176, 32213, 32250, 32285, 32318, 32351, 32382, 32412,
    32441, 32469, 32495, 32521, 32545, 32567, 32589, 32609,
    32628, 32646, 32663, 32678, 32692, 32705, 32717, 32728,
    32737, 32745, 32752, 32757, 32761, 32765, 32766, 32767,
];

#[derive(Copy, Clone, Debug)]
#[non_exhaustive]
pub enum Waveform {
    Off,
    SquareWave,
    TriangleWave,
    SineWave,
    /// If the phase velocity (frequency * 16) is an even number, output zeros.
    /// Otherwise, output the maximum possible square wave frequency.
    HyperSquare,
}

impl Waveform {
    pub fn name(&self) -> &'static str {
        match self {
            Self::Off => "Off",
            Self::SquareWave => "SquareWave",
            Self::TriangleWave => "TriangleWave",
            Self::SineWave => "SineWave",
            Self::HyperSquare => "HyperSquare",
        }
    }

    pub fn is_off(&self) -> bool {
        match self {
            Self::Off => true,
            _ => false,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    const SAMPLE_RATE: u16 = 10000;
    type WaveGen = WaveGenerator<SAMPLE_RATE>;

    #[test]
    fn construct_off_wavegen() {
        let gen = WaveGen::new();
        assert!(gen.is_off());
    }

    #[test]
    fn construct_sinegen() {
        let freq = 220;
        let amp = 8192;
        let gen = WaveGen::from_freq_vol(Waveform::SineWave, freq * 16, amp);
        assert!(!gen.is_off());
    }

    #[test]
    fn construct_default() {
        let gen = WaveGen::default();
        assert!(gen.is_off());
    }

    #[test]
    fn check_f440_accuracy() {
        const PI: f64 = core::f64::consts::PI;
        const NUM_TEST_CYCLES: f64 = 2.0;
        const TOLERABLE_ERR: u16 = (32767.0 * 0.0075) as u16; // 0.75% error
        let freq = 440;
        let amp = WaveGen::MAX_AMPLITUDE;
        let mut gen =
            WaveGen::from_freq_vol(Waveform::SineWave, freq * 16, amp);

        let mut phi: f64 = 0.0;
        let delta_phi = 2.0 * PI * freq as f64 / SAMPLE_RATE as f64;
        let mut max_err: u16 = 0;
        while phi < 2.0 * PI * NUM_TEST_CYCLES {
            let y = phi.sin(); // sine value in range -1.0..=1.0
            let expected_sample = (y * 32767.0) as i16; // range -32767..=32767
            let actual_sample = gen.next_sample();
            let err = if actual_sample > expected_sample {
                (actual_sample - expected_sample) as u16
            } else {
                (expected_sample - actual_sample) as u16
            };
            if err > max_err {
                max_err = err
            };
            phi += delta_phi;
        }
        println!("check_f440_accuracy: max_err = {max_err}");
        assert!(max_err < TOLERABLE_ERR);
    }

    #[test]
    fn two_tone_crash() {
        // This is the scenario that crashed pico-two-tone.rs
        // when this module had a particular bug.
        const SAMPLE_RATE: u16 = 22050;
        type WaveGen = WaveGenerator<SAMPLE_RATE>;

        let mut gen1 = WaveGen::new();
        gen1.set_waveform(Waveform::SineWave);
        gen1.set_phase(32831);
        gen1.set_phase_vel(653);
        gen1.set_amp(8192);

        let mut gen2 = WaveGen::new();
        gen2.set_waveform(Waveform::SineWave);
        gen2.set_phase(377);
        gen2.set_phase_vel(1307);
        gen2.set_amp(8192);

        println!("Just before the crash is provoked:");
        println!("gen1: {:?}", gen1);
        println!("gen2: {:?}", gen2);

        let sample1 = gen1.next_sample();
        let sample2 = gen2.next_sample();

        println!("Right after crash scenario:");
        println!("sample1={} sample2={}", sample1, sample2);
        println!("gen1: {:?}", gen1);
        println!("gen2: {:?}", gen2);
    }
}
