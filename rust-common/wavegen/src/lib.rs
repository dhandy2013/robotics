#![cfg_attr(not(test), no_std)]

#[cfg(feature = "std")]
extern crate std;

pub mod wavegen16;
pub mod wavegen8;
