//! Synchronized access to shared global data
use core::cell::{BorrowMutError, RefCell};
use critical_section::{self, CriticalSection, Mutex};

#[derive(Debug)]
pub enum Error {
    AlreadyBorrowed,
    NotInitialized,
}

impl From<BorrowMutError> for Error {
    fn from(_value: BorrowMutError) -> Self {
        Error::AlreadyBorrowed
    }
}

/// Get exclusive access to a shared static data struct and run `f` on it.
/// Return:
/// - Err(AlreadyBorrowed) if the shared static data has already been borrowed
///   e.g. if this function was called recursively,
/// - Err(NotInitialized) if the shared static data is still None instead of
///   Some,
/// - Ok(result_of_closure_f) otherwise.
///
/// Example of how to initialize and use a shared static data struct:
/// ```
/// struct Shared { counter: usize, };
/// static SHARED: Mutex<RefCell<Option<Shared>>> = Mutex::new(RefCell::new(None));
///
/// critical_section::with(move |cs| {
///     let mut maybe_shared = SHARED.borrow_ref_mut(cs);
///     maybe_shared.replace(Shared { counter: 0 });
/// });
///
/// with_shared_mut(&SHARED, |shared: &mut Shared| {
///     shared.counter += 1;
/// }).unwrap();
///
/// let current_count = with_shared_mut(&SHARED, |shared| shared.counter)?;
/// ```
pub fn with_shared_mut<F, R, S>(
    shared_mutex: &'static Mutex<RefCell<Option<S>>>,
    f: F,
) -> Result<R, Error>
where
    F: FnOnce(&mut S) -> R,
    S: Send,
{
    critical_section::with(|cs| {
        let mut maybe_unlocked_shared =
            shared_mutex.borrow(cs).try_borrow_mut()?;
        match maybe_unlocked_shared.as_mut() {
            Some(shared) => Ok(f(shared)),
            None => Err(Error::NotInitialized),
        }
    })
}

/// A container for a value that can be checked out, checked in, or replaced.
/// The value can be Some(T) or None. When the value is checked out the
/// container is Busy, otherwise it is available.
///
/// This is intended to be used for synchronization:
/// ```
/// struct Shared {}
/// static SHARED: Mutex<RefCell<BusyBox<Shared>>> =
///     Mutex::new(RefCell::new(BusyBox::new()));
/// ```
pub enum BusyBox<T> {
    Busy,
    Avail(Option<T>),
}

impl<T> BusyBox<T> {
    pub const fn new() -> Self {
        Self::Avail(None)
    }

    /// Return true iff the BusyBox is currently busy (checked out)
    pub fn is_busy(&self) -> bool {
        match self {
            Self::Busy => true,
            Self::Avail(_) => false,
        }
    }

    /// Return true iff the BusyBox is currently available (checked in)
    pub fn is_avail(&self) -> bool {
        match self {
            Self::Busy => false,
            Self::Avail(_) => true,
        }
    }

    /// Try to remove the available value/none and replace it with Busy.
    /// If the BusyBox was not Busy, return the previous value or none.
    /// If the BusyBox was already Busy, return Err(()) instead.
    pub fn try_checkout(&mut self) -> Result<Option<T>, ()> {
        match self {
            Self::Busy => return Err(()),
            Self::Avail(previous_value) => {
                let result = previous_value.take();
                *self = Self::Busy;
                Ok(result)
            }
        }
    }

    /// Spin until the BusyBox is no longer busy, then check it out.
    pub fn spin_checkout(&mut self) -> Option<T> {
        loop {
            match self.try_checkout() {
                Ok(maybe_value) => break maybe_value,
                Err(_) => {}
            }
        }
    }

    /// Try to replace the Busy value with a new value or none.
    /// If the BusyBox was not Busy, return Err(value).
    pub fn try_checkin(&mut self, value: Option<T>) -> Result<(), Option<T>> {
        match self {
            Self::Busy => {
                *self = Self::Avail(value);
                Ok(())
            }
            Self::Avail(_) => Err(value),
        }
    }

    /// Try to replace the previous value with a new one.
    /// If the BusyBox was not Busy, return Ok(previous_value).
    /// If the BusyBox was Busy, return Err(value).
    pub fn try_replace(
        &mut self,
        value: Option<T>,
    ) -> Result<Option<T>, Option<T>> {
        match self {
            Self::Busy => Err(value),
            Self::Avail(previous_value) => {
                let result = previous_value.take();
                *self = Self::Avail(value);
                Ok(result)
            }
        }
    }
}

/// Try to remove the available value/none and replace it with Busy.
/// If the BusyBox was not Busy, return the previous value or none.
/// If the BusyBox was already Busy, return Err(()) instead.
pub fn try_checkout_bb<T>(
    cs: CriticalSection,
    bb: &Mutex<RefCell<BusyBox<T>>>,
) -> Result<Option<T>, ()> {
    bb.borrow(cs).borrow_mut().try_checkout()
}

/// Try to replace the Busy value with a new value or none.
/// If the BusyBox was not Busy, return Err(value).
pub fn try_checkin_bb<T>(
    cs: CriticalSection,
    bb: &Mutex<RefCell<BusyBox<T>>>,
    value: Option<T>,
) -> Result<(), Option<T>> {
    bb.borrow(cs).borrow_mut().try_checkin(value)
}

/// Try to replace the previous value with a new one.
/// If the BusyBox was not Busy, return Ok(previous_value).
/// If the BusyBox was Busy, return Err(value).
pub fn try_replace_bb<T>(
    cs: CriticalSection,
    bb: &Mutex<RefCell<BusyBox<T>>>,
    value: Option<T>,
) -> Result<Option<T>, Option<T>> {
    bb.borrow(cs).borrow_mut().try_replace(value)
}

/// Return true iff the BusyBox is currently busy (checked out)
pub fn bb_is_busy<T>(
    cs: CriticalSection,
    bb: &Mutex<RefCell<BusyBox<T>>>,
) -> bool {
    bb.borrow(cs).borrow().is_busy()
}

/// Return true iff the BusyBox is currently available (checked in)
pub fn bb_is_avail<T>(
    cs: CriticalSection,
    bb: &Mutex<RefCell<BusyBox<T>>>,
) -> bool {
    bb.borrow(cs).borrow().is_avail()
}

/// Run a closure on a mutable reference to the contents of a BusyBox, if the
/// BusyBox is available and non-None.
///
/// If the BusyBox is available and non-None, return Ok(R) (the result of the
/// closure.)
///
/// If the BusyBox is busy, return Err(Error::AlreadyBorrowed).
///
/// If the BusyBox is available but empty (None), return
/// Err(Error::NotInitialized).
pub fn with_bb_mut<T, F, R>(
    cs: CriticalSection,
    bb: &Mutex<RefCell<BusyBox<T>>>,
    f: F,
) -> Result<R, Error>
where
    F: FnOnce(&mut T) -> R,
{
    if let Ok(mut maybe_value) = try_checkout_bb(cs, bb) {
        let result = match maybe_value.as_mut() {
            Some(value) => Ok(f(value)),
            None => Err(Error::NotInitialized),
        };
        try_checkin_bb(cs, bb, maybe_value).ok().unwrap();
        result
    } else {
        Err(Error::AlreadyBorrowed)
    }
}
