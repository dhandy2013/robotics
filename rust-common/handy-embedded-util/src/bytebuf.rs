//! A fixed-size byte buffer for handling I/O without reallocating

use core::ops::Range;

/// A byte buffer designed for low-level I/O, implemented as a ring buffer.
///
/// SIZE is the maximum number of bytes that can be stored in the buffer.
/// SIZE should be at least 1 or append() will trigger an assertion.
/// I would have used a compile-time assertion but those don't work well
/// with generic consts.
/// ``start`` is always in range 0..SIZE and is the read position.
/// ``end`` is always in range 0..SIZE and is the write position.
/// If start and end are the same, then the buffer is either empty or full.
/// The ``full`` flag distinguishes between those two cases.
pub struct ByteBuffer<const SIZE: usize> {
    buf: [u8; SIZE],
    start: usize,
    end: usize,
    full: bool,
}

impl<const SIZE: usize> ByteBuffer<SIZE> {
    /// Create a new, initialized ByteBuffer.
    pub const fn new() -> Self {
        Self {
            buf: [0u8; SIZE],
            start: 0,
            end: 0,
            full: false,
        }
    }

    /// Attempt to append slice ``b`` of bytes to the buffer.
    /// Return the number of bytes actually appended.
    /// If zero is returned, the buffer was full.
    /// If ``b`` is not empty but SIZE is zero then an assertion is triggered.
    /// This assertion is intended to avoid infinite loops in the calling code.
    pub fn append(&mut self, mut b: &[u8]) -> usize {
        assert!(SIZE > 0 || b.len() == 0);
        let (range1, range2) = self.empty_ranges();
        let start_count = b.len();
        if !range1.is_empty() {
            let n = usize::min(range1.end - range1.start, b.len());
            self.buf[range1][..n].copy_from_slice(&b[..n]);
            b = &b[n..];
        }
        if !range2.is_empty() {
            let n = usize::min(range2.end - range2.start, b.len());
            self.buf[range2][..n].copy_from_slice(&b[..n]);
            b = &b[n..];
        }
        let n = start_count - b.len();
        assert!(n <= SIZE);
        self.end += n;
        if self.end >= SIZE {
            self.end -= SIZE;
        }
        if self.start == self.end && n > 0 {
            self.full = true;
        }
        n
    }

    /// Call this immediately after remove() to "put back" the values in the
    /// buffer that had just been taken out, without having to copy them.
    ///
    /// ``n`` is the number of bytes to put back, starting with the last bytes
    /// removed and working backwards. ``n`` should be less than or equal to the
    /// value returned by remove(), otherwise you will get strange results with
    /// extra bytes being added back to the buffer.
    ///
    /// This method is helpful when bytes were removed to be sent to some other
    /// system, but the other system didn't accept all the bytes, so you need to
    /// retry again later.
    ///
    /// Return the number of bytes actually put back into the buffer. If n is
    /// greater than the amount of space available then the returned value will
    /// be less than n.
    pub fn putback(&mut self, n: usize) -> usize {
        let starting_space = self.empty_space();
        let n = usize::min(n, starting_space);
        self.start += SIZE - n;
        if self.start >= SIZE {
            self.start -= SIZE;
        }
        if n == starting_space {
            assert_eq!(self.start, self.end);
            if SIZE > 0 {
                self.full = true;
            }
        }
        n
    }

    /// Return two index ranges representing two spaces into which new bytes
    /// could be appended into the buffer, in order. Here are the possible
    /// return values:
    /// 0. (0..0, 0..0) // buffer is full
    /// 1. (end..SIZE, 0..0)
    /// 2. (end..SIZE, 0..start)
    /// 3. (end..start, 0..0)
    #[inline]
    fn empty_ranges(&self) -> (Range<usize>, Range<usize>) {
        if self.full {
            assert_eq!(self.start, self.end);
            return (0..0, 0..0);
        }
        match (self.start, self.end) {
            (0, end) => (end..SIZE, 0..0),
            (start, end) if end >= start => (end..SIZE, 0..start),
            (start, end) if end < start => (end..start, 0..0),
            (_, _) => {
                unreachable!()
            }
        }
    }

    /// Remove up to buf.len() bytes from the ByteBuffer and place them in buf.
    /// Return the number of bytes removed.
    /// If buf.len() > 0 and the returned value is zero, the buffer was empty.
    pub fn remove(&mut self, mut buf: &mut [u8]) -> usize {
        let (range1, range2) = self.avail_ranges();
        let start_count = buf.len();
        if !buf.is_empty() {
            let n = usize::min(range1.end - range1.start, buf.len());
            buf[..n].copy_from_slice(&self.buf[range1][..n]);
            buf = &mut buf[n..];
        } else {
            return 0;
        }
        if !buf.is_empty() {
            let n = usize::min(range2.end - range2.start, buf.len());
            buf[..n].copy_from_slice(&self.buf[range2][..n]);
            buf = &mut buf[n..];
        }
        let n = start_count - buf.len();
        assert!(n <= SIZE);
        self.start += n;
        if self.start >= SIZE {
            self.start -= SIZE;
        }
        if self.start != self.end || n > 0 {
            self.full = false;
        }
        n
    }

    /// Return two index ranges representing two continuous slices of bytes
    /// available for removal from the ByteBuffer. The possible return values
    /// are:
    /// 0. (0..0, 0..0) // empty
    /// 1. (start..end, 0..0)
    /// 2. (start..SIZE, 0..end)
    /// If both returned ranges end up empty then the ByteBuffer is empty.
    #[inline]
    fn avail_ranges(&self) -> (Range<usize>, Range<usize>) {
        if self.start == self.end {
            if !self.full {
                (0..0, 0..0)
            } else {
                (self.start..SIZE, 0..self.end)
            }
        } else {
            match (self.start, self.end) {
                (start, end) if start <= end => (start..end, 0..0),
                (start, end) if start > end => (start..SIZE, 0..end),
                (_, _) => {
                    unreachable!()
                }
            }
        }
    }

    /// Return true iff the ByteBuffer is empty
    pub fn is_empty(&self) -> bool {
        self.start == self.end && !self.full
    }

    /// Return true iff the ByteBuffer is full
    pub fn is_full(&self) -> bool {
        if self.full {
            assert_eq!(self.start, self.end);
            true
        } else {
            false
        }
    }

    /// Return the maximum number of bytes that can be stored in this ByteBuffer
    pub const fn max_bytes() -> usize {
        SIZE
    }

    /// Return the number of bytes currently in the buffer
    pub fn len(&self) -> usize {
        let (range1, range2) = self.avail_ranges();
        (range1.end - range1.start) + (range2.end - range2.start)
    }

    /// Return the number of bytes that could be successfully appended.
    pub fn empty_space(&self) -> usize {
        let (range1, range2) = self.empty_ranges();
        (range1.end - range1.start) + (range2.end - range2.start)
    }
}

impl<const SIZE: usize> Default for ByteBuffer<SIZE> {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn construct_bytebuf() {
        type ByteBuf = ByteBuffer<42>;
        let bb = ByteBuf::new();
        assert!(bb.is_empty());
        assert!(!bb.is_full());
        assert!(ByteBuf::max_bytes() == 42);
        assert!(bb.len() == 0);
        assert!(bb.empty_space() == 42);
    }

    #[test]
    #[should_panic]
    fn construct_bytebuf_toosmall() {
        // You can construct a zero-sized byte buffer...
        type ByteBuf = ByteBuffer<0>;
        let mut bb = ByteBuf::new();
        // ...but appending anything to it will immediately panic.
        bb.append(b"A");
    }

    #[test]
    fn append() {
        type ByteBuf = ByteBuffer<3>;
        let mut bb = ByteBuf::new();

        // Append empty byte slice
        let n = bb.append(b"");
        assert!(n == 0);
        assert!(bb.is_empty());
        assert!(!bb.is_full());
        assert!(bb.len() == 0);
        assert!(bb.empty_space() == 3);

        // Append one byte
        let n = bb.append(b"A");
        assert!(n == 1);
        assert!(!bb.is_empty());
        assert!(!bb.is_full());
        assert!(bb.len() == 1);
        assert!(bb.empty_space() == 2);

        // Append two more bytes, filling the buffer
        let n = bb.append(b"BC");
        assert!(n == 2);
        assert!(!bb.is_empty());
        assert!(bb.is_full());
        assert!(bb.len() == 3);
        assert!(bb.empty_space() == 0);

        // Attempt to append one more byte to full buffer.
        let n = bb.append(b"D");
        assert!(n == 0);
        assert!(!bb.is_empty());
        assert!(bb.is_full());
        assert!(bb.len() == 3);
        assert!(bb.empty_space() == 0);
    }

    #[test]
    fn remove() {
        type ByteBuf = ByteBuffer<3>;
        let mut bb = ByteBuf::new();

        // Attempt to append 4 bytes (only three should make it in)
        let n = bb.append(b"ABCD");
        assert!(n == 3);
        assert!(bb.len() == 3);

        let mut buf = [0u8; 4];

        // Remove one byte
        let n = bb.remove(&mut buf[..1]);
        assert!(n == 1);
        assert!(bb.len() == 2);
        assert!(&buf[..n] == b"A");

        // Remove two bytes
        let n = bb.remove(&mut buf[..2]);
        assert!(n == 2);
        assert!(bb.is_empty());
        assert!(bb.len() == 0);
        assert!(&buf[..n] == b"BC");

        // Attempt to remove three bytes, none should come out
        let n = bb.remove(&mut buf[..3]);
        assert!(n == 0);
        assert!(bb.len() == 0);
        assert!(bb.empty_space() == 3);
    }

    #[test]
    fn append_remove() {
        type ByteBuf = ByteBuffer<3>;
        let mut bb = ByteBuf::new();

        // Append two bytes
        let n = bb.append(b"AB");
        assert!(n == 2);
        assert!(bb.len() == 2);
        assert!(bb.empty_space() == 1);

        let mut buf = [0u8; 4];

        // Remove one byte
        let n = bb.remove(&mut buf[..1]);
        assert!(n == 1);
        assert!(&buf[..n] == b"A");
        assert!(!bb.is_full());
        assert!(!bb.is_empty());
        assert!(bb.len() == 1);
        assert!(bb.empty_space() == 2);

        // Append two more bytes, filling the buffer
        let n = bb.append(b"CD");
        assert!(n == 2);
        assert!(bb.is_full());
        assert!(!bb.is_empty());
        assert!(bb.len() == 3);
        assert!(bb.empty_space() == 0);

        // Remove one byte again
        let n = bb.remove(&mut buf[..1]);
        assert!(n == 1);
        assert!(&buf[..n] == b"B");
        assert!(!bb.is_full());
        assert!(!bb.is_empty());
        assert!(bb.len() == 2);
        assert!(bb.empty_space() == 1);

        // Attempt to append two more byte again, only one gets in
        let n = bb.append(b"Ef");
        assert!(n == 1);
        assert!(bb.len() == 3);
        assert!(bb.empty_space() == 0);
        assert!(!bb.is_empty());
        assert!(bb.is_full());

        // Attempt to remove 4 bytes, only three (all three) will come out
        let n = bb.remove(&mut buf[..]);
        assert!(n == 3);
        assert!(bb.len() == 0);
        assert!(bb.empty_space() == 3);
        assert!(bb.is_empty());
        assert!(!bb.is_full());
        assert!(&buf[..n] == b"CDE");

        // Attempt to append 5 bytes, only 3 will get in
        let n = bb.append(b"GHIjk");
        assert!(n == 3);
        assert!(bb.len() == 3);
        assert!(bb.empty_space() == 0);
        assert!(!bb.is_empty());
        assert!(bb.is_full());

        // Attempt again to remove 4 bytes, only three (all three) will come out
        let n = bb.remove(&mut buf[..]);
        assert!(n == 3);
        assert!(bb.len() == 0);
        assert!(bb.empty_space() == 3);
        assert!(bb.is_empty());
        assert!(!bb.is_full());
        assert!(&buf[..n] == b"GHI");
    }

    #[test]
    fn putback() {
        type ByteBuf = ByteBuffer<8>;
        let mut bb = ByteBuf::new();

        let mut buf = [0u8; 8];

        // Test putback() with no buffer wraparound
        bb.append(b"ABCDEFGH"); // append 8 bytes
        let n = bb.remove(&mut buf[..5]); // remove 5 bytes
        assert_eq!(&buf[..n], b"ABCDE");
        assert_eq!(bb.len(), 3); // 3 bytes left in buffer
        bb.putback(2); // put back last 2 bytes removed
        assert_eq!(bb.len(), 5); // now 5 in buffer
        let n = bb.remove(&mut buf[..4]); // remove 4 of those 5
        assert_eq!(&buf[0..n], b"DEFG");
        assert_eq!(bb.len(), 1); // 1 byte left in buffer
        assert_eq!(bb.empty_space(), 7);

        // Try again with buffer wraparound
        bb.append(b"IJKLMNO");
        let n = bb.remove(&mut buf[..4]);
        assert_eq!(&buf[..n], b"HIJK");
        bb.putback(3);
        let n = bb.remove(&mut buf[..5]);
        assert_eq!(&buf[..n], b"IJKLM");
    }

    #[test]
    fn putback_toomuch() {
        type ByteBuf = ByteBuffer<8>;
        let mut bb = ByteBuf::new();

        let mut buf = [0u8; 8];

        bb.append(b"ABCDEFGH"); // append 8 bytes
        bb.remove(&mut buf[..5]); // remove 5 bytes
        assert_eq!(bb.len(), 3); // 3 bytes left in buffer
        assert_eq!(bb.empty_space(), 5); // room for 5 more bytes
        let n = bb.putback(6); // attempt to put back more than is possible
        assert_eq!(n, 5); // only 5 bytes were put back
        assert!(bb.is_full()); // and now the buffer is full.
    }
}
