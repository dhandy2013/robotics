//! Utility modules that can work in a no-std (embedded) environment
#![cfg_attr(not(test), no_std)]

pub mod bytebuf;
pub mod sync;
