robotics project
================

Projects related to robotics and microcontrollers
See the README file in each sub-directory for more info.

Sub-directories:

arduino             Code to run on the Arduino UNO

circuit-playground  Circuit Playground Express and related hardware/software

lego-mindstorm      Info about programming/using the LEGO mindstorm RCX 2.0

rp2040              Code for microcontrollers based on the RP2040 chip

rpi-projects        Rasberry PI single-board computer projects
                    Includes code for the RASC 2014 Elevator Project

rust-common         Rust crates common to more than one microcontroller

scripts             Useful Python scripts
