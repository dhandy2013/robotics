#!/usr/bin/env python3
"""
Clockwatch - read time signal on serial port and check its accuracy.

The serial port may need to be configured first. One way to do that:
``picocom -b 115200 /dev/ttyUSB0`` then Ctrl-A Ctrl-Q to disconnect but
keep serial port configured.
"""
import argparse
import csv
import math
import sys
import time

import matplotlib.pyplot as plt


class EOF(Exception):
    pass


def read_clock(f):
    """
    Read the next time value from the serial port
    Return the integer number of seconds,
    or None if the data read was not a valid integer.
    Raise EOF if the serial port is disconnected.
    """
    raw_data = f.readline()
    if not raw_data:
        raise EOF("Serial port disconnected")
    data = raw_data.rstrip().decode('utf-8')
    try:
        return int(data)
    except ValueError:
        return None


def wait_for_t0(f):
    print("Waiting for t=0 clock signal on tty")
    while read_clock(f) != 0:
        pass
    return time.time()


def run_calibration_loop(args, f, t0):
    print("Running calibration loop")
    prev_pc_t = 0.0
    samples = []
    try:
        while True:
            ext_t = read_clock(f)
            pc_t = time.time() - t0
            ext_delta = 1.0
            pc_delta = pc_t - prev_pc_t
            prev_pc_t = pc_t
            clock_err = ext_delta - pc_delta
            samples.append((ext_t, pc_t, clock_err))
            print(
                f"#{len(samples):04d}"
                f"  PC clock: {pc_t:11.6f}"
                f"  Ext clock err: {clock_err:.6f}"
            )
    except KeyboardInterrupt:
        pass
    if not samples:
        return []
    print("Number of samples:", len(samples))
    n = len(samples)
    err_values = [x[-1] for x in samples]
    ave = sum(err_values) / n
    print("Min:", min(err_values))
    print("Ave:", ave)
    print("Max:", max(err_values))
    if n < 2:
        return samples
    std_dev = math.sqrt(
        (1.0 / (n - 1.0)) * sum((x - ave)**2 for x in err_values)
    )
    print("Standard deviation:", std_dev)
    return samples


def graph_samples(samples):
    err_values = [x[-1] for x in samples]
    plt.plot(err_values)
    plt.xlabel('Samples')
    plt.ylabel('Clock Err')
    plt.show()


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--ttyfile', default="/dev/ttyUSB0")
    parser.add_argument('--csvout', default="output.csv")
    args = parser.parse_args()
    with open(args.ttyfile, 'rb') as f:
        try:
            t0 = wait_for_t0(f)
        except EOF as eof:
            print(eof)
            return
        except KeyboardInterrupt:
            print("\nStop")
            return
        try:
            samples = run_calibration_loop(args, f, t0)
        except EOF as eof:
            print(eof)
            return
    print("Calibration finished, saving data to", args.csvout)
    with open(args.csvout, 'w', newline='') as csv_fileobj:
        csv_writer = csv.writer(csv_fileobj)
        csv_writer.writerow(["Ext_Clock", "PC_Clock", "Ext_Clock_Err"])
        for sample in samples:
            csv_writer.writerow(sample)
    if len(samples) > 2:
        graph_samples(samples)


if __name__ == '__main__':
    sys.exit(main())
