# Program for Raspberry Pi Pico W
# Print the device's WiFi MAC address
import network
import ubinascii

wlan = network.WLAN(network.STA_IF)
wlan.active(True)
mac = ubinascii.hexlify(wlan.config('mac'),':').decode()
print(mac)
