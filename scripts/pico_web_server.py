# Super simple webserver running on Raspberry Pi Pico W
# that can turn on/off the LED and report temperature sensor value.
# Based on example code at:
# https://projects.raspberrypi.org/en/projects/get-started-pico-w/0
import network
import socket
from time import sleep
from picozero import pico_temp_sensor, pico_led
import machine

ssid = 'YOUR-WIFI-NETWORK-NAME'
password = 'YOUR-WIFI-NETWORK-PASSWORD'

def connect_to_wlan():
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    wlan.connect(ssid, password)
    while wlan.isconnected() == False:
        print(f'Connecting to WiFI access point {ssid}')
        sleep(1)
    ip_addr = wlan.ifconfig()[0]
    print(f"Connected with IP addr {ip_addr}")
    return ip_addr

def open_socket(ip_addr):
    address = (ip_addr, 80)
    connection = socket.socket()
    connection.bind(address)
    connection.listen(1)
    print(f"Listening on {address}")
    return connection

def webpage(temperature, state):
    html = f"""
<!DOCTYPE html>
<html>
<body>
<form action="./lighton">
<input type="submit" value="Light on" />
</form>
<form action="./lightoff">
<input type="submit" value="Light off" />
</form>
<p>LED is {state}</p>
<p>Temperature is {temperature}</p>
</body>
</html>
"""
    return html

def serve(connection):
    #Start a web server
    state = 'OFF'
    pico_led.off()
    temperature = 0
    while True:
        client = connection.accept()[0]
        request = client.recv(1024)
        request = str(request)
        print(request[:80])
        parts = request.split()
        path = '/'
        if len(parts) >= 2:
            path = parts[1]
        if path == '/lighton?':
            pico_led.on()
            state = 'ON'
        elif path == '/lightoff?':
            pico_led.off()
            state = 'OFF'
        temperature = pico_temp_sensor.temp
        html = webpage(temperature, state)
        client.send(html)
        client.close()

try:
    ip_addr = connect_to_wlan()
    connection = open_socket(ip_addr)
    serve(connection)
except KeyboardInterrupt:
    machine.reset()

print(ip_config)
