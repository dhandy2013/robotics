Lego Mindstorm RCX 2.0
----------------------

Programs to control my Lego Mindstorm RCX 2.0 programmable brick.

My project wiki (may not be visible to outside internet):
http://blackbeauty/wiki/cpif/david/HOWTO/Linux/LEGO


Files extracted from the MINDSTORMS® SDK2.5
    URL : http://www.philohome.com/sdk25/sdk25.htm

    Name: firm0328.lgo
    Desc: The RCX 2.0 brick firmware, extracted 

    Name: RCX2-LASM-byte-codes.pdf
    Desc: RCX 2.0 Firmware Command Overview


nqc smoke test:
...............................................................................
$ sudo apt-get install nqc
$ nqc /usr/share/doc/nqc/examples/test.nqc
$ ls
test.rcx
$ nqc -L test.rcx 

*** Task 0 = main, size: 26 bytes
000 pwr      ABC, 7                            13 07 02 07 
004 dir      ABC, Fwd                          e1 87 
006 sent     0, Switch                         32 00 01 
009 senm     0, Boolean                        42 00 20 
012 out      A, On                             21 81 
014 chkl     1 != Input(0), 14                 95 82 09 01 00 00 fa ff 
022 plays    0                                 51 00 
024 out      A, Off                            21 41 

Total size: 26 bytes
...............................................................................
