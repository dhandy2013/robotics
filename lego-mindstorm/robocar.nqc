// robocar.nqc
// David Handy  Thu 28 Nov 2013
// Program to control the LEGO RCX 2.0 robot brick, to turn it into a
// button-controlled robot car car.

// To compile:
// nqc robocar.nqc

// To load the program onto the LEGO RCX 2.0 brick (assuming legousbtower
// module loaded and /dev/usb/lego0 device set up):
// export RCX_PORT=usb
// nqc -d robocar.rcx

// Sensor inputs    Result
// 1  2  3         
// -  -  -          ---------------
// 0  0  0          Stop
// 0  0  1          Turn right
// 0  1  0          Straight backward
// 0  1  1          Forward right
// 1  0  0          Turn left
// 1  0  1          Straight forward
// 1  1  0          Forward left
// 1  1  1          Beep

int s1, s2, s3;

task main()
{
    // Starting beep, just for fun
	PlaySound(SOUND_CLICK);

    // Set sensors 1, 2 and 3 to be touch switches
	SetSensor(SENSOR_1, SENSOR_TOUCH);
	SetSensor(SENSOR_2, SENSOR_TOUCH);
	SetSensor(SENSOR_3, SENSOR_TOUCH);

    // Start with all motors off
    Off(OUT_A+OUT_B+OUT_C);

    while (true)
    {
        s1 = SENSOR_1;
        s2 = SENSOR_2;
        s3 = SENSOR_3;

        if (s1 == 0 && s2 == 0 && s3 == 0)
        {
            // 0  0  0          Stop
            Off(OUT_A + OUT_C);
            continue;
        }

        if (s1 == 0 && s2 == 0 && s3 == 1) 
        {
            // 0  0  1          Turn right
            OnFwd(OUT_A);
            OnRev(OUT_C);
            continue;
        }

        if (s1 == 0 && s2 == 1 && s3 == 0) 
        {
            // 0  1  0          Straight backward
            OnRev(OUT_A);
            OnRev(OUT_C);
            continue;
        }

        if (s1 == 0 && s2 == 1 && s3 == 1) 
        {
            // 0  1  1          Forward right
            OnFwd(OUT_A);
            Off(OUT_C);
            continue;
        }

        if (s1 == 1 && s2 == 0 && s3 == 0) 
        {
            // 1  0  0          Turn left
            OnRev(OUT_A);
            OnFwd(OUT_C);
            continue;
        }

        if (s1 == 1 && s2 == 0 && s3 == 1) 
        {
            // 1  0  1          Straight forward
            OnFwd(OUT_A);
            OnFwd(OUT_C);
            continue;
        }

        if (s1 == 1 && s2 == 1 && s3 == 0) 
        {
            // 1  1  0          Forward left
            Off(OUT_A);
            OnFwd(OUT_C);
            continue;
        }

        if (s1 == 1 && s2 == 1 && s3 == 1) 
        {
            // 1  1  1          Beep
            PlaySound(SOUND_CLICK);
            continue;
        }

    }
}

// end-of-file
