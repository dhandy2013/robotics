#!/usr/bin/env python3
"""
Circuit Playground Express -- Reverse Engineering
"""
import argparse
import collections
import csv
import mmap
import operator
import os
import re
import struct
import subprocess
import sys


interrupt_handler_table_msg = """
Interrupt handler table
    Handler addresses have lowest bit set, indicating that the target code is in
    the Thumb instruction set. The ATSAMD21G18 microcontroller only handles
    Thumb instructions, and all instructions must begin on even addresses.
    Therefore, the addresses shown below are the real target addresses, with
    the lowest bit cleared, instead of the original table values.
""".strip()


def bold(s):
    """
    Return s wrapped in ANSI escape sequence to make the text bold
    """
    ESC = chr(0x1b)
    return f"{ESC}[1m{s}{ESC}[0m"


class MemoryLabel(object):

    def __init__(self, addr, size, mem_type, label, comment='', source='',
                 ref=None):
        FirmwareScanner.check_addr(addr)
        if not isinstance(size, int):
            raise TypeError("size must be an int")
        if size < 0 or size > FirmwareScanner.MAX_FIRMWARE_SIZE:
            raise ValueError("size out of range")
        if mem_type not in ('code', 'data'):
            raise ValueError(f"Invalid mem_type: {mem_type}")
        if mem_type == 'code' and size != 0:
            raise ValueError("Don't specify a non-zero memory size for code")
        self.addr = addr
        self.size = size
        self.mem_type = mem_type
        self.label = label
        self.comment = comment
        self.source = source
        self.refs = []
        if ref is not None:
            self.refs.append(ref)

    def __str__(self):
        return self.format(no_colors=True)

    def format(self, no_colors=False):
        if self.size:
            size_str = f" ({self.size} bytes)"
        else:
            size_str = ""
        if self.comment:
            comment_str = f"; {self.comment}"
        else:
            comment_str = ""
        refs_str = self._format_refs()
        label_str = self.label if no_colors else bold(self.label)
        return (f"{label_str}: "
                f"@0x{self.addr:08x}{size_str}{comment_str}{refs_str}")

    def _format_refs(self):
        result = []
        for ref in self.refs:
            result.append(f"\n; -> Referenced from: 0x{ref:08x}")
        return ''.join(result)

    @classmethod
    def from_dict(cls, d, source=''):
        return MemoryLabel(
            addr=int(d['addr'], 0),
            size=(0 if (not d['size'].strip()) else int(d['size'], 0)),
            mem_type=d['type'].strip(),
            label=d['label'].strip(),
            comment=d['comment'].strip(),
            source=source,
        )


class FirmwareScanner(object):
    FIELDNAMES = ('addr', 'size', 'type', 'label', 'comment')
    MAX_FIRMWARE_SIZE = 256 * 1024

    # Every disassembled instruction starts like this:
    #   (zero or more spaces)(hex digits):
    # Example instruction:
    #   1b0:       211e            movs    r1, #30
    INSTR_START_PATTERN = r"^\s*([0-9A-F]+):(.*)$"

    # Example load/store word instruction:
    # ldr     r2, [pc, #148]  ; (0x24c)
    LOAD_STORE_WORD_PATTERN = r"\s+(?:LDR|STR)\s+.*;\s+\(0x([0-9A-F]+)\)"

    # Example subroutine call ("branch and link"):
    # bl      0x434
    BRANCH_AND_LINK_PATTERN = r"\s+BL\s+0x([0-9A-F]+)$"

    # Example conditional branch instructions:
    # bne.n   0x2fc
    # bcc.n   0x306
    # beq.n   0x1072
    CONDITIONAL_BRANCH_PATTERN = (
        r"\s+(?:"
        r"BEQ\.N|BNE\.N|BCS\.N|BCC\.N|BMI\.N|BPL\.N|BVS\.N|BVC\.N"
        r"|BHI\.N|BLS\.N|BGE\.N|BLT\.N|BGT\.N|BLE\.N"
        r")\s+0x([0-9A-F]+)"
    )

    # Example unconditional branch instruction:
    # b.n     0x2f4
    UNCONDITIONAL_BRANCH_PATTERN = r"\s+(?:B\.N)\s+0x([0-9A-F]+)"

    # Example return instructions:
    # pop     {r3, r4, r5, r6, r7, pc}
    # bx      lr
    RETURN_PATTERN = r"\s+(?:POP\s+\{.*,\s+PC\}|BX\s+LR\b)"

    def __init__(self, bin_filename, *, no_colors=False):
        self.bin_filename = bin_filename
        self.no_colors = no_colors
        self.label_str_map = None  # { label_str: mem_label }
        self.label_addr_map = None  # { label_addr: mem_label }
        self.label_ref_map = None  # { instr_addr: mem_label }
        self.label_list = None  # [mem_label, ...]
        self.next_label_i = 0
        self.m = None
        self.firmware_size = None
        self.stack_size = None
        self.interrupt_table_size = None
        self.auto_subroutine_count = 0
        self.auto_branch_target_count = 0
        self.auto_datablock_count = 0

    def load_memory_labels(self):
        self.label_str_map = {}
        self.label_addr_map = {}
        self.label_ref_map = {}
        self.label_list = []
        csv_filename = os.path.splitext(self.bin_filename)[0] + '.csv'
        if not os.path.isfile(csv_filename):
            print("Label info file not available:", csv_filename)
            return
        print("Loading", csv_filename)
        with open(csv_filename, 'r', newline='') as csv_file:
            reader = csv.DictReader(csv_file, fieldnames=self.FIELDNAMES)
            for row_num, row in enumerate(reader, 1):  # row 1 is header
                if row_num == 1:
                    assert tuple(i.strip() for i in row) == self.FIELDNAMES
                    continue
                try:
                    mem_label = MemoryLabel.from_dict(
                        row, source=f"{csv_filename}:{row_num}")
                except (ValueError, TypeError) as err:
                    raise ValueError(
                        f"{csv_filename}:{row_num}: "
                        f"Format error: {err}"
                    )
                self.add_label(mem_label)
        self.sort_labels()

    def scan(self):
        with open(self.bin_filename, 'rb') as f:
            if os.name == 'posix':
                mmap_params = {'prot': mmap.PROT_READ}
            else:
                mmap_params = {}
            self.m = mmap.mmap(f.fileno(), 0, **mmap_params)
            try:
                self.firmware_size = len(self.m)
                print(f"Firmware size        : {self.firmware_size} bytes")
                if self.firmware_size < 8192:
                    print("WARNING: "
                          "file size is less than 8K, this script may crash!",
                          file=sys.stderr)
                prev_num_labels = 0
                self.scan_interrupt_table()
                while len(self.label_list) > prev_num_labels:
                    prev_num_labels = len(self.label_list)
                    self.scan_memory(quiet=True)
                self.scan_memory(quiet=False)
            finally:
                self.m.close()
                self.m = None

    def scan_interrupt_table(self):
        sp = self.unpack_word(0)
        print(f"Initial stack pointer: 0x{sp:08x}")
        self.stack_size = sp - 0x20000000
        print(f"Initial stack size   :", self.stack_size)

        reset_handler = self.unpack_word(4) & 0xFFFFFFFE
        print(f"Reset handler        : 0x{reset_handler:08x} (interrupt #1)")
        print()
        mem_label = MemoryLabel(reset_handler,
                                0,
                                'code',
                                '_reset_handler',
                                "Reset Handler -- START HERE")
        self.add_label(mem_label)

        print(interrupt_handler_table_msg)
        p = 0
        handler_table = collections.defaultdict(list)  # {addr: [interrupt]}
        for i in range(1, 100):
            p += 4
            addr = self.unpack_word(p) & 0xFFFFFFFE
            if addr == 0:
                # Ignore null pointers in interrupt table
                continue
            if not self.is_code_addr(addr):
                break
            handler_table[addr].append(i)
        self.interrupt_table_size = interrupt_table_size = p
        print(f"Guessed size of interrupt table: {interrupt_table_size} bytes")
        print()
        if not handler_table:
            raise AssertionError("No interrupt handlers found!")

        print("Handler Addr  Interrupts")
        print("------------  ----------")
        for i, handler_addr in enumerate(handler_table):
            interrupts = handler_table[handler_addr]
            print(f"0x{handler_addr:08x}    {interrupts}")
            mem_label = MemoryLabel(
                handler_addr, 0, 'code', f'_interrupt_handler_{i:02d}', "")
            self.add_label(mem_label)

    def scan_memory(self, quiet=True):
        self.sort_labels()
        cur_addr = self.interrupt_table_size
        cur_label, next_label = self._get_next_labels(cur_addr)
        while cur_addr < self.firmware_size:
            if cur_label is not None and cur_label.size > 0:
                # Known size block of memory
                end_addr = cur_addr + cur_label.size
            elif next_label is None:
                # No more labels between here and the end of firmware
                end_addr = self.firmware_size
            else:
                # End at the start of the next label
                end_addr = next_label.addr
            if cur_label is None:
                # Not at a label location, therefore unknown memory type
                if not quiet:
                    print()
                    print(f"<Unknown memory type: "
                          f"0x{cur_addr:08x} to 0x{end_addr:08x}>")
            else:
                if not quiet:
                    print()
                    print(cur_label.format(no_colors=self.no_colors))
                if cur_label.mem_type == 'data':
                    if not quiet:
                        self.hexdump_range(cur_addr, end_addr)
                elif cur_label.mem_type == 'code':
                    addr = self.disassemble_range(cur_addr, end_addr,
                                                  quiet=quiet)
                    if addr is not None and (cur_addr < addr < end_addr):
                        end_addr = addr
                else:
                    raise AssertionError(
                        f"Unknown label type: {cur_label.mem_type}")
            cur_addr = end_addr
            cur_label, next_label = self._get_next_labels(cur_addr, next_label)

    def _get_next_labels(self, addr, next_label=None):
        """Return (cur_label, next_label) given current addr"""
        if next_label is None:
            next_label = self.get_next_label()
        while True:
            cur_label = next_label
            if cur_label is None:
                # No (more) labels
                return (None, None)
            if cur_label.addr < addr:
                # Something went wrong - skip to next label
                cur_label = self.get_next_label()
                continue
            if cur_label.addr == addr:
                return (cur_label, self.get_next_label())
            return (None, cur_label)

    def get_next_label(self):
        """
        Return the next MemoryLabel in sequence after the last request,
        or the first Memory label the first time after sort_labels().
        """
        i = self.next_label_i
        if i >= len(self.label_list):
            # Stop at end of list of labels
            return None
        if i > 0 and self.label_list[i].addr < self.label_list[i - 1].addr:
            # Stop if we hit a MemoryLabel with an addr less than the previous
            return None
        self.next_label_i = i + 1
        return self.label_list[i]

    def sort_labels(self):
        self.label_list.sort(key=operator.attrgetter('addr'))
        self.next_label_i = 0

    def add_label(self, mem_label):
        """Return True iff a new label is added, false if label is merged"""
        existing_mem_label = self.label_addr_map.get(mem_label.addr)
        if existing_mem_label is not None:
            new_refs = self._merge_label(existing_mem_label, mem_label)
            for ref in new_refs:
                self.label_ref_map[ref] = existing_mem_label
            return False
        if mem_label.label in self.label_str_map:
            raise ValueError(
                f"{mem_label.source}: "
                f"Duplicate label: {mem_label.label}"
            )
        self.label_str_map[mem_label.label] = mem_label
        self.label_addr_map[mem_label.addr] = mem_label
        for ref in mem_label.refs:
            self.label_ref_map[ref] = mem_label
        self.label_list.append(mem_label)
        return True

    def _merge_label(self, existing_mem_label, mem_label):
        """
        Check compatiblity then merge with existing label
        Return collection of new ref addrs
        """
        if existing_mem_label.mem_type != mem_label.mem_type:
            raise AssertionError(
                f"Attempting to add label '{mem_label.label}'"
                f" of type {mem_label.mem_type}"
                f" at 0x{mem_label.addr:08x}"
                f" but existing label '{existing_mem_label.label}'"
                f" has type {existing_mem_label.mem_type}")
        if existing_mem_label.size != mem_label.size:
            raise AssertionError(
                f"Attempting to add label '{mem_label.label}'"
                f" with size {mem_label.size}"
                f" at 0x{mem_label.addr:08x}"
                f" but existing label '{existing_mem_label.label}'"
                f" has size {existing_mem_label.size}")
        if not existing_mem_label.source and mem_label.source:
            # Human-entered label text from the CSV file overrides
            # auto-generated label text.
            assert False, "This code is not expected to be executed"
            existing_mem_label.label = mem_label.source
            existing_mem_label.source = mem_label.source
        if not existing_mem_label.comment and mem_label.comment:
            # A non-empty comment overrides an empty comment
            existing_mem_label.comment = mem_label.comment
        # Merge the reference sets
        refs = set(existing_mem_label.refs)
        incoming_refs = set(mem_label.refs)
        new_refs = incoming_refs - refs
        refs.update(incoming_refs)
        existing_mem_label.refs = sorted(refs)
        return new_refs

    def unpack_word(self, offset):
        return struct.unpack_from('<I', self.m, offset)[0]

    @classmethod
    def is_code_addr(cls, addr):
        """
        Return True IFF addr is potentially executable code
        (flash memory or SRAM, even address)
        """
        if not cls.is_flash_addr(addr) or cls.is_sram_addr(addr):
            return False
        return (addr & 1) == 0

    @classmethod
    def is_flash_addr(cls, addr):
        cls.check_addr(addr)
        return addr < 0x40000

    @classmethod
    def is_sram_addr(cls, addr):
        cls.check_addr(addr)
        return 0x20000000 <= addr < 0x20008000

    @staticmethod
    def check_addr(addr):
        if not isinstance(addr, int):
            raise TypeError(f"Address {addr!r} is not an integer")
        if addr < 0 or addr > 0xFFFFFFFF:
            raise ValueError(f"Address {addr:x} is out of range")

    def disassemble_range(self, start_addr, end_addr, quiet=True):
        if not self.is_code_addr(start_addr):
            raise ValueError(f"Invalid start_addr: 0x{start_addr:08x}")
        if not self.is_code_addr(end_addr):
            raise ValueError(f"Invalid end_addr: 0x{end_addr:08x}")
        if not end_addr > start_addr:
            raise ValueError("end_addr must be greater than start_addr")
        cmd = [
            'arm-none-eabi-objdump',
            '-D', '-bbinary', '-marm', '-Mforce-thumb',
            f'--start-address=0x{start_addr:08x}',
            f'--stop-address=0x{end_addr:08x}',
            self.bin_filename,
        ]
        r = subprocess.run(cmd, stdout=subprocess.PIPE, check=True,
                           universal_newlines=True)
        return self.scan_src(r.stdout, quiet=quiet)

    def hexdump_range(self, start_addr, end_addr):
        if end_addr - start_addr == 4:
            # Special-case displaying 32-bit word
            word = self.unpack_word(start_addr)
            print(f"{start_addr:05X}:  {word:08X}")
            return
        self.m.seek(start_addr)
        hexdump(self.m, start_addr, end_addr)

    def scan_src(self, src, quiet=True):
        """
        Scan disassembler output
        If quiet is true, automatically add new memory labels based on src
        If quiet is false, print source lines.

        If UNCONDITIONAL_BRANCH_PATTERN or RETURN_PATTERN is encountered before
        end of src, stop and return addr of next instruction.
        Otherwise scan till end of src and return None
        """
        found_unconditional_branch = False
        for line in src.split('\n'):
            m = re.match(self.INSTR_START_PATTERN, line, re.I)
            if not m:
                continue
            instr_start_addr = int(m.group(1), 16)
            instr_text = m.group(2)
            if found_unconditional_branch:
                # Don't keep scanning past unconditional branch
                return instr_start_addr
            if not quiet:
                cur_label = self.label_ref_map.get(instr_start_addr)
                if cur_label is None:
                    label_text = ''
                else:
                    label_str = (cur_label.label if self.no_colors
                                 else bold(cur_label.label))
                    label_text = f" -> {label_str}"
                print(f"{instr_start_addr:05X}:{instr_text}{label_text}")
            m = re.search(self.LOAD_STORE_WORD_PATTERN, line, re.I)
            if m:
                data_addr = int(m.group(1), 16)
                if quiet:
                    self._ensure_label(data_addr, 4, 'data',
                                       '_data_word_',
                                       'auto_datablock_count',
                                       instr_start_addr)
                continue
            m = re.search(self.BRANCH_AND_LINK_PATTERN, line, re.I)
            if m:
                subroutine_addr = int(m.group(1), 16)
                if quiet:
                    self._ensure_label(subroutine_addr, 0, 'code',
                                       '_subroutine_',
                                       'auto_subroutine_count',
                                       instr_start_addr)
                continue
            m = re.search(self.CONDITIONAL_BRANCH_PATTERN, line, re.I)
            if m:
                branch_target = int(m.group(1), 16)
                if quiet:
                    self._ensure_label(branch_target, 0, 'code',
                                       '_branch_target_',
                                       'auto_branch_target_count',
                                       instr_start_addr)
                continue
            m = re.search(self.UNCONDITIONAL_BRANCH_PATTERN, line, re.I)
            if m:
                found_unconditional_branch = True
                branch_target = int(m.group(1), 16)
                if quiet:
                    self._ensure_label(branch_target, 0, 'code',
                                       '_branch_target_',
                                       'auto_branch_target_count',
                                       instr_start_addr)
                continue
            m = re.search(self.RETURN_PATTERN, line, re.I)
            if m:
                found_unconditional_branch = True
                continue
        # Reached end of src
        return None

    def _ensure_label(self, addr, size, mem_type, prefix, count_attr, ref):
        count = getattr(self, count_attr) + 1
        mem_label = MemoryLabel(
            addr, size, mem_type,
            f'{prefix}{count:04d}',
            ref=ref)
        if self.add_label(mem_label):
            setattr(self, count_attr, count)


def hexdump(infile, start_addr=None, end_addr=None, outfile=None, chunksize=16):
    if outfile is None:
        outfile = sys.stdout
    if start_addr is None:
        start_addr = infile.tell()
    if end_addr is None:
        end_addr = start_addr + 0xffffffff
    addr = start_addr
    while addr < end_addr:
        n = min(chunksize, end_addr - addr)
        chunk = infile.read(n)
        if not chunk:
            break
        outfile.write("%05X: " % addr)
        addr += len(chunk)
        for b in chunk:
            outfile.write(" %02X" % b)
        if len(chunk) < chunksize:
            outfile.write("   " * (chunksize - len(chunk)))
        outfile.write(" | ")
        for b in chunk:
            if b < 32 or b >= 127:
                outfile.write('.')
            else:
                outfile.write(chr(b))
        outfile.write('\n')
        outfile.flush()


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--bin-filename', metavar="BIN-FILENAME",
                        default='cpe-bootloader.bin',
                        help="BIN file containing CPE firmware")
    parser.add_argument('--no-colors', action='store_true',
                        help="Disable ANSI color escape sequences")
    args = parser.parse_args()
    print(__doc__.strip())
    print("Reading BIN file containing CPE firmware:", args.bin_filename)
    scanner = FirmwareScanner(args.bin_filename, no_colors=args.no_colors)
    scanner.load_memory_labels()
    scanner.scan()


if __name__ == '__main__':
    sys.exit(main())
