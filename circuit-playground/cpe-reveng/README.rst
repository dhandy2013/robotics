Circuit Playground Express Reverse Engineering
==============================================

See: https://gw.handysoftware.com/wiki/cpif/circuit-playground#Bootloader_Reverse-Engineering

This directory contains scripts and docs relating to reverse-engineering
the Circuit Playground Express (CPE) bootloader and also better understanding
how its hardware and software work in general.

