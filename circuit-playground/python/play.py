# Play a wave file from flash memory
# https://circuitpython.readthedocs.io/en/latest/shared-bindings/audioio/WaveFile.html
import time

import board
import audioio
import digitalio

# Required for CircuitPlayground Express
speaker_enable = digitalio.DigitalInOut(board.SPEAKER_ENABLE)
speaker_enable.switch_to_output(value=True)

# filename = "sweep-100-2000-2sec.wav"
filename = "sweep-400-4000-1sec.wav"
data = open(filename, "rb")
wav = audioio.WaveFile(data)
a = audioio.AudioOut(board.A0)

print("Playing", filename)
t0 = time.monotonic()
a.play(wav)
while a.playing:
    pass
print("Finished in", time.monotonic() - t0, "seconds")
