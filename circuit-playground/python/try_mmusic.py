import audioio
import board
import digitalio

import mmusic

# Required for CircuitPlayground Express
speaker_enable = digitalio.DigitalInOut(board.SPEAKER_ENABLE)
speaker_enable.switch_to_output(value=True)
audio = audioio.AudioOut(board.SPEAKER)


for n in range(60, 60 + 12):
    mmusic.play_note(audio, n, 32)
