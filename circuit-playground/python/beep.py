# Play a short tone on the Circuit Playground Express
import time

import board
from digitalio import DigitalInOut, Direction, Pull

import audioio

from sound import make_tone

speaker_enable = DigitalInOut(board.SPEAKER_ENABLE)
speaker_enable.switch_to_output(value=True)

tone_freq = 1000  # Hz
tone_time = 0.01  # seconds

samples = make_tone(tone_freq, tone_time, volume=0.7, sample_rate=22050)
dac = audioio.AudioOut(board.A0)
dac.play(samples, loop=True)
time.sleep(0.25)
dac.stop()
