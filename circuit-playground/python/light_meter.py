# Reads the on-board light sensor and graphs the brighness with NeoPixels
# Based on this demo:
# https://learn.adafruit.com/adafruit-circuit-playground-express/playground-light-sensor
# Except that I couldn't find simpleio or the map_range function anywhere
# so I had write something to substitute for that.
#
# Notes:
#  time.monotonic() precision is 1/128 sec
#  time.time() precision is 1 sec
import time
import math

import board
from analogio import AnalogIn
import neopixel

num_pixels = 10
min_thresh = 650    # this much or more == level 0.0
max_thresh = 10000  # this much or more == level 1.0 ish

def calc_exp_light_level(light_value):
    # Convert raw light reading to light level using exponential response
    # Light value: 0 through 65536 inclusive
    # Return a scaled brightness level 0.0 through 1.0 inclusive
    if light_value < min_thresh:
        return 0.0
    t = light_value - min_thresh
    T = (max_thresh - min_thresh) / 5.0
    return 1.0 - math.exp(-t / T)


def calc_lin_light_level(light_value):
    # Convert raw light reading to light level using linear scale
    # Light value: 0 through 65536 inclusive
    # Return a scaled brightness level 0.0 through 1.0 inclusive
    if light_value <= min_thresh:
        return 0.0
    elif light_value >= max_thresh:
        return 1.0
    else:
        return (light_value - min_thresh) / (max_thresh - min_thresh)


# Pick which light level algorithm to use
calc_light_level = calc_exp_light_level

pixels = neopixel.NeoPixel(board.NEOPIXEL, num_pixels, auto_write=0,
                           brightness=.05)
pixels.fill((0, 0, 0))
pixels.show()

light = AnalogIn(board.LIGHT)

num_samples = 0
sum_samples = 0
t0 = time.monotonic()
while True:
    # Calculate light level
    light_value = light.value
    light_level = calc_light_level(light_value)
    num_samples += 1
    sum_samples += light_value
    t1 = time.monotonic()
    if t1 - t0 >= 1.0:
        if num_samples > 0:
            ave_light_value = int(sum_samples / num_samples)
            ave_light_level = calc_light_level(ave_light_value)
            print("{:3} {:6} {:6.3f}".format(
                num_samples,
                ave_light_value,
                ave_light_level,
            ))
        num_samples = 0
        sum_samples = 0
        t0 = t1

    # Turn on the appropriate number of pixels
    pixel_level = light_level * num_pixels
    pixel_index = int(math.floor(pixel_level))
    for i in range(0, pixel_index):
        pixels[i] = (0, 255, 0)
    if pixel_index < num_pixels:
        led_value = int((pixel_level - i) * 255)
        clamped_led_value = min(max(0, led_value), 255)
        pixels[pixel_index] = (0, clamped_led_value, 0)
    for i in range(pixel_index + 1, num_pixels):
        pixels[i] = (0, 0, 0)
    pixels.show()
