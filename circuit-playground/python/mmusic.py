# Play musical notes from sound files on the Circuit Playground Express
import time

import audioio

notes1 = ['C','C#','D','D#','E','F','F#','G','G#','A','A#','B']


def play_note(audio, note_num, duration):
    # audio = audioio.AudioOut(board.SPEAKER)
    # note_num: MIDI note number, 60 is middle C
    # duration: Time in ticks, tick == 1/128 second
    n = note_num - 60
    index = n % 12
    octave = (n // 12) + 4
    filename = "notes/{}{}.wav".format(notes1[index], octave)
    print("Playing", filename)
    data = open(filename, "rb")
    wav = audioio.WaveFile(data)
    tend = time.monotonic() + (duration / 128)
    audio.play(wav)
    while time.monotonic() < tend:
        pass
    audio.stop()
    data.close()
