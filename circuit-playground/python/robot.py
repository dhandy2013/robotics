# Robot Control Module
#
# Pad assignments:
# See https://forums.adafruit.com/viewtopic.php?f=60&t=148017#p734226
# A0/D12: Right bumper switch sensor
# A1/D6 : Right motor forward (PWM capable)
# A2/D9 : Right motor reverse (PWM capable)
# A3/D10: Left motor forward (PWM capable with fixed frequency with A2)
# A4/D3 : Right motor brake control
# A5/D2 : Left motor brake control
# A6/D0 : Left motor reverse (PWM capable)
# A7/D1 : Left bumper switch sensor
#
# ***********************************************************************
# * Never set forward, reverse, or brake signals high at the same time! *
# * Only set one at a time high. Otherwise leave them all low.          *
# ***********************************************************************
import time

import board
from digitalio import DigitalInOut, Direction, DriveMode, Pull
from pulseio import PWMOut


class Bumped(Exception):
    # Exception raised when one of the bumper switches goes low
    pass


class OutputConflict(Exception):
    # Exception raised when you try to turn on two outputs that conflict.
    # This protects you from burning up your microcontroller or external
    # components.
    pass


class Motor:

    MAX_DUTY_CYCLE = 0xffff
    PWM_FREQUENCY = 100

    def __init__(self, forward_pin, reverse_pin, brake_pin,
                 forward_handicap=1.0, reverse_handicap=1.0):
        # handicap values are between 0 and 1.0 and indicate by how much
        # the power of this motor should be scaled down, to match the other
        # motor. 1.0 means don't scale down, 0.5 means scale down 50%.
        self.forward_pwm = PWMOut(
            forward_pin,
            duty_cycle=0,
            frequency=self.PWM_FREQUENCY,
        )
        self.reverse_pwm = PWMOut(
            reverse_pin,
            duty_cycle=0,
            frequency=self.PWM_FREQUENCY,
        )
        self.brake_out = DigitalInOut(brake_pin)
        self.brake_out.switch_to_output(value=False,
                                        drive_mode=DriveMode.PUSH_PULL)
        self.forward_handicap = forward_handicap
        self.reverse_handicap = reverse_handicap

    def forward(self, power):
        # Turn on the control signal that sends power to the motor, with a
        # polarity that makes the motor go forward. Call stop() to turn it off.
        # power controls the PWM of the signal - 0 means off, 1.0 means full on
        if not (0.0 <= power <= 1.0):
            raise ValueError("Invalid power value")
        if power:
            if self.brake_out.value:
                raise OutputConflict("Trying to go forward with the brake on")
            if self.reverse_pwm.duty_cycle:
                raise OutputConflict("Trying to go both reverse and forward")
        self.forward_pwm.duty_cycle = int(
            power * self.forward_handicap * self.MAX_DUTY_CYCLE
        )

    def reverse(self, power):
        # Turn on the control signal that sends power to the motor, with a
        # polarity that makes the motor reverse. Call stop() to turn it off.
        # power controls the PWM of the signal - 0 means off, 1.0 means full on
        if not (0.0 <= power <= 1.0):
            raise ValueError("Invalid power value")
        if power:
            if self.brake_out.value:
                raise OutputConflict("Trying to go in reverse with brake on")
            if self.forward_pwm.duty_cycle:
                raise OutputConflict("Trying to go both forward and reverse")
        self.reverse_pwm.duty_cycle = int(
            power * self.reverse_handicap * self.MAX_DUTY_CYCLE
        )

    def brake(self):
        # Activate the brake signal. This causes the GPIO pin to source about
        # 2.6 * 2 == 5.2 milliamps of current, so don't leave it on too long.
        # Call stop() to turn off the brake signal.
        if self.forward_pwm.duty_cycle:
            raise OutputConflict("Trying to put brake on while going forward")
        if self.reverse_pwm.duty_cycle:
            raise OutputConflict("Trying to put brake on while in reverse")
        self.brake_out.value = True

    def stop(self):
        # Turn off control signals.
        self.forward_pwm.duty_cycle = 0
        self.reverse_pwm.duty_cycle = 0
        self.brake_out.value = False


class Robot:

    # Indices into the motors list
    LEFT = 0
    RIGHT = 1

    # Adjustments to motor strengths to try to balance the two motors.
    # Obviously this is specific to a certain robot.
    left_forward_handicap = 0.5
    left_reverse_handicap = 1.0
    right_forward_handicap = 1.0
    right_reverse_handicap = 1.0

    def __init__(self):
        self.led = DigitalInOut(board.D13)  # built-in red LED
        self.led.switch_to_output(value=False, drive_mode=DriveMode.PUSH_PULL)

        self.left_switch = DigitalInOut(board.A7)  # a.k.a. D1
        self.left_switch.direction = Direction.INPUT
        self.left_switch.pull = Pull.UP

        self.right_switch = DigitalInOut(board.A0)  # a.k.a. D12
        self.right_switch.direction = Direction.INPUT
        self.right_switch.pull = Pull.UP

        self.motors = [
            # Left motor
            Motor(forward_pin=board.A3,     # a.k.a. D10
                  reverse_pin=board.A6,     # a.k.a. D0
                  brake_pin=board.A5,       # a.k.a. D2
                  forward_handicap=self.left_forward_handicap,
                  reverse_handicap=self.left_reverse_handicap),
            # Right motor
            Motor(forward_pin=board.A1,     # a.k.a. D6
                  reverse_pin=board.A2,     # a.k.a. D9
                  brake_pin=board.A4,       # a.k.a. D3
                  forward_handicap=self.right_forward_handicap,
                  reverse_handicap=self.right_reverse_handicap),
        ]

        self.power = 1.0

    def turn_right(self, duration, raise_exception_on_bump=True):
        # duration: Number of time ticks to turn right
        self.stop()
        self.motors[self.LEFT].forward(self.power)
        self.motors[self.RIGHT].reverse(self.power)
        return self.run(duration, raise_exception_on_bump)

    def turn_left(self, duration, raise_exception_on_bump=True):
        # duration: Number of time ticks to turn left
        self.stop()
        self.motors[self.RIGHT].forward(self.power)
        self.motors[self.LEFT].reverse(self.power)
        return self.run(duration, raise_exception_on_bump)

    def forward(self, duration, raise_exception_on_bump=True):
        # duration: Number of time ticks to go forward
        self.stop()
        for motor in self.motors:
            motor.forward(self.power)
        return self.run(duration, raise_exception_on_bump)

    def reverse(self, duration, raise_exception_on_bump=True):
        # duration: Number of time ticks to go in reverse
        self.stop()
        for motor in self.motors:
            motor.reverse(self.power)
        return self.run(duration, raise_exception_on_bump)

    def stop(self):
        # Turn off all output signals
        for motor in self.motors:
            motor.stop()

    def run(self, duration, raise_exception_on_bump=True):
        # duration: Number of time ticks to wait before stopping
        # A tick is about 0.01 second.
        #
        # raise_exception: If true, raise a Bumped exception if an obstacle is
        # hit. Otherwise, return True if an obstacle is hit, False otherwise.
        #
        # If one of the bumper switches goes low (is pressed) while running,
        # put the brakes and the LED on for a short time, then raise the Bumped
        # exception.
        #
        # I use loop counting to measure ticks because time.time() only has
        # 1-second resolution. time.sleep() allows fractional seconds but
        # cannot be interrupted by the bumper switch.
        loops_per_second = 14045  # CircuitPython 5.3.0
        # loops_per_second = 739    # CircuitPython 5.4.0-beta.0
        loop_count = int((duration * loops_per_second) / 100)
        try:
            loops = 0
            left_switch = self.left_switch
            right_switch = self.right_switch
            while loops < loop_count:
                if left_switch.value and right_switch.value:
                    loops += 1
                    continue
                # If we reach this point, bumper switch got pressed
                self.stop()
                for motor in self.motors:
                    motor.brake()
                self.led.value = True
                time.sleep(0.5)
                self.led.value = False
                if raise_exception_on_bump:
                    raise Bumped()
                return True
        finally:
            # No matter how this method exits, turn off all signals!
            self.stop()
        return False
