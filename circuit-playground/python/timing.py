# Measure the timing of various activities on the Circuit Playground Express
import time

import board
from digitalio import DigitalInOut, Direction, Pull

switch = DigitalInOut(board.D7)  # For Circuit Playground Express
switch.direction = Direction.INPUT
switch.pull = Pull.UP


def time_counting_while_loop(n=100000):
    # Measured 16295.4 loops / second = 61.4 usec per loop
    print("Timing a basic while loop, incrementing an integer counter")
    i = 0
    t0 = time.monotonic()
    while i < n:
        i += 1
    t = time.monotonic() - t0
    print(n, "loops in", t, "seconds = ", n / t, "loops per second")


def time_counting_while_waiting_for_ctrl_c():
    # Measured 26993.5 loops / second = 37.0 usec per loop
    print("Timing ``while True: i += 1``, press Ctrl-C to stop")
    i = 0
    t0 = time.monotonic()
    try:
        while True:
            i += 1
    except KeyboardInterrupt:
        t = time.monotonic() - t0
        print("Ctrl-C pressed!")
    print(i, "loops in", t, "seconds = ", i / t, "loops per second")


def time_empty_for_loop(n=100000):
    # Measured 16431.3 loops per second = 60.9 usec per loop
    print("Timing an empty ``for i in range({})`` loop".format(n))
    t0 = time.monotonic()
    for i in range(n):
        pass
    t = time.monotonic() - t0
    print(n, "loops in", t, "seconds = ", n / t, "loops per second")


def time_calling_monotonic_time(t=5.0):
    # Measured 6605.2 loops / second = 151 usec per loop
    print("Timing the overhead of calling time.monotonic() in while loop")
    i = 0
    t0 = time.monotonic()
    while time.monotonic() - t0 < t:
        i += 1
    print(i, "loops in about", t, "seconds = ", i / t, "loops per second")


def time_reading_switch_state(n=20000):
    # Measured 9094.14 loops / second = 110 usec per loop
    # 49.1 usec slower compared to empty for loop
    print("Timing reading a switch value in ``for i in range()`` loop")
    t0 = time.monotonic()
    for i in range(n):
        switch_value = switch.value  # noqa: F841
        # switch.value  # only 1 usec quicker
        # switch_value = True  # 35.6 usec quicker
    t = time.monotonic() - t0
    print(n, "loops in", t, "seconds = ", n / t, "loops per second")


# time_counting_while_loop()
# time_empty_for_loop()
# time_calling_monotonic_time()
time_reading_switch_state()
# time_counting_while_waiting_for_ctrl_c()
