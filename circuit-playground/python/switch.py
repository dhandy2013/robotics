# Based on the following demos/docs:
# https://learn.adafruit.com/circuitpython-essentials/circuitpython-digital-in-out
# https://circuitpython.readthedocs.io/en/latest/shared-bindings/audioio/AudioOut.html
import time

import audioio
import board
from digitalio import DigitalInOut, Direction, Pull

led = DigitalInOut(board.D13)
led.direction = Direction.OUTPUT

switch = DigitalInOut(board.D7)  # For Circuit Playground Express
switch.direction = Direction.INPUT
switch.pull = Pull.UP


class SwitchChecker:

    count_till_change = 3

    def __init__(self, init_value=None):
        self.prev_value = init_value
        self.count = 0

    def __call__(self):
        cur_value = switch.value
        if cur_value != self.prev_value:
            self.count += 1
            if self.count >= self.count_till_change:
                self.prev_value = cur_value
                self.count = 0
                return cur_value
        else:
            self.count = 0
        return self.prev_value


switch_checker = SwitchChecker(True)

print(time.time(), "Switch left to turn red LED on, right to turn off")
while True:
    print("Switch on")
    while switch_checker():
        # Switch is on
        led.value = True
    print("Switch off")
    while not switch_checker():
        # Switch is off
        led.value = False


# The "old" way, not using SwitchChecker
if False:
    prev_switch_value = None
    while True:
        cur_switch_value = switch.value
        if cur_switch_value == prev_switch_value:
            time.sleep(0.01)
            continue
        if cur_switch_value:
            print("Switch on")
            led.value = True
        else:
            print("Switch off")
            led.value = False
        prev_switch_value = cur_switch_value

        time.sleep(0.01)  # debounce delay
