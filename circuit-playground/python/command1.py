# Robot control script 1
import time
print("Hello from command1.py")
import board
from digitalio import DigitalInOut, Direction, Pull
import robot

left_button = DigitalInOut(board.D4)
left_button.direction = Direction.INPUT
left_button.pull = Pull.DOWN

r = robot.Robot()

while True:
    print("Waiting for Button A to be pressed")
    while left_button.value == False:
        pass

    delay = 2.0
    print("Waiting", delay, "seconds")
    time.sleep(delay)

    print("Running the robot program")
    r.forward(500)
