"""
Utilities for generating sounds
"""
import array
import math

import audioio


def make_tone(freq, duration, volume=1.0, sample_rate=22050):
    """
    Return a RawSample object set up to play a tone
    freq: Frequency of tone in Hz
    duration: duration of tone in seconds (float)
    volume: Loudness scale, 0.0 through 1.0
    sample_rate: samples per second
    """
    num_samples = int(duration * sample_rate)
    a = array.array('h', [0] * num_samples)
    amplitude = volume * float(1 << 15)
    sin = math.sin
    radians_per_sample = (2.0 * math.pi * freq) / sample_rate
    for i in range(num_samples):
        a[i] = int(amplitude * sin(i * radians_per_sample))
    return audioio.RawSample(a, sample_rate=sample_rate)
