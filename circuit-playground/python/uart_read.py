# See:
# https://learn.adafruit.com/circuitpython-essentials/circuitpython-uart-serial
# CircuitPython Demo - USB/Serial echo
BAUDRATE = 115200

import board
import busio
import digitalio

led = digitalio.DigitalInOut(board.D13)
led.direction = digitalio.Direction.OUTPUT

uart = busio.UART(board.TX, board.RX, baudrate=BAUDRATE)

while True:
    led.value = True
    data = uart.read(1)  # blocking read up to 32 bytes
    led.value = False

    if data is not None:
        # convert bytearray to string
        data_string = ''.join([chr(b) for b in data])
        print(data_string, end="")
