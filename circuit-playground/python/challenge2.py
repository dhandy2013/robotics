import time
print("Challenge 2")
import board
from digitalio import DigitalInOut, Direction, Pull
import robot

left_button = DigitalInOut(board.D4)
left_button.direction = Direction.INPUT
left_button.pull = Pull.DOWN

r = robot.Robot()

while True:
    print("Waiting for Button A to be pressed")
    while left_button.value == False:
        pass

    delay = 2.0
    print("Waiting", delay, "seconds")
    time.sleep(delay)

    print("Running the robot program")
    #challenge 2
    print("cup 1")
    r.forward(500)
    time.sleep(0.2)
    r.turn_right(90)
    time.sleep(0.2)
    print("cup 2")
    r.forward(400)
    time.sleep(0.2)
    r.turn_right(90)
    time.sleep(0.2)
    r.forward(500)
    time.sleep(0.2)
    r.turn_left(90)
    time.sleep(0.2)
    print("cup 3")
    r.forward(400)
    time.sleep(0.2)
    r.turn_left(90)
    time.sleep(0.2)
    r.forward(500)
    print("end challenge, begin extra points")

    #Extra points
    time.sleep(0.2)
    r.turn_right(180)
    r.forward(500)
    time.sleep(0.2)
    r.turn_right(90)
    time.sleep(0.2)
    r.forward(400)
    time.sleep(0.2)
    r.turn_right(90)
    time.sleep(0.2)
    r.forward(500)
    time.sleep(0.2)
    r.turn_left(90)
    time.sleep(0.2)
    r.forward(400)
    time.sleep(0.2)
    r.turn_left(90)
    time.sleep(0.2)
    r.forward(500)