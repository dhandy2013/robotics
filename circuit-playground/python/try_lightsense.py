# Capture light sense values
# Print min, ave, max on Ctrl-C
#
# Board shut in a drawer: 496
# Board held close to strong LED lamp: 63520
import time

from analogio import AnalogIn
import board

light = AnalogIn(board.LIGHT)

min_light = 999999999
max_light = -999999999
sum_light = 0
num_samples = 0
try:
    while True:
        v = light.value
        num_samples += 1
        min_light = min(min_light, v)
        max_light = max(max_light, v)
        sum_light += v
        print(v)
        time.sleep(0.1)
except KeyboardInterrupt:
    print("Min:", min_light)
    if num_samples > 0:
        print("Ave:", sum_light / num_samples)
    print("Max:", max_light)
