# christmas.py
# Christmas Box project
import random
import time

from analogio import AnalogIn
import audioio
import board
import digitalio
from digitalio import DigitalInOut, Direction, Pull
import neopixel

# filename = "unto-us-child-born-15-soft.wav"
filename = "unto-us-child-born-15-loud.wav"
dark_threshold = 1000
light_threshold = 1100

light = AnalogIn(board.LIGHT)

num_pixels = 10
pixels = neopixel.NeoPixel(board.NEOPIXEL, num_pixels, auto_write=0,
                           brightness=.05)
pixels.fill((0, 0, 0))
pixels.show()

# Required for CircuitPlayground Express
speaker_enable = digitalio.DigitalInOut(board.SPEAKER_ENABLE)
speaker_enable.switch_to_output(value=True)
audio = audioio.AudioOut(board.SPEAKER)

led = DigitalInOut(board.D13)
led.direction = Direction.OUTPUT

switch = DigitalInOut(board.D7)  # For Circuit Playground Express
switch.direction = Direction.INPUT
switch.pull = Pull.UP


class SwitchChecker:

    count_till_change = 3

    def __init__(self, init_value):
        self.prev_value = init_value
        self.count = 0

    def __call__(self):
        cur_value = switch.value
        if cur_value != self.prev_value:
            self.count += 1
            if self.count >= self.count_till_change:
                self.prev_value = cur_value
                self.count = 0
                return cur_value
        else:
            self.count = 0
        return self.prev_value


switch_checker = SwitchChecker(False)


class LightChecker:

    def __init__(self):
        self.size = 10
        self.samples = [0] * self.size
        self.num_samples = 0
        self.sum_samples = 0

    def __call__(self):
        light_value = light.value
        index = self.num_samples % self.size
        if self.num_samples >= self.size:
            self.sum_samples -= self.samples[index]
        self.samples[index] = light_value
        self.num_samples += 1
        self.sum_samples += light_value
        if self.num_samples < self.size:
            return None
        return self.sum_samples / self.size


light_checker = LightChecker()


def wait_till_dark(trigger_delay=1.0):
    print("Waiting till dark...")
    t0 = None
    while switch_checker():
        light_state = light_checker()
        if light_state is None:
            continue
        if light_state < dark_threshold:
            if t0 is None:
                t0 = time.monotonic()
            elif time.monotonic() - t0 >= trigger_delay:
                return
        else:
            t0 = None


def wait_till_light(trigger_delay=0.5):
    print("Waiting till light...")
    t0 = None
    while switch_checker():
        light_state = light_checker()
        if light_state is None:
            continue
        if light_state > light_threshold:
            if t0 is None:
                t0 = time.monotonic()
            elif time.monotonic() - t0 >= trigger_delay:
                return
        else:
            t0 = None


def pulse_pixel(pixel_index, color_index):
    for i in range(0, 256, 5):
        if not switch_checker():
            return
        color = [0, 0, 0]
        color[color_index] = i
        pixels[pixel_index] = tuple(color)
        pixels.show()
    for i in range(255, -1, -5):
        if not switch_checker():
            return
        color = [0, 0, 0]
        color[color_index] = i
        pixels[pixel_index] = tuple(color)
        pixels.show()


def play_music_flash_pixels():
    print("Playing", filename)
    data = open(filename, "rb")
    wav = audioio.WaveFile(data)
    audio.play(wav)
    while switch_checker():
        pixel_index = random.randrange(num_pixels)
        color_index = random.randrange(3)
        pulse_pixel(pixel_index, color_index)
    # Reset everything
    audio.stop()
    data.close()
    pixels.fill((0, 0, 0))
    pixels.show()


while True:
    while not switch_checker():
        pass
    wait_till_dark()
    if not switch_checker():
        continue
    wait_till_light()
    if not switch_checker():
        continue
    play_music_flash_pixels()
