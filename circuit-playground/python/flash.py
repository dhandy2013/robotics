# Flash neopixels different colors and brightness
# https://circuitpython.readthedocs.io/en/latest/shared-bindings/neopixel_write/__init__.html
import time

import board
import neopixel_write as npw
import digitalio

pin = digitalio.DigitalInOut(board.NEOPIXEL)
pin.direction = digitalio.Direction.OUTPUT
pixel_off = bytearray([0, 0, 0])
npw.neopixel_write(pin, pixel_off)

max_bright = 128
for g in range(0, max_bright, 2):
    for r in range(0, max_bright, 2):
        for b in range(0, max_bright, 2):
            npw.neopixel_write(pin, bytearray([g, r, b]))
