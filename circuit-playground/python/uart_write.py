# See:
# https://learn.adafruit.com/circuitpython-essentials/circuitpython-uart-serial
# https://www.adafruit.com/product/954 (USB to TTL Serial Cable)
# https://cdn-learn.adafruit.com/assets/assets/000/047/156/original/circuit_playground_Adafruit_Circuit_Playground_Express_Pinout.png?1507829017
BAUDRATE = 115200

import board
import busio
import digitalio

led = digitalio.DigitalInOut(board.D13)
led.direction = digitalio.Direction.OUTPUT

uart = busio.UART(board.TX, board.RX, baudrate=BAUDRATE)
uart.write(b"\r\nHello from Circuit Playground Express serial port!\r\n")
for i in range(10):
    uart.write(bytes(str(i), 'utf-8') + b"\r\n")
