//! Build script for integrating C and Assembler code with this project.

fn main() {
    cc::Build::new()
        .file("src/asmhelpers.s")   // Assemble asmhelpers.s
        .file("src/chelpers.c")     // Compile chelpers.c
        .compile("lowlevel");       // Generate liblowlevel.a
    // This line in Cargo.toml will link liblowlevel.a with the Rust code:
    // links = "lowlevel"
    println!("cargo:rerun-if-changed=src/asmhelpers.s");
    println!("cargo:rerun-if-changed=src/chelpers.c");
}
