Rust Embedded Apps for the Circuit Playground Express
=====================================================

This package contains a library crate with utility code for working with the
Circuit Playground Express ("CPX"), and multiple binary crates. Each binary
create is an embedded app that can be turned into a .uf2 file that can be loaded
as firmware for the [Adafruit Circuit Playground Express board](https://www.adafruit.com/product/3333).

Setup
-----

Set up the gcc toolchain and Rust to cross-compile for the Armv6m architecture
as described in the [atsamd repository README](https://github.com/atsamd-rs/atsamd/blob/master/README.md).

Get the [uf2conf.py script](https://github.com/Microsoft/uf2) and put it in your
PATH.

Building
--------

In this directory, run:
```
./build.py <app-name>
```
where ``<app-name>`` matches a source file named: ``src/bin/<app-name>.rs``

The build script will run ``cargo build``, then convert the output ELF file to a
``.bin`` file, then convert the ``.bin`` file to a deployable ``.uf2`` file.  If
a Circuit Playground Express is plugged in to the computer via USB, the build
script will also deploy the ``.uf2`` file to the board.

Running
-------

Plug in the CPX to the host computer via USB cable. Double-click the reset
button to put the CPX in bootloader mode. Re-run the ``build.py`` script as
described above. The compiled ``.uf2`` file will be deployed, and will start
running immediately.

If the code you want is already deployed to the CPX, just press the reset button
once. The CPX will restart (takes about 1 second) and re-run the code from the
beginning.

Connecting to the Alternate Serial Port from Linux
--------------------------------------------------

Get a USB-serial adapter such as [this one from Adafruit](https://www.adafruit.com/product/954).

Make the following circuit connections:

- USB-serial adapter ground (black wire) -> Circuit Playground Express GND pin
- USB-serial adapter TX signal (green wire) -> Circuit Playground Express RX/A6 pin
- USB-serial adapter RX signal (white wire) -> Circuit Playground Express TX/A7 pin
- USB-serial adaptier 5-volts (red wire) -> If you are powering the Circuit Playground Express from the USB-serial adapter, connect the red wire to Vout pin (unregulated voltage). Otherwise, cover it with tape or otherwise insulate it so it doesn't short out anything. DO NOT CONNECT IT TO ANY OF THE 3.3V PINS.

On the Linux host, run this ``picocom`` command:
```
picocom -b 115200 /dev/ttyUSB0
```

Press Ctrl-A Ctrl-Q to quit ``picocom`` but leave the serial port configured.

To be able to enter line-oriented input, disconnect ``picocom`` and run:
```
socat readline file:/dev/ttyUSB0,b115200
```

On the Circuit Playground Express, run code like this to initialize, transmit and recieve (CircuitPython example):
```
BAUDRATE = 115200
import board
import busio
uart = busio.UART(board.TX, board.RX, baudrate=BAUDRATE)
uart.write(b"\r\nHello from Circuit Playground Express serial port!\r\n")
data = uart.read(1)  # blocking read up to 32 bytes
```

Timing and Performance
----------------------

According to the SYST::get_ticks_per_10ms() method,
the system timer clock rate is 8MHz; one tick is 0.125 microseconds.

But according to the implementation of Delay at
https://atsamd-rs.github.io/atsamd/atsamd21g18a/src/atsamd21_hal/delay.rs.html#11-14
and confirmed by experiment, one tick of the system timer is equal to the period
of the main processor clock at 48MHz.

Minimum delay between two back-to-back timer reads: 2 to 3 ticks

GPIO digital read: 22 to 28 ticks

GPIO digital write: 25 to 31 ticks

Calling syst.has_wrapped(): 4 to 7 ticks

if syst.has_wrapped() { break; }: 10 to 17 ticks

Calling SYST::get_ticks_per_10ms(): 4 to 5 ticks

Accessing chip.ticks_per_10ms: 3 to 4 ticks

Setting the Real Time Clock time
--------------------------------

To set the DS3231 RTC time to the current UTC time:

- Connect the DS3231 to the labeled I2C pads on the CPX
- Run clockctl.rs and press button B on the CPX to get in time setting mode.
- Disconnect picocom from the serial port connected to the CPX by pressing Ctrl-A Ctrl-Q
- Run this command to send the current UTC time to the CPX:

```
./sendtime.py > /dev/ttyUSB0
```

This should result in the RTC being set to the correct UTC time to within
milliseconds. Compare clockctl.rs output with: https://www.time.gov/index.html
