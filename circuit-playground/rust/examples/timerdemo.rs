//! Circuit Playground Express example: High resolution timer
#![no_std]
#![no_main]

extern crate panic_halt;

use cortex_m_rt::entry;
use cpx_apps::chip;

#[entry]
fn main() -> ! {
    chip::timer_demo(chip::Chip::new());
}
