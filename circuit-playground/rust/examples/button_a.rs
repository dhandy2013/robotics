//! Circuit Playground Express example: Light red LED when button A pressed
#![no_std]
#![no_main]

extern crate panic_halt;

use core::fmt::Write; // needed for writeln!()
use cortex_m_rt::entry;
use cpx_apps::Chip;

#[entry]
fn main() -> ! {
    let mut chip = Chip::new();
    writeln!(chip.sercom, "\nRed LED lights up when button A is pressed").ok();

    loop {
        chip.set_red_led(chip.get_button_a());
    }
}
