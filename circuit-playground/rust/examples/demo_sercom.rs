//! David's UART sercom demo app for the Circuit Playground Express
#![no_std]
#![no_main]

extern crate panic_halt;

use core::fmt::Write; // needed for writeln!()
use core::str::FromStr;
use cortex_m_rt::entry;
use cpx_apps::{Chip, SerialComm};


/// Return true iff the byte is ASCII whitespace
fn is_whitespace(byte: u8) -> bool {
    match byte {
        b' ' => true,
        b'\n' => true,
        b'\r' => true,
        b'\t' => true,
        _ => false,
    }
}

/// Return the subset of string slice s with leading and trailing
/// ASCII whitespace bytes removed.
fn trim(s: &str) -> &str {
    let mut start = s.len();
    for (i, byte) in s.bytes().enumerate() {
        if !is_whitespace(byte) {
            start = i;
            break;
        }
    }
    let s = &s[start..];
    let mut end = 0usize;
    for (i, byte) in s.bytes().rev().enumerate() {
        if !is_whitespace(byte) {
            end = s.len() - i;
            break;
        }
    }
    &s[..end]
}

/// Display a prompt and input a value from a line of bytes.
/// The line must be no longer than 128 bytes including newline.
/// Leading and trailing whitespace bytes are removed before conversion.
fn input<T>(sercom: &mut SerialComm, prompt: &str) -> Option<T>
    where T: Copy + FromStr,
{
    sercom.write_bytes(prompt.as_bytes());
    let mut buf = [0u8; 128];
    if let Some(s) = sercom.read_line_str(&mut buf) {
        match T::from_str(trim(s)) {
            Ok(result) => Some(result),
            Err(_) => None,
        }
    } else {
        None
    }
}

#[entry]
fn main() -> ! {
    let mut chip = Chip::new();
    writeln!(chip.sercom, "\nHello from demo_sercom!").ok();

    loop {
        let maybe_a = input::<u32>(&mut chip.sercom, "Enter integer A: ");
        let maybe_b = input::<u32>(&mut chip.sercom, "Enter integer B: ");
        if let (Some(a), Some(b)) = (maybe_a, maybe_b) {
            writeln!(chip.sercom, "A + B = {}", a + b).ok();
            break;
        }
        writeln!(chip.sercom, "Enter two valid unsigned integers please.").ok();
    }

    writeln!(chip.sercom, "Done.").ok();
    loop {
        chip.set_red_led(true);
        chip.delay_us(1_000_000);
        chip.set_red_led(false);
        chip.delay_us(1_000_000);
    }
}
