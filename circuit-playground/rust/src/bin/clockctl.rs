//! Circuit Playground Express app: clock control program
//! Connections between Circuit Playground Express and Real-Time Clock:
//! CPX     DS3231
//! ------  ------
//! A4/SCL  SCL
//! A5/SDA  SDA
//! A0/D12  32.768kHz output
//! A1/D6   SQW square wave output (optional based on sqw_input feature)
#![no_std]
#![no_main]

extern crate panic_halt;

use circuit_playground_express as hal;
use core::fmt::Write; // needed for writeln!()
use core::str::FromStr;
use cortex_m_rt::entry;
use cpx_apps::{Chip, EdgeTrigger, SerialComm};
use cpx_apps::hitime::{NUM_CALIBRATION_POINTS, SpinTimer};
use ds323x::{self, DateTime, Ds323x, Hours};

type DS323x = Ds323x<
    ds323x::interface::I2cInterface<
        hal::sercom::I2CMaster5<
            hal::sercom::Sercom5Pad0<hal::gpio::Pb2<hal::gpio::PfD>>,
            hal::sercom::Sercom5Pad1<hal::gpio::Pb3<hal::gpio::PfD>>
        >
    >,
    ds323x::ic::DS3231
>;

const START_MESSAGE: &[u8] = b"\r\nDS3231 Clock Control Program\r\n";
const NUM_TESTS: usize = 32;

/// Read the current time from the DS3231 clock and print it.
/// Does not print a trailing newline.
fn show_datetime(mut out: &mut SerialComm, rtc: &mut DS323x) {
    match rtc.get_datetime() {
        Result::Ok(datetime) => {
            format_datetime(&mut out, &datetime);
        },
        Err(err) => {
            write!(out, "Error reading datetime from RTC: {:#?}", err).ok();
        },
    };
}

fn format_datetime<W: Write>(out: &mut W, datetime: &DateTime) {
    let hours = match datetime.hour {
        Hours::H24(h) | Hours::AM(h) => h,
        Hours::PM(h) => h + 12,
    };
    write!(out,
        "{:04}-{:02}-{:02} {:02}:{:02}:{:02}",
        datetime.year, datetime.month, datetime.day,
        hours, datetime.minute, datetime.second).ok();
}

fn calibrate_cpu_clock(chip: &mut Chip) {
    writeln!(chip.sercom, "Start!").ok();
    let mut timer = SpinTimer::new(chip, |chip: &Chip| { chip.get_d12() });
    for _ in 0..5_000_000 {
        timer.update(&chip);
    }
    writeln!(chip.sercom, "Stop! {:.6} seconds",
             (timer.timestamp().as_micros() as f64) / 1_000_000.0).ok();

    chip.init_timer();
    let t0 = Chip::get_ticks();
    chip.sercom.write_bytes(b".");
    let t1 = Chip::get_ticks();
    writeln!(chip.sercom,
             "Writing one byte to serial port: {} CPU ticks.", t0 - t1).ok();

    writeln!(chip.sercom,
             "\nMaking {} measurements of period of 32.768kHz clock signal",
             NUM_TESTS).ok();
    let mut tick_counts = [0u32; NUM_TESTS + 1];

    chip.init_timer();
    while chip.get_d12() { } // wait for signal to be low
    for i in 0..NUM_TESTS + 1 {
        while !chip.get_d12() { } // wait for signal to go high
        tick_counts[i] = Chip::get_ticks();
        while chip.get_d12() { } // wait for signal to go low
    }
    writeln!(chip.sercom, "Clock period tick counts:").ok();
    for k in 0..(NUM_TESTS / 8) {
        for j in 0..8 {
            let i = (k * 8) + j;
            if i >= NUM_TESTS { break; }
            write!(chip.sercom,
                " {:5}", tick_counts[i] - tick_counts[i + 1]
            ).ok();
        }
        chip.sercom.write_bytes(b"\r\n");
    }
    writeln!(chip.sercom,
        "Average ticks per 32.768kHz cycle: {:.6}",
        (tick_counts[0] - tick_counts[NUM_TESTS]) as f64 / (NUM_TESTS as f64)
    ).ok();
}

fn cpu_interval(t0: u32, t1: u32) -> u32 {
    if t0 >= t1 { t0 - t1 } else { Chip::max_ticks() - t0 - t1 + 1 }
}

fn try_spin_timer(chip: &mut Chip) {
    writeln!(chip.sercom, "\nTrying out SpinTimer").ok();
    let mut timer = SpinTimer::new(chip, |chip: &Chip| { chip.get_d12() });

    // Measure minimum time delta
    let t0 = timer.timestamp();
    timer.update(&chip);
    let t1 = timer.timestamp();

    // Measure overhead of update(), timestamp(), and get_ticks()
    let t2 = Chip::get_ticks();
    timer.update(&chip);
    let t3 = Chip::get_ticks();
    let t4 = timer.timestamp();
    let t5 = Chip::get_ticks();
    let t6 = t4.as_micros();
    let t7 = Chip::get_ticks();

    // Measure 0.25 second interval
    while chip.get_d12() { timer.update(&chip); }
    while !chip.get_d12() { timer.update(&chip); }
    let t8 = timer.timestamp();
    for _ in 0..8192 {
        while chip.get_d12() { timer.update(&chip); }
        while !chip.get_d12() { timer.update(&chip); }
    }
    let t9 = timer.timestamp();

    writeln!(chip.sercom, "").ok();
    writeln!(chip.sercom,
        "NUM_CALIBRATION_POINTS: {}", NUM_CALIBRATION_POINTS).ok();
    writeln!(chip.sercom,
        "calibration_total: {}", timer.calibration_total()).ok();
    writeln!(chip.sercom,
        "After new()         : {:7} us {:5} ext ticks {:8} cpu count",
        t0.as_micros(), t0.ext_ticks(), t0.cpu_count()).ok();
    writeln!(chip.sercom,
        "After first update(): {:7} us {:5} ext ticks {:8} cpu count",
        t1.as_micros(), t1.ext_ticks(), t1.cpu_count()).ok();

    writeln!(chip.sercom, "").ok();
    writeln!(chip.sercom,
        "SpinTimer.update() overhead   : {:4} CPU cycles",
        cpu_interval(t2, t3)).ok();
    writeln!(chip.sercom,
        "SpinTimer.timestamp() overhead: {:4} CPU cycles",
        cpu_interval(t3, t5)).ok();
    writeln!(chip.sercom,
        "SpinTimer.as_micros() overhead: {:4} CPU cycles (returned {} us)",
        cpu_interval(t5, t7), t6).ok();

    writeln!(chip.sercom, "").ok();
    writeln!(chip.sercom,
        "8K cycles of 32K ext clock: expected: 250000 us, actual: {} us",
        t9.as_micros() - t8.as_micros()).ok();
}

#[cfg_attr(not(feature = "sqw_input"), allow(dead_code))]
fn try_spin_timer_with_1hz_clock(chip: &mut Chip) {
    writeln!(chip.sercom, "\nTrying out SpinTimer with 1Hz clock").ok();
    let mut timer = SpinTimer::new(chip, |chip: &Chip| { chip.get_d12() });

    // Measure 500ms half-wave of 1Hz clock
    while chip.get_d6() { timer.update(&chip); }
    while !chip.get_d6() { timer.update(&chip); }
    let t1 = timer.timestamp();
    while chip.get_d6() { timer.update(&chip); }
    let t2 = timer.timestamp();
    while !chip.get_d6() { timer.update(&chip); }
    let t3 = timer.timestamp();

    writeln!(chip.sercom,
        "1Hz Clock goes high: {:7} us {:5} ext ticks {:8} cpu count",
        t1.as_micros(), t1.ext_ticks(), t1.cpu_count()).ok();
    writeln!(chip.sercom,
        "1Hz Clock goes low : {:7} us {:5} ext ticks {:8} cpu count",
        t2.as_micros(), t2.ext_ticks(), t2.cpu_count()).ok();
    writeln!(chip.sercom,
        "1Hz Clock goes high: {:7} us {:5} ext ticks {:8} cpu count",
        t3.as_micros(), t3.ext_ticks(), t3.cpu_count()).ok();
}

/// Read date and time value from string slice.
/// Return Some(datetime) if it is in valid YYYY-MM-DD HH:MM:SS 24-hour format,
/// None otherwise.
/// Note: This sets the weekday value to 1, whatever that means.
fn parse_datetime(s: &str) -> Option<DateTime> {
    if s.len() < 19 {
        return None
    }

    let mut datetime = DateTime {
        year: 2000,
        month: 1,
        day: 1,
        weekday: 1,
        hour: Hours::H24(0),
        minute: 0,
        second: 0,
    };
    datetime.year = match u16::from_str(&s[0..4]) {
        Ok(y) => if y <= 2099 { y } else { return None },
        Err(_) => return None,
    };
    datetime.month = match u8::from_str(&s[5..7]) {
        Ok(m) => if m >= 1 && m <= 12 { m } else { return None },
        Err(_) => return None,
    };
    datetime.day = match u8::from_str(&s[8..10]) {
        Ok(d) => if d >= 1 && d <= 31 { d } else { return None },
        Err(_) => return None,
    };
    datetime.hour = Hours::H24(
        match u8::from_str(&s[11..13]) {
            Ok(h) => if h <= 23 { h } else { return None },
            Err(_) => return None,
        }
    );
    datetime.minute = match u8::from_str(&s[14..16]) {
        Ok(m) => if m <= 59 { m } else { return None },
        Err(_) => return None,
    };
    datetime.second = match u8::from_str(&s[17..19]) {
        Ok(s) => if s <= 59 { s } else { return None },
        Err(_) => return None,
    };

    Some(datetime)
}

/// Write prompt and read line from serial port, then set the RTC datetime.
/// If the datetime string is invalid, print an error message and return false,
/// else set the RTC and quietly return true.
fn set_rtc_clock(io: &mut SerialComm, rtc: &mut DS323x) -> bool {
    io.write_bytes(b"Send UTC time in 24 hour format: YYYY-MM-DD HH:MM:SS\r\n");
    let mut buf = [0u8; 21]; // formatted time + CR + LF
    if let Some(s) = io.read_line_str(&mut buf) {
        if let Some(datetime) = parse_datetime(s) {
            match rtc.set_datetime(&datetime) {
                Ok(_) => { },
                Err(err) => {
                    writeln!(io, "ERROR: {:?}", err).ok();
                    return false;
                }
            }
        } else {
            io.write_bytes(b"ERROR: Text not in valid datetime format\r\n");
            return false;
        }
    } else {
        io.write_bytes(b"ERROR: Invalid text sent\r\n");
        return false;
    }
    true
}

#[entry]
fn main() -> ! {
    let mut chip = Chip::new();
    chip.sercom.write_bytes(START_MESSAGE);

    let mut button_a = EdgeTrigger::new(true,
                                        |chip: &Chip| chip.get_button_a());
    let mut button_b = EdgeTrigger::new(true,
                                        |chip: &Chip| chip.get_button_b());
    chip.configure_d12_pull_up_input(); // pad A0
    #[cfg(not(feature = "sqw_input"))]
    let mut ext_32k_clk = EdgeTrigger::new(true, |chip: &Chip| chip.get_d12());

    #[cfg(feature = "sqw_input")]
    let mut onehertz = EdgeTrigger::new(true, |chip: &Chip| !chip.get_d6());
    #[cfg(feature = "sqw_input")]
    chip.configure_d6_pull_up_input(); // pad A1

    let i2c_master = chip.create_i2c_master();
    let mut rtc = Ds323x::new_ds3231(i2c_master.unwrap());
    rtc.set_square_wave_frequency(ds323x::SqWFreq::_1Hz).ok();
    rtc.use_int_sqw_output_as_square_wave().ok();

    loop {
        chip.sercom.write_bytes(b"\r\n");
        chip.sercom.write_bytes(b"Press button A to calibrate CPU clock\r\n");
        chip.sercom.write_bytes(b"Press button B to set RTC clock time\r\n");
        let mut red_led_state = true;
        let (mut a_triggered, mut b_triggered);
        'clock_mode: loop {
            chip.set_red_led(red_led_state);

            #[cfg(not(feature = "sqw_input"))]
            let mut button_loop_count: u32 = 0;
            loop {
                a_triggered = button_a.triggered(&chip);
                b_triggered = button_b.triggered(&chip);
                if a_triggered || b_triggered {
                    break 'clock_mode;
                }

                #[cfg(feature = "sqw_input")]
                {
                    // Break when 1Hz clock signal goes low signifying change
                    if onehertz.triggered(&chip) { break; }
                }
                #[cfg(not(feature = "sqw_input"))]
                {
                    // Use 32.768kHz ext clock to break at 1 sec.
                    if ext_32k_clk.triggered(&chip) { button_loop_count += 1; }
                    if button_loop_count >= 32768 {
                        break;
                    }
                }
            }

            show_datetime(&mut chip.sercom, &mut rtc);
            chip.sercom.write_bytes(b"\r");
            red_led_state = !red_led_state;
        }
        chip.sercom.write_bytes(b"\n");
        if a_triggered {
            calibrate_cpu_clock(&mut chip);
            try_spin_timer(&mut chip);

            #[cfg(feature = "sqw_input")]
            try_spin_timer_with_1hz_clock(&mut chip);
        } else if b_triggered {
            set_rtc_clock(&mut chip.sercom, &mut rtc);
        }
    }
}
