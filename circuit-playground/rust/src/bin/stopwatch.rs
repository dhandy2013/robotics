//! Circuit Playground Express app: Stopwatch
#![no_std]
#![no_main]

extern crate panic_halt;

use core::fmt::Write; // needed for writeln!()
use cortex_m_rt::entry;
use cpx_apps::{Chip, EdgeTrigger};

const START_MESSAGE: &[u8] =
    b"\r\nStopwatch: Button A start/stop, Button B: mark/clear\r\n";

/// Display the current time given a number of centiseconds
fn display_current_time(chip: &mut Chip, total_cs: u32) {
    let cs = total_cs % 100;
    let total_seconds = total_cs / 100;
    let seconds = total_seconds % 60;
    let total_minutes = total_seconds / 60;
    let minutes = total_minutes % 60;
    let total_hours = total_minutes / 60;
    let hours = total_hours % 60;
    write!(chip.sercom, "{:02}:{:02}:{:02}.{:02}\r",
           hours, minutes, seconds, cs).ok();
}

#[entry]
fn main() -> ! {
    let mut chip = Chip::new();
    chip.sercom.write_bytes(START_MESSAGE);

    let clock_f = chip.ticks_per_sec();
    let interval_f: u32 = 100; // Hz
    let mut total_ticks: u64; // wraps after more than 12K years
    let mut total_cs: u32; // wraps after 497 days
    let mut button_a = EdgeTrigger::new(true,
                                        |chip: &Chip| chip.get_button_a());
    let mut button_b = EdgeTrigger::new(true,
                                        |chip: &Chip| chip.get_button_b());
    chip.init_timer();

    'mainloop: loop {
        total_ticks = 0;
        total_cs = 0;
        chip.sercom.write_bytes(b"\r\n");
        display_current_time(&mut chip, total_cs);

        // Wait for button A to transition low to high
        while !button_a.triggered(&chip) { }

        'runtimer: loop {
            chip.reset_timer();
            let mut prev_ticks = Chip::max_ticks() - Chip::get_ticks();
            loop {
                // Get the effective total time in ticks
                let cur_ticks = Chip::max_ticks() - Chip::get_ticks();
                if cur_ticks <= prev_ticks {
                    // Wraparound
                    total_ticks += (Chip::max_ticks() + 1) as u64;
                }
                prev_ticks = cur_ticks;
                let eff_total_ticks = total_ticks + (cur_ticks as u64);

                // Calculate the total time in centiseconds
                let mut cur_cs = ((eff_total_ticks * (interval_f as u64))
                    / (clock_f as u64)) as u32;

                // Adjust for clock skew (based on calibration)
                cur_cs = ((cur_cs as u64 * 1_009_555) / 1_000_000) as u32;

                // Show the current total centiseconds if it has changed
                if cur_cs == total_cs {
                    continue;
                }
                total_cs = cur_cs;
                display_current_time(&mut chip, total_cs);

                // Stop if button A pressed, mark if button B pressed
                if button_a.triggered(&chip) {
                    chip.sercom.write_bytes(b"\n");
                    break;
                }
                if button_b.triggered(&chip) {
                    chip.sercom.write_bytes(b"\n");
                }
            }

            // Wait for button A (to resume) or button B (to clear)
            loop {
                if button_a.triggered(&chip) {
                    continue 'runtimer;
                }

                if button_b.triggered(&chip) {
                    continue 'mainloop;
                }
            }
        }
    }
}
