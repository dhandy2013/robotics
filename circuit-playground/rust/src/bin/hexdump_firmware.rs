//! Dump the entire 256kb contents of the SAMD21G18A firmware in hex format.
#![no_std]
#![no_main]

extern crate panic_halt;

use core::fmt::Write; // needed for write!(), writeln!()
use cortex_m_rt::entry;
use cpx_apps::Chip;

/// Return the byte at the integer memory location.
/// I wrote this function because I need to read memory starting at address
/// zero, and Rust doesn't let you form slices starting at address zero.
/// (It refuses to iterate over them, stops iteration early.)
unsafe fn peek_byte(loc: usize) -> u8 {
    *(loc as *const u8)
}

#[entry]
fn main() -> ! {
    let mut chip = Chip::new();
    let start_loc: usize = 0;
    let size: usize = 256 * 1024;
    writeln!(
        chip.sercom,
        "\n# BEGIN HEXDUMP: {} bytes starting at 0x{:08x}",
        size, start_loc
    )
    .ok();

    for offset in 0..size {
        let byte = unsafe { peek_byte(start_loc + offset) };
        write!(chip.sercom, "{:02X}", byte).ok();
        if (offset & 0xf) == 0xf {
            chip.set_red_led(true);
            write!(chip.sercom, "\n").ok();
            chip.set_red_led(false);
        } else {
            write!(chip.sercom, " ").ok();
        }
    }
    if (size & 0xf) != 0 {
        writeln!(chip.sercom, "").ok();
    }

    writeln!(chip.sercom, "# END HEXDUMP").ok();
    loop {
        chip.set_red_led(true);
        chip.delay_us(1_000_000);
        chip.set_red_led(false);
        chip.delay_us(1_000_000);
    }
}
