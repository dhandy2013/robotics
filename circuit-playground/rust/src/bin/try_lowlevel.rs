//! Circuit Playground Express app: Try out low-level routines
#![no_std]
#![no_main]

extern crate panic_halt;

use core::fmt::Write; // needed for writeln!()
use cortex_m_rt::entry;
use cpx_apps::Chip;
use cpx_apps::lowlevel::{asm_add, RegisterBuf};

#[entry]
fn main() -> ! {
    let mut chip = Chip::new();
    writeln!(chip.sercom, "\nTrying out low-level routines!").ok();

    writeln!(chip.sercom, "\nAbout to call asm_add...").ok();
    let a = 2;
    let b = 3;
    let c = unsafe { asm_add(a, b) };
    writeln!(chip.sercom, "{} + {} = {}", a, b, c).ok();

    let mut reg = RegisterBuf::new();
    reg.load();
    writeln!(chip.sercom, "r0  = 0x{:08x}", reg.r0).ok();
    writeln!(chip.sercom, "r1  = 0x{:08x}", reg.r1).ok();
    writeln!(chip.sercom, "r2  = 0x{:08x}", reg.r2).ok();
    writeln!(chip.sercom, "r3  = 0x{:08x}", reg.r3).ok();
    writeln!(chip.sercom, "r4  = 0x{:08x}", reg.r4).ok();
    writeln!(chip.sercom, "r5  = 0x{:08x}", reg.r5).ok();
    writeln!(chip.sercom, "r6  = 0x{:08x}", reg.r6).ok();
    writeln!(chip.sercom, "r7  = 0x{:08x}", reg.r7).ok();
    writeln!(chip.sercom, "r8  = 0x{:08x}", reg.r8).ok();
    writeln!(chip.sercom, "r9  = 0x{:08x}", reg.r9).ok();
    writeln!(chip.sercom, "r10 = 0x{:08x}", reg.r10).ok();
    writeln!(chip.sercom, "r11 = 0x{:08x}", reg.r11).ok();
    writeln!(chip.sercom, "r12 = 0x{:08x}", reg.r12).ok();
    writeln!(chip.sercom, "sp  = 0x{:08x}", reg.sp).ok();
    writeln!(chip.sercom, "lr  = 0x{:08x}", reg.lr).ok();
    writeln!(chip.sercom, "pc  = 0x{:08x}", reg.pc).ok();

    loop {
        chip.set_red_led(true);
        chip.delay_us(1_000_000u32);
        chip.set_red_led(false);
        chip.delay_us(1_000_000u32);
    }
}
