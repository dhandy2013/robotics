//! Circuit Playground Express app: onehertz
//! Output to serial port a timing signal every second
#![no_std]
#![no_main]

extern crate panic_halt;

use core::fmt::Write; // needed for writeln!()
use cortex_m_rt::entry;
use cpx_apps::Chip;

#[entry]
fn main() -> ! {
    let mut chip = Chip::new();

    let clock_f = chip.ticks_per_sec();
    let interval_f: u32 = 1; // Hz
    let mut total_ticks: u64 = 0; // wraps after more than 12K years
    let mut total_sec: u32 = 0; // wraps after 136 years
    write!(chip.sercom, "\n{}\n", total_sec).ok();
    chip.init_timer();
    let mut prev_ticks = Chip::max_ticks() - Chip::get_ticks();

    loop {
        // Get the effective total time in ticks
        let cur_ticks = Chip::max_ticks() - Chip::get_ticks();
        if cur_ticks <= prev_ticks {
            // Wraparound
            total_ticks += (Chip::max_ticks() + 1) as u64;
        }
        prev_ticks = cur_ticks;
        let eff_total_ticks = total_ticks + (cur_ticks as u64);

        // Calculate the total time in seconds
        let cur_sec = ((eff_total_ticks * (interval_f as u64))
            / (clock_f as u64)) as u32;
        if cur_sec == total_sec {
            continue;
        }
        total_sec = cur_sec;
        write!(chip.sercom, "{}\n", total_sec).ok();

        // Toggle red LED on/off every second
        chip.set_red_led((total_sec & 1) == 0);
    }
}
