//! Circuit Playground Express app: Blink morse code "V"
#![no_std]
#![no_main]

extern crate panic_halt;

use core::fmt::Write; // needed for writeln!()
use cortex_m_rt::entry;
use cpx_apps::Chip;

fn dit(chip: &mut Chip) {
    chip.set_red_led(true);
    chip.delay_us(250_000);
    chip.set_red_led(false);
    chip.delay_us(250_000);
}

fn dah(chip: &mut Chip) {
    chip.set_red_led(true);
    chip.delay_us(750_000);
    chip.set_red_led(false);
    chip.delay_us(250_000);
}

#[entry]
fn main() -> ! {
    let mut chip = Chip::new();
    writeln!(chip.sercom, "\nSending the letter 'V' in Morse code").ok();

    loop {
        dit(&mut chip);
        dit(&mut chip);
        dit(&mut chip);
        dah(&mut chip);
        write!(chip.sercom, ".").ok();
        chip.delay_us(2_000_000);
    }
}
