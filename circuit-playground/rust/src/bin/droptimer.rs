//! Circuit Playground Express app: Drop Timer
//! Record precise time falling objects pass photosensors in a tube
#![no_std]
#![no_main]

extern crate panic_halt;

use core::fmt::Write; // needed for writeln!()
use cortex_m_rt::entry;
use cpx_apps::Chip;
use cpx_apps::hitime::{SpinTimer, TimeStamp};

const NUM_SENSORS: usize = 3;

fn get_stable_switch_state(chip: &mut Chip) -> bool {
    // Make sure the switch is not in a transient state
    let mut switch_state = false;
    let mut stable_switch_count: u32 = 0;
    while stable_switch_count < 10 {
        if chip.get_slide_switch() == switch_state {
            stable_switch_count += 1;
        } else {
            stable_switch_count = 0;
            switch_state = !switch_state;
        }
        chip.delay_us(1000);
    }
    switch_state
}

fn calibration_loop(mut chip: Chip) -> ! {
    writeln!(chip.sercom, "\nDrop Timer: Calibration Mode").ok();
    writeln!(chip.sercom, "Sensor values starting with #0 on the left").ok();
    let mut prev_sensor_state: u32 = 0xffff_ffff;
    let mut out_buf = [0u8; NUM_SENSORS + 2];
    out_buf[NUM_SENSORS] = b'\r';
    out_buf[NUM_SENSORS + 1] = b'\n';
    loop {
        let sensor_state: u32 =
            chip.get_d6() as u32
            | ((chip.get_d9() as u32) << 1)
            | ((chip.get_d10() as u32) << 2)
        ;
        chip.set_red_led(sensor_state > 0);
        if prev_sensor_state != sensor_state {
            for i in 0..NUM_SENSORS {
                out_buf[i] =
                    if (sensor_state & (1 << i)) > 0 { b'1' } else { b'0' };
            }
            chip.sercom.write_bytes(&out_buf);
        }
        prev_sensor_state = sensor_state;
    }
}

fn report_times(chip: &mut Chip, trial_num: u32, timestamps: &[TimeStamp]) {
    write!(chip.sercom, "{}", trial_num).ok();
    for ts in timestamps.iter() {
        let t = ts.as_micros() as f64 / 1_000_000.0;
        write!(chip.sercom, ",{:.6}", t).ok();
    }
    writeln!(chip.sercom, "").ok();
}

fn timer_loop(mut chip: Chip) -> ! {
    // Flash red LED on while we are sending text to serial output
    chip.set_red_led(true);
    writeln!(chip.sercom, "\nDrop Timer: Timing Mode").ok();
    writeln!(chip.sercom, "Drop an object down the tube repeatedly.").ok();
    writeln!(chip.sercom,
        "Each trial result will be reported as a row in CSV format.").ok();
    writeln!(chip.sercom, "\ntrial,t0,t1,t2").ok();

    let mut timestamps = [TimeStamp::new(); NUM_SENSORS];
    let mut trial_count: u32 = 0;

    loop {
        chip.set_red_led(false);
        trial_count += 1;
        let mut timer = SpinTimer::new(&mut chip,
                                       |chip: &Chip| { chip.get_d12() });

        // Wait for sensor #0
        while !chip.get_d6() { timer.update(&chip); }
        timestamps[0] = timer.timestamp();

        // Wait for sensor #1
        while !chip.get_d9() { timer.update(&chip); }
        timestamps[1] = timer.timestamp();

        // Wait for sensor #2
        while !chip.get_d10() { timer.update(&chip); }
        timestamps[2] = timer.timestamp();

        // Report
        chip.set_red_led(true);
        report_times(&mut chip, trial_count, &timestamps);
    }
}

#[entry]
fn main() -> ! {
    let mut chip = Chip::new();
    chip.configure_d12_pull_up_input(); // pad A0
    if get_stable_switch_state(&mut chip) {
        // slide switch == true == left == drop timer mode
        writeln!(chip.sercom, "\nRESET; slide switch is to the left").ok();
        timer_loop(chip);
    } else {
        // slide switch == false == right == calibration mode
        writeln!(chip.sercom, "\nRESET; slide switch is to the right").ok();
        calibration_loop(chip);
    }
}
