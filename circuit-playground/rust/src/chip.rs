//! Chip Resources encapsulation module
use circuit_playground_express as hal;
use core::fmt::Write; // needed for writeln!()
use core::mem;
use cortex_m::peripheral::SYST;
use crate::sercom::SerialComm;
use hal::clock::GenericClockController;
use hal::gpio::Parts;
use hal::pac::gclk::clkctrl::GEN_A;
use hal::pac::gclk::genctrl::SRC_A;
use hal::pac::{CorePeripherals, Peripherals};
use hal::prelude::*; // needed for .set_high(), .set_low(), and more
use hal::sercom::{I2CMaster5, PadPin, Sercom4Pad0, Sercom4Pad1, UART4};
use hal::time::{Hertz, KiloHertz};

// For a full list of signals see column "circuitplayground_express" in:
// https://github.com/adafruit/circuitpython/blob/master/ports/atmel-samd/README.rst

// PA17 == samd21g18a pin 26 == Circuit Playground Express signal D13
type RedLED = hal::gpio::Pa17<hal::gpio::Output<hal::gpio::OpenDrain>>;

// PA28 == samd21g18a pin 41 == Circuit Playground Express signal D4
type ButtonA = hal::gpio::Pa28<hal::gpio::Input<hal::gpio::PullDown>>;

// PA14 == samd21g18a pin 23 == Circuit Playground Express signal D5
type ButtonB = hal::gpio::Pa14<hal::gpio::Input<hal::gpio::PullDown>>;

// PA15 == samd21g18a pin 24 == Circuit Playground Express signal D7
type SlideSwitch = hal::gpio::Pa15<hal::gpio::Input<hal::gpio::PullUp>>;

// PA30 == samd21g18a pin ?? == Circuit Playground Express signal SPEAKER_ENABLE
type SpeakerEnable = hal::gpio::Pa30<hal::gpio::Output<hal::gpio::PushPull>>;

// PA02 == samd21g18a pin 3 == Circuit Playground Express signal D12/A0
pub enum PinA0 {
    DigitalInFloat(hal::gpio::Pa2<hal::gpio::Input<hal::gpio::Floating>>),
    DigitalInPullUp(hal::gpio::Pa2<hal::gpio::Input<hal::gpio::PullUp>>),
    DigitalOutPushPull(hal::gpio::Pa2<hal::gpio::Output<hal::gpio::PushPull>>),
    Taken,
}

// PA05 == samd21g18a pin 10 == Circuit Playground Express signal D6/A1
pub enum PinA1 {
    DigitalInFloat(hal::gpio::Pa5<hal::gpio::Input<hal::gpio::Floating>>),
    DigitalInPullUp(hal::gpio::Pa5<hal::gpio::Input<hal::gpio::PullUp>>),
    DigitalOutPushPull(hal::gpio::Pa5<hal::gpio::Output<hal::gpio::PushPull>>),
    Taken,
}

// PA06 == samd21g18a pin 11 == Circuit Playground Express signal D9/A2
pub enum PinA2 {
    DigitalInFloat(hal::gpio::Pa6<hal::gpio::Input<hal::gpio::Floating>>),
    DigitalInPullUp(hal::gpio::Pa6<hal::gpio::Input<hal::gpio::PullUp>>),
    DigitalOutPushPull(hal::gpio::Pa6<hal::gpio::Output<hal::gpio::PushPull>>),
    Taken,
}

// PA07 == samd21g18a pin 12 == Circuit Playground Express signal D10/A3
pub enum PinA3 {
    DigitalInFloat(hal::gpio::Pa7<hal::gpio::Input<hal::gpio::Floating>>),
    DigitalInPullUp(hal::gpio::Pa7<hal::gpio::Input<hal::gpio::PullUp>>),
    DigitalOutPushPull(hal::gpio::Pa7<hal::gpio::Output<hal::gpio::PushPull>>),
    Taken,
}

// PB03 == samd21g18a pin 48 == Circuit Playground Express signal D3/A4
pub enum PinA4 {
    DigitalInFloat(hal::gpio::Pb3<hal::gpio::Input<hal::gpio::Floating>>),
    DigitalInPullUp(hal::gpio::Pb3<hal::gpio::Input<hal::gpio::PullUp>>),
    DigitalOutPushPull(hal::gpio::Pb3<hal::gpio::Output<hal::gpio::PushPull>>),
    Taken,
}

impl PinA4 {
    fn is_taken(&self) -> bool {
        match self {
            Self::Taken => true,
            _ => false,
        }
    }
}

// PB02 == samd21g18a pin 47 == Circuit Playground Express signal D2/A5
pub enum PinA5 {
    DigitalInFloat(hal::gpio::Pb2<hal::gpio::Input<hal::gpio::Floating>>),
    DigitalInPullUp(hal::gpio::Pb2<hal::gpio::Input<hal::gpio::PullUp>>),
    DigitalOutPushPull(hal::gpio::Pb2<hal::gpio::Output<hal::gpio::PushPull>>),
    Taken,
}

impl PinA5 {
    fn is_taken(&self) -> bool {
        match self {
            Self::Taken => true,
            _ => false,
        }
    }
}

type I2CMaster = I2CMaster5<
    hal::sercom::Sercom5Pad0<hal::gpio::Pb2<hal::gpio::PfD>>,
    hal::sercom::Sercom5Pad1<hal::gpio::Pb3<hal::gpio::PfD>>,
>;

pub struct Chip {
    // Clocks and controls
    core: CorePeripherals,
    port: hal::gpio::Port,
    clocks: GenericClockController,
    sysclock: Hertz,
    pm: hal::pac::PM, // power manager
    sercom5: Option<hal::pac::SERCOM5>,

    // Built-in devices
    red_led: RedLED,
    button_a: ButtonA,
    button_b: ButtonB,
    slide_switch: SlideSwitch,

    // General-purpose I/O pins
    pin_a0: PinA0,
    pin_a1: PinA1,
    pin_a2: PinA2,
    pin_a3: PinA3,
    pin_a4: PinA4,
    pin_a5: PinA5,

    // Internal I/O signals
    speaker_enable: SpeakerEnable,

    // External bus and port interfaces
    pub sercom: SerialComm,
}

impl Chip {
    pub fn new() -> Chip {
        let core = CorePeripherals::take().unwrap();
        let mut peripherals = Peripherals::take().unwrap();
        let mut clocks = GenericClockController::with_internal_32kosc(
            peripherals.GCLK,
            &mut peripherals.PM,
            &mut peripherals.SYSCTRL,
            &mut peripherals.NVMCTRL,
        );
        let sysclock = clocks.gclk0().clone().into();

        let Parts {
            mut port,

            pa2, // A0/D12/DAC
            pa5, // A1/D6
            pa6, // A2/D9
            pa7, // A3/D10
            pa14, // Button B (D5)
            pa15, // Slide Switch (D7)
            pa17, // Red LED (D13)
            pa28, // Button A (D4)
            pa30: speaker_enable, // SPEAKER_ENABLE

            pb2, // A5/D3/SDA
            pb3, // A4/D2/SCL
            pb8: tx, // TX/A7
            pb9: rx, // RX/A6

            ..
        } = peripherals.PORT.split();

        let mut pm = peripherals.PM;

        // Configure serial interface
        clocks.configure_gclk_divider_and_source(
            GEN_A::GCLK2, 1, SRC_A::DFLL48M, false);
        let gclk2 = clocks
            .get_gclk(GEN_A::GCLK2)
            .expect("Could not get clock 2");
        let rx: Sercom4Pad1<_> = rx
            .into_pull_down_input(&mut port)
            .into_pad(&mut port);
        let tx: Sercom4Pad0<_> = tx
            .into_pull_down_input(&mut port)
            .into_pad(&mut port);
        let uart = UART4::new(
            &clocks.sercom4_core(&gclk2).unwrap(),
            115200.hz(),
            peripherals.SERCOM4,
            &mut pm,
            (rx, tx),
        );

        let red_led: RedLED = pa17.into_open_drain_output(&mut port);
        let sercom = SerialComm::new(uart);

        let button_a: ButtonA = pa28.into_pull_down_input(&mut port);
        let button_b: ButtonB = pa14.into_pull_down_input(&mut port);
        let slide_switch: SlideSwitch = pa15.into_pull_up_input(&mut port);

        // Multi-purpose pins
        let pin_a0 = PinA0::DigitalInFloat(pa2.into_floating_input(&mut port));
        let pin_a1 = PinA1::DigitalInFloat(pa5.into_floating_input(&mut port));
        let pin_a2 = PinA2::DigitalInFloat(pa6.into_floating_input(&mut port));
        let pin_a3 = PinA3::DigitalInFloat(pa7.into_floating_input(&mut port));
        let pin_a4 = PinA4::DigitalInFloat(pb3.into_floating_input(&mut port));
        let pin_a5 = PinA5::DigitalInFloat(pb2.into_floating_input(&mut port));

        // Internal signals
        let mut speaker_enable: SpeakerEnable =
            speaker_enable.into_push_pull_output(&mut port);
        speaker_enable.set_low().unwrap(); // Turn off the speaker right away!

        Chip {
            core,
            port,
            clocks,
            sysclock,
            pm,
            sercom5: Some(peripherals.SERCOM5),
            red_led,
            button_a,
            button_b,
            slide_switch,
            pin_a0,
            pin_a1,
            pin_a2,
            pin_a3,
            pin_a4,
            pin_a5,
            speaker_enable,
            sercom,
        }
    }

    /// Return the nominal CPU clock frequency in Hz.
    /// Measured CPU clock frequency has been found to be ~1% slower than this.
    pub fn nominal_cpu_clock_freq(&self) -> u32 {
        self.sysclock.0
    }

    /// Delay the given number of system timer ticks
    // See: https://atsamd-rs.github.io/atsamd/atsamd21g18a/src/atsamd21_hal/delay.rs.html#11-14
    pub fn delay_ticks(&mut self, total_ticks: u32) {
        let syst = &mut self.core.SYST;
        const MAX_RVR: u32 = 0x00FF_FFFF;
        let mut ticks = total_ticks;

        while ticks > 0 {
            let current_rvr = if ticks <= (MAX_RVR + 1) {
                ticks - 1
            } else {
                MAX_RVR
            };

            syst.set_reload(current_rvr);
            syst.clear_current();
            syst.enable_counter();

            // Update the tracking variable while we are waiting...
            ticks -= current_rvr + 1;

            while !syst.has_wrapped() {}

            syst.disable_counter();
        }
    }

    /// Convert a time interval in microseconds to ticks
    /// Saturates at 0xffffffff
    pub fn us_to_ticks(&self, us: u32) -> u32 {
        let ticks_per_sec = self.ticks_per_sec();
        let result = ((us as u64) * (ticks_per_sec as u64)) / 1_000_000u64;
        if result > 0xffffffffu64 {
            0xffffffffu32
        } else {
            result as u32
        }
    }

    /// The number of clock ticks per second
    pub fn ticks_per_sec(&self) -> u32 {
        // self.sysclock.0 returns exactly 48_000_000 but this is wrong.
        // Here is a measured value when compared with a highly-accurate
        // real-time clock. Of course this will vary by device and over time.
        47_536_128
    }

    /// Delay the given number of microseconds
    pub fn delay_us(&mut self, us: u32) {
        self.delay_ticks(self.us_to_ticks(us));
    }

    /// Set state of red LED. true == on, false == off
    pub fn set_red_led(&mut self, state: bool) {
        match state {
            true => { self.red_led.set_high().unwrap() },
            false => { self.red_led.set_low().unwrap() },
        }
    }

    /// Get state of button A. true == pressed, false == not pressed
    pub fn get_button_a(&self) -> bool {
        self.button_a.is_high().ok().unwrap()
    }

    /// Get state of button B. true == pressed, false == not pressed
    pub fn get_button_b(&self) -> bool {
        self.button_b.is_high().ok().unwrap()
    }

    /// Get state of Slide Switch. true == left, false == right
    pub fn get_slide_switch(&self) -> bool {
        self.slide_switch.is_high().ok().unwrap()
    }

    /// Configure pin A0/D12 as an input with internal pullup resistance.
    /// By default on power-up it is a floating input.
    pub fn configure_d12_pull_up_input(&mut self) {
        let input = mem::replace(&mut self.pin_a0, PinA0::Taken);
        if let PinA0::DigitalInFloat(p) = input {
            self.pin_a0 = PinA0::DigitalInPullUp(
                p.into_pull_up_input(&mut self.port));
        } else {
            panic!("Tried to configure A1/D6 when it was already configured");
        }
    }

    /// Configure pin A1/D6 as an input with internal pullup resistance.
    /// By default on power-up it is a floating input.
    pub fn configure_d6_pull_up_input(&mut self) {
        let input = mem::replace(&mut self.pin_a1, PinA1::Taken);
        if let PinA1::DigitalInFloat(p) = input {
            self.pin_a1 = PinA1::DigitalInPullUp(
                p.into_pull_up_input(&mut self.port));
        } else {
            panic!("Tried to configure A1/D6 when it was already configured");
        }
    }

    /// Configure pin A2/D9 as an input with internal pullup resistance.
    /// By default on power-up it is a floating input.
    pub fn configure_d9_pull_up_input(&mut self) {
        let input = mem::replace(&mut self.pin_a2, PinA2::Taken);
        if let PinA2::DigitalInFloat(p) = input {
            self.pin_a2 = PinA2::DigitalInPullUp(
                p.into_pull_up_input(&mut self.port));
        } else {
            panic!("Tried to configure A2/D9 when it was already configured");
        }
    }

    /// Configure pin A3/D10 as an input with internal pullup resistance.
    /// By default on power-up it is a floating input.
    pub fn configure_d10_pull_up_input(&mut self) {
        let input = mem::replace(&mut self.pin_a3, PinA3::Taken);
        if let PinA3::DigitalInFloat(p) = input {
            self.pin_a3 = PinA3::DigitalInPullUp(
                p.into_pull_up_input(&mut self.port));
        } else {
            panic!("Tried to configure A3/D10 when it was already configured");
        }
    }

    /// Configure pin A4/D3 as an input with internal pullup resistance.
    /// By default on power-up it is a floating input.
    pub fn configure_d3_pull_up_input(&mut self) {
        let input = mem::replace(&mut self.pin_a4, PinA4::Taken);
        if let PinA4::DigitalInFloat(p) = input {
            self.pin_a4 = PinA4::DigitalInPullUp(
                p.into_pull_up_input(&mut self.port));
        } else {
            panic!("Tried to configure A4/D3 when it was already configured");
        }
    }

    /// Configure pin A5/D2 as an input with internal pullup resistance.
    /// By default on power-up it is a floating input.
    pub fn configure_d2_pull_up_input(&mut self) {
        let input = mem::replace(&mut self.pin_a5, PinA5::Taken);
        if let PinA5::DigitalInFloat(p) = input {
            self.pin_a5 = PinA5::DigitalInPullUp(
                p.into_pull_up_input(&mut self.port));
        } else {
            panic!("Tried to configure A5/D2 when it was already configured");
        }
    }

    /// Get state of pin A0 as digital input D12. true == high, false == low
    /// Panic if pin is configured as an output (Which is too bad. The
    /// SAMD21G18A chip allows reading from outputs but the HAL does not.)
    pub fn get_d12(&self) -> bool {
        match &self.pin_a0 {
            PinA0::DigitalInFloat(p) => {
                p.is_high().ok().unwrap()
            },
            PinA0::DigitalInPullUp(p) => {
                p.is_high().ok().unwrap()
            },
            _ => {
                panic!("Reading from D12/A0 when not configured properly");
            },
        }
    }

    /// Get state of pin A1 as digital input D6. true == high, false == low
    /// Panic if pin is configured as an output (Which is too bad. The
    /// SAMD21G18A chip allows reading from outputs but the HAL does not.)
    pub fn get_d6(&self) -> bool {
        match &self.pin_a1 {
            PinA1::DigitalInFloat(p) => {
                p.is_high().ok().unwrap()
            },
            PinA1::DigitalInPullUp(p) => {
                p.is_high().ok().unwrap()
            },
            _ => {
                panic!("Reading from D6/A1 when not configured properly");
            },
        }
    }

    /// Get state of pin A2 as digital input D9. true == high, false == low
    /// Panic if pin is configured as an output (Which is too bad. The
    /// SAMD21G18A chip allows reading from outputs but the HAL does not.)
    pub fn get_d9(&self) -> bool {
        match &self.pin_a2 {
            PinA2::DigitalInFloat(p) => {
                p.is_high().ok().unwrap()
            },
            PinA2::DigitalInPullUp(p) => {
                p.is_high().ok().unwrap()
            },
            _ => {
                panic!("Reading from D9/A2 when not configured properly");
            },
        }
    }

    /// Get state of pin A3 as digital input D10. true == high, false == low
    /// Panic if pin is configured as an output (Which is too bad. The
    /// SAMD21G18A chip allows reading from outputs but the HAL does not.)
    pub fn get_d10(&self) -> bool {
        match &self.pin_a3 {
            PinA3::DigitalInFloat(p) => {
                p.is_high().ok().unwrap()
            },
            PinA3::DigitalInPullUp(p) => {
                p.is_high().ok().unwrap()
            },
            _ => {
                panic!("Reading from D10/A3 when not configured properly");
            },
        }
    }

    /// Get state of pin A4 as digital input D3. true == high, false == low
    /// Panic if pin is configured as an output (Which is too bad. The
    /// SAMD21G18A chip allows reading from outputs but the HAL does not.)
    pub fn get_d3(&self) -> bool {
        match &self.pin_a4 {
            PinA4::DigitalInFloat(p) => {
                p.is_high().ok().unwrap()
            },
            PinA4::DigitalInPullUp(p) => {
                p.is_high().ok().unwrap()
            },
            _ => {
                panic!("Reading from D3/A4 when not configured properly");
            },
        }
    }

    /// Get state of pin A5 as digital input D2. true == high, false == low
    /// Panic if pin is configured as an output (Which is too bad. The
    /// SAMD21G18A chip allows reading from outputs but the HAL does not.)
    pub fn get_d2(&self) -> bool {
        match &self.pin_a5 {
            PinA5::DigitalInFloat(p) => {
                p.is_high().ok().unwrap()
            },
            PinA5::DigitalInPullUp(p) => {
                p.is_high().ok().unwrap()
            },
            _ => {
                panic!("Reading from D3/A4 when not configured properly");
            },
        }
    }

    /// Enable/disable the speaker output.
    /// Set state == true to enable, state == false to disable.
    pub fn set_speaker_enable(&mut self, state: bool) {
        match state {
            true => { self.speaker_enable.set_high().unwrap() },
            false => { self.speaker_enable.set_low().unwrap() },
        }
    }

    /// Initialize the I2C interface. You can only do this once.
    /// Return Some(i2c_master) if successful, None if resources already taken.
    pub fn create_i2c_master(&mut self) -> Option<I2CMaster> {
        let scl = mem::replace(&mut self.pin_a4, PinA4::Taken);
        let sda = mem::replace(&mut self.pin_a5, PinA5::Taken);
        let sercom5 = self.sercom5.take();
        if scl.is_taken() || sda.is_taken() || sercom5.is_none() {
            // Either SCL, SDA, or SERCOM5 is not available
            // Put everything back the way we found it and return None
            mem::replace(&mut self.pin_a4, scl);
            mem::replace(&mut self.pin_a5, sda);
            self.sercom5 = sercom5;
            None
        } else if let (PinA4::DigitalInFloat(scl),
                       PinA5::DigitalInFloat(sda),
                       Some(sercom5)) = (scl, sda, sercom5) {
            Some(hal::i2c_master(
                &mut self.clocks,
                KiloHertz(400),
                sercom5,
                &mut self.pm,
                sda,
                scl,
                &mut self.port,
            ))
        } else {
            // Unfortuntely the "if let" moved the scl, sda, and sercom5
            // values into temporary variables and dropped them, even if the
            // pattern didn't match. That's why we need this 'else' clause
            // even though this code is in reality unreachable.
            None
        }
    }

    /// Initialize the 24-bit counter/timer with the recommended reset sequence
    pub fn init_timer(&mut self) {
        let syst = &mut self.core.SYST;
        syst.set_reload(0x00ff_ffff);
        syst.clear_current();
        syst.enable_counter();
    }

    /// Reset the 24-bit counter/timer to the reload value.
    /// Assumes init_timer() was called previously.
    pub fn reset_timer(&mut self) {
        let syst = &mut self.core.SYST;
        syst.clear_current();
    }

    /// Get the raw tick count from the 24-bit counter/timer
    /// Note: The counter counts down to zero then wraps to 0x00ff_ffff
    pub fn get_ticks() -> u32 {
        SYST::get_current()
    }

    /// Get tick count and wraparound flag
    pub fn get_ticks_wrapped(&mut self) -> (u32, bool) {
        let syst = &mut self.core.SYST;
        (SYST::get_current(), syst.has_wrapped())
    }

    pub const fn max_ticks() -> u32 {
        0x00ff_ffff
    }
}

pub fn timer_demo(mut chip: Chip) -> ! {
    writeln!(chip.sercom, "\nTimer Demo").ok();

    let syst = &mut chip.core.SYST;

    // Initialize the timer
    // Just in case, do the recommended reset sequence
    syst.set_reload(0x00ff_ffff);
    syst.clear_current();
    syst.enable_counter();

    // Print general timer stats
    writeln!(chip.sercom, "SYST::get_current(): {}", SYST::get_current()).ok();
    writeln!(chip.sercom, "SYST::get_reload(): {}", SYST::get_reload()).ok();
    writeln!(chip.sercom, "SYST::get_ticks_per_10ms(): {}",
             SYST::get_ticks_per_10ms()).ok();
    writeln!(chip.sercom, "SYST::has_reference_clock(): {}",
             SYST::has_reference_clock()).ok();
    writeln!(chip.sercom, "SYST::is_precise(): {}", SYST::is_precise()).ok();

    let sysclock: Hertz = chip.clocks.gclk0().into();
    writeln!(chip.sercom, "sysclock.0: {}", sysclock.0).ok();

    writeln!(chip.sercom, "syst.has_wrapped(): {}", syst.has_wrapped()).ok();
    writeln!(chip.sercom, "syst.is_counter_enabled(): {}",
             syst.is_counter_enabled()).ok();
    writeln!(chip.sercom, "syst.is_interrupt_enabled(): {}",
             syst.is_interrupt_enabled()).ok();
    writeln!(chip.sercom, "").ok();

    const NUM_LOOPS: usize = 10;
    let mut timestamps = [0u32; NUM_LOOPS];

    writeln!(chip.sercom, "Minimum delay (in ticks) between timestamps").ok();
    for i in 0..NUM_LOOPS {
        timestamps[i] = SYST::get_current() - SYST::get_current();
    }
    for i in 0..NUM_LOOPS {
        writeln!(chip.sercom, "Loop #{:02}: {}", i, timestamps[i]).ok();
    }

    loop {
        chip.set_red_led(true);
        chip.delay_us(1000000);
        chip.set_red_led(false);
        chip.delay_us(1000000);
    }
}
