//! Highly Accurate Timer
//! Uses a combination of external 32.768kHz signal and CPU countdown timer
//! to measure time intervals accurate to 1 microsecond.
use super::Chip;

pub const NUM_CALIBRATION_POINTS: u32 = 32;
pub const EXT_TICK_LIMIT: u32 = 140_737_488;

/// Measures time intervals with 1 microsecond accuracy.
/// Called "spin timer" because the update method must be called in a tight
/// loop to detect the rising edge of the external 32.768kHz clock.
pub struct SpinTimer<CB> where CB: FnMut(&Chip) -> bool {
    /// Previous state of the external clock
    ext_clk_state: bool,
    /// Callback function/closure that takes a &Chip parameter and returns the
    /// current external clock level.
    callback: CB,

    /// Number of external clock ticks since timer was created
    ext_ticks: u32,
    /// CPU clock tick count on latest external clock rising edge
    edge_cpu_ticks: u32,
    /// CPU clock tick count on last call to update()
    last_cpu_ticks: u32,

    /// Calibration data:
    /// Number of CPU clock cycles per NUM_CALIBRATION_POINTS external clock
    /// cycles.
    calibration_total: u32,
}

/// Represents an elapsed time between when a SpinTimer was created and the last
/// time SpinTimer.update() was called.
#[derive(Debug, Copy, Clone)]
pub struct TimeStamp {
    ext_ticks: u32,
    cpu_count: u32,
    calibration_total: u32,
}

impl<CB> SpinTimer<CB> where CB: FnMut(&Chip) -> bool {
    /// callback: Closure that returns current external clock signal level.
    /// Note: Initialization will take NUM_CALIBRATION_POINTS external clock
    /// cycles. (About 1ms if NUM_CALIBRATION_POINTS == 32)
    pub fn new(chip: &mut Chip, callback: CB) -> SpinTimer<CB>
        where CB: FnMut(&Chip) -> bool
    {
        let mut timer = SpinTimer::<CB> {
            ext_clk_state: true,
            callback,
            ext_ticks: 0,
            edge_cpu_ticks: 0,
            last_cpu_ticks: 0,
            calibration_total: 0,
        };

        // Start countdown timer that runs at CPU clock speed.
        // Calibration will take only 1ms, so we don't expect that the
        // 24-bit CPU timer will wrap around during this function call.
        chip.init_timer();

        // Calculate the calibration data
        while (timer.callback)(&chip) { } // Wait for ext clock low
        while !(timer.callback)(&chip) { } // Wait for ext clock high
        let start_cpu_ticks = Chip::get_ticks();
        for _ in 0..NUM_CALIBRATION_POINTS {
            while  (timer.callback)(&chip) { } // Wait for ext clock low
            while  !(timer.callback)(&chip) { } // Wait for ext clock high
        }
        let end_cpu_ticks = Chip::get_ticks();
        timer.edge_cpu_ticks = end_cpu_ticks;
        timer.last_cpu_ticks = end_cpu_ticks;
        // Remember, these ticks count down.
        timer.calibration_total = start_cpu_ticks - end_cpu_ticks;

        timer
    }

    /// Update the current time. Call this at least every 15 microseconds.
    /// Return the current whole number of external clock ticks since this timer
    /// was created.
    pub fn update(&mut self, chip: &Chip) -> u32 {
        let ext_clk_state = (self.callback)(&chip);
        self.last_cpu_ticks = Chip::get_ticks();

        if !self.ext_clk_state && ext_clk_state {
            // External clock went from low to high
            // Update CPU clock timestamp marking latest rising edge
            self.edge_cpu_ticks = self.last_cpu_ticks;
            // Increment external clock tick count
            self.ext_ticks += 1;
        }

        self.ext_clk_state = ext_clk_state;
        self.ext_ticks
    }

    /// Return the current external clock tick count.
    /// If this this value is greater than  EXT_TICK_LIMIT then the timer is
    /// saturated and as_micros() will return 0xffff_ffff.
    pub fn ext_ticks(&self) -> u32 {
        self.ext_ticks
    }

    /// Return the CPU countdown timer value as of the last external clock
    /// rising edge.
    pub fn edge_cpu_ticks(&self) -> u32 {
        self.edge_cpu_ticks
    }

    /// Return the CPU countdown timer value as of the last update().
    pub fn last_cpu_ticks(&self) -> u32 {
        self.last_cpu_ticks
    }

    /// Internal calibration value.
    pub fn calibration_total(&self) -> u32 {
        self.calibration_total
    }

    /// Return a TimeStamp object representing the elapsed time since this timer
    /// was created, as of the last time update() was called.
    pub fn timestamp(&self) -> TimeStamp {
        // Compute CPU clock ticks since last rising edge of ext clock.
        // Remember, this clock counts down.
        let cpu_count = if self.edge_cpu_ticks >= self.last_cpu_ticks {
            self.edge_cpu_ticks - self.last_cpu_ticks
        } else {
            Chip::max_ticks() - self.edge_cpu_ticks - self.last_cpu_ticks + 1
        };

        TimeStamp {
            ext_ticks: self.ext_ticks,
            cpu_count,
            calibration_total: self.calibration_total,
        }
    }
}

impl TimeStamp {
    /// Construct a zero-valued TimeStamp as filler for arrays
    pub fn new() -> TimeStamp {
        TimeStamp {
            ext_ticks: 0,
            cpu_count: 0,
            calibration_total: 0,
        }
    }

    /// Return the time in microseconds since the clock was initialized
    /// as of the last time update() was called.
    pub fn as_micros(&self) -> u32 {
        // Convert count of ext clock ticks to microseconds * 32k
        let t1 = self.ext_ticks as u64 * 1_000_000;

        // Convert count of CPU clock ticks to microseconds * 32k
        let t0 = if self.cpu_count > 0 {
            (self.cpu_count as u64 * NUM_CALIBRATION_POINTS as u64 * 1_000_000)
                / self.calibration_total as u64
        } else {
            0u64
        };

        // The right shift divides by 32768 which is the ext clock frequency.
        let result = (t1 + t0) >> 15;
        if result > 0xffff_ffff {
            0xffff_ffff
        } else {
            result as u32
        }
    }

    /// Return this timestamp's external clock tick count.
    /// If this this value is greater than EXT_TICK_LIMIT then the timer was
    /// saturated and as_micros() will return 0xffff_ffff.
    pub fn ext_ticks(&self) -> u32 {
        self.ext_ticks
    }

    /// Return the number of CPU clock cycles between the previous external
    /// clock rising edge (as measured by ext_ticks) and the instant in time
    /// represented by this timestamp.
    pub fn cpu_count(&self) -> u32 {
        self.cpu_count
    }

    /// Internal calibration value.
    pub fn calibration_total(&self) -> u32 {
        self.calibration_total
    }
}
