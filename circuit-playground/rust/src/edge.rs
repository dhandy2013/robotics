//! Edge Detection module

/// Input that detects when an input goes from low (false) to high (true)
/// Generic for callback CB that takes a parameter of type P

pub struct EdgeTrigger<CB> {
    prev_state: bool,
    callback: CB,
}

impl<CB> EdgeTrigger<CB> {
    /// init_state: The initial value of the prev_state field
    /// callback: Closure that reads input and returns its value
    pub fn new(prev_state: bool, callback: CB) -> EdgeTrigger<CB> {
        EdgeTrigger { prev_state, callback }
    }

    /// Return true if the input goes from low to high
    pub fn triggered<P>(&mut self, param: P) -> bool
        where CB: FnMut(P) -> bool
    {
        let new_state = (self.callback)(param);
        let result = !self.prev_state && new_state;
        self.prev_state = new_state;
        result
    }

    /// Read and save the current input state, ignore edge trigger if any
    pub fn poll<P>(&mut self, param: P) -> bool
        where CB: FnMut(P) -> bool
    {
        self.prev_state = (self.callback)(param);
        self.prev_state
    }
}
