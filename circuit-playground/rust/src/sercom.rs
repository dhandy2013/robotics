//! Serial Communications module
use circuit_playground_express as hal;
use core::fmt::{Result, Write};
use hal::prelude::*; // needed for .set_high(), .set_low(), and more
use hal::sercom::{Sercom4Pad0, Sercom4Pad1, UART4};

// Serial port set up using A6/A7 rx/tx pads
type SerialPort = UART4<
    Sercom4Pad1<hal::gpio::Pb9<hal::gpio::PfD>>,
    Sercom4Pad0<hal::gpio::Pb8<hal::gpio::PfD>>,
    (),
    (),
>;

pub struct SerialComm {
    uart: SerialPort,
}

impl SerialComm {
    pub fn new(uart: SerialPort) -> SerialComm {
        SerialComm { uart }
    }

    pub fn write_bytes(&mut self, buf: &[u8]) {
        for b in buf {
            block!(self.uart.write(*b)).unwrap();
        }
    }

    /// Non-blocking read.
    /// Return Some(byte) if a byte is ready, None otherwise.
    pub fn read_byte(&mut self) -> Option<u8> {
        match self.uart.read() {
            Ok(byte) => Some(byte),
            Err(nb::Error::WouldBlock) => None,
            Err(err) => {
                panic!("Error while reading uart: {:?}", err);
            }
        }
    }

    pub fn read_byte_blocking(&mut self) -> u8 {
        block!(self.uart.read()).unwrap()
    }

    /// Read bytes and store them in buf until a newline is received or buf is
    /// full, whichever happens first. The newline is stored in buf.
    /// Return the number of bytes read.
    pub fn read_line_bytes(&mut self, buf: &mut [u8]) -> usize {
        let mut count: usize = 0;
        while count < buf.len() {
            let byte = self.read_byte_blocking();
            buf[count] = byte;
            count += 1;
            if byte == b'\n' {
                break;
            }
        }
        count
    }

    /// Read a line of bytes using read_line_byes() and covert it to str.
    /// If the line is not all valid UTF-8 text, return None, otherwise
    /// return Some(s).
    pub fn read_line_str<'a>(&mut self, buf: &'a mut [u8])
            -> Option<&'a str> {
        let n = self.read_line_bytes(buf);
        match core::str::from_utf8_mut(&mut buf[0..n]) {
            Ok(s) => Some(s),
            Err(_) => None,
        }
    }
}

impl Write for SerialComm {
    fn write_str(&mut self, s: &str) -> Result {
        for b in s.bytes() {
            if b == b'\n' {
                block!(self.uart.write(b'\r')).unwrap();
            }
            block!(self.uart.write(b)).unwrap();
        }
        Ok(())
    }
}
