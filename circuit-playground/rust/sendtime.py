#!/usr/bin/env python3
"""
Print the time in the format expected by clockctl.rs
"""
from datetime import datetime
import sys
import time


def main():
    # Spin until start of next second
    t = time.time()
    t_next_second = int(t) + 1.0
    while t < t_next_second:
        t = time.time()
    sys.stdout.write(
        datetime.utcfromtimestamp(t).isoformat(sep=" ", timespec="seconds")
    )
    sys.stdout.write("\n")
    sys.stdout.flush()


if __name__ == "__main__":
    sys.exit(main())
