Circuit Playground Express and CircuitPython
--------------------------------------------

See my wiki notes at: https://gw.handysoftware.com/wiki/cpif/circuit-playground

Command-Line Tools for Circuit Playground / CircuitPython
.........................................................

Some commands I use often when working with the Circuit Playground::

   # To give myself (user "david") permission
   sudo usermod -a -G dialout david
   # Log out and then back in again

   # USB filesystem
   export BOARD=/run/media/david/CIRCUITPY

   # Connect terminal to the USB serial port
   screen /dev/ttyACM0 115200

Run these commands inside the ``vim`` editor to run code on the board::

   :let $BOARD='/run/media/david/CIRCUITPY'
   :w! $BOARD/code.py

Re-building the CircuitPython Firmware
......................................

Instructions:
https://github.com/adafruit/circuitpython/tree/3.x/ports/atmel-samd

Run these commands to install the gcc prerequisites on Fedora Linux::

   sudo yum install gcc-arm-linux-gnu arm-none-eabi-gcc-cs-c++ arm-none-eabi-gcc-cs arm-none-eabi-newlib arm-none-eabi-binutils-cs arm-none-eabi-gdb

Then run these commands to build the firmware::

   # cd to root of circuitpython git checkout
   git submodule update --init --recursive
   make -C mpy-cross
   cd ports/atmel-samd
   make BOARD=circuitplayground_express

It seemed to work for me. I got this output at the end::

   10136 bytes free in flash out of 253440 bytes ( 247.5 kb ).
   26372 bytes free in ram for stack out of 32768 bytes ( 32.0 kb ).

   Converting to uf2, output size: 486912, start address: 0x2000
   Wrote 486912 bytes to build-circuitplayground_express/firmware.uf2.

Right now the boot_out.txt file in CIRCUITPY says::

   Adafruit CircuitPython 3.1.1 on 2018-11-02; Adafruit CircuitPlayground Express with samd21g18

After I double-clicked the "reset" button and installed my newly-build .uf2
file, it says::

   Adafruit CircuitPython 4.0.0-alpha.3-32-gab94344ba on 2018-11-28; Adafruit CircuitPlayground Express with samd21g18


Christmas Box project
.....................

See ``christmas.py``

It requires a music file "unto-us-child-born-15-loud.wav" to be copied to the
CIRCUITPY drive. I have a copy of this file in my ~/Music/ directory.

Update: 2019-08-07:
I pulled and built the latest version of CircuitPython for
circuitplayground_express (5.0.0-alpha.0-153-g47a0b7cba).  After installing
that, my ``christmas.py`` program wouldn't work - no more audioio.WaveFile.

