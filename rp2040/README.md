# Code for microcontrollers based on the RP2040 chip

- handy-pro-micro-rp2040: Code for the sparkfun Pro Micro RP2040
- handy-rp2040-common: RP2040 code common to multiple microntroller boards
- handy-rpi-pico: Code for the Raspberry Pi Pico

## Compiling and running a program on an RP2040 device with debug output

Example: Run `pico-hello.rs` on a Raspberry Pi Pico

Connect the Pico to the host computer via USB. Use a direct USB port, not a port
on a hub. Otherwise you will not be able to put the Pico in BOOTSEL mode without
first disconnecting the UART from the terminal program.

Connect a [USB-UART bridge](https://www.adafruit.com/product/954) to your host
computer. (It is Ok to plug the USB-UART bridge into a USB hub.)

- Connect the USB-UART RX pin to the RP2040 UART0 TX pin (pin 1 on the Pico)
- Connect the USB-UART TX pin to the RP2040 UART0 RX pin (pin 2 on the Pico)
- Connect the USB-UART ground (black) to RP2040 ground (pin 3 on the Pico)

Start a terminal program and connect it to the USB-UART bridge at 115200 baud.
Example Linux command: (Press Ctrl-A Ctrl-Q to disconnect)
```
picocom -b 115200 /dev/ttyUSB0
```

Put the Pico in BOOTSEL mode by pressing and holding the BOOTSEL button, then
pressing the reset button. (Connect a pushbutton between pin 30 and ground to
give the Pico a reset button.)

Compile and run the program:
```
cargo run --release --bin pico-hello
```

In the terminal window connected to the UART you will see the debug output
messages.

## Compiling all RP2040 examples and programs

Do this after refactoring code to make sure you didn't break anything:
```
cargo build --release --bins --examples
```
