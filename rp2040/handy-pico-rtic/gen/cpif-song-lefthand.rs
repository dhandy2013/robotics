// *** Machine-generated file, do not edit by hand. ***
// Generated by: gen_song_data.py
// Generated from music text file: music/cpif-song-lefthand.txt
// Generation time: 2024-07-17 23:50:54.617548
// This file is to be included into the top level of a Rust source file.

mod cpif_song_lefthand {
    use handy_rp2040_common::sonic_ng::WaveParams;

    /// Convert an integer frequency in Hz times 16 to phase units per sample
    /// (usable by the sonic_ng module) as an unsigned 16-bit integer.
    const fn freq(freq_x_16: u16) -> u16 {
        WaveParams::freq_x16_to_phase_vel(freq_x_16)
    }

    /// The number of items in the SONG array
    const SONG_LEN: usize = 31;

    /// An array of (frequency, duration) tuples to specify the notes and rests
    /// in the song.
    ///
    /// The frequencies are in phase units per sample; the `freq` function is
    /// used to convert from Hz*16 to this unit. A zero frequency means rest.
    ///
    /// The durations are in milliseconds as an unsigned 16-bit integer.
    pub static SONG: [(u16, u16); SONG_LEN] = [
        (0, 500),
        (freq(2093), 438),
        (0, 63),
        (freq(3136), 438),
        (0, 63),
        (freq(2093), 438),
        (0, 63),
        (freq(3136), 438),
        (0, 63),
        (freq(2093), 438),
        (0, 63),
        (freq(3136), 438),
        (0, 63),
        (freq(2794), 438),
        (0, 63),
        (freq(3136), 438),
        (0, 63),
        (freq(2093), 438),
        (0, 63),
        (freq(3136), 438),
        (0, 63),
        (freq(2093), 438),
        (0, 63),
        (freq(3136), 438),
        (0, 63),
        (freq(3136), 438),
        (0, 63),
        (freq(2794), 438),
        (0, 63),
        (freq(2637), 875),
        (0, 125),
    ];
}
