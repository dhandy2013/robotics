//! Re-usable sound effects, works with sonic_ng module
use crate::timing::DurationUS;
use core::mem::swap;
use handy_rp2040_common::sonic_ng::{ControlHandle, WaveParams};
use rtic_monotonics::{rp2040::prelude::ExtU64, Monotonic};

/// Play a waveform with steady pitch and amplitude
/// The waveform, pitch and amplitude come from `wave_params`
pub async fn steady_tone<M>(
    controls: &ControlHandle,
    wave_params: WaveParams,
    time_ms: u16,
) where
    M: Monotonic<Duration = DurationUS>,
{
    controls.set_wave_params(&wave_params);
    M::delay((time_ms as u64).millis()).await;
}

/// Play a waveform with steady pitch but decaying amplitude
/// The waveform, pitch and starting amplitude come from `wave_params`
pub async fn decay_tone<M>(
    controls: &ControlHandle,
    mut wave_params: WaveParams,
    time_ms: u16,
) where
    M: Monotonic<Duration = DurationUS>,
{
    // I did the the math, and this value should result in an amplitude
    // decay half-time of 500ms
    let decay_rate = 16_361;

    let time_step = 1.millis();
    let mut next_time = M::now();
    let mut amp = wave_params.amp();
    for _ in 0..time_ms {
        wave_params.set_amp(amp);
        controls.set_wave_params(&wave_params);
        amp = (((amp as u32) * decay_rate) / 16_384) as u16;
        next_time = next_time + time_step;
        M::delay_until(next_time).await;
    }
}

/// Set the amplitude to zero for the `time_ms` duration in ms.
/// Restore the original amplitude after that duration expires.
pub async fn quiet_time<M>(
    controls: &ControlHandle,
    mut wave_params: WaveParams,
    time_ms: u16,
) where
    M: Monotonic<Duration = DurationUS>,
{
    let starting_amp = wave_params.amp();
    wave_params.set_amp(0);
    controls.set_wave_params(&wave_params);
    M::delay((time_ms as u64).millis()).await;
    wave_params.set_amp(starting_amp);
    controls.set_wave_params(&wave_params);
}

/// Play a waveform over a range of frequencies over a time period.
/// The waveform and amplitude come from `wave_params`.
pub async fn sweep_freq<M>(
    controls: &ControlHandle,
    mut wave_params: WaveParams,
    start_freq_hz: u16,
    end_freq_hz: u16,
    time_delta_ms: u16,
) where
    M: Monotonic<Duration = DurationUS>,
{
    let start_phase_vel = WaveParams::freq_to_phase_vel(start_freq_hz);
    let end_phase_vel = WaveParams::freq_to_phase_vel(end_freq_hz);
    let (start_phase_vel, end_phase_vel, delta_phase_vel, do_reverse) =
        if end_phase_vel >= start_phase_vel {
            (
                start_phase_vel,
                end_phase_vel,
                (end_phase_vel - start_phase_vel),
                false,
            )
        } else {
            (
                end_phase_vel,
                start_phase_vel,
                (start_phase_vel - end_phase_vel),
                true,
            )
        };
    let step_by =
        (((delta_phase_vel as u32) << 15) / (time_delta_ms as u32)) >> 15;
    let step_by = step_by.max(1);
    let range = (start_phase_vel..=end_phase_vel).step_by(step_by as usize);

    let time_step = 1.millis();
    let mut next_time = M::now();

    if do_reverse {
        for phase_vel in range.rev() {
            wave_params.set_phase_vel(phase_vel);
            controls.set_wave_params(&wave_params);
            next_time = next_time + time_step;
            M::delay_until(next_time).await;
        }
    } else {
        for phase_vel in range {
            wave_params.set_phase_vel(phase_vel);
            controls.set_wave_params(&wave_params);
            next_time = next_time + time_step;
            M::delay_until(next_time).await;
        }
    }
}

#[derive(Debug)]
pub struct ToneWobbler {
    lo_phase_vel: u16,
    hi_phase_vel: u16,
    cur_phase_vel: u16,
    accel: i16,
}

impl ToneWobbler {
    /// Construct a new ToneWobbler
    ///
    /// lo_freq_x16: lowest frequency it will reach in Hz * 16
    /// hi_freq_x16: highest frequency it will reqch in Hz * 16
    /// accel: change in tone in phase units per cycle per ms
    ///
    /// There are 65536 phase units per cycle. At a sample rate of 25KHz,
    /// accel = 1 means a change of about 0.3815 Hz per ms or 381.5 Hz / sec.
    pub fn new(mut lo_freq_x16: u16, mut hi_freq_x16: u16, accel: i16) -> Self {
        if lo_freq_x16 > hi_freq_x16 {
            swap(&mut lo_freq_x16, &mut hi_freq_x16);
        }
        let lo_phase_vel = WaveParams::freq_x16_to_phase_vel(lo_freq_x16);
        let hi_phase_vel = WaveParams::freq_x16_to_phase_vel(hi_freq_x16);
        Self {
            lo_phase_vel,
            hi_phase_vel,
            cur_phase_vel: lo_phase_vel,
            accel,
        }
    }

    /// Start tone at bottom of frequency range
    pub fn start(&self, wave_params: &mut WaveParams) {
        wave_params.set_phase_vel(self.cur_phase_vel);
    }

    /// Make tone go up or down depending on current state
    pub fn update(&mut self, wave_params: &mut WaveParams) {
        if self.accel >= 0 {
            let accel = self.accel as u16;
            self.cur_phase_vel = self.cur_phase_vel.saturating_add(accel);
        } else {
            let accel = (-self.accel) as u16;
            self.cur_phase_vel = self.cur_phase_vel.saturating_sub(accel);
        }
        if (self.cur_phase_vel <= self.lo_phase_vel && self.accel < 0)
            || (self.cur_phase_vel >= self.hi_phase_vel && self.accel > 0)
        {
            self.accel = -self.accel;
        }
        wave_params.set_phase_vel(self.cur_phase_vel);
    }
}

/// Create a default ToneWobbler that oscillates on a 2-sec cycle
impl Default for ToneWobbler {
    fn default() -> Self {
        Self::new(
            440 * 16,              // low: 440 Hz
            (821.5 * 16.0) as u16, // high: 821.5 Hz
            1,                     // change tone 0.3815 Hz per ms
        )
    }
}

/// Play a waveform that oscillates between 440 Hz and 821.5 Hz on a 2-second
/// cycle. The waveform and amplitude come from `wave_params`.  Delay for
/// `time_ms` ms before returning.
pub async fn wobble_tone<M>(
    controls: &ControlHandle,
    mut wave_params: WaveParams,
    time_ms: u16,
) where
    M: Monotonic<Duration = DurationUS>,
{
    let mut tone_wobbler = ToneWobbler::default();
    tone_wobbler.start(&mut wave_params);
    controls.set_wave_params(&wave_params);
    let time_step = 1.millis();
    let mut next_time = M::now() + time_step;
    for _ in 0..time_ms {
        M::delay_until(next_time).await;
        next_time = next_time + time_step;
        tone_wobbler.update(&mut wave_params);
        controls.set_wave_params(&wave_params);
    }
}

#[derive(Clone, Copy, Debug)]
pub enum DTMF {
    Tone0,
    Tone1,
    Tone2,
    Tone3,
    Tone4,
    Tone5,
    Tone6,
    Tone7,
    Tone8,
    Tone9,
    ToneA,
    ToneB,
    ToneC,
    ToneD,
    ToneStar,
    ToneHash,
}

impl DTMF {
    /// Return the two tones, in Hz, of this DTMF code
    pub fn as_tones(&self) -> (u16, u16) {
        use DTMF::*;
        match self {
            // row 1
            Tone1 => (697, 1209),
            Tone2 => (697, 1336),
            Tone3 => (697, 1477),
            ToneA => (697, 1633),
            // row 2
            Tone4 => (770, 1209),
            Tone5 => (770, 1336),
            Tone6 => (770, 1477),
            ToneB => (770, 1633),
            // row 3
            Tone7 => (852, 1209),
            Tone8 => (852, 1336),
            Tone9 => (852, 1477),
            ToneC => (852, 1633),
            // row 4
            ToneStar => (941, 1209),
            Tone0 => (941, 1336),
            ToneHash => (941, 1477),
            ToneD => (941, 1633),
        }
    }
}
