//! Common code for running a background task for printing to UART0
use alloc::{borrow::Cow, slice::SliceIndex, string::String};
use core::sync::atomic::{AtomicUsize, Ordering};
use handy_rp2040_common::uart::{self, Uart0Writer, UART_BUFFER_SIZE};
use rp_pico::pac::Interrupt;
use rtic_monotonics::{rp2040::prelude::ExtU64, Monotonic};
use rtic_sync::channel::{Receiver, Sender, TrySendError};

/// The number of print messages that can be queued before blocking.
pub const PRINT_QUEUE_SIZE: usize = 5;
pub const PRINT_DEFAULT_ALLOC_SIZE: usize = 64;

pub struct PrintMsg(Cow<'static, [u8]>);

impl PrintMsg {
    /// Return length of print message in bytes
    pub fn len(&self) -> usize {
        self.0.len()
    }

    /// Returns a reference to a byte or subslice of bytes depending on the type
    /// of index.
    /// - If given a position, returns a reference to the byte at that position
    ///   or None if out of bounds.
    /// - If given a range, returns the subslice corresponding to that range, or
    ///   None if out of bounds.
    pub fn get<I>(&self, index: I) -> Option<&<I as SliceIndex<[u8]>>::Output>
    where
        I: SliceIndex<[u8]>,
    {
        self.0.get(index)
    }
}

impl From<String> for PrintMsg {
    fn from(value: String) -> Self {
        Self(value.into_bytes().into())
    }
}

impl From<&'static str> for PrintMsg {
    fn from(value: &'static str) -> Self {
        Self(value.as_bytes().into())
    }
}

pub type PrintSender = Sender<'static, PrintMsg, PRINT_QUEUE_SIZE>;
pub type PrintReceiver = Receiver<'static, PrintMsg, PRINT_QUEUE_SIZE>;

#[derive(Clone)]
pub struct PrintHandle {
    sender: PrintSender,
}

impl PrintHandle {
    pub fn new(sender: &PrintSender) -> Self {
        Self {
            sender: sender.clone(),
        }
    }

    pub async fn send<M>(&mut self, msg: M)
    where
        M: Into<PrintMsg>,
    {
        let _ = self.sender.send(msg.into()).await;
        // Cause the UART0 interrupt handler to be called, otherwise the
        // message will just sit in the queue and not be sent.
        rtic::pend(Interrupt::UART0_IRQ);
    }

    pub fn try_send<M>(&mut self, msg: M) -> Result<(), TrySendError<PrintMsg>>
    where
        M: Into<PrintMsg>,
    {
        self.sender.try_send(msg.into())?;
        // Cause the UART0 interrupt handler to be called, otherwise the
        // message will just sit in the queue and not be sent.
        rtic::pend(Interrupt::UART0_IRQ);
        Ok(())
    }
}

static UART0_BUF_EMPTY_COUNT: AtomicUsize = AtomicUsize::new(0);

/// Call this from the UART interrupt handler to increment the number of times
/// that UART0 handler exits because the buffer and message queue are empty.
///
/// This is used by `flush()` to detect if pending messages have been processed.
pub fn incr_uart0_buf_empty_count() {
    // There is a natural race condition between the `load` and the `store`.
    // However this is only called from the UART0_IRQ interrupt handler, and the
    // Arm architecture guarantees the interrupt handler is not called
    // recursively nor concurrently, so in practice this never races.
    let count = UART0_BUF_EMPTY_COUNT.load(Ordering::SeqCst);
    UART0_BUF_EMPTY_COUNT.store(count.wrapping_add(1), Ordering::SeqCst);
}

/// Wait until print output is sent to UART0. This will not return until the
/// following things happen in sequence:
/// - The UART interrupt handler buffer and queue were empty at least once
/// - The UART transmitter has been non-busy at least once
///
/// The UART is "busy" when there is data in the transmit FIFO or any bits
/// left in the transmit shift registor or any stop bits left to be sent.
///
/// The Monotonic timer is used to wait for short delays between checking that
/// the interrupt has happened or the UART is still busy.
///
/// Return true if the flush succeeds. This means all bytes sent to the UART
/// handler prior to calling this function have been completely transmitted.
///
/// Return false if `timeout_ms` milliseconds from the start of this function
/// expire before the UART handler buffer becomes empty or the UART FIFO and
/// shift register become empty.
pub async fn flush_uart0<M>(timeout_ms: u32) -> bool
where
    M: Monotonic<
        Duration = rtic_monotonics::fugit::Duration<u64, 1, 1_000_000>,
    >,
{
    let starting_buf_empty_count = UART0_BUF_EMPTY_COUNT.load(Ordering::SeqCst);
    let loop_delay_interval = 500.micros();
    let mut buffer_was_emptied = false;
    let start_time = M::now();
    let timeout_time = start_time + (timeout_ms as u64).millis();
    while M::now() < timeout_time {
        rtic::pend(Interrupt::UART0_IRQ);
        M::delay(loop_delay_interval).await;
        let cur_buf_empty_count = UART0_BUF_EMPTY_COUNT.load(Ordering::SeqCst);
        if cur_buf_empty_count != starting_buf_empty_count {
            buffer_was_emptied = true;
            break;
        }
    }
    if !buffer_was_emptied {
        return false;
    }
    let mut all_bytes_transmitted = false;
    while M::now() < timeout_time {
        if !uart::uart0_is_busy() {
            all_bytes_transmitted = true;
            break;
        }
        M::delay(loop_delay_interval).await;
    }
    all_bytes_transmitted
}

pub struct PrintOutputBuf {
    buf: PrintMsg,
    pos: usize,
}

impl PrintOutputBuf {
    /// Construct a new empty PrintOutputBuf
    pub const fn new() -> Self {
        Self {
            buf: PrintMsg(Cow::Borrowed(b"")),
            pos: 0,
        }
    }

    /// Turn a PrintMsg into a PrintOutbutBuf
    pub fn from_printmsg(msg: PrintMsg) -> Self {
        Self { buf: msg, pos: 0 }
    }

    pub fn is_empty(&self) -> bool {
        // This should always be true at the start of every public function
        assert!(self.pos <= self.buf.len());

        self.pos == self.buf.len()
    }

    /// Remove `n` bytes from the buffer by advancing the buffer position and
    /// returning a slice of bytes with length <= `n`. If the returned slice is
    /// empty then the print output buffer was empty or `n` was zero.
    pub fn remove_bytes(&mut self, n: usize) -> &[u8] {
        // This should always be true at the start of every public function
        assert!(self.pos <= self.buf.len());

        let n = usize::min(n, self.buf.len() - self.pos);
        let prev_pos = self.pos;
        self.pos += n;
        &self.buf.0[prev_pos..self.pos]
    }

    /// Put back the last `n` bytes into the buffer that had just been removed
    /// with `remove_bytes` by regressing the buffer position by `n`.  PANIC if
    /// `n` is greater than the current buffer position. This should never panic
    /// if the bytes put back really are bytes that had just been removed.
    pub fn putback_bytes(&mut self, n: usize) {
        // This should always be true at the start of every public function
        assert!(self.pos <= self.buf.len());

        assert!(n <= self.pos);
        self.pos -= n;
    }
}

/// Call this from the UART0_IRQ handler to implement background printing.
///
/// `uart0_writer`: The writing half of the UART0 device.
/// `print_receiver`: RTIC consumer portion of mpsc for print messages
/// `print_output_buf`: Holds bytes to be sent from one invocation to another
pub fn uart0_print_intr_handler(
    uart0_writer: &mut Uart0Writer,
    print_receiver: &mut PrintReceiver,
    print_output_buf: &mut PrintOutputBuf,
) {
    // Send as many pending output bytes as possible without blocking.
    loop {
        let bytes = print_output_buf.remove_bytes(UART_BUFFER_SIZE);
        let (num_bytes_to_put_back, output_fifo_is_full) =
            match uart0_writer.write_raw(bytes) {
                Ok(unwritten_slice) => (unwritten_slice.len(), false),
                Err(_) => (bytes.len(), true),
            };
        print_output_buf.putback_bytes(num_bytes_to_put_back);

        if output_fifo_is_full {
            // Exit. As the FIFO drains this handler will be called again.
            break;
        }

        let more_to_print = if print_output_buf.is_empty() {
            // See if there are more print messages in the queue.
            match print_receiver.try_recv() {
                Ok(msg) => {
                    // This DROPS memory. Keep an eye on performance here.
                    *print_output_buf = PrintOutputBuf::from_printmsg(msg);
                    true
                }
                Err(_) => false,
            }
        } else {
            true
        };

        if !more_to_print {
            // Clear the TX interrupt flag before leaving, or else this
            // interrupt handler will be called forever.
            uart::clear_uart0_tx_intr();
            // Signal to print::flush() that buffer has been emptied
            incr_uart0_buf_empty_count();
            break;
        }
    }
}
