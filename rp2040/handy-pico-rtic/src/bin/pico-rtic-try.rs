//! Experimental RPi Pico program using RTIC
//!
//! RTIC stands for "Real-Time Interrupt-Driven Concurrency".
//! This program is for experiements and testing new things that might be added
//! to my handy_pico_rtic library later.
//!
//! Pins and signals used (currently):
//! - GPIO0 : Pin 1: UART tx -> USB-serial rx (white wire on my device)
//! - GPIO1 : Pin 2: UART rx -> USB-serial tx (green wire on my device)
//! - GND   : Pin 3: Ground  -> USB-serial ground
//! - GPIO14: Pin 19 Trace output 1
//! - GPIO15: Pin 20 Trace output 2
//! - GPIO22: Pin 29 connected to application pushbutton
//! - RUN   : Pin 30 connected to reset pushbutton
//! - GPIO25: Connected to onboard LED, toggles once per second.
#![no_std]
#![no_main]

extern crate alloc;

use alloc::string::String;
use core::fmt::{self, Write}; // for write!(), writeln!()
use embedded_hal::digital::OutputPin;
use handy_pico_rtic::{
    buttons,
    print::{self, PrintHandle, PrintMsg, PrintOutputBuf, PrintReceiver},
};
use handy_rp2040_common::{
    chip,
    uart::{self, Uart0Reader, Uart0Writer, UART_BUFFER_SIZE},
};
use panic_halt as _;
use rp_pico::hal::{
    self,
    gpio::{
        self,
        bank0::{Gpio14, Gpio15, Gpio25},
        FunctionSioOutput, PullNone,
    },
    pac, Clock,
};
use rtic_monotonics::{
    rp2040::prelude::ExtU64, rp2040_timer_monotonic, Monotonic,
};
use rtic_sync::channel::{Receiver, Sender};
use usb_device::{class_prelude::*, prelude::*};
use usbd_serial::SerialPort;

/// Picked to match the size of the R2040 UART input buffer.
const INPUT_BUFFER_SIZE: usize = uart::UART_BUFFER_SIZE;

rp2040_timer_monotonic!(Mono);

type ButtonPin =
    gpio::Pin<gpio::bank0::Gpio22, gpio::FunctionSioInput, gpio::PullUp>;

#[derive(Default)]
pub struct UsbStats {
    interrupt_count: usize,
    poll_count: usize,
    read_err_count: usize,
    read_empty_count: usize,
    read_nonempty_count: usize,
    num_bytes_read: usize,
    write_count: usize,
    num_bytes_written: usize,
}

impl UsbStats {
    fn dump(&self, out: &mut dyn Write) -> fmt::Result {
        write!(
            out,
            "USB i:{} p:{} re:{},r0:{},rn:{},rb:{} w:{},wb:{}\r\n",
            self.interrupt_count,
            self.poll_count,
            self.read_err_count,
            self.read_empty_count,
            self.read_nonempty_count,
            self.num_bytes_read,
            self.write_count,
            self.num_bytes_written,
        )
    }
}

#[rtic::app(device = rp_pico::hal::pac, dispatchers = [SW0_IRQ, SW1_IRQ, SW2_IRQ])]
mod app {
    use super::*;

    #[shared]
    struct Shared {
        counter: usize,
        usb_stats: UsbStats,
    }

    #[local]
    struct Local {
        led: gpio::Pin<Gpio25, FunctionSioOutput, PullNone>,
        uart0_reader: Uart0Reader,
        uart0_writer: Uart0Writer,
        uart0_input_sender: Sender<'static, u8, INPUT_BUFFER_SIZE>,
        pwm_slice: hal::pwm::Slice<hal::pwm::Pwm4, hal::pwm::FreeRunning>,
        button_pin: ButtonPin,
        button_input_sender: Sender<'static, u8, INPUT_BUFFER_SIZE>,
        print_receiver: PrintReceiver,
        print_output_buf: PrintOutputBuf,
        trace1_pin: gpio::Pin<Gpio14, FunctionSioOutput, PullNone>,
        trace2_pin: gpio::Pin<Gpio15, FunctionSioOutput, PullNone>,
        usb_device: UsbDevice<'static, hal::usb::UsbBus>,
        usb_serial: SerialPort<'static, hal::usb::UsbBus>,
    }

    #[init(local = [usb_bus: Option<usb_device::bus::UsbBusAllocator<hal::usb::UsbBus>> = None])]
    fn init(ctx: init::Context) -> (Shared, Local) {
        // Setup using the chip module. This configures the default 125 MHz
        // system clock and the watchdog timer.
        let mut chip = chip::Chip::from_core_peripherals(
            rp_pico::XOSC_CRYSTAL_FREQ,
            ctx.core,
            ctx.device,
        );

        let mut uart0 = uart::init_uart0(
            chip.UART0,
            chip.pins.gpio0,
            chip.pins.gpio1,
            &mut chip.RESETS,
            chip.clocks.peripheral_clock.freq(),
        );

        // LED pin
        let mut led = chip
            .pins
            .gpio25
            .into_pull_type::<PullNone>()
            .into_push_pull_output();
        led.set_low().unwrap();

        // Pin connected to application button
        let button_pin = chip.pins.gpio22.into_pull_up_input();

        // Debug tracing pins
        let trace1_pin = chip
            .pins
            .gpio14
            .into_push_pull_output()
            .into_pull_type::<PullNone>();
        let trace2_pin = chip
            .pins
            .gpio15
            .into_push_pull_output()
            .into_pull_type::<PullNone>();

        uart0.write_str("\r\nHello from pico-rtic-try!\r\n").ok();

        let (mut uart0_reader, mut uart0_writer) = uart0.split();
        uart0_reader.enable_rx_interrupt();
        uart0_writer.enable_tx_interrupt();

        let (input_sender, input_receiver) =
            rtic_sync::make_channel!(u8, INPUT_BUFFER_SIZE);

        let (print_sender, print_receiver) =
            rtic_sync::make_channel!(PrintMsg, { print::PRINT_QUEUE_SIZE });

        let pwm_slices = hal::pwm::Slices::new(chip.PWM, &mut chip.RESETS);
        let mut pwm4 = pwm_slices.pwm4;
        // Configure PWM4 for 100.0089608 Hz
        pwm4.default_config();
        pwm4.set_top(39058);
        pwm4.set_div_int(32);
        pwm4.enable();
        pwm4.enable_interrupt();

        let usb_bus: &'static _ = ctx.local.usb_bus.insert(
            UsbBusAllocator::new(hal::usb::UsbBus::new(
                chip.USBCTRL_REGS,
                chip.USBCTRL_DPRAM,
                chip.clocks.usb_clock,
                true,
                &mut chip.RESETS,
            )),
        );

        // You *must* create the SerialPort before the USB device or it will
        // crash.
        let usb_serial = SerialPort::new(&usb_bus);

        let usb_device =
            UsbDeviceBuilder::new(&usb_bus, UsbVidPid(0x16c0, 0x27dd))
                .strings(&[StringDescriptors::default()
                    .manufacturer("Fake company")
                    .product("Serial port")
                    .serial_number("TEST")])
                .unwrap()
                .device_class(2) // from: https://www.usb.org/defined-class-codes
                .build();

        blinker::spawn(PrintHandle::new(&print_sender)).ok();
        heartbeat::spawn(PrintHandle::new(&print_sender)).ok();
        input_processor::spawn(input_receiver, PrintHandle::new(&print_sender))
            .ok();
        button_processor::spawn(PrintHandle::new(&print_sender)).ok();

        // Start the monotonic timer
        Mono::start(chip.TIMER, &mut chip.RESETS);

        uart0_writer.write_str("Finished initialization\r\n").ok();
        // Return shared and local resources
        (
            Shared {
                counter: 0,
                usb_stats: <UsbStats as Default>::default(),
            },
            Local {
                led,
                uart0_reader,
                uart0_writer,
                uart0_input_sender: input_sender.clone(),
                pwm_slice: pwm4,
                button_pin,
                button_input_sender: input_sender.clone(),
                print_output_buf: PrintOutputBuf::new(),
                print_receiver,
                trace1_pin,
                trace2_pin,
                usb_device,
                usb_serial,
            },
        )
    }

    #[task(shared = [counter], priority = 1)]
    async fn heartbeat(ctx: heartbeat::Context, mut print_handle: PrintHandle) {
        let mut counter_mutex = ctx.shared.counter;
        let heartbeat_interval = 60.secs();
        let mut msg = String::with_capacity(64);
        writeln!(
            msg,
            "Starting heartbeat with interval {heartbeat_interval}\r"
        )
        .unwrap();
        print_handle.send(msg).await;
        let mut heartbeat_time = Mono::now();
        loop {
            let counter: usize = counter_mutex.lock(|counter| *counter);
            let mut msg = String::with_capacity(64);
            writeln!(msg, "Timestamp: {heartbeat_time} counter={counter}\r")
                .unwrap();
            print_handle.send(msg).await;
            heartbeat_time += heartbeat_interval;
            Mono::delay_until(heartbeat_time).await;
        }
    }

    #[task(local = [led], priority = 1)]
    async fn blinker(ctx: blinker::Context, mut print_handle: PrintHandle) {
        let led: &mut gpio::Pin<Gpio25, FunctionSioOutput, PullNone> =
            ctx.local.led;
        let blink_period_ms = 1000;
        let mut msg = String::with_capacity(64);
        writeln!(msg, "Starting blinker with {blink_period_ms}ms period\r")
            .unwrap();
        print_handle.send(msg).await;
        let mut first_time = true;
        let mut blink_start = Mono::now();
        loop {
            led.set_high().unwrap();

            let high_time_ms = if first_time {
                first_time = false;
                800
            } else {
                200
            };

            Mono::delay_until(blink_start + high_time_ms.millis()).await;
            led.set_low().unwrap();

            blink_start += blink_period_ms.millis();
            Mono::delay_until(blink_start).await;
        }
    }

    #[task(shared=[usb_stats], priority = 1)]
    async fn input_processor(
        mut ctx: input_processor::Context,
        mut receiver: Receiver<'static, u8, INPUT_BUFFER_SIZE>,
        mut print_handle: PrintHandle,
    ) {
        print_handle.send("Starting input_processor\r\n").await;
        while let Ok(cmd) = receiver.recv().await {
            let mut msg = String::with_capacity(64);
            writeln!(msg, "Received command byte: {cmd}\r").unwrap();
            print_handle.send(msg).await;
            if cmd == b'?' {
                // Dump USB stats to UART
                let mut msg = String::with_capacity(64);
                ctx.shared.usb_stats.lock(|usb_stats: &mut UsbStats| {
                    usb_stats.dump(&mut msg).ok();
                });
                print_handle.send(msg).await;
            }
        }
    }

    #[task(local = [button_pin, button_input_sender, trace1_pin], priority = 1)]
    async fn button_processor(
        ctx: button_processor::Context,
        mut print_handle: PrintHandle,
    ) {
        let trace1_pin = ctx.local.trace1_pin;
        let button_pin: &mut ButtonPin = ctx.local.button_pin;
        let button_input_sender: &mut Sender<'static, u8, INPUT_BUFFER_SIZE> =
            ctx.local.button_input_sender;
        print_handle.send("Starting button_processor\r\n").await;
        loop {
            buttons::wait_for_button_press::<_, Mono>(button_pin).await;
            trace1_pin.set_high().unwrap();
            print_handle.send("Button pressed\r\n").await;
            print::flush_uart0::<Mono>(10).await;
            trace1_pin.set_low().unwrap();

            button_input_sender.send(b'?').await.ok();

            buttons::wait_for_button_release::<_, Mono>(button_pin).await;
            print_handle.send("Button released\r\n").await;
        }
    }

    #[task(
        binds = USBCTRL_IRQ,
        local=[usb_device, usb_serial],
        shared=[usb_stats],
        priority = 2
    )]
    fn usb_handler(mut ctx: usb_handler::Context) {
        // Do not return early from this handler because the other USB stats are
        // updated at the end of the function body.
        ctx.shared.usb_stats.lock(|usb_stats: &mut UsbStats| {
            usb_stats.interrupt_count += 1;
        });
        let usb_device: &mut UsbDevice<'static, hal::usb::UsbBus> =
            ctx.local.usb_device;
        let usb_serial: &mut SerialPort<'static, hal::usb::UsbBus> =
            ctx.local.usb_serial;

        // USB stats that will be tracked
        let mut poll_count: usize = 0;
        let mut read_err_count: usize = 0;
        let mut read_empty_count: usize = 0;
        let mut read_nonempty_count: usize = 0;
        let mut num_bytes_read: usize = 0;
        let mut write_count: usize = 0;
        let mut num_bytes_written: usize = 0;

        // Poll the USB driver with all of our supported USB Classes
        if usb_device.poll(&mut [usb_serial]) {
            poll_count += 1;
            let mut read_buf = [0u8; 64];
            match usb_serial.read(&mut read_buf) {
                Err(_usb_error) => {
                    // Do nothing
                    read_err_count += 1;
                }
                Ok(0) => {
                    // Do nothing
                    read_empty_count += 1;
                }
                Ok(count) => {
                    read_nonempty_count += 1;
                    num_bytes_read += count;

                    // Convert to upper case
                    read_buf.iter_mut().take(count).for_each(|b| {
                        b.make_ascii_uppercase();
                    });
                    // Send back to the host
                    let mut wr_ptr = &read_buf[..count];
                    while !wr_ptr.is_empty() {
                        write_count += 1;
                        let _usb_error = usb_serial.write(wr_ptr).map(|len| {
                            wr_ptr = &wr_ptr[len..];
                            num_bytes_written += len;
                        });
                    }
                }
            }
        }

        // Update USB stats on exit from interupt handler
        ctx.shared.usb_stats.lock(|usb_stats: &mut UsbStats| {
            usb_stats.interrupt_count += 1;
            usb_stats.poll_count += poll_count;
            usb_stats.read_err_count += read_err_count;
            usb_stats.read_empty_count += read_empty_count;
            usb_stats.read_nonempty_count += read_nonempty_count;
            usb_stats.num_bytes_read += num_bytes_read;
            usb_stats.write_count += write_count;
            usb_stats.num_bytes_written += num_bytes_written;
        });
    }

    #[task(
        binds = UART0_IRQ,
        local = [
            uart0_reader, uart0_writer, uart0_input_sender,
            print_receiver, print_output_buf,
            trace2_pin,
        ],
        priority = 2,
    )]
    fn uart0_handler(ctx: uart0_handler::Context) {
        let _trace2_pin = ctx.local.trace2_pin;

        let uart0_reader: &mut Uart0Reader = ctx.local.uart0_reader;
        let uart0_writer: &mut Uart0Writer = ctx.local.uart0_writer;
        let input_sender: &mut Sender<'static, u8, INPUT_BUFFER_SIZE> =
            ctx.local.uart0_input_sender;
        let print_receiver: &mut PrintReceiver = ctx.local.print_receiver;
        let print_output_buf: &mut PrintOutputBuf = ctx.local.print_output_buf;

        // Read any pending bytes from UART0 and turn them into messages
        // sent to the input processor task. This read should clear any pending
        // read interrupt flag.
        let mut buffer: [u8; UART_BUFFER_SIZE] = [0; UART_BUFFER_SIZE];
        if let Ok(num_bytes) = uart0_reader.read_raw(&mut buffer[..]) {
            for i in 0..num_bytes {
                if let Err(_) = input_sender.try_send(buffer[i]) {
                    // input_sender buffer is full, nothing I can do about it.
                    break;
                }
            }
        }

        print::uart0_print_intr_handler(
            uart0_writer,
            print_receiver,
            print_output_buf,
        );
    }

    #[task(binds = PWM_IRQ_WRAP, shared = [counter], local = [pwm_slice], priority = 3)]
    fn pwm_handler(ctx: pwm_handler::Context) {
        // Clear the PWM interrupt signal or else the handler will immediately
        // be re-triggered after leaving this function. That would prevent
        // anything else from being able to run because this is the highest
        // priority task.
        let pwm_slice: &mut hal::pwm::Slice<
            hal::pwm::Pwm4,
            hal::pwm::FreeRunning,
        > = ctx.local.pwm_slice;
        pwm_slice.clear_interrupt();

        let mut counter_mutex = ctx.shared.counter;
        counter_mutex
            .lock(|counter: &mut usize| *counter = counter.wrapping_add(1));
    }
}

#[allow(unused)]
fn debug_trace_uart_settings(
    trace_pin: &mut gpio::Pin<Gpio15, FunctionSioOutput, PullNone>,
) {
    use cortex_m::peripheral::NVIC;
    if NVIC::is_pending(pac::Interrupt::UART0_IRQ) {
        // Scope tracing: It looked like this was always false
    }
    // I thought we need to unpend the interrupt when it was forced by calling
    // pend, but experimentation showed that is not the case.
    // NVIC::unpend(pac::Interrupt::UART0_IRQ);

    // Get an (unsafe) duplicate instance of pac::uart0::RegisterBlock
    // so I can access the internals and found out why the UART0_IRQ handler
    // keeps getting triggered again and again.
    // See RP2040 Datasheet section 4.2.8 List of Registers (UART)
    let uart0 = unsafe { pac::UART0::steal() };

    let uartclr_h = uart0.uartlcr_h(); // Line Control Register, UARTLCR_H
    let fen = uartclr_h.read().fen().bit_is_set(); // FEN - Enable FIFOs
    if fen {
        // Scope tracing: It looked like this was always true
    }

    let uartfr = uart0.uartfr(); // UARTFR - Flag Register
    let txfe = uartfr.read().txfe().bit_is_set(); // TXFE - Transmit FIFO empty
    if txfe {
        // Scope tracing: It looked like this was always false
    }

    let uartmis = uart0.uartmis(); // Masked Interrupt Status Register, UARTMIS
    let txmis = uartmis.read().txmis().bit_is_set(); // Transmit masked interrupt status
    if txmis {
        // Scope tracing: It looked like this was always true
        trace_pin.set_high().unwrap();
        trace_pin.set_low().unwrap();
    }

    // "Write to clear" register - write a 1 to a bit to clear that interrupt
    let uarticr = uart0.uarticr(); // Interrupt Clear Register, UARTICR
    uarticr.modify(|_r, w| {
        w.txic().clear_bit_by_one(); // Transmit interrupt clear. Clears the UARTTXINTR interrupt
        w
    });

    // Check the masked TX interrrupt status again
    let txmis = uartmis.read().txmis().bit_is_set(); // Transmit masked interrupt status
    if txmis {
        // Scope tracing: It looked like this was always false
    }
}
