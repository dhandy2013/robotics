//! RPi Pico + RTIC program: Sound Effects using asynchronous programming
//! PWM slice 2 is used for the "driver" PWM controlling H-bridge side 1
//! PWM slice 6 is used for the "driver" PWM controlling H-bridge side 2
//! PWM slice 7 is used for the "pacing" PWM controlling sample timing
//! Pins and signals used:
//! - GPIO0 : Pin 1  UART tx -> USB-serial rx (white wire)
//! - GPIO1 : Pin 2  UART rx -> USB-serial tx (green wire)
//! - GND   : Pin 3  Ground  -> USB-serial ground
//! - GPIO4 : Pin 6  PWM2 channel A -> H-bridge side 1 PNP base
//! - GPIO5 : Pin 7  PWM2 channel B -> H-bridge side 1 NPN base
//! - GPIO12: Pin 16 PWM6 channel A -> H-bridge side 2 PNP base
//! - GPIO13: Pin 17 PWM6 channel B -> H-bridge side 2 NPN base
//! - GPIO14: Pin 19 pacer PWM slice overflow = audio sample tick
//! - GPIO15: Pin 20 sonic pump sound trace output
//! - GPIO22: Pin 29 connected to application pushbutton
//! - RUN   : Pin 30 connected to reset pushbutton
//! - GPIO25: Connected to onboard LED, on while button is pressed
#![no_std]
#![no_main]

extern crate alloc;

use alloc::{boxed::Box, string::String};
use core::{convert::TryFrom, fmt::Write};
use embedded_hal::digital::OutputPin;
use futures::{join, pin_mut, select_biased, FutureExt};
use handy_pico_rtic::{
    music,
    print::{self, PrintHandle, PrintMsg, PrintOutputBuf, PrintReceiver},
    sound_effects,
};
use handy_rp2040_common::{
    chip,
    random::{self, EnabledRingOscillator},
    sonic_ng::{
        self, play_dual_wave, play_noise, play_wave, pump_sound, NoiseChannel,
        SonicVoice, WaveGen, WaveParams,
    },
    uart::{self, Uart0Reader, Uart0Writer, UART_BUFFER_SIZE},
};
use panic_halt as _;
use rp_pico::hal::{
    self,
    gpio::{self, bank0::Gpio25, FunctionSioOutput, PullNone},
    rosc::RingOscillator,
    Clock,
};
use rtic_monotonics::{
    rp2040::prelude::ExtU64, rp2040_timer_monotonic, Monotonic,
};
use rtic_sync::channel::{Receiver, Sender};
use wavegen::wavegen16::Waveform;

include!("../../gen/cpif-song-lefthand.rs");
include!("../../gen/cpif-song-righthand.rs");

/// Picked to match the size of the R2040 UART input buffer.
const INPUT_BUFFER_SIZE: usize = uart::UART_BUFFER_SIZE;

rp2040_timer_monotonic!(Mono);

type ButtonPin =
    gpio::Pin<gpio::bank0::Gpio22, gpio::FunctionSioInput, gpio::PullUp>;
type LedPin = gpio::Pin<Gpio25, FunctionSioOutput, PullNone>;

const CLEAR_SCREEN: &str = "\x1b[2J";
const HOME: &str = "\x1b[1;1H";
const ERASE_CUR_LINE: &str = "\x1b[2K";

const MENU: &str = "\
***** \x1b[1mSOUND EFFECTS\x1b[0m *****\r
Press a key to play a sound effect, Q to make it stop.\r
\r
Q   Silence             A CPIF Song melody\r
W   SineWave            S CPIF Song harmony\r
E   TriangleWave        D Ding\r
R   SquareWave          F Noise burst\r
T   LowTone\r
Y   HighTone\r
U   WobbleTone\r
I   BirdChirp\r
O   SweepUp\r
P   SweepDown\r
?   Print menu again    0-9 DTMF tones\r
\r
";

async fn print_menu(print_handle: &mut PrintHandle) {
    print_handle.send(CLEAR_SCREEN).await;
    print_handle.send(HOME).await;
    print_handle.send(MENU).await;
}

async fn print_status(
    print_handle: &mut PrintHandle,
    cur_effect: SoundEffect,
    status_msg: &str,
) {
    let mut msg = String::with_capacity(64);
    write!(msg, "{ERASE_CUR_LINE}\r{} {status_msg}", cur_effect.name())
        .unwrap();
    print_handle.send(msg).await;
}

#[derive(Clone, Copy, Debug)]
enum SoundEffect {
    Silence,
    SineWave,
    TriangleWave,
    SquareWave,
    LowTone,
    HighTone,
    WobbleTone,
    BirdChirp,
    SweepUp,
    SweepDown,
    DTMF(sound_effects::DTMF),
    CPIFSongMelody,
    CPIFSongHarmony,
    Ding,
    Noise,
}

impl SoundEffect {
    pub fn name(&self) -> &'static str {
        use SoundEffect::*;
        match self {
            Silence => "Silence",
            SineWave => "SineWave",
            TriangleWave => "TriangleWave",
            SquareWave => "SquareWave",
            LowTone => "LowTone",
            HighTone => "HighTone",
            WobbleTone => "WobbleTone",
            BirdChirp => "BirdChirp",
            SweepUp => "SweepUp",
            SweepDown => "SweepDown",
            DTMF(dtmf) => match dtmf {
                sound_effects::DTMF::Tone0 => "DTMF 0",
                sound_effects::DTMF::Tone1 => "DTMF 1",
                sound_effects::DTMF::Tone2 => "DTMF 2",
                sound_effects::DTMF::Tone3 => "DTMF 3",
                sound_effects::DTMF::Tone4 => "DTMF 4",
                sound_effects::DTMF::Tone5 => "DTMF 5",
                sound_effects::DTMF::Tone6 => "DTMF 6",
                sound_effects::DTMF::Tone7 => "DTMF 7",
                sound_effects::DTMF::Tone8 => "DTMF 8",
                sound_effects::DTMF::Tone9 => "DTMF 9",
                sound_effects::DTMF::ToneA => "DTMF A",
                sound_effects::DTMF::ToneB => "DTMF B",
                sound_effects::DTMF::ToneC => "DTMF C",
                sound_effects::DTMF::ToneD => "DTMF D",
                sound_effects::DTMF::ToneStar => "DTMF *",
                sound_effects::DTMF::ToneHash => "DTMF #",
            },
            CPIFSongMelody => "CPIF Song melody",
            CPIFSongHarmony => "CPIF Song harmony",
            Ding => "Ding",
            Noise => "Noise burst",
        }
    }
}

impl TryFrom<u8> for SoundEffect {
    type Error = ();
    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            // 1st (top) row of keyboard
            b'0' => Ok(SoundEffect::DTMF(sound_effects::DTMF::Tone0)),
            b'1' => Ok(SoundEffect::DTMF(sound_effects::DTMF::Tone1)),
            b'2' => Ok(SoundEffect::DTMF(sound_effects::DTMF::Tone2)),
            b'3' => Ok(SoundEffect::DTMF(sound_effects::DTMF::Tone3)),
            b'4' => Ok(SoundEffect::DTMF(sound_effects::DTMF::Tone4)),
            b'5' => Ok(SoundEffect::DTMF(sound_effects::DTMF::Tone5)),
            b'6' => Ok(SoundEffect::DTMF(sound_effects::DTMF::Tone6)),
            b'7' => Ok(SoundEffect::DTMF(sound_effects::DTMF::Tone7)),
            b'8' => Ok(SoundEffect::DTMF(sound_effects::DTMF::Tone8)),
            b'9' => Ok(SoundEffect::DTMF(sound_effects::DTMF::Tone9)),
            // 2nd row
            b'Q' | b'q' => Ok(SoundEffect::Silence),
            b'W' | b'w' => Ok(SoundEffect::SineWave),
            b'E' | b'e' => Ok(SoundEffect::TriangleWave),
            b'R' | b'r' => Ok(SoundEffect::SquareWave),
            b'T' | b't' => Ok(SoundEffect::LowTone),
            b'Y' | b'y' => Ok(SoundEffect::HighTone),
            b'U' | b'u' => Ok(SoundEffect::WobbleTone),
            b'I' | b'i' => Ok(SoundEffect::BirdChirp),
            b'O' | b'o' => Ok(SoundEffect::SweepUp),
            b'P' | b'p' => Ok(SoundEffect::SweepDown),
            // 3rd row
            b'A' | b'a' => Ok(SoundEffect::CPIFSongMelody),
            b'S' | b's' => Ok(SoundEffect::CPIFSongHarmony),
            b'D' | b'd' => Ok(SoundEffect::Ding),
            b'F' | b'f' => Ok(SoundEffect::Noise),
            _ => Err(()),
        }
    }
}

enum InputMsg {
    ButtonPress,
    ButtonRelease,
    PrintMenu,
    SoundEffect(SoundEffect),
}

struct MsgParser {}

impl MsgParser {
    fn new() -> Self {
        Self {}
    }

    /// Parse the input byte and look at it along with other previously-
    /// recevied input byts. Return an InputMsg if another complete input
    /// message has been received, None otherwise.
    ///
    /// This takes a &mut self parameter because in the future I might make
    /// parsing stateful, by having multi-byte commands.
    fn parse(&mut self, input_byte: u8) -> Option<InputMsg> {
        match input_byte {
            b'?' => Some(InputMsg::PrintMenu),
            maybe_effect_code => match SoundEffect::try_from(maybe_effect_code)
            {
                Ok(sound_effect) => Some(InputMsg::SoundEffect(sound_effect)),
                Err(_) => None,
            },
        }
    }
}

struct EffectController {
    sonic_voice: Box<dyn SonicVoice>,
    print_handle: PrintHandle,
}

impl EffectController {
    /// Return the starting sound effects state
    pub fn new(
        sonic_voice: Box<dyn SonicVoice>,
        print_handle: PrintHandle,
    ) -> Self {
        Self {
            sonic_voice,
            print_handle,
        }
    }

    /// Process and respond to an input message.
    /// If the effect of the input message is to switch sound effects, return
    /// the new sound effect (without starting it yet.)
    pub async fn handle_input(
        &self,
        msg: InputMsg,
        cur_effect: SoundEffect,
    ) -> Option<SoundEffect> {
        use InputMsg::*;
        match msg {
            ButtonPress => {
                self.start_sound();
                None
            }
            ButtonRelease => {
                self.stop_sound();
                None
            }
            PrintMenu => {
                let mut print_handle = self.print_handle.clone();
                print_menu(&mut print_handle).await;
                print_status(&mut print_handle, cur_effect, "selected").await;
                None
            }
            SoundEffect(next_effect) => {
                let mut print_handle = self.print_handle.clone();
                print_status(&mut print_handle, next_effect, "playing").await;
                Some(next_effect)
            }
        }
    }

    pub fn start_sound(&self) {
        self.sonic_voice.start();
    }

    pub fn stop_sound(&self) {
        self.sonic_voice.stop();
    }

    /// Start a sound effect
    pub async fn start(&self, next_effect: SoundEffect) {
        use SoundEffect::*;
        match next_effect {
            Silence => self.silence().await,
            SineWave => self.play_wave(Waveform::SineWave).await,
            TriangleWave => self.play_wave(Waveform::TriangleWave).await,
            SquareWave => self.play_wave(Waveform::SquareWave).await,
            LowTone => self.low_tone().await,
            HighTone => self.high_tone().await,
            WobbleTone => self.wobble_tone().await,
            BirdChirp => self.bird_chirp().await,
            SweepUp => self.sweep_up().await,
            SweepDown => self.sweep_down().await,
            DTMF(dtmf) => self.dtmf_tone(dtmf).await,
            CPIFSongMelody => self.cpif_song_melody().await,
            CPIFSongHarmony => self.cpif_song_harmony().await,
            Ding => self.decay_wave(Waveform::SineWave).await,
            Noise => self.noise_burst().await,
        }
    }

    async fn silence(&self) {
        let wave_params = WaveParams::new();
        let controls = play_wave(&*self.sonic_voice, &wave_params);
        controls.set_wave_params(&wave_params);
        loop {
            Mono::delay(1000.millis()).await;
        }
    }

    /// Play a waveform at a steady 1KHz pitch and half volume
    async fn play_wave(&self, waveform: Waveform) {
        let mut wave_params = WaveParams::new();
        wave_params.set_waveform(waveform);
        wave_params.set_pitch(1000 * 16);
        wave_params.set_amp(WaveGen::MAX_AMPLITUDE / 2);
        let controls = play_wave(&*self.sonic_voice, &wave_params);
        sound_effects::steady_tone::<Mono>(&controls, wave_params, 1000).await;
    }

    /// Play a waveform with a 1KHz pitch and decaying amplitude
    async fn decay_wave(&self, waveform: Waveform) {
        let mut wave_params = WaveParams::new();
        wave_params.set_waveform(waveform);
        wave_params.set_pitch(1000 * 16);
        wave_params.set_amp(WaveGen::MAX_AMPLITUDE / 2);
        let controls = play_wave(&*self.sonic_voice, &wave_params);
        sound_effects::decay_tone::<Mono>(&controls, wave_params, 2500).await;
    }

    async fn low_tone(&self) {
        let mut wave_params = WaveParams::new();
        wave_params.set_waveform(Waveform::SineWave);
        wave_params.set_pitch(250 * 16);
        wave_params.set_amp(WaveGen::MAX_AMPLITUDE / 2);
        let controls = play_wave(&*self.sonic_voice, &wave_params);
        sound_effects::steady_tone::<Mono>(&controls, wave_params, 1000).await;
    }

    async fn high_tone(&self) {
        let mut wave_params = WaveParams::new();
        wave_params.set_waveform(Waveform::SineWave);
        wave_params.set_pitch(2000 * 16);
        wave_params.set_amp(WaveGen::MAX_AMPLITUDE / 2);
        let controls = play_wave(&*self.sonic_voice, &wave_params);
        sound_effects::steady_tone::<Mono>(&controls, wave_params, 1000).await;
    }

    async fn wobble_tone(&self) {
        let mut wave_params = WaveParams::new();
        wave_params.set_waveform(Waveform::SineWave);
        wave_params.set_amp(WaveParams::MAX_AMPLITUDE / 2);
        let controls = play_wave(&*self.sonic_voice, &wave_params);
        sound_effects::wobble_tone::<Mono>(&controls, wave_params, 2000).await;
    }

    async fn bird_chirp(&self) {
        let mut wave_params = WaveParams::new();
        wave_params.set_waveform(Waveform::SineWave);
        wave_params.set_amp(WaveParams::MAX_AMPLITUDE / 2);
        wave_params.set_pitch(1000 * 16);
        let controls = play_wave(&*self.sonic_voice, &wave_params);
        controls.set_wave_params(&wave_params);

        // 4KHz to 1KHz in 200ms
        sound_effects::sweep_freq::<Mono>(
            &controls,
            wave_params,
            4000,
            1000,
            200,
        )
        .await;

        // 3KHz for 50ms
        wave_params.set_pitch(3000 * 16);
        sound_effects::steady_tone::<Mono>(&controls, wave_params, 200).await;

        // 6KHz to 5KHz in 50ms
        // temporarily reduce volume, 6KHz is really piercing for some reason
        wave_params.set_amp(WaveParams::MAX_AMPLITUDE / 4);
        controls.set_wave_params(&wave_params);
        sound_effects::sweep_freq::<Mono>(
            &controls,
            wave_params,
            6000,
            5000,
            50,
        )
        .await;
        wave_params.set_amp(WaveParams::MAX_AMPLITUDE / 2);
        controls.set_wave_params(&wave_params);

        // silence for 200ms
        sound_effects::quiet_time::<Mono>(&controls, wave_params, 200).await;

        for _ in 0..3 {
            // 7KHz to 2KHz in 200ms
            sound_effects::sweep_freq::<Mono>(
                &controls,
                wave_params,
                7000,
                2000,
                200,
            )
            .await;

            // 4KHz for 50ms
            wave_params.set_pitch(4000 * 16);
            sound_effects::steady_tone::<Mono>(&controls, wave_params, 50)
                .await;

            // 7KHz to 6KHz in 100ms
            sound_effects::sweep_freq::<Mono>(
                &controls,
                wave_params,
                7000,
                6000,
                200,
            )
            .await;

            // silence for 50ms
            sound_effects::quiet_time::<Mono>(&controls, wave_params, 50).await;
        }
    }

    async fn sweep_up(&self) {
        let mut wave_params = WaveParams::new();
        wave_params.set_waveform(Waveform::SineWave);
        wave_params.set_amp(WaveParams::MAX_AMPLITUDE / 2);
        let controls = play_wave(&*self.sonic_voice, &wave_params);
        // 500Hz to 1KHz in 250ms
        sound_effects::sweep_freq::<Mono>(
            &controls,
            wave_params,
            500,
            1000,
            250,
        )
        .await;
    }

    async fn sweep_down(&self) {
        let mut wave_params = WaveParams::new();
        wave_params.set_waveform(Waveform::SineWave);
        wave_params.set_amp(WaveParams::MAX_AMPLITUDE / 2);
        let controls = play_wave(&*self.sonic_voice, &wave_params);
        // 1KHz to 500Hz in 250ms
        sound_effects::sweep_freq::<Mono>(
            &controls,
            wave_params,
            1000,
            500,
            250,
        )
        .await;
    }

    async fn dtmf_tone(&self, dtmf: sound_effects::DTMF) {
        let (freq1_hz, freq2_hz) = dtmf.as_tones();
        let mut wave_params_1 = WaveParams::new();
        let mut wave_params_2 = WaveParams::new();

        wave_params_1.set_waveform(Waveform::SineWave);
        wave_params_1.set_pitch(freq1_hz * 16);
        wave_params_1.set_amp(WaveParams::MAX_AMPLITUDE / 2);

        wave_params_2.set_waveform(Waveform::SineWave);
        wave_params_2.set_pitch(freq2_hz * 16);
        wave_params_2.set_amp(WaveParams::MAX_AMPLITUDE / 2);

        let (_controls1, _controls2) =
            play_dual_wave(&*self.sonic_voice, &wave_params_1, &wave_params_2);
        Mono::delay(200.millis()).await;
    }

    async fn cpif_song_melody(&self) {
        let mut wave_params = WaveParams::new();
        wave_params.set_waveform(Waveform::TriangleWave);

        let controls = play_wave(&*self.sonic_voice, &wave_params);
        music::play_song::<Mono>(
            controls,
            wave_params,
            &cpif_song_righthand::SONG[..],
            WaveParams::MAX_AMPLITUDE / 2,
        )
        .await;
    }

    async fn cpif_song_harmony(&self) {
        let mut wave_params_1 = WaveParams::new();
        let mut wave_params_2 = WaveParams::new();

        wave_params_1.set_waveform(Waveform::SineWave);
        wave_params_2.set_waveform(Waveform::SineWave);

        let (controls1, controls2) =
            play_dual_wave(&*self.sonic_voice, &wave_params_1, &wave_params_2);
        let right_hand = music::play_song::<Mono>(
            controls1,
            wave_params_1,
            &cpif_song_righthand::SONG[..],
            WaveParams::MAX_AMPLITUDE / 2,
        );
        let left_hand = music::play_song::<Mono>(
            controls2,
            wave_params_2,
            &cpif_song_lefthand::SONG[..],
            WaveParams::MAX_AMPLITUDE / 2,
        );
        let (_, _) = join!(left_hand, right_hand);
    }

    async fn noise_burst(&self) {
        let amp = WaveParams::MAX_AMPLITUDE / 2;
        let time_ms = 2500;
        let mut wave_params = WaveParams::new();
        wave_params.set_amp(amp);
        let controls =
            play_noise(&*self.sonic_voice, NoiseChannel::One, wave_params);
        sound_effects::decay_tone::<Mono>(&controls, wave_params, time_ms).await
    }
}

struct ButtonEnableMsg {
    /// delay ms then (re)enable GPIO interrupt
    delay_ms: u16,
    /// true to enable interrupt on level high, false for low
    signal_level: bool,
}

const BUTTON_ENABLE_QSIZE: usize = 3;

const BUTTON_DEBOUNCE_MS: u16 = 10;

#[rtic::app(device = rp_pico::hal::pac, dispatchers = [SW0_IRQ, SW1_IRQ, SW2_IRQ])]
mod app {
    use super::*;

    #[shared]
    struct Shared {
        button_pin: ButtonPin,
    }

    #[local]
    struct Local {
        led: LedPin,
        uart0_reader: Uart0Reader,
        uart0_writer: Uart0Writer,
        uart0_input_sender: Sender<'static, InputMsg, INPUT_BUFFER_SIZE>,
        gpio_input_sender: Sender<'static, InputMsg, INPUT_BUFFER_SIZE>,
        button_enable_sender:
            Sender<'static, ButtonEnableMsg, BUTTON_ENABLE_QSIZE>,
        print_receiver: PrintReceiver,
        print_output_buf: PrintOutputBuf,
        effect_controller: EffectController,
        msg_parser: MsgParser,
        rosc: EnabledRingOscillator,
    }

    #[init]
    fn init(ctx: init::Context) -> (Shared, Local) {
        // Setup using the chip module. This configures the default 125 MHz
        // system clock and the watchdog timer.
        let mut chip = chip::Chip::from_core_peripherals(
            rp_pico::XOSC_CRYSTAL_FREQ,
            ctx.core,
            ctx.device,
        );

        // Get the Ring Oscillator used for random number generation
        let rosc = RingOscillator::new(chip.ROSC).initialize();

        let mut uart0 = uart::init_uart0(
            chip.UART0,
            chip.pins.gpio0,
            chip.pins.gpio1,
            &mut chip.RESETS,
            chip.clocks.peripheral_clock.freq(),
        );

        // LED pin
        let mut led = chip
            .pins
            .gpio25
            .into_pull_type::<PullNone>()
            .into_push_pull_output();
        led.set_low().unwrap();

        uart0
            .write_str("\r\nHello from pico-rtic-sound-effects-async!\r\n")
            .unwrap();

        // Pin connected to application button
        let button_pin = chip.pins.gpio22.into_pull_up_input();

        let (mut uart0_reader, mut uart0_writer) = uart0.split();
        uart0_reader.enable_rx_interrupt();
        uart0_writer.enable_tx_interrupt();

        let (input_sender, input_receiver) =
            rtic_sync::make_channel!(InputMsg, INPUT_BUFFER_SIZE);

        let (print_sender, print_receiver) =
            rtic_sync::make_channel!(PrintMsg, { print::PRINT_QUEUE_SIZE });

        let (mut button_enable_sender, button_enable_receiver) =
            rtic_sync::make_channel!(ButtonEnableMsg, BUTTON_ENABLE_QSIZE);

        // Start the monotonic timer
        Mono::start(chip.TIMER, &mut chip.RESETS);

        // Init PWM slices
        let pwm_slices = hal::pwm::Slices::new(chip.PWM, &mut chip.RESETS);

        // PWM2 is the H-bridge side 1 driver PWM slice
        let mut driver1_pwm = pwm_slices.pwm2;
        driver1_pwm.channel_a.output_to(chip.pins.gpio4);
        driver1_pwm.channel_b.output_to(chip.pins.gpio5);

        // PWM6 is the H-bridge side 2 driver PWM slice
        let mut driver2_pwm = pwm_slices.pwm6;
        driver2_pwm.channel_a.output_to(chip.pins.gpio12);
        driver2_pwm.channel_b.output_to(chip.pins.gpio13);

        // PWM7 is the pacing PWM slice
        let mut pacing_pwm = pwm_slices.pwm7;
        // Trace pacing PWM
        pacing_pwm.channel_a.output_to(chip.pins.gpio14);
        // Trace pump_sound execution
        let trace_pin =
            chip.pins.gpio15.into_push_pull_output().into_pull_type();

        // Initialize sonic audio driver
        let mut pacer_token = sonic_ng::init_pacer(pacing_pwm, Some(trace_pin));
        let sonic_voice = sonic_ng::init_driver_t4_voice1(
            &mut pacer_token,
            driver1_pwm,
            driver2_pwm,
        );
        sonic_ng::start(pacer_token, true, false);

        // Spawn software tasks
        main_loop::spawn(input_receiver, PrintHandle::new(&print_sender)).ok();
        button_enabler::spawn(button_enable_receiver).ok();

        // Send command to enable the button handler
        button_enable_sender
            .try_send(ButtonEnableMsg {
                delay_ms: BUTTON_DEBOUNCE_MS,
                signal_level: false,
            })
            .ok();

        // Return shared and local resources
        (
            Shared { button_pin },
            Local {
                led,
                uart0_reader,
                uart0_writer,
                uart0_input_sender: input_sender.clone(),
                gpio_input_sender: input_sender.clone(),
                button_enable_sender,
                print_output_buf: PrintOutputBuf::new(),
                print_receiver,
                effect_controller: EffectController::new(
                    sonic_voice,
                    PrintHandle::new(&print_sender),
                ),
                msg_parser: MsgParser::new(),
                rosc,
            },
        )
    }

    /// Idle Task: replenish random numbers in the background
    #[idle(local = [rosc])]
    fn idle(ctx: idle::Context) -> ! {
        let rosc: &'static mut EnabledRingOscillator = ctx.local.rosc;
        loop {
            random::pump(rosc);
        }
    }

    /// Application Main Loop
    ///
    /// Print the command menu, then loop waiting for commands and executing
    /// them.
    #[task(local = [effect_controller], priority = 1)]
    async fn main_loop(
        ctx: main_loop::Context,
        mut input_receiver: Receiver<'static, InputMsg, INPUT_BUFFER_SIZE>,
        mut print_handle: PrintHandle,
    ) {
        let effect_controller: &mut EffectController =
            ctx.local.effect_controller;
        let mut cur_effect = SoundEffect::Silence;
        print_menu(&mut print_handle).await;
        print_status(&mut print_handle, cur_effect, "selected").await;

        let mut cur_effect_future = {
            let effect_future = effect_controller.start(cur_effect).fuse();
            Box::pin(effect_future)
        };

        loop {
            let next_msg = input_receiver.recv().fuse();
            pin_mut!(next_msg);

            select_biased! {
                msg = next_msg => {
                    match msg {
                        Ok(msg) => {
                            let maybe_next_effect =
                                effect_controller.handle_input(msg, cur_effect).await;
                            if let Some(next_effect) = maybe_next_effect {
                                cur_effect = next_effect;
                                let fused_effect_future =
                                    effect_controller.start(cur_effect).fuse();
                                cur_effect_future = Box::pin(fused_effect_future);
                                effect_controller.start_sound();
                            }
                        }
                        Err(_) => {
                            // Error receiving input message -
                            // I think this can only happen if the channel was dropped.
                        }
                    }
                },
                () = cur_effect_future => {
                    // The current effect completed naturally.
                    effect_controller.stop_sound();
                    print_status(&mut print_handle, cur_effect, "completed").await;
                    cur_effect = SoundEffect::Silence;
                    let fused_effect_future =
                        effect_controller.start(cur_effect).fuse();
                    cur_effect_future = Box::pin(fused_effect_future);
                },
            }
        }
    }

    /// Loop waiting for and executing commands to delay a certain number of
    /// milliseconds and then enable the GPIO interrupt on the button pin,
    /// level-triggered high or low depending on the signal_level parameter.
    ///
    /// Perhaps you are thinking of refactoring this function to not take a
    /// channel receiver object, but instead just take a delay and signal
    /// level.  You want to avoid looping and reading from a channel and just
    /// spawn this task whenever needed to re-enable the GPIO button interrupt.
    /// BE CAREFUL! You wrote it this way on purpose.  Think through the
    /// intialization scenario. The GPIO interrupt handler runs at a higher
    /// priority than software tasks. As soon as you re-enable the interrupt,
    /// the interrupt handler can pre-empt this task. At that point it can't
    /// spawn this task, because tasks are not re-entrant. So the signal gets
    /// lost, or you have a deadlock.
    #[task(shared = [button_pin], priority = 1)]
    async fn button_enabler(
        mut ctx: button_enabler::Context,
        mut button_enable_receiver: Receiver<
            'static,
            ButtonEnableMsg,
            BUTTON_ENABLE_QSIZE,
        >,
    ) {
        loop {
            let button_enable_msg = match button_enable_receiver.recv().await {
                Ok(msg) => msg,
                Err(_) => break,
            };
            let ButtonEnableMsg {
                delay_ms,
                signal_level,
            } = button_enable_msg;
            let interrupt = if signal_level {
                gpio::Interrupt::LevelHigh
            } else {
                gpio::Interrupt::LevelLow
            };
            Mono::delay((delay_ms as u64).millis()).await;
            ctx.shared.button_pin.lock(|button_pin: &mut ButtonPin| {
                button_pin.set_interrupt_enabled(interrupt, true);
            });
        }
    }

    /// Single-Cycle I/O (SIO) interrupt handler for processor 0.
    ///
    /// This interrupt is triggered by GPIO signal level on the pin connected to
    /// the application button. The interrupt level is attached directly to the
    /// GPIO signal level and is not latched, therefore it cannot be cleared.
    /// So instead we disable the SIO interrupt on the button pin and send a
    /// message to a software task to re-enable the GPIO interrupt after a
    /// debounce delay.
    #[task(
        binds = IO_IRQ_BANK0,
        local = [gpio_input_sender, button_enable_sender, led],
        shared = [button_pin],
        priority = 2,
    )]
    fn sio_irq_proc0_handler(mut ctx: sio_irq_proc0_handler::Context) {
        // Get the current signal level and disable the GPIO interrupt
        let signal_level =
            ctx.shared.button_pin.lock(|button_pin: &mut ButtonPin| {
                let (signal_level, interrupt) = if button_pin
                    .is_interrupt_enabled(gpio::Interrupt::LevelHigh)
                {
                    (true, gpio::Interrupt::LevelHigh)
                } else {
                    (false, gpio::Interrupt::LevelLow)
                };
                button_pin.set_interrupt_enabled(interrupt, false);
                signal_level
            });

        // Send input message to the main loop, also set LED accordingly
        let led: &mut LedPin = ctx.local.led;
        let input_sender: &mut Sender<'static, InputMsg, INPUT_BUFFER_SIZE> =
            ctx.local.gpio_input_sender;
        let cmd = if signal_level {
            // signal level high = button released
            led.set_low().unwrap();
            InputMsg::ButtonRelease
        } else {
            // signal level low = button pressed
            led.set_high().unwrap();
            InputMsg::ButtonPress
        };
        let _ = input_sender.try_send(cmd);

        // Re-enable the GPIO interrupt after button debounce delay
        let button_enable_sender: &mut Sender<
            'static,
            ButtonEnableMsg,
            BUTTON_ENABLE_QSIZE,
        > = ctx.local.button_enable_sender;
        let _ = button_enable_sender.try_send(ButtonEnableMsg {
            delay_ms: BUTTON_DEBOUNCE_MS,
            signal_level: !signal_level,
        });
    }

    #[task(
        binds = UART0_IRQ,
        local = [
            uart0_reader, uart0_writer,
            uart0_input_sender, print_receiver, print_output_buf,
            msg_parser,
        ],
        priority = 2,
    )]
    fn uart0_handler(ctx: uart0_handler::Context) {
        let uart0_reader: &mut Uart0Reader = ctx.local.uart0_reader;
        let uart0_writer: &mut Uart0Writer = ctx.local.uart0_writer;
        let input_sender: &mut Sender<'static, InputMsg, INPUT_BUFFER_SIZE> =
            ctx.local.uart0_input_sender;
        let print_receiver: &mut PrintReceiver = ctx.local.print_receiver;
        let print_output_buf: &mut PrintOutputBuf = ctx.local.print_output_buf;
        let msg_parser: &mut MsgParser = ctx.local.msg_parser;

        // Read any pending bytes from UART0 and turn them into messages
        // sent to the main loop task. This read should clear any pending
        // read interrupt flag.
        let mut buffer: [u8; UART_BUFFER_SIZE] = [0; UART_BUFFER_SIZE];
        if let Ok(num_bytes) = uart0_reader.read_raw(&mut buffer[..]) {
            for i in 0..num_bytes {
                let maybe_msg = msg_parser.parse(buffer[i]);
                if let Some(msg) = maybe_msg {
                    if input_sender.try_send(msg).is_err() {
                        // input_sender buffer is full, nothing I can do.
                        break;
                    }
                }
            }
        }

        print::uart0_print_intr_handler(
            uart0_writer,
            print_receiver,
            print_output_buf,
        );
    }

    #[task(binds = PWM_IRQ_WRAP, priority = 3)]
    fn pwm_handler(_ctx: pwm_handler::Context) {
        pump_sound();
    }
}
