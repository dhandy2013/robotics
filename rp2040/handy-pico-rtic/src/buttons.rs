//! Common code for handing buttons in RP2040 RTIC environment

use crate::timing::DurationUS;
use embedded_hal::digital::InputPin;
use rp_pico::hal::gpio;
use rtic_monotonics::{rp2040::prelude::ExtU64, Monotonic};

/// Wait until pullup input pin goes low, presumably because a pushbutton
/// connected to it has been pressed. Uses an rtic_monotonic timer to delay 1ms
/// between times it samples the pin state.
pub async fn wait_for_button_press<G, M>(
    button_pin: &mut gpio::Pin<G, gpio::FunctionSioInput, gpio::PullUp>,
) where
    G: gpio::PinId,
    M: Monotonic<Duration = DurationUS>,
{
    loop {
        if button_pin.is_low().unwrap() {
            break;
        }
        M::delay(1.millis()).await;
    }
}

/// Wait until pullup input pin goes high for at least 10ms, presumably because
/// the connected pushbutton has been released. Uses an rtic_monotomic timer to
/// delay 1ms between times it samples the pin state.
pub async fn wait_for_button_release<G, M>(
    button_pin: &mut gpio::Pin<G, gpio::FunctionSioInput, gpio::PullUp>,
) where
    G: gpio::PinId,
    M: Monotonic<Duration = DurationUS>,
{
    const DEBOUNCE_THRESHOLD_MS: u8 = 10;
    let mut debounce_ms_count: u8 = 0;
    loop {
        debounce_ms_count = if button_pin.is_low().unwrap() {
            0
        } else {
            debounce_ms_count + 1
        };
        if debounce_ms_count >= DEBOUNCE_THRESHOLD_MS {
            break;
        }
        M::delay(1.millis()).await;
    }
}
