//! Music-related items
use crate::timing::DurationUS;
use handy_rp2040_common::sonic_ng::{ControlHandle, WaveParams};
use rtic_monotonics::{rp2040::prelude::ExtU64, Monotonic};

/// Play a song on the controllable wave generator with `controls`.
///
/// `notes` is a slice of (phase_vel, duration) tuples where `phase_vel` is the
/// frequency in phase units per sample as expected by `WaveParams` and
/// `duration` is time in milliseconds. `phase_vel` of zero means rest.
///
/// `peak_volume` is the maximum amplitude that the volume is ramped up to.
/// Maximum allowed value is `WaveParams::MAX_AMPLITUDE`.
pub async fn play_song<M>(
    controls: ControlHandle,
    mut wave_params: WaveParams,
    notes: &[(u16, u16)],
    peak_volume: u16,
) where
    M: Monotonic<Duration = DurationUS>,
{
    let mut note_on: bool;
    let mut volume: u16 = 0;
    let time_step = (1 as u64).millis();
    let mut next_time = M::now();
    for (phase_vel, duration_ms) in notes.iter() {
        if *phase_vel == 0 {
            // rest
            note_on = false;
        } else {
            // play new note
            wave_params.set_phase_vel(*phase_vel);
            note_on = true;
        }
        let rampup_rate = 1102; // amplitude units per ms
        let taperoff_rate = 220; // amplitude units per ms
        for _ in 0..*duration_ms {
            if note_on {
                // Ramp up volume
                volume = u16::min(volume + rampup_rate, peak_volume);
            } else {
                // Taper off volume
                volume = volume.saturating_sub(taperoff_rate);
            }
            wave_params.set_amp(volume);
            controls.set_wave_params(&wave_params);
            next_time = next_time + time_step;
            M::delay_until(next_time).await;
        }
    }
}
