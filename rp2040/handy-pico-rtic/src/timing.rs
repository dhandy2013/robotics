//! Timing related utilities

/// Duration with microsecond resolution
/// Used with the rtic_monotonics::Monotonic trait.
pub type DurationUS = rtic_monotonics::fugit::Duration<u64, 1, 1_000_000>;
