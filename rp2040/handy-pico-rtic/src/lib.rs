//! Common library for the RP2040 and RTIC
#![no_std]

pub mod buttons;
pub mod music;
pub mod print;
pub mod sound_effects;
pub mod timing;

extern crate alloc;
