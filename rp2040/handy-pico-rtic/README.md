# Handy Programs for the Raspberry Pi Pico using RTIC

This package is for programs for the Rpi Pico that use
[RTIC](https://rtic.rs/2/book/en/) - The hardware accelerated Rust RTOS.

