# Music Files

## GWBASIC PLAY format

The .txt files in this directory contain music in the ancient GWBASIC [PLAY]
(https://hwiegman.home.xs4all.nl/gw-man/PLAY.html) format.

Changes from the old GWBASIC PLAY format:

- The input can contain multiple lines of ASCII text
- The "*" character means comment till end-of-line
- Octave numbers follow the international standard:
    - C in octave 4 is middle C (not octave 3 like GWBASIC)
    - Default is still octave 4 however
- N command not yet implemented
- MF (music foreground) and MB (music background) have no effect

Example: Notes going up and down the scale
```
c8 e8 g8 > c < g8 e8 c2
```

## Generating Rust Code from Music Files

The intended purpose of these files is to run a Python script to transform
them into Rust code to be included into Rust programs in order to play songs.

First, install the Python `cpif` library [available on SourceForge](https://sourceforge.net/p/handy-cpif/code/ci/default/tree/).

Then run the `gen_song_data.py` script to process a music file and produce Rust code.
Example (run from the `rp2040` directory):
```
python scripts/gen_song_data.py music/cpif-song-righthand.txt > handy-pico-rtic/gen/cpif-song-righthand.rs
```

Then in your Rust program put a line like this:
```
include!("../../gen/cpif-song-righthand.rs");
```
By default the script generates a variable named `SONG` that is an array of
frequency and timing info. Read the docs for the `handy_rp2040_comon::sonic` and
`sonig_ng` module and the example `pico-music.rs` program for more information
on how to turn this data into sound.
