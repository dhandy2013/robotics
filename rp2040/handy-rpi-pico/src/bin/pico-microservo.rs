//! Raspberry Pi Pico program:
//! Control an SG90 MicroServo 9g (compatible) servo motor via PWM signal
//! Pins used:
//! - Outputs:
//!     - GP25: Onboard LED
//!     - GP09: Pin 12
//! - Inputs:
//!     - GP22: Pin 29 connected to pushbutton that connects to ground
//! Expected behavior:
//! - Wait for pushbutton to ground pin 29
//! - Move servo arm to home position if it wasn't there already
//! - Rapidly rotate arm 45 degrees
//! - Move servo arm back to home position
//! - Wait for button to be released for at least 10 ms
//! - Repeat
#![no_std]
#![no_main]

use cortex_m_rt::entry;
use embedded_hal::delay::DelayNs;
use embedded_hal_0_2::PwmPin;
use handy_rp2040_common::buttons::{
    wait_for_button_press, wait_for_button_release,
};
use panic_halt as _;
use rp_pico::hal::{self, gpio, pac, timer::Timer};

/// The spec for the Micro Servo says "Operating speed: 0.1 s/60 degree".
/// The spec says the servo has 180 degrees of motion, so to get all the way
/// over from left to right should take about 0.3 seconds = 300ms.
const MAX_SERVO_DELAY_MS: u32 = 300;

/// Time servo arm will be allowed to travel before it is sent back to home.
const SERVO_TRAVEL_MS: u32 = 150;

// Duty cycle of 3279 == 1.00 ms pulse width
const NARROW_PULSE_WIDTH: u16 = 3279;

// Duty cycle of 6557 == 2.00 ms pulse width
const WIDE_PULSE_WIDTH: u16 = 6557;

/// Disconnect a pin from a PWM channel and disable it (so it is not an output
/// stuck at a random value.)
fn disconnect_pwm_pin(
    pin: &mut gpio::Pin<gpio::DynPinId, gpio::DynFunction, gpio::DynPullType>,
) {
    let _ = pin.try_set_function(gpio::DynFunction::Null);
    pin.set_pull_type(gpio::DynPullType::None);
}

#[entry]
fn main() -> ! {
    // Grab our singleton objects
    let mut pac = pac::Peripherals::take().unwrap();
    let _core = pac::CorePeripherals::take().unwrap();

    // Set up the watchdog driver - needed by the clock setup code
    let mut watchdog = hal::Watchdog::new(pac.WATCHDOG);

    // Configure the clocks
    //
    // The default is to generate a 125 MHz system clock
    let clocks = hal::clocks::init_clocks_and_plls(
        rp_pico::XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let mut timer = Timer::new(pac.TIMER, &mut pac.RESETS, &clocks);

    // Init PWMs
    let mut pwm_slices = hal::pwm::Slices::new(pac.PWM, &mut pac.RESETS);

    // Configure PWM4 for 50.0288 Hz when fsys is 125MHz
    let pwm = &mut pwm_slices.pwm4;
    pwm.default_config();
    pwm.set_div_int(38);
    pwm.set_div_frac(2);

    // Single-cycle I/O block
    let sio = hal::Sio::new(pac.SIO);

    let pins = rp_pico::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    // According to the Raspberry Pi Pico datasheet, PWM slice 4 channel B controls
    // GPIO9 (pin 12 on the Pico) and GPIO25 (built-in LED on the Pico.)
    // That means that when we set the function for those two pins to PWM, then
    // PWM slice 4 channel B will control both those signals simultaneously.
    //
    // So let's go get those two signals. We represent them as "dynamic" pins so
    // we can easily change the pin function from PWM to disabled and back
    // again.
    let mut pin1: gpio::Pin<
        gpio::DynPinId,
        gpio::DynFunction,
        gpio::DynPullType,
    > = {
        let p = pins.led.into_dyn_pin();
        // SAFETY: Yes, I know I am creating a dynamic pin.
        unsafe { p.into_unchecked() }
    };
    let mut pin2: gpio::Pin<
        gpio::DynPinId,
        gpio::DynFunction,
        gpio::DynPullType,
    > = {
        let p = pins.gpio9.into_dyn_pin();
        // SAFETY: Yes, I know I am creating a dynamic pin.
        unsafe { p.into_unchecked() }
    };

    // Connect pin 29 (GP22) to button
    let mut button = pins.gpio22.into_pull_up_input();

    let mut first_button_press = true;
    loop {
        wait_for_button_press(&mut button);

        if first_button_press {
            first_button_press = false;

            // Turn on PWM signal
            pwm.channel_b.set_duty(NARROW_PULSE_WIDTH);
            let _ = pin1.try_set_function(gpio::DynFunction::Pwm);
            let _ = pin2.try_set_function(gpio::DynFunction::Pwm);
            pwm.enable();

            // Wait for servo arm to get settled in home position
            timer.delay_ms(MAX_SERVO_DELAY_MS);

            // Turn off PWM signal
            pwm.disable();
            // Avoid stuck-on glitches.
            disconnect_pwm_pin(&mut pin1);
            disconnect_pwm_pin(&mut pin2);
        }

        // Turn on PWM signal
        pwm.channel_b.set_duty(WIDE_PULSE_WIDTH);
        let _ = pin1.try_set_function(gpio::DynFunction::Pwm);
        let _ = pin2.try_set_function(gpio::DynFunction::Pwm);
        pwm.enable();

        // Give servo arm time to move about 45 degrees
        timer.delay_ms(SERVO_TRAVEL_MS);

        // Change pulse with to tell servo to return to home position
        pwm.channel_b.set_duty(NARROW_PULSE_WIDTH);

        // Wait for servo arm to get settled in home position
        timer.delay_ms(MAX_SERVO_DELAY_MS);

        // Turn off PWM signal
        pwm.disable();
        // If you don't disconnect and disable the PWM pins after disabling the
        // PWM slice, the pins are left as outputs with random values (0 or 1)
        // which is not usually what anyone wants.
        disconnect_pwm_pin(&mut pin1);
        disconnect_pwm_pin(&mut pin2);

        wait_for_button_release(&mut button, &mut timer);
    }
}
