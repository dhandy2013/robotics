//! Raspberry Pi Pico program: PWM timer experiments
//! Pins and signals used:
//! - GPIO0: Pin 1: UART tx -> USB-serial rx (white wire on my device)
//! - GPIO1: Pin 2: UART rx -> USB-serial tx (green wire on my device)
//! - GND  : Pin 3: Ground  -> USB-serial ground
//! - GP25: Onboard LED
#![no_std]
#![no_main]

use core::cell::RefCell;
use core::fmt::Write; // for write!(), writeln!()
use core::sync::atomic::{AtomicUsize, Ordering};
use cortex_m_rt::entry;
use critical_section::Mutex;
use embedded_hal::digital::OutputPin;
use panic_halt as _;
use rp_pico::hal::pwm::{FreeRunning, Pwm4, Slice};
use rp_pico::hal::{self, pac::interrupt, prelude::*};

/// Static structure to transfer the PWM slice from the main program
/// to the interrupt handler.
static PWM4: Mutex<RefCell<Option<PwmSlice>>> = Mutex::new(RefCell::new(None));
type PwmSlice = Slice<Pwm4, FreeRunning>;

/// Count of timer ticks since pacing PWM interrupts were enabled.
/// This will roll over after about 994 days.
static TIMER_TICKS: AtomicUsize = AtomicUsize::new(0);

#[entry]
fn main() -> ! {
    // Grab our singleton objects
    let mut pac = hal::pac::Peripherals::take().unwrap();
    let core = hal::pac::CorePeripherals::take().unwrap();

    // Set up the watchdog driver - needed by the clock setup code
    let mut watchdog = hal::Watchdog::new(pac.WATCHDOG);

    // Configure the clocks
    //
    // The default is to generate a 125 MHz system clock
    let clocks = hal::clocks::init_clocks_and_plls(
        rp_pico::XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let mut delay = cortex_m::delay::Delay::new(
        core.SYST,
        clocks.system_clock.freq().to_Hz(),
    );

    // Init PWMs
    let pwm_slices = hal::pwm::Slices::new(pac.PWM, &mut pac.RESETS);

    // Configure PWM4 for 50.0288 Hz when fsys is 125MHz
    let mut pwm = pwm_slices.pwm4;
    pwm.default_config();
    pwm.set_div_int(38);
    pwm.set_div_frac(2);
    pwm.set_top(65535);

    // Single-cycle I/O block
    let sio = hal::Sio::new(pac.SIO);

    let pins = rp_pico::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    // UART for debug printing
    let uart_pins = (
        // UART TX (characters sent from RP2040) on pin 1 (GPIO0)
        pins.gpio0.into_function::<hal::gpio::FunctionUart>(),
        // UART RX (characters received by RP2040) on pin 2 (GPIO1)
        pins.gpio1.into_function::<hal::gpio::FunctionUart>(),
    );
    let mut uart =
        hal::uart::UartPeripheral::new(pac.UART0, uart_pins, &mut pac.RESETS)
            .enable(
                // 115200 baud, 8 data bits, 1 stop bit, no parity
                hal::uart::UartConfig::default(),
                clocks.peripheral_clock.freq(),
            )
            .unwrap();
    writeln!(uart, "Hello from pico-pwm-timer.rs\r").unwrap();

    // Useful microsecond-resolution timer
    let timer = hal::Timer::new(pac.TIMER, &mut pac.RESETS, &clocks);

    // LED for diagnostic signals
    let mut led = pins.led.into_push_pull_output();

    // Tell the NVIC to start calling the PWM interrupt handler function
    unsafe {
        hal::pac::NVIC::unmask(hal::pac::Interrupt::PWM_IRQ_WRAP);
    }

    // Enable the interrupt for this PWM slice
    pwm.enable_interrupt();

    critical_section::with(move |cs| {
        // Put this pwm slice into a static struct accessible from the
        // interrupt handler.
        PWM4.borrow(cs).replace(Some(pwm));
        // Borrow the pwm slice back from the static struct so we can
        // start the PWM clock running.
        let pwm_ref = PWM4.borrow(cs);
        let mut pwm_mut = pwm_ref.borrow_mut();
        let pwm = pwm_mut.as_mut().unwrap();
        // Start the PWM clock running
        pwm.enable();
    });

    for i in 0..10 {
        let t0_ticks = TIMER_TICKS.load(Ordering::SeqCst);
        let t0_timer = timer.get_counter();

        while TIMER_TICKS.load(Ordering::SeqCst) == t0_ticks {}
        let t1_timer = timer.get_counter();

        writeln!(
            uart,
            "[{}] TIMER_TICKS period: {} microseconds\r",
            i,
            (t1_timer - t0_timer)
        )
        .unwrap();
    }

    // Slow blink to signal end of program
    loop {
        led.set_high().unwrap();
        delay.delay_ms(500);
        led.set_low().unwrap();
        delay.delay_ms(500);
    }
}

/// PWM interrupt handler
/// https://raspberrypi.github.io/pico-sdk-doxygen/group__hardware__irq.html
#[allow(non_snake_case)]
#[interrupt]
fn PWM_IRQ_WRAP() {
    // We MUST grab the pwm slice out of the static struct and clear the
    // interrupt. Otherwise, this handler function will be called again
    // immediately upon returning - forever.
    critical_section::with(move |cs| {
        let pwm_ref = PWM4.borrow(cs);
        let mut pwm_mut = pwm_ref.borrow_mut();
        if let Some(ref mut pwm) = pwm_mut.as_mut() {
            // If you comment-out this line, this program will crash:
            pwm.clear_interrupt();
        }
    });

    // TODO: Figure out exactly which PWM slice interrupt was triggered.
    //       However, this is Ok for now as long as some other code doesn't
    //       enable interrupts for a differnent PWM slice.

    // According to the docs, the ARM thumbv6m does not support fetch_add().  It
    // can only do load() and store(). But this is Ok because this interrupt
    // routine and init() are the only places where TIMER_TICKS.store() is
    // called.
    let timer_ticks = TIMER_TICKS.load(Ordering::SeqCst);
    TIMER_TICKS.store(timer_ticks.wrapping_add(1), Ordering::SeqCst);
}
