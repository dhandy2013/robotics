//! Pico program: Test setting and controlling PWM interrupt handler.
//! PWM slice 0 is used for the "driver" PWM (signals sent to speaker)
//! PWM slice 1 is used for the "pacing" PWM controlling sample timing
//! Pins and signals used:
//! - GPIO0 : Pin 1  UART0 tx -> USB-serial rx (white wire on my device)
//! - GPIO1 : Pin 2  UART0 rx -> USB-serial tx (green wire on my device)
//! - GND   : Pin 3  Ground -> USB-serial ground
//! - GPIO8 : Pin 11 output -> NPN BJT controlling LED (PWM slice 4 channel A)
//! - GPIO22: Pin 29 pullup input -> application pushbutton
//! - GPIO25:        output -> onboard LED (PWM slice 4 channel B)
//! - RUN   : Pin 30 pullup input -> connected to reset pushbutton
//!
//! While the application button is pressed, the built-LED and the external LED
//! pulse on and off on a 2-second cycle in opposite phases from each other.
#![no_std]
#![no_main]

extern crate alloc;

use alloc::boxed::Box;
use core::cell::Cell;
use core::fmt::Write;
use cortex_m_rt::entry;
use critical_section::Mutex;
use embedded_hal::{delay::DelayNs, digital::InputPin};
use handy_rp2040_common::{
    buttons::wait_for_button_press,
    chip,
    pwmutil::{self, DynamicPwmSlice},
    uart,
};
use panic_halt as _;
use rp_pico::hal::{self, gpio, pac::interrupt, timer::Timer, Clock};

#[derive(Clone, Copy)]
struct PwmControls {
    chan_a: u16,
    chan_b: u16,
}

static PWM_CONTROLS: Mutex<Cell<PwmControls>> =
    Mutex::new(Cell::new(PwmControls {
        chan_a: 0,
        chan_b: 0,
    }));

fn set_pulse_widths(chan_a: u16, chan_b: u16) {
    critical_section::with(|cs| {
        let pwm_controls_cell = PWM_CONTROLS.borrow(cs);
        pwm_controls_cell.set(PwmControls { chan_a, chan_b });
    });
}

#[entry]
fn main() -> ! {
    // Grab our singleton objects
    let mut chip = chip::Chip::new(rp_pico::XOSC_CRYSTAL_FREQ);
    let mut uart0 = uart::init_uart0(
        chip.UART0,
        chip.pins.gpio0,
        chip.pins.gpio1,
        &mut chip.RESETS,
        chip.clocks.peripheral_clock.freq(),
    );

    // The timer object lets us wait for specified amounts of time (in
    // milliseconds)
    let mut timer = Timer::new(chip.TIMER, &mut chip.RESETS, &chip.clocks);

    writeln!(uart0, "\r").unwrap();
    writeln!(uart0, "Hello from pico-test-pwm-handler\r").unwrap();

    let (slice_id, pwm_handler) = {
        // Init Dynamic PWM Slice wrapper around PWM4
        let pwm_slices = hal::pwm::Slices::new(chip.PWM, &mut chip.RESETS);
        let pwm4 = pwm_slices.pwm4;
        writeln!(uart0, "Instantiating DynamicPwmSlice\r").unwrap();
        let mut dyn_pwm = DynamicPwmSlice::from(pwm4);

        // Set PWM clock to run at 1KHz and start the clock running.
        dyn_pwm.default_config();
        dyn_pwm.set_div_int(125);
        dyn_pwm.set_div_frac(0);
        dyn_pwm.set_top(999);
        dyn_pwm.enable();
        dyn_pwm.enable_interrupt();

        // Return the slice ID and interrupt handler
        let slice_id = dyn_pwm.slice_id();
        (
            slice_id,
            Box::new(move || {
                // On PWM interrupt get the latest control signals
                let pwm_controls = critical_section::with(|cs| {
                    let duty_cycles_cell = PWM_CONTROLS.borrow(cs);
                    duty_cycles_cell.get()
                });
                dyn_pwm.write_channel_a_b(
                    pwm_controls.chan_a,
                    pwm_controls.chan_b,
                );
            }),
        )
    };
    pwmutil::set_interrupt_handler(slice_id, Some(pwm_handler));
    pwmutil::enable_pwm_interrupts();

    // Connect LEDs to PWM channels to measure duty cycle via brightness.
    // See RP2040 datasheet section 4.5.2 for the map of pins to PWM channels.

    // Connect external LED on GPIO8 (Pico pin 11) go PWM slice 4 channel A
    let _led1 = chip.pins.gpio8.into_function::<gpio::FunctionPwm>();

    // Connect built-in LED to PWM slice 4 channel B.
    let _led2 = chip.pins.gpio25.into_function::<gpio::FunctionPwm>();

    // Connect pin 29 (GPIO22) to start/stop button
    let mut button = chip.pins.gpio22.into_pull_up_input();

    writeln!(
        uart0,
        "Press and hold the button to generate PWM signal on LED\r"
    )
    .unwrap();

    fn button_release_check(
        button: &mut gpio::Pin<
            gpio::bank0::Gpio22,
            gpio::FunctionSioInput,
            gpio::PullUp,
        >,
        count: &mut u8,
    ) -> bool {
        if button.is_low().unwrap() {
            // Signal low == ground == button still pressed
            *count = 0;
        } else {
            *count += 1;
        }
        *count >= 10
    }

    loop {
        wait_for_button_press(&mut button);
        write!(uart0, "Generating PWM signal...").unwrap();

        let mut button_release_count: u8 = 0;
        'outer_loop: loop {
            uart0.write_char('<').unwrap();
            for i in 0..1000 {
                if button_release_check(&mut button, &mut button_release_count)
                {
                    break 'outer_loop;
                }
                set_pulse_widths(i, 999 - i);
                timer.delay_ms(1);
            }
            uart0.write_char('>').unwrap();
            for i in (0..1000).rev() {
                if button_release_check(&mut button, &mut button_release_count)
                {
                    break 'outer_loop;
                }
                set_pulse_widths(i, 999 - i);
                timer.delay_ms(1);
            }
        }

        set_pulse_widths(0, 0);
        writeln!(uart0, "done.\r").unwrap();
    }
}

/// Remember to define this function whenever you call
/// `pwmutil::enable_pwm_interrupts()` and call `.enable_interrupt()` on a PWM
/// slice. Otherwise, a default handler is used that gets run in an infinite
/// loop, causing your program to effectively halt.
#[allow(non_snake_case)]
#[interrupt]
fn PWM_IRQ_WRAP() {
    pwmutil::pwm_irq_wrap_handler();
}
