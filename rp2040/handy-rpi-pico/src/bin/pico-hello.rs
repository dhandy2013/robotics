//! Raspberry Pi Pico app that prints a hello message and stops.
//! Pins and signals used:
//! - GPIO0 : Pin 1: UART tx -> USB-serial rx (white wire on my device)
//! - GPIO1 : Pin 2: UART rx -> USB-serial tx (green wire on my device)
//! - GND   : Pin 3: Ground  -> USB-serial ground
//! - RUN   : Pin 30 connected to reset pushbutton
//! - GPIO25: Connected to onboard LED
#![no_std]
#![no_main]

use core::fmt::Write; // for write!(), writeln!()
use cortex_m_rt::entry;
use handy_rp2040_common::{chip, uart};
use panic_halt as _;
use rp_pico::hal::clocks::Clock;

#[entry]
fn main() -> ! {
    // Standard setup
    let mut chip = chip::Chip::new(rp_pico::XOSC_CRYSTAL_FREQ);

    let mut uart0 = uart::init_uart0(
        chip.UART0,
        chip.pins.gpio0,
        chip.pins.gpio1,
        &mut chip.RESETS,
        chip.clocks.peripheral_clock.freq(),
    );

    writeln!(uart0, "\r").unwrap();
    writeln!(uart0, "Hello from pico-hello.rs!\r").unwrap();

    loop {}
}
