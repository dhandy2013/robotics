//! Pico program: Play 1kHz sine wave on speaker using t4 driver
//! PWM slice 2 is used for the "driver" PWM controlling H-bridge side 1
//! PWM slice 6 is used for the "driver" PWM controlling H-bridge side 2
//! PWM slice 7 is used for the "pacing" PWM controlling sample timing
//! Pins and signals used:
//! - GPIO0 : Pin 1  UART0 tx -> USB-serial rx (white wire on my device)
//! - GPIO1 : Pin 2  UART0 rx -> USB-serial tx (green wire on my device)
//! - GND   : Pin 3  Ground   -> USB-serial ground
//! - GPIO4 : Pin 6  connected to PWM2 channel A -> PNP 1 gate
//! - GPIO5 : Pin 7  connected to PWM2 channel B -> NPN 1 gate
//! - GPIO12: Pin 16 connected to PWM6 channel A -> NPN 2 gate
//! - GPIO13: Pin 17 connected to PWM6 channel B -> PNP 2 gate
//! - GPIO14: Pin 19 connected to PWM7 channel A -> trigger on sample start
//! - GPIO15: Pin 20 connected to PWM7 channel B -> trace interrupt handler
//! - GND   : Pin 23 ground connection on breadboard / circuitboard
//! - GPIO20: Pin 26 connected to PWM2 channel A (inverted) -> NPN 2 gate
//! - GPIO21: Pin 27 connected to PWM2 channel B (inverted) -> PNP 2 gate
//! - GPIO22: Pin 29 connected to on/off pushbutton
//! - RUN   : Pin 30 connected to reset pushbutton
//! - GPIO25: connected to internal LED to signal when button is pressed
//!
//! The collectors of PNP 1 and NPN 1 are connected to speaker input 1;
//! the collectors of PNP 2 and NPN 2 are connected to speaker input 2.
//!
//! This code is careful to ensure that PNP 1 and NPN 1 are never turned on at
//! the same time, and PNP 2 and NPN 2 are never turned on at the same time.
//! Also there is a "dead zone" of 225 nS between when the PNP transistor is
//! turned off and the corresponding NPN transitor is turned on and vise versa.
//!
//! While the application button is pressed, a 1KHz sine wave is generated.
#![no_std]
#![no_main]

use core::fmt::Write; // for write!(), writeln!()
use cortex_m_rt::entry;
use embedded_hal::digital::OutputPin;
use handy_rp2040_common::{
    buttons::{wait_for_button_press, wait_for_button_release},
    pwmutil,
    sonic::{self, Channel, WaveGen, Waveform},
};
use panic_halt as _;
use rp_pico::hal::{self, gpio, pac::interrupt, timer::Timer, Clock};

#[entry]
fn main() -> ! {
    // Grab our singleton objects
    let mut pac = hal::pac::Peripherals::take().unwrap();
    let _core = hal::pac::CorePeripherals::take().unwrap();

    // Set up the watchdog driver - needed by the clock setup code
    let mut watchdog = hal::Watchdog::new(pac.WATCHDOG);

    // Configure the clocks
    // The default is to generate a 125 MHz system clock
    let clocks = hal::clocks::init_clocks_and_plls(
        rp_pico::XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let mut timer = Timer::new(pac.TIMER, &mut pac.RESETS, &clocks);

    // Single-cycle I/O block
    let sio = hal::Sio::new(pac.SIO);

    let pins = rp_pico::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    let uart0_pins = (
        // UART0 TX (characters sent from RP2040) on pin 1 (GPIO0)
        pins.gpio0.into_function::<hal::gpio::FunctionUart>(),
        // UART0 RX (characters received by RP2040) on pin 2 (GPIO1)
        pins.gpio1.into_function::<hal::gpio::FunctionUart>(),
    );
    let mut uart0 =
        hal::uart::UartPeripheral::new(pac.UART0, uart0_pins, &mut pac.RESETS)
            .enable(
                // 115200 baud, 8 data bits, 1 stop bit, no parity
                hal::uart::UartConfig::default(),
                clocks.peripheral_clock.freq(),
            )
            .unwrap();

    writeln!(uart0, "\r").unwrap();
    writeln!(uart0, "Hello from pico-sine1k-t4.rs!\r").unwrap();

    // Init PWM slices
    let pwm_slices = hal::pwm::Slices::new(pac.PWM, &mut pac.RESETS);

    // PWM2 is the H-bridge side 1 driver PWM slice
    let driver1_pwm = pwm_slices.pwm2;
    let driver1_channel_a_pin = pins.gpio4.into_pull_type();
    let driver1_channel_b_pin = pins.gpio5.into_pull_type();

    // PWM6 is the H-bridge side 2 driver PWM slice
    let driver2_pwm = pwm_slices.pwm6;
    let driver2_channel_a_pin = pins.gpio12.into_pull_type();
    let driver2_channel_b_pin = pins.gpio13.into_pull_type();

    // PWM7 is the pacing PWM slice
    let pacing_pwm = pwm_slices.pwm7;

    // Connect to the pacing PWM slice channel A
    let _pacing_a_pin = pins
        .gpio14
        .into_function::<gpio::FunctionPwm>()
        .into_pull_type::<gpio::PullNone>();

    // Configure this output to be used as an output to trace interrupt handler
    // execution.
    let handler_trace_pin = pins
        .gpio15
        .into_function::<gpio::FunctionSioOutput>()
        .into_pull_type::<gpio::PullNone>();

    let sonic_client = sonic::init_t4driver(
        pacing_pwm,
        driver1_pwm,
        driver1_channel_a_pin,
        driver1_channel_b_pin,
        driver2_pwm,
        driver2_channel_a_pin,
        driver2_channel_b_pin,
        Some(handler_trace_pin),
    );

    // LED to give feedback when button is pressed
    let mut led = pins.led.into_push_pull_output();
    // Connect pin 29 (GPIO22) to start/stop button
    let mut button = pins.gpio22.into_pull_up_input();

    let waveform = Waveform::SineWave;
    let freq_x16: u16 = 1000 * 16;
    let channel = Channel::ChannelA;
    sonic_client.set_waveform(channel, waveform);
    sonic_client.set_freq(channel, freq_x16);
    sonic_client.set_volume(channel, WaveGen::MAX_AMPLITUDE / 2);

    writeln!(
        uart0,
        "Press and hold the button to generate 1KHz sine wave\r"
    )
    .unwrap();

    loop {
        wait_for_button_press(&mut button);

        sonic_client.set_sound_output(true);
        led.set_high().unwrap();
        write!(uart0, "Generating 1KHz sine wave...").unwrap();

        wait_for_button_release(&mut button, &mut timer);

        sonic_client.set_sound_output(false);
        led.set_low().unwrap();
        writeln!(uart0, "done.\r").unwrap();
    }
}

#[allow(non_snake_case)]
#[interrupt]
fn PWM_IRQ_WRAP() {
    pwmutil::pwm_irq_wrap_handler();
}
