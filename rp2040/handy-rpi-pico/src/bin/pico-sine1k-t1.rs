//! Pico program: Play 1kHz sine wave on speaker using t4 driver
//! PWM slice 0 is used for the "driver" PWM (signals sent to speaker)
//! PWM slice 1 is used for the "pacing" PWM controlling sample timing
//! Pins and signals used:
//! - GPIO0 : Pin 1  UART0 tx -> USB-serial rx (white wire on my device)
//! - GPIO1 : Pin 2  UART0 rx -> USB-serial tx (green wire on my device)
//! - GND   : Pin 3  Ground   -> USB-serial ground
//! - GPIO2 : Pin 4  connected to PWM1 channel A t1 pacing
//! - GPIO3 : Pin 5  connected to PWM1 channel B t1 interrupt handler execution
//! - GPIO16: Pin 21 connected to PWM0 channel A t1 driver speaker 1
//! - GPIO17: Pin 22 connected to PWM0 channel B t1 driver speaker 2
//! - GPIO22: Pin 29 connected to on/off pushbutton
//! - RUN   : Pin 30 connected to reset pushbutton
//! - GPIO25: connected to internal LED to signal when button is pressed
//!
//! While the application button is pressed, the same 1KHz sine wave is
//! generated on both GPIO16 (pin 21) and GIO17 (pin 22).
#![no_std]
#![no_main]

use core::fmt::Write; // for write!(), writeln!()
use cortex_m_rt::entry;
use embedded_hal::digital::OutputPin;
use handy_rp2040_common::{
    buttons::{wait_for_button_press, wait_for_button_release},
    pwmutil,
    sonic::{self, Channel, WaveGen, Waveform},
};
use panic_halt as _;
use rp_pico::hal::{self, gpio, pac::interrupt, timer::Timer, Clock};

#[entry]
fn main() -> ! {
    // Grab our singleton objects
    let mut pac = hal::pac::Peripherals::take().unwrap();
    let _core = hal::pac::CorePeripherals::take().unwrap();

    // Set up the watchdog driver - needed by the clock setup code
    let mut watchdog = hal::Watchdog::new(pac.WATCHDOG);

    // Configure the clocks
    // The default is to generate a 125 MHz system clock
    let clocks = hal::clocks::init_clocks_and_plls(
        rp_pico::XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    // The timer object lets us wait for specified amounts of time (in
    // milliseconds)
    let mut timer = Timer::new(pac.TIMER, &mut pac.RESETS, &clocks);

    // Single-cycle I/O block
    let sio = hal::Sio::new(pac.SIO);

    let pins = rp_pico::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    let uart0_pins = (
        // UART0 TX (characters sent from RP2040) on pin 1 (GPIO0)
        pins.gpio0.into_function::<hal::gpio::FunctionUart>(),
        // UART0 RX (characters received by RP2040) on pin 2 (GPIO1)
        pins.gpio1.into_function::<hal::gpio::FunctionUart>(),
    );
    let mut uart0 =
        hal::uart::UartPeripheral::new(pac.UART0, uart0_pins, &mut pac.RESETS)
            .enable(
                // 115200 baud, 8 data bits, 1 stop bit, no parity
                hal::uart::UartConfig::default(),
                clocks.peripheral_clock.freq(),
            )
            .unwrap();

    writeln!(uart0, "\r").unwrap();
    writeln!(uart0, "Hello from pico-sine1k-t1.rs!\r").unwrap();

    // Init PWM slices
    // See RP2040 datasheet section 4.5.2 for a chart of which pins go with
    // which PWM slices.
    let pwm_slices = hal::pwm::Slices::new(pac.PWM, &mut pac.RESETS);

    // Split out the driver PWM slice 0 and connect pins for channels A and B.
    let driver_pwm = pwm_slices.pwm0;
    let driver_a_pin = pins
        .gpio16
        .into_function::<gpio::FunctionNull>()
        .into_pull_type::<gpio::PullNone>();
    let driver_b_pin = pins
        .gpio17
        .into_function::<gpio::FunctionNull>()
        .into_pull_type::<gpio::PullNone>();

    // Split out the pacing PWM slice 1 and connect GPIO2 (pin 4) to channel A.
    // Use this as the oscilloscope trigger on each new audio sample.
    let pacing_pwm = pwm_slices.pwm1;
    let _pacing_a_pin = pins
        .gpio2
        .into_function::<gpio::FunctionPwm>()
        .into_pull_type::<gpio::PullNone>();

    // Configure GPIO3 (pin 5) to be used as an output to trace interrupt
    // handler execution.
    let handler_trace_pin = pins
        .gpio3
        .into_function::<gpio::FunctionSioOutput>()
        .into_pull_type::<gpio::PullNone>();

    let sonic_client = sonic::init_t1driver(
        driver_pwm,
        pacing_pwm,
        Some(driver_a_pin),
        Some(driver_b_pin),
        Some(handler_trace_pin),
    );

    // LED to give feedback when button is pressed
    let mut led = pins.led.into_push_pull_output();
    // Connect pin 29 (GPIO22) to start/stop button
    let mut button = pins.gpio22.into_pull_up_input();

    let waveform = Waveform::SineWave;
    let freq_x16: u16 = 1000 * 16;
    for channel in [Channel::ChannelA, Channel::ChannelB] {
        sonic_client.set_waveform(channel, waveform);
        sonic_client.set_freq(channel, freq_x16);
        sonic_client.set_volume(channel, WaveGen::MAX_AMPLITUDE / 2);
    }

    writeln!(
        uart0,
        "Press and hold the button to generate 1KHz sine wave\r"
    )
    .unwrap();

    loop {
        wait_for_button_press(&mut button);

        sonic_client.set_sound_output(true);
        led.set_high().unwrap();
        write!(uart0, "Generating 1KHz sine wave...").unwrap();

        wait_for_button_release(&mut button, &mut timer);

        sonic_client.set_sound_output(false);
        led.set_low().unwrap();
        writeln!(uart0, "done.\r").unwrap();
    }
}

#[allow(non_snake_case)]
#[interrupt]
fn PWM_IRQ_WRAP() {
    pwmutil::pwm_irq_wrap_handler();
}
