//! Pico program: Get 4 PWM outputs from 1 PWM slice.
//! PWM slice 2 is used for this experiment, running at 1kHz.
//! Pins and signals used:
//! - GPIO0 : Pin 1  UART0 tx -> USB-serial rx (white wire on my device)
//! - GPIO1 : Pin 2  UART0 rx -> USB-serial tx (green wire on my device)
//! - GND   : Pin 3  Ground   -> USB-serial ground
//! - GPIO4 : Pin 6  connected to PWM2 channel A
//! - GPIO5 : Pin 7  connected to PWM2 channel B
//! - GPIO20: Pin 26 connected to PWM2 channel A
//! - GPIO21: Pin 27 connected to PWM2 channel B
//! - GPIO22: Pin 29 connected to application pushbutton
//! - RUN   : Pin 30 connected to reset pushbutton
//! - GPIO25: connected to internal LED to signal when button is pressed
//!
//! While the application button is pressed, PWM1 is enabled with 4 outputs:
//! GPIO4 / pin 6: 25% duty cycle
//! GPIO5 / pin 7: 50% duty cycle
//! GPIO20 / pin 26: 25% duty cycle, inverted
//! GPIO21 / pin 27: 50% duty cycle, inverted
//!
#![no_std]
#![no_main]

use core::fmt::Write; // for write!(), writeln!()
use cortex_m_rt::entry;
use embedded_hal::{digital::OutputPin, pwm::SetDutyCycle};
use handy_rp2040_common::buttons::{
    wait_for_button_press, wait_for_button_release,
};
use panic_halt as _;
use rp_pico::hal::{self, gpio, timer::Timer, Clock};

#[entry]
fn main() -> ! {
    // Grab our singleton objects
    let mut pac = hal::pac::Peripherals::take().unwrap();
    let _core = hal::pac::CorePeripherals::take().unwrap();

    // Set up the watchdog driver - needed by the clock setup code
    let mut watchdog = hal::Watchdog::new(pac.WATCHDOG);

    // Configure the clocks
    // The default is to generate a 125 MHz system clock
    let clocks = hal::clocks::init_clocks_and_plls(
        rp_pico::XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let mut timer = Timer::new(pac.TIMER, &mut pac.RESETS, &clocks);

    // Single-cycle I/O block
    let sio = hal::Sio::new(pac.SIO);

    let pins = rp_pico::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    let uart0_pins = (
        // UART0 TX (characters sent from RP2040) on pin 1 (GPIO0)
        pins.gpio0.into_function::<hal::gpio::FunctionUart>(),
        // UART0 RX (characters received by RP2040) on pin 2 (GPIO1)
        pins.gpio1.into_function::<hal::gpio::FunctionUart>(),
    );
    let mut uart0 =
        hal::uart::UartPeripheral::new(pac.UART0, uart0_pins, &mut pac.RESETS)
            .enable(
                // 115200 baud, 8 data bits, 1 stop bit, no parity
                hal::uart::UartConfig::default(),
                clocks.peripheral_clock.freq(),
            )
            .unwrap();

    writeln!(uart0, "\r").unwrap();
    writeln!(uart0, "Hello from pico-try-pwm-quad-1slice.rs!\r").unwrap();

    // Grab the PWM slices
    let pwm_slices = hal::pwm::Slices::new(pac.PWM, &mut pac.RESETS);

    // PWM2 is the PWM slice of interest
    let mut pwm_slice = pwm_slices.pwm2;

    // Set PWM slice to run at 1KHz
    pwm_slice.default_config();
    pwm_slice.set_div_int(125);
    pwm_slice.set_div_frac(0);
    pwm_slice.set_top(999);

    // Set Channel A to 25% duty cycle, channel B to 50% duty cycle.
    pwm_slice.channel_a.set_duty_cycle(250).unwrap();
    pwm_slice.channel_b.set_duty_cycle(500).unwrap();

    // Start the PWM clock running.
    pwm_slice.enable();

    let mut pwm_output_pins = [
        pins.gpio4
            .into_dyn_pin()
            .into_function::<gpio::DynFunction>()
            .into_pull_type::<gpio::PullNone>(),
        pins.gpio5
            .into_dyn_pin()
            .into_function::<gpio::DynFunction>()
            .into_pull_type::<gpio::PullNone>(),
        pins.gpio20
            .into_dyn_pin()
            .into_function::<gpio::DynFunction>()
            .into_pull_type::<gpio::PullNone>(),
        pins.gpio21
            .into_dyn_pin()
            .into_function::<gpio::DynFunction>()
            .into_pull_type::<gpio::PullNone>(),
    ];

    // LED to give feedback when button is pressed
    let mut led = pins.led.into_push_pull_output();
    // Connect pin 29 (GPIO22) to start/stop button
    let mut button = pins.gpio22.into_pull_up_input();

    writeln!(
        uart0,
        "Press and hold the button to generate 4 signals from 1 PWM slice\r"
    )
    .unwrap();

    loop {
        wait_for_button_press(&mut button);

        led.set_high().unwrap();
        write!(uart0, "PWM outputs...").unwrap();
        connect_pwm_outputs(&mut pwm_output_pins);

        wait_for_button_release(&mut button, &mut timer);

        led.set_low().unwrap();
        writeln!(uart0, "done.\r").unwrap();
        disconnect_pwm_outputs(&mut pwm_output_pins);
    }
}

fn connect_pwm_outputs(
    pwm_output_pins: &mut [gpio::Pin<
        gpio::DynPinId,
        gpio::DynFunction,
        gpio::PullNone,
    >],
) {
    for (i, p) in pwm_output_pins.iter_mut().enumerate() {
        if i >= 2 {
            p.set_output_override(gpio::OutputOverride::Invert);
        }
        let _ = p.try_set_function(gpio::DynFunction::Pwm);
    }
}

fn disconnect_pwm_outputs(
    pwm_output_pins: &mut [gpio::Pin<
        gpio::DynPinId,
        gpio::DynFunction,
        gpio::PullNone,
    >],
) {
    for (i, p) in pwm_output_pins.iter_mut().enumerate() {
        let _ = p.try_set_function(gpio::DynFunction::Null);
        if i >= 2 {
            p.set_output_override(gpio::OutputOverride::DontInvert);
        }
    }
}
