//! Play music on the Raspberry Pi Pico using the sonic API
//! PWM slice 2 is used for the "driver" PWM controlling H-bridge side 1
//! PWM slice 6 is used for the "driver" PWM controlling H-bridge side 2
//! PWM slice 7 is used for the "pacing" PWM controlling sample timing
//! Pins and signals used:
//! - GPIO0: Pin 1: UART tx -> USB-serial rx (white wire on my device)
//! - GPIO1: Pin 2: UART rx -> USB-serial tx (green wire on my device)
//! - GND  : Pin 3: Ground  -> USB-serial ground
//! - GPIO4 : Pin 6  connected to PWM2 channel A -> PNP 1 gate
//! - GPIO5 : Pin 7  connected to PWM2 channel B -> NPN 1 gate
//! - GPIO12: Pin 16 connected to PWM6 channel A -> NPN 2 gate
//! - GPIO13: Pin 17 connected to PWM6 channel B -> PNP 2 gate
//! - GND   : Pin 23 ground connection on breadboard / circuitboard
//! - GND   : Pin 28 ground connection on breadboard / circuitboard
//! - GPIO22: Pin 29 connected to start/stop pushbutton
//! - RUN   : Pin 30 connected to reset pushbutton
//! - GPIO25: connected to on-board LED used to indicate button is pressed
#![no_std]
#![no_main]

use core::fmt::Write; // for write!(), writeln!()
use cortex_m_rt::entry;
use embedded_hal::digital::OutputPin;
use handy_rp2040_common::{
    buttons::{is_button_pressed, wait_for_button_press},
    pwmutil,
    sonic::{self, Channel, WaveGen, Waveform},
};
use panic_halt as _;
use rp_pico::hal::{
    self,
    gpio::{self, DynPinId},
    pac::interrupt,
    Clock,
};

include!("../../gen/song.rs");

#[entry]
fn main() -> ! {
    // Grab our singleton objects
    let mut pac = hal::pac::Peripherals::take().unwrap();

    // Set up the watchdog driver - needed by the clock setup code
    let mut watchdog = hal::Watchdog::new(pac.WATCHDOG);

    // Configure the clocks
    // The default is to generate a 125 MHz system clock
    let clocks = hal::clocks::init_clocks_and_plls(
        rp_pico::XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    // Single-cycle I/O block
    let sio = hal::Sio::new(pac.SIO);

    let pins = rp_pico::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    let uart_pins = (
        // UART TX (characters sent from RP2040) on pin 1 (GPIO0)
        pins.gpio0.into_function::<hal::gpio::FunctionUart>(),
        // UART RX (characters received by RP2040) on pin 2 (GPIO1)
        pins.gpio1.into_function::<hal::gpio::FunctionUart>(),
    );
    let mut uart =
        hal::uart::UartPeripheral::new(pac.UART0, uart_pins, &mut pac.RESETS)
            .enable(
                // 115200 baud, 8 data bits, 1 stop bit, no parity
                hal::uart::UartConfig::default(),
                clocks.peripheral_clock.freq(),
            )
            .unwrap();

    writeln!(uart, "\r").unwrap();
    writeln!(uart, "Hello from pico-music.rs!\r").unwrap();

    // Init PWM slices
    let pwm_slices = hal::pwm::Slices::new(pac.PWM, &mut pac.RESETS);

    // PWM2 is the H-bridge side 1 driver PWM slice
    let driver1_pwm = pwm_slices.pwm2;
    let driver1_channel_a_pin = pins.gpio4.into_pull_type();
    let driver1_channel_b_pin = pins.gpio5.into_pull_type();

    // PWM6 is the H-bridge side 2 driver PWM slice
    let driver2_pwm = pwm_slices.pwm6;
    let driver2_channel_a_pin = pins.gpio12.into_pull_type();
    let driver2_channel_b_pin = pins.gpio13.into_pull_type();

    // PWM7 is the pacing PWM slice
    let pacing_pwm = pwm_slices.pwm7;

    let sonic_client =
        sonic::init_t4driver(
            pacing_pwm,
            driver1_pwm,
            driver1_channel_a_pin,
            driver1_channel_b_pin,
            driver2_pwm,
            driver2_channel_a_pin,
            driver2_channel_b_pin,
            Option::<
                gpio::Pin<DynPinId, gpio::FunctionSioOutput, gpio::PullNone>,
            >::None,
        );

    // LED to give feedback when button is pressed
    let mut led = pins.led.into_push_pull_output();
    // Connect pin 29 (GPIO22) to start/stop button
    let mut button = pins.gpio22.into_pull_up_input();

    // Counter to detect when button has been released
    let mut release_count: u8;
    let debounce_thresh: u8 = 221; // about 10ms

    loop {
        // Make sure button is not pressed
        release_count = 0;
        while release_count < debounce_thresh {
            if is_button_pressed(&mut button) {
                release_count = 0;
            } else {
                release_count += 1;
            }
            sonic_client.wait_for_timer_tick();
        }

        writeln!(uart, "Press the button to play the song\r").unwrap();
        wait_for_button_press(&mut button);
        release_count = 0;
        let mut note_on: bool;
        let mut volume: u16 = 0;
        sonic_client.set_sound_params(
            Waveform::TriangleWave,
            WaveGen::MIN_PHASE_VEL,
            volume,
        );
        sonic_client.set_sound_output(true);
        led.set_high().unwrap();

        'song: for (phase_vel, ticks) in SONG.iter() {
            if *phase_vel == 0 {
                // rest
                note_on = false;
            } else {
                // play new note
                sonic_client.set_phase_vel(Channel::ChannelA, *phase_vel);
                sonic_client.set_phase_vel(Channel::ChannelB, *phase_vel);
                note_on = true;
            }

            let mut count = *ticks;
            while count > 0 {
                if is_button_pressed(&mut button) {
                    if release_count >= debounce_thresh {
                        // Button pressed again after it was released -
                        // stop the song!
                        writeln!(uart, "Stopped.\r").unwrap();
                        break 'song;
                    }
                    release_count = 0;
                } else {
                    release_count = release_count.saturating_add(1);
                }

                if note_on {
                    // Ramp up volume
                    volume = u16::min(volume + 50, WaveGen::MAX_AMPLITUDE);
                } else {
                    // Taper off volume
                    volume = volume.saturating_sub(10);
                }
                sonic_client.set_volume(Channel::ChannelA, volume);
                sonic_client.set_volume(Channel::ChannelB, volume);

                sonic_client.wait_for_timer_tick();
                count -= 1;
            }
        }

        sonic_client.set_sound_output(false);
        led.set_low().unwrap();
    }
}

#[allow(non_snake_case)]
#[interrupt]
fn PWM_IRQ_WRAP() {
    pwmutil::pwm_irq_wrap_handler();
}
