//! Raspberry Pi Pico app to test the random module
//! Pins and signals used:
//! - GPIO0 : Pin 1: UART tx -> USB-serial rx (white wire on my device)
//! - GPIO1 : Pin 2: UART rx -> USB-serial tx (green wire on my device)
//! - GND   : Pin 3: Ground  -> USB-serial ground
//! - RUN   : Pin 30 connected to reset pushbutton
//! - GPIO25: Connected to onboard LED
#![no_std]
#![no_main]

use core::fmt::Write; // for write!(), writeln!()
use cortex_m_rt::entry;
use handy_rp2040_common::{chip, random, uart};
use panic_halt as _;
use rp_pico::hal::{clocks::Clock, rosc::RingOscillator};

#[entry]
fn main() -> ! {
    // Standard setup
    let mut chip = chip::Chip::new(rp_pico::XOSC_CRYSTAL_FREQ);

    let mut uart0 = uart::init_uart0(
        chip.UART0,
        chip.pins.gpio0,
        chip.pins.gpio1,
        &mut chip.RESETS,
        chip.clocks.peripheral_clock.freq(),
    );

    writeln!(uart0, "\r").unwrap();
    writeln!(uart0, "Hello from pico-test-random.rs!\r").unwrap();

    let rosc = RingOscillator::new(chip.ROSC).initialize();
    writeln!(
        uart0,
        "Ring Oscillator (ROSC) initalized to {}\r",
        rosc.operating_frequency()
    )
    .unwrap();

    let mut num_random_values: usize = 100;
    writeln!(
        uart0,
        "Generating {num_random_values} random 32-bit values\r"
    )
    .unwrap();

    while num_random_values > 0 {
        for _ in 0..4 {
            if num_random_values == 0 {
                break;
            }
            num_random_values -= 1;
            let random_value = random::gen_random_bits(&rosc, 32);
            write!(uart0, "0x{random_value:08x}  ").unwrap();
        }
        uart0.write_str("\r\n").unwrap();
    }

    loop {}
}
