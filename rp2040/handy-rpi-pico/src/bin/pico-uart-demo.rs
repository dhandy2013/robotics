//! Raspberry Pi Pico app to demonstrate writing over UART (serial port)
//! Pins and signals used:
//! - RUN   : Pin 30 connected to reset pushbutton
//! - GPIO22: Pin 29 connected to start/stop pushbutton
//! - GPIO25: Onboard LED
//! On a Linux host, to see the serial port output, run this command:
//!   picocom -b 115200 /dev/ttyUSB0
//! Hint:
//!   To reprogram the pico, you have to disconnect picocom first. For some
//!   reason the pico won't go into boot mode while picocom is listening on the
//!   USB-adapter device. You don't need to unplug the USB-serial adapter, but
//!   you do need to shut down the program talking to it. Once the pico is in
//!   boot mode you can restart picocom.
#![no_std]
#![no_main]

use core::fmt::Write; // for write!(), writeln!()
use cortex_m_rt::entry;
use embedded_hal::{delay::DelayNs, digital::OutputPin};
use handy_rp2040_common::buttons;
use panic_halt as _;
use rp_pico::hal::{self, pac, Clock};

#[entry]
fn main() -> ! {
    // Grab our singleton objects
    let mut pac = pac::Peripherals::take().unwrap();
    let _core = pac::CorePeripherals::take().unwrap();

    // Set up the watchdog driver - needed by the clock setup code
    let mut watchdog = hal::Watchdog::new(pac.WATCHDOG);

    // Configure the clocks
    // The default is to generate a 125 MHz system clock
    let clocks = hal::clocks::init_clocks_and_plls(
        rp_pico::XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    // Single-cycle I/O block
    let sio = hal::Sio::new(pac.SIO);

    let pins = rp_pico::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    // LED for diagnostic signals
    let mut led = pins.led.into_push_pull_output();

    // Connect pin 29 (GPIO22) to button
    let mut button = pins.gpio22.into_pull_up_input();

    let uart_pins = (
        // UART TX (characters sent from RP2040) on pin 1 (GPIO0)
        pins.gpio0.into_function::<hal::gpio::FunctionUart>(),
        // UART RX (characters received by RP2040) on pin 2 (GPIO1)
        pins.gpio1.into_function::<hal::gpio::FunctionUart>(),
    );
    let mut uart =
        hal::uart::UartPeripheral::new(pac.UART0, uart_pins, &mut pac.RESETS)
            .enable(
                // 115200 baud, 8 data bits, 1 stop bit, no parity
                hal::uart::UartConfig::default(),
                clocks.peripheral_clock.freq(),
            )
            .unwrap();
    let mut timer = hal::Timer::new(pac.TIMER, &mut pac.RESETS, &clocks);

    writeln!(&mut uart, "Hello from pico-uart-demo.rs!\r").unwrap();

    // One 250ms LED blink to indicate we made it past first attempted print.
    led.set_high().unwrap();
    timer.delay_ms(250);
    led.set_low().unwrap();
    timer.delay_ms(250);

    loop {
        writeln!(&mut uart, "Press the button to start speed test\r").unwrap();
        buttons::wait_for_button_press(&mut button);

        // Quick 125ms blink so we can tell that program is running and button
        // press was detected, even if serial output is not working.
        led.set_high().unwrap();
        timer.delay_ms(125);
        led.set_low().unwrap();
        timer.delay_ms(125);

        let t0 = timer.get_counter();
        const TEST_LINES: u16 = 1000;
        const BYTES_PER_LINE: u16 = 30; // count below to make sure it matches
        for i in 0..TEST_LINES {
            write!(&mut uart, "{:3}: ***********************\r\n", i).unwrap();
        }
        let t1 = timer.get_counter();

        writeln!(
            &mut uart,
            "Wrote {} bytes in {} microseconds\r\n\r",
            TEST_LINES * BYTES_PER_LINE,
            t1 - t0
        )
        .unwrap();
        buttons::wait_for_button_release(&mut button, &mut timer);
    }
}
