//! Raspberry Pi Pico program: Generate two tones on separate channels
//! PWM slice 0 is used for the "driver" PWM (signals sent to speaker)
//! PWM slice 1 is used for the "pacing" PWM controlling sample timing
//! Pins and signals used:
//! - GPIO0: Pin 1: UART tx -> USB-serial rx (white wire on my device)
//! - GPIO1: Pin 2: UART rx -> USB-serial tx (green wire on my device)
//! - GND  : Pin 3: Ground  -> USB-serial ground
//! - GPIO16: Pin 21 connected to left speaker driver  (PWM0 channel A)
//! - GPIO17: Pin 22 connected to right speaker driver (PWM0 channel B)
//! - GPIO22: Pin 29 connected to start/stop pushbutton
//! - RUN   : Pin 30 connected to reset pushbutton
//! Reserved pins (used by other programs):
//! - GPIO9: Pin 12 connected to servo
#![no_std]
#![no_main]

use core::fmt::Write; // for write!(), writeln!()
use cortex_m_rt::entry;
use embedded_hal::{delay::DelayNs, digital::OutputPin};
use handy_rp2040_common::{
    buttons::{is_button_pressed, wait_for_button_press},
    pwmutil,
    sonic::{
        self, Channel, WaveGen, Waveform, ACTUAL_PACING_PWM_FREQ,
        DRIVER_PWM_FREQ, TARGET_DRIVER_PWM_FREQ, TICKS_PER_SECOND,
    },
};
use panic_halt as _;
use rp_pico::hal::{self, pac::interrupt, Clock};

#[entry]
fn main() -> ! {
    // Grab our singleton objects
    let mut pac = hal::pac::Peripherals::take().unwrap();
    let core = hal::pac::CorePeripherals::take().unwrap();

    // Set up the watchdog driver - needed by the clock setup code
    let mut watchdog = hal::Watchdog::new(pac.WATCHDOG);

    // Configure the clocks
    //
    // The default is to generate a 125 MHz system clock
    let clocks = hal::clocks::init_clocks_and_plls(
        rp_pico::XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let mut delay = cortex_m::delay::Delay::new(
        core.SYST,
        clocks.system_clock.freq().to_Hz(),
    );

    // Single-cycle I/O block
    let sio = hal::Sio::new(pac.SIO);

    let pins = rp_pico::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    // LED for diagnostic signals
    let mut led = pins.led.into_push_pull_output();
    // Connect pin 29 (GPIO22) to start/stop button
    let mut button = pins.gpio22.into_pull_up_input();

    let uart_pins = (
        // UART TX (characters sent from RP2040) on pin 1 (GPIO0)
        pins.gpio0.into_function::<hal::gpio::FunctionUart>(),
        // UART RX (characters received by RP2040) on pin 2 (GPIO1)
        pins.gpio1.into_function::<hal::gpio::FunctionUart>(),
    );
    let mut uart =
        hal::uart::UartPeripheral::new(pac.UART0, uart_pins, &mut pac.RESETS)
            .enable(
                // 115200 baud, 8 data bits, 1 stop bit, no parity
                hal::uart::UartConfig::default(),
                clocks.peripheral_clock.freq(),
            )
            .unwrap();
    let mut timer = hal::Timer::new(pac.TIMER, &mut pac.RESETS, &clocks);

    let freq1: u16 = 440;
    let freq2: u16 = 880;
    let volume = WaveGen::MAX_AMPLITUDE / 2;

    writeln!(uart, "\r").unwrap();
    writeln!(uart, "Hello from pico-two-tone.rs!\r").unwrap();
    writeln!(uart, "Signal freq1 = {} Hz\r", freq1).unwrap();
    writeln!(uart, "Signal freq2 = {} Hz\r", freq2).unwrap();
    writeln!(uart, "Sample rate target: {} Hz\r", TICKS_PER_SECOND).unwrap();
    writeln!(uart, "Sample rate actual: {} Hz\r", ACTUAL_PACING_PWM_FREQ)
        .unwrap();
    writeln!(
        uart,
        "PWM target frequency: {} Hz\r",
        TARGET_DRIVER_PWM_FREQ
    )
    .unwrap();
    writeln!(uart, "PWM actual frequency: {} Hz\r", DRIVER_PWM_FREQ).unwrap();
    writeln!(uart, "\r").unwrap();
    writeln!(uart, "Press and hold the button to generate tones\r").unwrap();

    // Init PWM slices
    let pwm_slices = hal::pwm::Slices::new(pac.PWM, &mut pac.RESETS);
    let driver_pwm = pwm_slices.pwm0;
    let pacing_pwm = pwm_slices.pwm1;
    let channel_a_pin = pins.gpio16.into_function().into_pull_type();
    let channel_b_pin = pins.gpio17.into_function().into_pull_type();
    let sonic_client = sonic::init_t1driver(
        driver_pwm,
        pacing_pwm,
        Some(channel_a_pin),
        Some(channel_b_pin),
        sonic::MaybeOutput::None,
    );

    sonic_client.set_waveform(Channel::ChannelA, Waveform::SineWave);
    sonic_client.set_freq(Channel::ChannelA, freq1 * 16);
    sonic_client.set_volume(Channel::ChannelA, volume);

    sonic_client.set_waveform(Channel::ChannelB, Waveform::SineWave);
    sonic_client.set_freq(Channel::ChannelB, freq2 * 16);
    sonic_client.set_volume(Channel::ChannelB, volume);

    // Short blink pattern to indicate program is starting
    for _ in 0..2 {
        led.set_high().unwrap();
        delay.delay_ms(100);
        led.set_low().unwrap();
        delay.delay_ms(100);
        led.set_high().unwrap();
        delay.delay_ms(400);
        led.set_low().unwrap();
        delay.delay_ms(400);
    }

    loop {
        wait_for_button_press(&mut button);
        sonic_client.set_sound_output(true);
        writeln!(uart, "Sound On\r").unwrap();
        led.set_high().unwrap();

        let mut release_count: u8 = 0;
        let debounce_thresh: u8 = 10; // about 10ms
        loop {
            if is_button_pressed(&mut button) {
                release_count = 0;
            } else {
                release_count += 1;
                if release_count >= debounce_thresh {
                    break;
                }
            }

            timer.delay_ms(1);
        }

        led.set_low().unwrap();
        sonic_client.set_sound_output(false);
        writeln!(uart, "Sound Off\r").unwrap();
        writeln!(uart, "\r").unwrap();
    }
}

#[allow(non_snake_case)]
#[interrupt]
fn PWM_IRQ_WRAP() {
    pwmutil::pwm_irq_wrap_handler();
}
