//! pico-2cores.rs -
//! Experiment with running code on both processor cores of the Raspberry Pi Pico.
//!
//! Pins and signals used:
//! - GPIO0 : Pin 1 - UART0 TX
//! - GPIO1 : Pin 2 - UART0 RX
//! - GND   : Pin 3 - UART0 GND
//! - GPIO4 : Pin 6 - UART1 TX
//! - GPIO5 : Pin 7 - UART1 RX
//! - GND   : Pin 8 - UART1 GND
//! - GND   : Pin 23 - GND for pushbuttons
//! - GPIO22: Pin 29 connected to application pushbutton
//! - RUN   : Pin 30 connected to reset pushbutton
//! - GPIO25: Onboard LED
//!
//! Core 0 algorithm:
//! - Configure UART0 for 115300 baud.
//! - Print "hello from core 0" message on UART0.
//! - Blink the onboard LED on for 250ms, then pause 250ms.
//! - Activate core 1 (the second core) and send it the initialization message.
//! - Loop top:
//! - Wait for reply message from core 1.
//! - Blink onboard LED on for 250ms, pause 250ms.
//! - Print core 0 acknowledgement message on UART0
//! - Wait for application button press.
//! - Send message to core 1.
//! - Go to loop top.
//!
//! Core 1 algorithm:
//! - Configure UART1 for 115300 baud.
//! - Loop top:
//! - Print "hello from core 1" message on UART1.
//! - Send reply message back to core 0.
//! - Wait for message from core 0.
//! - Go to loop top.
#![no_std]
#![no_main]

use core::fmt::Write; // for write!(), writeln!()
use cortex_m_rt::entry;
use embedded_hal::{delay::DelayNs, digital::OutputPin};
use handy_rp2040_common::{
    buttons, chip,
    systick::SysTick,
    uart::{self, Uart1Type},
};
use panic_halt as _;
use rp_pico::hal::{
    self,
    clocks::Clock,
    fugit::HertzU32,
    multicore::{Multicore, Stack},
    pac,
};

static mut CORE1_STACK: Stack<4096> = Stack::new();

/// Expected first message sent back from core 1
const EXPECTED_ACK: u32 = 0xffff_ffff;

#[entry]
fn core0_main() -> ! {
    // Standard setup
    let mut chip = chip::Chip::new(rp_pico::XOSC_CRYSTAL_FREQ);

    // LED for diagnostic signals
    let mut led = chip.pins.gpio25.into_push_pull_output();

    // Connect pin 29 (GPIO22) to button
    let mut button = chip.pins.gpio22.into_pull_up_input();

    let mut uart0 = uart::init_uart0(
        chip.UART0,
        chip.pins.gpio0,
        chip.pins.gpio1,
        &mut chip.RESETS,
        chip.clocks.peripheral_clock.freq(),
    );

    writeln!(&mut uart0, "\r\nHello from pico-2cores.rs!\r").unwrap();

    // Initialize short-period (134ms max) high-resolution countdown timer.
    let mut systick =
        SysTick::new(chip.core.SYST, chip.clocks.system_clock.freq());

    // One 250ms LED blink to indicate we made it past first attempted print.
    led.set_high().unwrap();
    systick.delay_ms(250);
    led.set_low().unwrap();
    systick.delay_ms(250);

    writeln!(
        &mut uart0,
        "core 0: press button to launch task on core 1\r"
    )
    .unwrap();
    buttons::wait_for_button_press(&mut button);

    // Turn SysTick timer off and print its current count.
    systick.disable_counter();
    writeln!(
        &mut uart0,
        "core 0: Button pressed. SYST count: {}\r",
        SysTick::get_current()
    )
    .unwrap();

    // Turn LED on to indicate button press was detected and core 1 launch is
    // being initiated.
    led.set_high().unwrap();

    //
    // Prepare parameters to pass to core 1 task.
    //

    let uart1 = uart::init_uart1(
        chip.UART1,
        chip.pins.gpio4,
        chip.pins.gpio5,
        &mut chip.RESETS,
        chip.clocks.peripheral_clock.freq(),
    );

    let mut mc = Multicore::new(&mut chip.PSM, &mut chip.PPB, &mut chip.fifo);
    let cores = mc.cores();
    let core1 = &mut cores[1];

    // Launch the core 1 task.
    let system_clock_freq = chip.clocks.system_clock.freq();
    let spawn_result = core1
        .spawn(unsafe { &mut CORE1_STACK.mem }, move || {
            core1_main(uart1, system_clock_freq)
        });
    if let Err(err) = spawn_result {
        writeln!(&mut uart0, "core 0: error spawning core 1 task: {err:?}\r")
            .unwrap();
        // LED will remain stuck on, indicating an error.
        loop {}
    }

    let ack = chip.fifo.read_blocking();
    if ack != EXPECTED_ACK {
        writeln!(
            &mut uart0,
            "core 0: error: core 1 returned ACK {ack:08x}, expected {EXPECTED_ACK:08x}\r"
        )
        .unwrap();
        // LED will remain stuck on, indicating an error.
        loop {}
    }
    writeln!(
        &mut uart0,
        "core 0: Received ACK from core 1. SYST count: {} (should be unchanged)\r",
        SysTick::get_current()
    )
    .unwrap();
    systick.enable_counter(); // re-enable counter for button debounce delay

    let mut count: u32 = 1;
    loop {
        buttons::wait_for_button_release(&mut button, &mut systick);
        led.set_low().unwrap();

        writeln!(
            &mut uart0,
            "Press button to send message {count} to core 1\r"
        )
        .unwrap();
        buttons::wait_for_button_press(&mut button);
        led.set_high().unwrap();

        chip.fifo.write(count);
        count = chip.fifo.read_blocking();
    }
}

/// The main function for core 1. Never returns.
///
/// Lesson learned the hard way:
/// Do not call gpio::Pins::new() from core 1 or else all the pins in use by
/// core 0 will get reset and de-initialized. Instead, initialize any pins
/// needed by core 1 in the initialization code in core 0, and then pass
/// those pins as parameters to the core 1 main function.
fn core1_main(mut uart1: Uart1Type, system_clock_freq: HertzU32) {
    writeln!(&mut uart1, "\r\nHello from core 1!\r").unwrap();

    // The SYST count here is always 0xffffff no matter what count core 0 sees.
    // This proves that the SYST timer/counters on core 0 and core 1 are
    // independent of one another.
    writeln!(
        &mut uart1,
        "core 1: SYST count: {}\r",
        SysTick::get_current()
    )
    .unwrap();

    // Therefore it's Ok to "steal" the SYST core peripheral and use it here.
    let core = unsafe { pac::CorePeripherals::steal() };
    let mut systick = SysTick::new(core.SYST, system_clock_freq);
    writeln!(
        &mut uart1,
        "core 1: SYST counter running? {}  SYST count: {}\r",
        systick.is_counter_enabled(),
        SysTick::get_current()
    )
    .unwrap();

    // It must be Ok the "steal" the rp2040_pac peripherals from core 1, or else
    // how could core 0 and core 1 possibly communicate? They both need the SIO
    // peripheral in order to access the FIFO to send messages back and forth.
    let pac = unsafe { pac::Peripherals::steal() };
    let mut sio = hal::Sio::new(pac.SIO);
    // Send acknowledgement back to core 0 to let it know core 1 is ready.
    sio.fifo.write(EXPECTED_ACK);

    loop {
        let count = sio.fifo.read_blocking();
        writeln!(&mut uart1, "core 1: received {count} from core 0\r").unwrap();
        sio.fifo.write(count.wrapping_add(1));
    }
}
