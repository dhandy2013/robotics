//! Raspberry Pi Pico app to use Box to store simulated interrupt handlers
//! Pins and signals used:
//! - GPIO0 : Pin 1: UART tx -> USB-serial rx (white wire on my device)
//! - GPIO1 : Pin 2: UART rx -> USB-serial tx (green wire on my device)
//! - GND   : Pin 3: Ground  -> USB-serial ground
//! - RUN   : Pin 30 connected to reset pushbutton
#![no_std]
#![no_main]

extern crate alloc;

use alloc::{
    alloc::{alloc_zeroed, dealloc, Layout},
    boxed::Box,
};
use core::cell::RefCell;
use core::fmt::Write; // for write!(), writeln!()
use cortex_m_rt::entry;
use critical_section::Mutex;
use handy_rp2040_common::{chip, heap, uart};
use panic_halt as _;
use rp_pico::hal::clocks::Clock;

type InterruptHandler = Box<dyn FnMut() + Send + 'static>;

const NUM_INTERRUPT_HANDLERS: usize = 4;
static INTERRUPT_HANDLERS: Mutex<
    RefCell<[Option<InterruptHandler>; NUM_INTERRUPT_HANDLERS]>,
> = Mutex::new(RefCell::new([None, None, None, None]));

/// Set (Some) or clear (None) a global interrupt handler.
/// i: Interrupt handler number in range 0..NUM_INTERRUPT_HANDLERS
/// If `i` is out of range this function will panic.
/// Return the old handler, or None if there wasn't any at the moment.
///
/// This function returns None if a handler was previously set but is busy
/// executing. In that case when the old handler finishes executing the new
/// handler will take effect and the old handler will be dropped.
fn set_interrupt_handler(
    i: usize,
    maybe_handler: Option<InterruptHandler>,
) -> Option<InterruptHandler> {
    critical_section::with(move |cs| {
        let ih_ref = INTERRUPT_HANDLERS.borrow(cs);
        let mut ih_mut = ih_ref.borrow_mut();
        match maybe_handler {
            Some(handler) => ih_mut[i].replace(handler),
            None => ih_mut[i].take(),
        }
    })
}

/// Call a global interrupt handler if it is set.
/// i: Interrupt handler number in range 0..NUM_INTERRUPT_HANDLERS
/// If `i` is out of range this function will panic.
fn call_interrupt_handler(i: usize) {
    // Grab the interrupt handler if any
    let maybe_handler = critical_section::with(move |cs| {
        let ih_ref = INTERRUPT_HANDLERS.borrow(cs);
        let mut ih_mut = ih_ref.borrow_mut();
        ih_mut[i].take()
    });

    // Return early if no handler was set
    let mut handler = match maybe_handler {
        Some(handler) => handler,
        None => return,
    };

    // The handler is a closure. It is expected to have side effects.
    handler();

    // Replace the handler back into the INTERRUPT_HANDLERS array,
    // if a new handler was not set while the old one was running.
    let maybe_old_handler = critical_section::with(move |cs| {
        let ih_ref = INTERRUPT_HANDLERS.borrow(cs);
        let mut ih_mut = ih_ref.borrow_mut();
        match ih_mut[i] {
            Some(_) => Some(handler), // returns the old handler
            None => ih_mut[i].replace(handler), // returns None
        }
    });

    // If a new handler was set while the old handler was running, drop the old
    // handler _outside_ of the critical section.
    core::mem::drop(maybe_old_handler);
}

#[entry]
fn main() -> ! {
    // Standard setup
    let mut chip = chip::Chip::new(rp_pico::XOSC_CRYSTAL_FREQ);

    let mut uart0 = uart::init_uart0(
        chip.UART0,
        chip.pins.gpio0,
        chip.pins.gpio1,
        &mut chip.RESETS,
        chip.clocks.peripheral_clock.freq(),
    );

    writeln!(uart0, "\r").unwrap();
    writeln!(uart0, "Hello from pico-box-intr-handler.rs!\r").unwrap();
    writeln!(uart0, "Heap free mem before init: {}\r", heap::free()).unwrap();

    // What happens if you try to use the global allocator before init?

    if true {
        writeln!(
            uart0,
            "Raw memory allocation before init should return null ptr\r"
        )
        .unwrap();
        let layout = Layout::new::<[u8; 13]>();
        let p = unsafe { alloc_zeroed(layout) };
        writeln!(
            uart0,
            "Address of allocated mem: 0x{:08x} size: {} align: {}\r",
            p as usize,
            layout.size(),
            layout.align()
        )
        .unwrap();
        writeln!(
            uart0,
            "  Heap memory used: {} free: {} total: {}\r",
            heap::used(),
            heap::free(),
            heap::used() + heap::free()
        )
        .unwrap();
    }

    if false {
        writeln!(
            uart0,
            "Allocating String before heap::init() should crash\r"
        )
        .unwrap();
        let s0 = alloc::string::String::from("This is a test.");
        // If you see this message something is strange and wrong.
        // But there has to be some code referencing s0 or else the
        // optimizer will remove the String heap allocation altogether.
        writeln!(uart0, "Address of s0: 0x{:08x}\r", s0.as_ptr() as usize)
            .unwrap();
        writeln!(
            uart0,
            "  Heap memory used: {} free: {} total: {}\r",
            heap::used(),
            heap::free(),
            heap::used() + heap::free()
        )
        .unwrap();
    }

    // Must initialize the heap before using Box, Vec, etc.
    heap::init();

    writeln!(uart0, "Start of heap memory: 0x{:08x}\r", heap::addr()).unwrap();
    writeln!(
        uart0,
        "  Heap memory used: {} free: {} total: {}\r",
        heap::used(),
        heap::free(),
        heap::used() + heap::free()
    )
    .unwrap();

    // Allocate memory chunks and trace heap used/free
    const NUM_CHUNKS: usize = 4;
    let chunk_sizes: [usize; NUM_CHUNKS] = [65520, 8, 8, 1];
    let mut mem_pointers: [*mut u8; NUM_CHUNKS] =
        [core::ptr::null_mut(); NUM_CHUNKS];
    for (p, &chunk_size) in
        core::iter::zip(mem_pointers.iter_mut(), chunk_sizes.iter())
    {
        let layout = Layout::from_size_align(chunk_size, 1).unwrap();
        let prev_used = heap::used();
        *p = unsafe { alloc_zeroed(layout) };
        writeln!(
            uart0,
            "Allocated memory addr: 0x{:08x} size: {} align: {}\r",
            *p as usize,
            layout.size(),
            layout.align()
        )
        .unwrap();
        writeln!(
            uart0,
            "  Heap memory used: {} (increased by {}) free: {} total: {}\r",
            heap::used(),
            heap::used() - prev_used,
            heap::free(),
            heap::used() + heap::free()
        )
        .unwrap();
        if p.is_null() {
            writeln!(uart0, "Null pointer returned, out of memory.\r").unwrap();
            break;
        }
    }

    // Look for non-zero bytes in the heap space
    {
        let heap_start: *const u8 = heap::addr() as *const u8;
        let mut num_non_zero_bytes: usize = 0;
        for i in 0..heap::size() {
            unsafe {
                let p = heap_start.offset(i as isize);
                let byte = *p;
                if byte != 0 {
                    if num_non_zero_bytes == 0 {
                        writeln!(
                            uart0,
                            "First non-zero byte in heap at addr 0x{:08x}\r",
                            p as usize
                        )
                        .unwrap();
                    }
                    num_non_zero_bytes += 1;
                }
            }
        }
        writeln!(
            uart0,
            "Found {num_non_zero_bytes} non-zero bytes in heap.\r"
        )
        .unwrap();
    }

    // Deallocate memory chunks and trace heap used/free
    for (&p, &chunk_size) in
        core::iter::zip(mem_pointers.iter(), chunk_sizes.iter())
    {
        if p.is_null() {
            break;
        }
        let layout = Layout::from_size_align(chunk_size, 1).unwrap();
        writeln!(
            uart0,
            "Deallocating memory addr: 0x{:08x} size: {} align: {}\r",
            p as usize,
            layout.size(),
            layout.align()
        )
        .unwrap();
        let prev_used = heap::used();
        unsafe {
            dealloc(p, layout);
        }
        writeln!(
            uart0,
            "  Heap memory used: {} (decreased by {}) free: {} total: {}\r",
            heap::used(),
            prev_used - heap::used(),
            heap::free(),
            heap::used() + heap::free()
        )
        .unwrap();
    }

    let s1 = alloc::string::String::from("This is a test.");
    writeln!(
        uart0,
        "Allocated string at address 0x{:08x} with {} bytes\r",
        s1.as_ptr() as usize,
        s1.as_bytes().len()
    )
    .unwrap();
    writeln!(uart0, "  Used heap memory: {}\r", heap::used()).unwrap();
    writeln!(uart0, "  Free heap memory: {}\r", heap::free()).unwrap();

    let handler = Box::new(move || {
        let _ = writeln!(
            uart0,
            "Hello from inside the simulated interrupt handler!\r"
        );
        writeln!(
            uart0,
            "Closure contains string at address 0x{:08x} with {} bytes\r",
            s1.as_ptr() as usize,
            s1.as_bytes().len()
        )
        .unwrap();
        writeln!(uart0, "  Used heap memory: {}\r", heap::used()).unwrap();
        writeln!(uart0, "  Free heap memory: {}\r", heap::free()).unwrap();
    });

    // Set an interrupt handler for just one of the slots
    set_interrupt_handler(3, Some(handler));

    // Call *all* of the interrupt handlers. Only 1 should run.
    for i in 0..NUM_INTERRUPT_HANDLERS {
        call_interrupt_handler(i);
    }

    // Now just call interrupt handler 3 again to make sure it runs again.
    call_interrupt_handler(3);

    loop {}
}
