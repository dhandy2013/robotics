//! Debug PWM issue where signal is stuck high unexpectedly
//! Raspberry Pi Pico program
//! Signals used:
//! - Outputs:
//!     - LED    == GP25
//! Expected behavior:
//! - LED glows dim for a short period
//! - LED turns off
//! Actual behavor:
//! - LED glows dim for a short period as expected
//! - LED glows very bright and doesn't turn off
//! Solution:
//!     Disabling the PWM slice is not enough. The pin associated with the PWM
//!     channel remains configured as an output, and is stuck on whatever
//!     random value it was at (0 or 1) when the PWM slice was disabled. You
//!     must disconnect the pin from the PWM channel and then put the pin in
//!     the correct state. In this example I turn it into a floating input.
#![no_std]
#![no_main]

use cortex_m_rt::entry;
use embedded_hal::digital::OutputPin;
use embedded_hal_0_2::PwmPin;
use panic_halt as _;
use rp_pico::hal::{self, gpio, pac, prelude::*};

// If the delay is 400ms then we have the stuck on high bug.
// If the delay is 490ms then the bug goes away.
// Update: Or we can disconnect the LED pin from the PWM slice and turn
// it into a floating input - that is a reliable solution.
const DELAY_MS: u32 = 400;

// Duty cycle of 3279 == 1.00 ms pulse width
const NARROW_PULSE_WIDTH: u16 = 3279;

#[entry]
fn main() -> ! {
    // Grab our singleton objects
    let mut pac = pac::Peripherals::take().unwrap();
    let core = pac::CorePeripherals::take().unwrap();

    // Set up the watchdog driver - needed by the clock setup code
    let mut watchdog = hal::Watchdog::new(pac.WATCHDOG);

    // Configure the clocks
    // The default is to generate a 125 MHz system clock
    let clocks = hal::clocks::init_clocks_and_plls(
        rp_pico::XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let mut delay = cortex_m::delay::Delay::new(
        core.SYST,
        clocks.system_clock.freq().to_Hz(),
    );

    // Init PWMs
    let mut pwm_slices = hal::pwm::Slices::new(pac.PWM, &mut pac.RESETS);

    // From the RP2040 dataset section 4.5.2.6. Configuring PWM Period:
    //
    // period = (TOP + 1) * (CSR_PH_CORRECT + 1) * (DIV_INT + (DIV_FRAC / 16))
    // PWM frequency = FSYS / period
    //
    // CSR_PH_CORRECT is 0 for normal mode (default), 1 for phase correct mode.
    //
    // Configure PWM4 for 50.0296 Hz when FSYS is 125MHz
    let pwm = &mut pwm_slices.pwm4;
    pwm.default_config();
    pwm.set_top(0xfffe); // purposely made one less than the maximum
    pwm.set_div_int(38);
    pwm.set_div_frac(2);

    // Single-cycle I/O block
    let sio = hal::Sio::new(pac.SIO);

    let pins = rp_pico::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    // Output channel B on PWM4 to the LED pin
    let pwm_pin = pwm.channel_b.output_to(pins.led);

    // Generate short burst of narrow pulses
    pwm.channel_b.set_duty(NARROW_PULSE_WIDTH);
    pwm.enable();
    delay.delay_ms(DELAY_MS);
    pwm.disable();

    // Force the PWM output pin low. If we don't do this, the LED will remain
    // stuck at whatever random value it was at (0 or 1) when the PWM slice was
    // disabled.

    // I was hoping I could force the PWM output to go low by manually setting
    // the PWM counter value. I verified experimentally that this doesn't work.
    // It is necessary to change the PWM output pin to a regular output and
    // manually force it low.
    if false {
        let cc = pwm.channel_b.get_duty();
        pwm.set_counter(cc);
        pwm.set_counter(cc.saturating_add(1));
    }

    let mut led_pin = pwm_pin
        .into_function::<gpio::FunctionNull>()
        .into_function::<gpio::FunctionSioOutput>()
        .into_pull_type::<gpio::PullNone>();
    led_pin.set_low().unwrap();

    // Stop
    loop {}
}
