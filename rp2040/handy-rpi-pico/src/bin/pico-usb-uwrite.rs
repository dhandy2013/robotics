//! Raspberry Pi Pico app to demonstrate use of usb_write
//! Pins and signals used:
//! - RUN   : Pin 30 connected to reset pushbutton
//! - GPIO22: Pin 29 connected to start/stop pushbutton
//! - GPIO25: Onboard LED
#![no_std]
#![no_main]

use cortex_m_rt::entry;
use embedded_hal::{delay::DelayNs, digital::OutputPin};
use handy_rp2040_common::{buttons, usb_uwrite};
use panic_halt as _;
use rp_pico::hal::{self, pac};
use ufmt;
use usb_device::class_prelude::UsbBusAllocator;
use void::ResultVoidExt;

#[entry]
fn main() -> ! {
    // Grab our singleton objects
    let mut pac = pac::Peripherals::take().unwrap();
    let _core = pac::CorePeripherals::take().unwrap();

    // Set up the watchdog driver - needed by the clock setup code
    let mut watchdog = hal::Watchdog::new(pac.WATCHDOG);

    // Configure the clocks
    // The default is to generate a 125 MHz system clock
    let clocks = hal::clocks::init_clocks_and_plls(
        rp_pico::XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let mut timer = hal::Timer::new(pac.TIMER, &mut pac.RESETS, &clocks);

    // Single-cycle I/O block
    let sio = hal::Sio::new(pac.SIO);

    let pins = rp_pico::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    // LED for diagnostic signals
    let mut led = pins.led.into_push_pull_output();

    // Connect pin 29 (GPIO22) to button
    let mut button = pins.gpio22.into_pull_up_input();

    // Set up the USB driver
    let usb_bus = UsbBusAllocator::new(hal::usb::UsbBus::new(
        pac.USBCTRL_REGS,
        pac.USBCTRL_DPRAM,
        clocks.usb_clock,
        true,
        &mut pac.RESETS,
    ));

    let mut usb = usb_uwrite::UsbWriter::new(&usb_bus);
    if !usb.init(&timer, 1_000_000, 1_000_000) {
        // Timeout expired before USB device became configured.
        // Long LED blink to indicate error condition.
        led.set_high().unwrap();
        timer.delay_ms(2000);
        led.set_low().unwrap();
    }

    ufmt::uwriteln!(&mut usb, "Hello from pico-usb-uwrite.rs!\r").void_unwrap();
    // Check whether the host computer is actually listening to what we are
    // sending it, and disable output if it is not.
    // TODO: Make this work more reliably right after init().
    if usb.check_or_uninit(&timer, 1_000_000) {
        // One 250ms LED blink to indicate all is well.
        led.set_high().unwrap();
        timer.delay_ms(250);
        led.set_low().unwrap();
        timer.delay_ms(250);
    } else {
        // Nope. The host is not listening.
        // Signal with two short blinks and one longer one.
        for _ in 0..2 {
            led.set_high().unwrap();
            timer.delay_ms(125);
            led.set_low().unwrap();
            timer.delay_ms(125);
        }
        led.set_high().unwrap();
        timer.delay_ms(250);
        led.set_low().unwrap();
        timer.delay_ms(250);
    }

    loop {
        ufmt::uwriteln!(&mut usb, "Press the button to start speed test\r")
            .void_unwrap();
        usb.flush();
        buttons::wait_for_button_press_with_callback(&mut button, &mut || {
            usb.poll();
            true
        });

        // Quick 125ms blink so we can tell that program is running and button
        // press was detected, even if USB serial output is not working.
        led.set_high().unwrap();
        timer.delay_ms(125);
        led.set_low().unwrap();
        timer.delay_ms(125);

        let count0 = usb.wr_count();
        let t0 = timer.get_counter();
        const TEST_LINES: u16 = 1000;
        for i in 0..TEST_LINES {
            ufmt::uwrite!(&mut usb, "{}: ********************\r\n", i)
                .void_unwrap();
        }
        usb.flush();
        let t1 = timer.get_counter();
        let count1 = usb.wr_count();

        ufmt::uwriteln!(
            &mut usb,
            "Wrote {} bytes in {} microseconds\r\n\r",
            count1 - count0,
            (t1 - t0).to_micros(),
        )
        .void_unwrap();
        buttons::wait_for_button_release_with_callback(
            &mut button,
            &mut timer,
            &mut || {
                usb.poll();
                true
            },
        );
    }
}
