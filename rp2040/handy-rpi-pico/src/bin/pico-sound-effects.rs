//! Pico Sound Effects program - using sonic API
//! PWM slice 2 is used for the "driver" PWM controlling H-bridge side 1
//! PWM slice 6 is used for the "driver" PWM controlling H-bridge side 2
//! PWM slice 7 is used for the "pacing" PWM controlling sample timing
//! Pins and signals used:
//! - GPIO0: Pin 1: UART tx -> USB-serial rx (white wire on my device)
//! - GPIO1: Pin 2: UART rx -> USB-serial tx (green wire on my device)
//! - GND  : Pin 3: Ground  -> USB-serial ground
//! - GPIO4 : Pin 6  connected to PWM2 channel A -> PNP 1 gate
//! - GPIO5 : Pin 7  connected to PWM2 channel B -> NPN 1 gate
//! - GPIO12: Pin 16 connected to PWM6 channel A -> NPN 2 gate
//! - GPIO13: Pin 17 connected to PWM6 channel B -> PNP 2 gate
//! - GND   : Pin 23 ground connection on breadboard / circuitboard
//! - GND   : Pin 28 ground connection on breadboard / circuitboard
//! - GPIO22: Pin 29 connected to start/stop pushbutton
//! - RUN   : Pin 30 connected to reset pushbutton
//! - GPIO25: connected to on-board LED used to indicate button is pressed
#![no_std]
#![no_main]

extern crate alloc;

use core::fmt::Write; // for write!(), writeln!()
use core::mem::swap;
use cortex_m_rt::entry;
use embedded_hal::digital::OutputPin;
use handy_rp2040_common::{
    buttons::{is_button_pressed, wait_for_button_press},
    pwmutil,
    sonic::{self, Channel, SonicClient, WaveGen, Waveform},
};
use panic_halt as _;
use rp_pico::hal::{
    self,
    gpio::{self, DynPinId},
    pac::interrupt,
    Clock,
};

#[derive(Debug)]
enum SoundEffects {
    SineWave1KHz,
    TriangleWave1KHz,
    SquareWave1KHz,
    LowTone,
    HighTone,
    WobbleTone(ToneWobbler),
}

impl SoundEffects {
    /// Return the starting sound effect
    fn new() -> Self {
        Self::SineWave1KHz
    }

    /// Start the current sound effect
    fn start(&self, sonic_client: &SonicClient) {
        match self {
            Self::SineWave1KHz => {
                Self::set_waveform(sonic_client, Waveform::SineWave);
                Self::set_freq(sonic_client, 1000 * 16);
                Self::set_volume(sonic_client, WaveGen::MAX_AMPLITUDE / 2);
            }
            Self::TriangleWave1KHz => {
                Self::set_waveform(sonic_client, Waveform::TriangleWave);
                Self::set_freq(sonic_client, 1000 * 16);
                Self::set_volume(sonic_client, WaveGen::MAX_AMPLITUDE / 2);
            }
            Self::SquareWave1KHz => {
                Self::set_waveform(sonic_client, Waveform::SquareWave);
                Self::set_freq(sonic_client, 1000 * 16);
                Self::set_volume(sonic_client, WaveGen::MAX_AMPLITUDE / 2);
            }
            Self::LowTone => {
                Self::set_waveform(sonic_client, Waveform::SineWave);
                Self::set_freq(sonic_client, 250 * 16);
                Self::set_volume(sonic_client, WaveGen::MAX_AMPLITUDE / 2);
            }
            Self::HighTone => {
                Self::set_waveform(sonic_client, Waveform::SineWave);
                Self::set_freq(sonic_client, 2000 * 16);
                Self::set_volume(sonic_client, WaveGen::MAX_AMPLITUDE / 2);
            }
            Self::WobbleTone(wobbler) => {
                wobbler.start(sonic_client);
            }
        }
    }

    /// Implement any dynamic changes while sound effect is in progress
    /// This is called once per audio sample, with a small amount of jitter.
    fn update(&mut self, sonic_client: &SonicClient) {
        match self {
            Self::WobbleTone(wobbler) => {
                wobbler.update(sonic_client);
            }
            _ => {}
        }
    }

    /// Return the next sound effect after this one
    fn next(&self) -> Self {
        match *self {
            Self::SineWave1KHz => Self::TriangleWave1KHz,
            Self::TriangleWave1KHz => Self::SquareWave1KHz,
            Self::SquareWave1KHz => Self::LowTone,
            Self::LowTone => Self::HighTone,
            Self::HighTone => {
                Self::WobbleTone(ToneWobbler::new(
                    440 * 16, // low: 440 Hz
                    880 * 16, // high: 880 Hz
                ))
            }
            Self::WobbleTone(_) => Self::SineWave1KHz,
        }
    }

    fn set_waveform(sonic_client: &SonicClient, waveform: Waveform) {
        for channel in [Channel::ChannelA, Channel::ChannelB] {
            sonic_client.set_waveform(channel, waveform);
        }
    }

    fn set_freq(sonic_client: &SonicClient, freq_x16: u16) {
        for channel in [Channel::ChannelA, Channel::ChannelB] {
            sonic_client.set_freq(channel, freq_x16);
        }
    }

    fn set_phase_vel(sonic_client: &SonicClient, phase_vel: u16) {
        for channel in [Channel::ChannelA, Channel::ChannelB] {
            sonic_client.set_phase_vel(channel, phase_vel);
        }
    }

    fn set_volume(sonic_client: &SonicClient, volume: u16) {
        for channel in [Channel::ChannelA, Channel::ChannelB] {
            sonic_client.set_volume(channel, volume);
        }
    }
}

#[derive(Debug)]
struct ToneWobbler {
    lo_phase_vel: u16,
    hi_phase_vel: u16,
    cur_phase_vel: u16,
    accel: i16,
}

impl ToneWobbler {
    fn new(mut lo_freq_x16: u16, mut hi_freq_x16: u16) -> Self {
        if lo_freq_x16 > hi_freq_x16 {
            swap(&mut lo_freq_x16, &mut hi_freq_x16);
        }
        let lo_phase_vel = WaveGen::freq_x16_to_phase_vel(lo_freq_x16);
        let hi_phase_vel = WaveGen::freq_x16_to_phase_vel(hi_freq_x16);
        Self {
            lo_phase_vel,
            hi_phase_vel,
            cur_phase_vel: lo_phase_vel,
            accel: 1,
        }
    }

    /// Start tone at bottom of frequency range
    fn start(&self, sonic_client: &SonicClient) {
        SoundEffects::set_waveform(sonic_client, Waveform::SineWave);
        SoundEffects::set_phase_vel(sonic_client, self.cur_phase_vel);
        SoundEffects::set_volume(sonic_client, WaveGen::MAX_AMPLITUDE / 2);
    }

    /// Make tone go up or down depending on current state
    fn update(&mut self, sonic_client: &SonicClient) {
        if self.accel >= 0 {
            let accel = self.accel as u16;
            self.cur_phase_vel = self.cur_phase_vel.saturating_add(accel);
        } else {
            let accel = (-self.accel) as u16;
            self.cur_phase_vel = self.cur_phase_vel.saturating_sub(accel);
        }
        if (self.cur_phase_vel <= self.lo_phase_vel && self.accel < 0)
            || (self.cur_phase_vel >= self.hi_phase_vel && self.accel > 0)
        {
            self.accel = -self.accel;
        }
        SoundEffects::set_phase_vel(sonic_client, self.cur_phase_vel);
    }
}

#[entry]
fn main() -> ! {
    // Grab our singleton objects
    let mut pac = hal::pac::Peripherals::take().unwrap();

    // Set up the watchdog driver - needed by the clock setup code
    let mut watchdog = hal::Watchdog::new(pac.WATCHDOG);

    // Configure the clocks
    //
    // The default is to generate a 125 MHz system clock
    let clocks = hal::clocks::init_clocks_and_plls(
        rp_pico::XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    // Single-cycle I/O block
    let sio = hal::Sio::new(pac.SIO);

    let pins = rp_pico::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    let uart_pins = (
        // UART TX (characters sent from RP2040) on pin 1 (GPIO0)
        pins.gpio0.into_function::<hal::gpio::FunctionUart>(),
        // UART RX (characters received by RP2040) on pin 2 (GPIO1)
        pins.gpio1.into_function::<hal::gpio::FunctionUart>(),
    );
    let mut uart =
        hal::uart::UartPeripheral::new(pac.UART0, uart_pins, &mut pac.RESETS)
            .enable(
                // 115200 baud, 8 data bits, 1 stop bit, no parity
                hal::uart::UartConfig::default(),
                clocks.peripheral_clock.freq(),
            )
            .unwrap();

    writeln!(uart, "\r").unwrap();
    writeln!(uart, "Hello from pico-sound-effects.rs!\r").unwrap();

    // Init PWM slices
    let pwm_slices = hal::pwm::Slices::new(pac.PWM, &mut pac.RESETS);

    // PWM2 is the H-bridge side 1 driver PWM slice
    let driver1_pwm = pwm_slices.pwm2;
    let driver1_channel_a_pin = pins.gpio4.into_pull_type();
    let driver1_channel_b_pin = pins.gpio5.into_pull_type();

    // PWM6 is the H-bridge side 2 driver PWM slice
    let driver2_pwm = pwm_slices.pwm6;
    let driver2_channel_a_pin = pins.gpio12.into_pull_type();
    let driver2_channel_b_pin = pins.gpio13.into_pull_type();

    // PWM7 is the pacing PWM slice
    let pacing_pwm = pwm_slices.pwm7;

    let sonic_client =
        sonic::init_t4driver(
            pacing_pwm,
            driver1_pwm,
            driver1_channel_a_pin,
            driver1_channel_b_pin,
            driver2_pwm,
            driver2_channel_a_pin,
            driver2_channel_b_pin,
            Option::<
                gpio::Pin<DynPinId, gpio::FunctionSioOutput, gpio::PullNone>,
            >::None,
        );

    // LED to give feedback when button is pressed
    let mut led = pins.led.into_push_pull_output();
    // Connect pin 29 (GPIO22) to start/stop button
    let mut button = pins.gpio22.into_pull_up_input();

    let mut effect = SoundEffects::new();

    writeln!(uart, "Press and hold the button to make sounds\r").unwrap();
    loop {
        wait_for_button_press(&mut button);
        effect.start(&sonic_client);
        sonic_client.set_sound_output(true);
        led.set_high().unwrap();
        writeln!(uart, "{:?}\r", effect).unwrap();

        let mut release_count: u8 = 0;
        let debounce_thresh: u8 = 221; // about 10ms
        loop {
            if is_button_pressed(&mut button) {
                release_count = 0;
            } else {
                release_count += 1;
                if release_count >= debounce_thresh {
                    break;
                }
            }

            sonic_client.wait_for_timer_tick();
            effect.update(&sonic_client);
        }

        sonic_client.set_sound_output(false);
        effect = effect.next();
        led.set_low().unwrap();
    }
}

#[allow(non_snake_case)]
#[interrupt]
fn PWM_IRQ_WRAP() {
    pwmutil::pwm_irq_wrap_handler();
}
