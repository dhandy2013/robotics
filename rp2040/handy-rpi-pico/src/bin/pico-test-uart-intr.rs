//! Pico program: Test setting and controlling UART interrupt handler.
//! UART0 is used for this test.
//! Pins and signals used:
//! - GPIO0 : Pin 1  UART0 tx -> USB-serial rx (white wire on my device)
//! - GPIO1 : Pin 2  UART0 rx -> USB-serial tx (green wire on my device)
//! - GND   : Pin 3  Ground -> USB-serial ground
//! - GPIO22: Pin 29 pullup input -> application pushbutton
//! - GPIO25:        output -> onboard LED (PWM slice 4 channel B)
//! - RUN   : Pin 30 pullup input -> connected to reset pushbutton
//!
//! When the application button is pressed, the UART0_IRQ interrupt will be
//! enabled and bytes will be sent to that UART. If the test finishes without
//! getting stuck the onboard LED will blink at 1Hz.
#![no_std]
#![no_main]

extern crate alloc;

use alloc::{string::String, vec::Vec};
use core::{cell::RefCell, fmt::Write as FmtWrite};
use cortex_m_rt::entry;
use critical_section::{self, Mutex};
use embedded_hal::{delay::DelayNs, digital::OutputPin};
use handy_embedded_util::sync;
use handy_rp2040_common::{
    buttons::wait_for_button_press,
    chip,
    uart::{self, Uart0Type},
};
use panic_halt as _;
use rp_pico::hal::{
    gpio::{
        bank0::{Gpio0, Gpio1},
        FunctionUart, Pin, PullDown,
    },
    pac::{self, interrupt},
    timer::Timer,
    uart::UartPeripheral,
    Clock,
};

/// Data shared between the main program and the interrupt handler
struct Shared {
    intr_count: usize,
    num_writes: usize,
    tx_fifo_full_count: usize,
    tx_bytes: usize,
    min_write_size: usize,
    max_write_size: usize,
    first_write_size: usize,
    last_write_size: usize,
    uart0: UartPeripheral<
        rp_pico::hal::uart::Enabled,
        pac::UART0,
        (
            Pin<Gpio0, FunctionUart, PullDown>,
            Pin<Gpio1, FunctionUart, PullDown>,
        ),
    >,
    buf: Vec<u8>,
}

static SHARED: Mutex<RefCell<Option<Shared>>> = Mutex::new(RefCell::new(None));

static LOTS_OF_TEXT: &'static str = "\
Mary had a little lamb\r
it's fleece was white as snow\r
and everywhere that Mary went\r
the lamb was sure to go.\r
";

#[entry]
fn main() -> ! {
    // Grab our singleton objects
    let mut chip = chip::Chip::new(rp_pico::XOSC_CRYSTAL_FREQ);
    let mut uart0 = uart::init_uart0(
        chip.UART0,
        chip.pins.gpio0,
        chip.pins.gpio1,
        &mut chip.RESETS,
        chip.clocks.peripheral_clock.freq(),
    );

    writeln!(uart0, "\r").unwrap();
    writeln!(uart0, "Hello from pico-test-uart-intr!\r").unwrap();

    // The timer object lets us wait for specified amounts of time (in
    // milliseconds)
    let mut timer = Timer::new(chip.TIMER, &mut chip.RESETS, &chip.clocks);

    let mut button = chip.pins.gpio22.into_pull_up_input();

    let mut led = chip.pins.gpio25.into_push_pull_output();

    writeln!(uart0, "Press the button to generate UART0 interrupts.\r")
        .unwrap();
    wait_for_button_press(&mut button);
    led.set_high().unwrap();

    writeln!(uart0, "Enabling UART0 interrupts\r").unwrap();
    <Uart0Type as embedded_io::Write>::flush(&mut uart0).unwrap();
    critical_section::with(move |cs| {
        let mut maybe_shared = SHARED.borrow_ref_mut(cs);
        maybe_shared.replace(Shared {
            intr_count: 0,
            num_writes: 0,
            tx_fifo_full_count: 0,
            tx_bytes: 0,
            min_write_size: usize::MAX,
            max_write_size: 0,
            first_write_size: 0,
            last_write_size: 0,
            uart0,
            buf: Vec::new(),
        });
    });
    sync::with_shared_mut(&SHARED, |shared| {
        shared.uart0.enable_tx_interrupt();
    })
    .unwrap();
    uart::enable_uart0_interrupts();

    // Send bytes to the UART
    sync::with_shared_mut(&SHARED, |shared| {
        if false {
            // Short message, less than the 16-byte FIFO threshold
            shared.buf = String::from("Hello IRQ\r\n").into_bytes();
        }
        if true {
            // Longer message that I assumed would fill the TX FIFO at least
            // once (but it did not!)
            shared.buf = String::from(LOTS_OF_TEXT).into_bytes();
        }
    })
    .unwrap();
    uart::pend_uart0_intr();
    timer.delay_ms(100);

    let (
        intr_count,
        num_writes,
        tx_fifo_full_count,
        tx_bytes,
        min_write_size,
        max_write_size,
        first_write_size,
        last_write_size,
    ) = sync::with_shared_mut(&SHARED, |shared| {
        (
            shared.intr_count,
            shared.num_writes,
            shared.tx_fifo_full_count,
            shared.tx_bytes,
            shared.min_write_size,
            shared.max_write_size,
            shared.first_write_size,
            shared.last_write_size,
        )
    })
    .unwrap();
    let mut msg = String::with_capacity(64);
    write!(
        msg,
        "\r\n\
        Interrupt count: {intr_count}\r\n\
        Number of raw_write calls: {num_writes}\r\n\
        TX FIFO full count: {tx_fifo_full_count}\r\n\
        TX byte count: {tx_bytes}\r\n\
        Smallest sized write chunk: {min_write_size}\r\n\
        Largest sized write chunk: {max_write_size}\r\n\
        First write chunk size: {first_write_size}\r\n\
        Last write chunk size: {last_write_size}\r\n\
        "
    )
    .unwrap();
    write_str(&msg);

    loop {
        timer.delay_ms(500);
        led.set_low().unwrap();

        timer.delay_ms(500);
        led.set_high().unwrap();
    }
}

fn write_str(msg: &str) {
    sync::with_shared_mut(&SHARED, |shared| {
        shared.uart0.write_full_blocking(msg.as_bytes());
    })
    .unwrap();
}

#[allow(non_snake_case)]
#[interrupt]
fn UART0_IRQ() {
    uart::clear_uart0_tx_intr();
    let _ = sync::with_shared_mut(&SHARED, |shared| handle_uart0_irq(shared));
}

fn handle_uart0_irq(shared: &mut Shared) {
    shared.intr_count += 1;
    while shared.buf.len() > 0 {
        shared.num_writes += 1;
        // Experiments showed: If the TX FIFO is completely empty *and* the UART
        // output shift register is empty, `write_raw` will actually write 33
        // bytes: 32 into the TX FIFO and 1 into the TX shift register.
        match shared.uart0.write_raw(&shared.buf) {
            Ok(remaining) => {
                let n = shared.buf.len() - remaining.len();
                shared.tx_bytes += n;
                if n < shared.min_write_size {
                    shared.min_write_size = n;
                }
                if n > shared.max_write_size {
                    shared.max_write_size = n;
                }
                if shared.num_writes == 1 {
                    shared.first_write_size = n;
                }
                shared.last_write_size = n;
                shared.buf.drain(..n);
            }
            Err(_err) => {
                // I don't understand how this match arm compiles, because
                // `_err` is of type `Error<Infallible>`.
                // According to https://doc.rust-lang.org/core/convert/enum.Infallible.html
                // `Infallible` is "The error type for errors that can never
                // happen.". Yet this match arm not only compiles, it gets
                // executed! I've proved it!
                //
                // Update: Ok, I figured it out. `_err` is actually of type
                // `nb::Error<Infallible>`, and `nb::Error` is defined like
                // this:
                // ```
                // pub enum Error<E> {
                //     /// A different kind of error
                //     Other(E),
                //     /// This operation requires blocking behavior to complete
                //     WouldBlock,
                // }
                // ```
                // So `Error<Infallible>` means that the _only_ kind of `Error`
                // that can be constructed is `Error::WouldBlock`, because the
                // `Other` variant of `Error` cannot be constructed.
                shared.tx_fifo_full_count += 1;
                break;
            }
        }
    }
}
