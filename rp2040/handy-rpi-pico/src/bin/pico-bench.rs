//! Raspberry Pi Pico app that does instruction timing measurements.
//! Pins and signals used:
//! - GPIO0 : Pin 1: UART tx -> USB-serial rx (white wire on my device)
//! - GPIO1 : Pin 2: UART rx -> USB-serial tx (green wire on my device)
//! - GND   : Pin 3: Ground  -> USB-serial ground
//! - GPIO22: Pin 29 Configured as input with internal pullup resistor
//! - RUN   : Pin 30 connected to reset pushbutton
//!
//! To see the generated assembly language, run these commands in the workspace
//! directory:
//! ```
//! pushd handy-rpi-pico; cargo rustc --release --bin pico-bench -- --emit asm; popd
//! less -FRX $(find target -name "pico_bench*.s")
//! ```
#![no_std]
#![no_main]

use core::fmt::Write; // for write!(), writeln!()
use cortex_m_rt::entry;
use embedded_hal::{delay::DelayNs, digital::InputPin};
use handy_rp2040_common::{
    buttons::ButtonType,
    chip,
    random::{self, EnabledRingOscillator},
    systick::SysTick,
    uart,
};
use panic_halt as _;
use rp_pico::hal::{self, clocks::Clock, rosc::RingOscillator, timer::Timer};

/// Print info about the SYST clock
/// From the RP2040 datasheet section 2.4.5.1.1 "SysTick Timer":
/// A 24-bit SysTick system timer, extends the functionality of both the
/// processor and the NVIC and provides:
/// - A 24-bit system timer (SysTick).
/// - Additional configurable priority SysTick interrupt.
/// By default SysTick runs off of a 1 microsecond timer period tick from the
/// watchdog timer. The SysTick timer can also run from the system clock.
#[inline(never)]
#[no_mangle]
fn characterize_syst(
    out: &mut dyn Write,
    clocks: &hal::clocks::ClocksManager,
    button: &mut ButtonType,
    rosc: &EnabledRingOscillator,
    mut syst: cortex_m::peripheral::SYST,
) -> hal::pac::SYST {
    writeln!(out, "Characterizing SysTick Timer (SYST)\r").unwrap();
    let result = hal::pac::SYST::get_ticks_per_10ms();
    writeln!(out, "SYST::get_ticks_per_10ms(): {result}\r").unwrap();
    let result = syst.get_clock_source();
    writeln!(out, "Default SYST clock source: {result:?}\r").unwrap();

    writeln!(out, "Enabling SysTick hi-resolution timer\r").unwrap();
    let mut systick = SysTick::new(syst, clocks.system_clock.freq());

    writeln!(
        out,
        "System clock frequency: {freq}\r",
        freq = systick.frequency()
    )
    .unwrap();

    let ticks_per_ms = systick.ms_to_ticks(1);
    writeln!(out, "Calculated SysTick ticks per ms: {ticks_per_ms}\r").unwrap();

    let ms: u16 = 1;
    let ticks_per_ms: u64 =
        (ms as u64 * systick.frequency().to_Hz() as u64) / 1000u64;
    writeln!(
        out,
        "Calculated SysTick ticks per ms (method 2): {ticks_per_ms}\r"
    )
    .unwrap();

    writeln!(
        out,
        "Current SysTick count: {count}\r",
        count = SysTick::get_current()
    )
    .unwrap();

    let mut loop_count: u32 = 0;
    systick.clear_current();
    while !systick.has_wrapped() {
        loop_count += 1;
    }
    writeln!(
        out,
        "Looped {loop_count} times waiting for SysTick count to wrap\r"
    )
    .unwrap();

    // Reset tick counter before doing micro-benchmarks
    systick.clear_current();

    // Get the interval if you just read SysTick twice in a row fast.
    let (t0, t1) = measure_syst_min_interval();
    let syst_min_interval_1 = t0 - t1;
    let (t0, t1) = measure_syst_min_interval();
    let syst_min_interval_2 = t0 - t1;

    // Time reading GPIO pin
    let (t0, t1, t2) = measure_gpio_read_time_twice(button);
    let gpio_read_1 = t0 - t1;
    let gpio_read_2 = t1 - t2;
    let (t0, t1, t2) = measure_gpio_read_time_twice(button);
    let gpio_read_3 = t0 - t1;
    let gpio_read_4 = t1 - t2;

    // Measure multiply time using unpredictable operand values
    let param = gen_random_u32(rosc);
    let (t0, t1, result1) = measure_multiply_time(param);
    let multiply_1 = t0 - t1;
    let param = gen_random_u32(rosc);
    let (t0, t1, result2) = measure_multiply_time(param);
    let multiply_2 = t0 - t1;

    // Measure multiply then devide with 64-bit intermediate result
    let (t0, t1, _mult_div_result_1) = measure_mult_div_time(rosc, 125_000_000);
    let mult_div_1 = t0 - t1;
    let (t0, t1, _mult_div_result_2) = measure_mult_div_time(rosc, 125_000_000);
    let mult_div_2 = t0 - t1;

    // Measure time to generate random numbers using the Ring Oscillator

    let (rand0_t, rand0_val) = {
        let t0 = SysTick::get_current();
        let result = random::gen_random_bits(rosc, 0);
        cortex_m::asm::dmb(); // prevent hoisting of SysTick::get_current();
        let t1 = SysTick::get_current();
        ((t0 - t1), result)
    };

    let (rand1_t, rand1_val) = {
        let t0 = SysTick::get_current();
        let result = random::gen_random_bits(rosc, 1);
        cortex_m::asm::dmb(); // prevent hoisting of SysTick::get_current();
        let t1 = SysTick::get_current();
        ((t0 - t1), result)
    };

    let (rand16_t, rand16_val) = {
        let t0 = SysTick::get_current();
        let result = random::gen_random_bits(rosc, 16);
        cortex_m::asm::dmb(); // prevent hoisting of SysTick::get_current();
        let t1 = SysTick::get_current();
        ((t0 - t1), result)
    };

    // Measure time it takes to generate a "random" 32-bit number
    let (rand32_t, rand32_val) = {
        let t0 = SysTick::get_current();
        let result = random::gen_random_bits(rosc, 32);
        cortex_m::asm::dmb(); // prevent hoisting of SysTick::get_current();
        let t1 = SysTick::get_current();
        ((t0 - t1), result)
    };

    writeln!(out, "Minimum interval 1: {syst_min_interval_1}\r").unwrap();
    writeln!(out, "Minimum interval 2: {syst_min_interval_2}\r").unwrap();
    writeln!(out, "GPIO read 1       : {gpio_read_1} ticks\r").unwrap();
    writeln!(out, "GPIO read 2       : {gpio_read_2} ticks\r").unwrap();
    writeln!(out, "GPIO read 3       : {gpio_read_3} ticks\r").unwrap();
    writeln!(out, "GPIO read 4       : {gpio_read_4} ticks\r").unwrap();
    writeln!(out, "32-bit multiply 1 : {multiply_1} ticks\r").unwrap();
    writeln!(out, "32-bit multiply result: {result1}\r").unwrap();
    writeln!(out, "32-bit multiply 2 : {multiply_2} ticks\r").unwrap();
    writeln!(out, "32-bit multiply result: {result2}\r").unwrap();
    writeln!(out, "Multiply then divide 1: {mult_div_1} ticks\r").unwrap();
    writeln!(out, "Multiply then divide 2: {mult_div_2} ticks\r").unwrap();
    writeln!(out, "0-bit random number: {rand0_t} ticks {rand0_val}\r")
        .unwrap();
    writeln!(out, "1-bit random number: {rand1_t} ticks {rand1_val}\r")
        .unwrap();
    writeln!(out, "16-bit random number: {rand16_t} ticks {rand16_val}\r")
        .unwrap();
    writeln!(out, "32-bit random number: {rand32_t} ticks {rand32_val}\r")
        .unwrap();

    systick.free()
}

/// Return a "random" u32 unpredictable by the compiler.
#[inline(never)]
#[no_mangle]
fn gen_random_u32(rosc: &EnabledRingOscillator) -> u32 {
    random::gen_random_bits(rosc, 32)
}

/// Return a "random" u16 unpredictable by the compiler.
fn gen_random_u16(rosc: &EnabledRingOscillator) -> u16 {
    random::gen_random_bits(rosc, 16) as u16
}

/// Read the SysTick timer twice in a row with no delay in between.
/// Return the two count values read: (t0, t1)
#[inline(never)]
#[no_mangle]
fn measure_syst_min_interval() -> (u32, u32) {
    let t0 = SysTick::get_current();
    let t1 = SysTick::get_current();
    (t0, t1)
}

/// Use SysTick timer to measure time it takes to do a "single cycle" GPIO read.
/// Do two GPIO reads. Return (before, between, after) SysTick counts.
#[inline(never)]
#[no_mangle]
fn measure_gpio_read_time_twice(button: &mut ButtonType) -> (u32, u32, u32) {
    let t0 = SysTick::get_current();
    let _b1 = button.is_high().unwrap();
    let t1 = SysTick::get_current();
    let _b2 = button.is_high().unwrap();
    let t2 = SysTick::get_current();
    (t0, t1, t2)
}

/// Use SysTick timer to measure 32-bit multiply time.
/// The parameter is multiplied by the first SysTick count to ensure that the
/// compiler doesn't put the multiply before the first SysTick read.
/// Return (SysTick count before, SysTick count after, multiply result)
#[inline(never)]
#[no_mangle]
fn measure_multiply_time(param: u32) -> (u32, u32, u32) {
    let t0 = SysTick::get_current();
    let result = t0 * param;
    let t1 = SysTick::get_current();
    (t0, t1, result)
}

/// Use SysTick timer to measure multiply then divide by constant, where the
/// multiplication has a 64-bit intermediate value and the result is 32 bits.
/// This simulates the calculation done by SysTick::ms_to_ticks.
///
/// Return (start_ticks, end_ticks, result)
#[inline(never)]
#[no_mangle]
fn measure_mult_div_time(
    rosc: &EnabledRingOscillator,
    m2: u32,
) -> (u32, u32, u32) {
    let m1 = gen_random_u16(rosc);
    let t0 = SysTick::get_current();
    let result = ((m1 as u64 * m2 as u64) / 1000u64).min(0xffff_ffff) as u32;
    // This Data Memory Barrier (DMB) instruction is to get the compiler to not
    // hoist the last SysTick::get_current() _before_ the calculation.
    cortex_m::asm::dmb();
    let t1 = SysTick::get_current();
    (t0, t1, result)
}

fn compare_systick_vs_timer(
    out: &mut dyn Write,
    clocks: &hal::clocks::ClocksManager,
    resets: &mut hal::pac::RESETS,
    syst: hal::pac::SYST,
    timer_pac: hal::pac::TIMER,
) {
    let mut systick = SysTick::new(syst, clocks.system_clock.freq());
    let mut timer = Timer::new(timer_pac, resets, clocks);
    let _ = writeln!(out, "Comparing SysTick vs Timer\r");
    systick.clear_current();
    let t0 = SysTick::get_current();
    timer.delay_ms(10);
    let measured_ticks = t0 - SysTick::get_current();
    let _ =
        writeln!(out, "timer.delay_ms(10): {measured_ticks} system ticks\r");
}

#[entry]
fn main() -> ! {
    // Standard setup
    let mut chip = chip::Chip::new(rp_pico::XOSC_CRYSTAL_FREQ);

    let mut uart0 = uart::init_uart0(
        chip.UART0,
        chip.pins.gpio0,
        chip.pins.gpio1,
        &mut chip.RESETS,
        chip.clocks.peripheral_clock.freq(),
    );

    writeln!(uart0, "\r").unwrap();
    writeln!(uart0, "Hello from pico-bench.rs!\r").unwrap();

    // Configure GPIO (pin 29) as a pullup input pin. We won't really use the
    // input for anything other than a source of volatile, unpredictable input
    // to keep the compiler from optimizing away code we care about.
    let mut button = chip.pins.gpio22.into_pull_up_input();

    // Get the Ring Oscillator to use as a source of random numbers.
    let rosc: EnabledRingOscillator =
        RingOscillator::new(chip.ROSC).initialize();

    let syst = characterize_syst(
        &mut uart0,
        &chip.clocks,
        &mut button,
        &rosc,
        chip.core.SYST,
    );

    compare_systick_vs_timer(
        &mut uart0,
        &chip.clocks,
        &mut chip.RESETS,
        syst,
        chip.TIMER,
    );

    writeln!(&mut uart0, "Done.\r").unwrap();
    loop {}
}
