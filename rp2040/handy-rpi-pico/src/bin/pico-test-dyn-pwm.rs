//! Test DynamicPwmSlice.
//!
//! Pins and signals used:
//! - GPIO0 : Pin 1  UART0 tx -> USB-serial rx (white wire on my device)
//! - GPIO1 : Pin 2  UART0 rx -> USB-serial tx (green wire on my device)
//! - GND   : Pin 3  Ground   -> USB-serial ground
//! - GPIO16: Pin 21 connected to PWM0 channel A
//! - GPIO17: Pin 22 connected to PWM0 channel B
//! - GND   : Pin 23 Ground
//! - RUN   : Pin 30 connected to reset pushbutton
//!
//! The test runs immediately on power on or when the chip is reset.
#![no_std]
#![no_main]

use core::fmt::Write; // for write!(), writeln!()
use cortex_m_rt::entry;
use embedded_hal_0_2::digital::v2::InputPin;
use handy_rp2040_common::{
    chip, pwmutil::DynamicPwmSlice, systick::SysTick, uart,
};
use panic_halt as _;
use rp_pico::hal::{self, fugit::HertzU32, gpio, Clock};

#[entry]
fn main() -> ! {
    // Standard setup
    let mut chip = chip::Chip::new(rp_pico::XOSC_CRYSTAL_FREQ);

    let mut uart0 = uart::init_uart0(
        chip.UART0,
        chip.pins.gpio0,
        chip.pins.gpio1,
        &mut chip.RESETS,
        chip.clocks.peripheral_clock.freq(),
    );

    writeln!(uart0, "\r\nHello from pico-test-dyn-pwm.rs!\r").unwrap();

    let pin_pwm0_chan_a = into_dynamic_output_pin(chip.pins.gpio16);
    let pin_pwm0_chan_b = into_dynamic_output_pin(chip.pins.gpio17);

    // Initialize short-period (134ms max) high-resolution countdown timer.
    let mut systick =
        SysTick::new(chip.core.SYST, chip.clocks.system_clock.freq());

    // Init PWM slice
    let pwm_slices = hal::pwm::Slices::new(chip.PWM, &mut chip.RESETS);
    let mut dyn_pwm_s0 = DynamicPwmSlice::from(pwm_slices.pwm0);

    // Connect pins to the channel A and B PWM outputs
    let mut pin_pwm0_chan_a = pin_pwm0_chan_a
        .into_dyn_pin()
        .try_into_function::<gpio::FunctionPwm>()
        .unwrap_or_else(|_| panic!("Pin not valid for PWM0"));
    let mut pin_pwm0_chan_b = pin_pwm0_chan_b
        .into_dyn_pin()
        .try_into_function::<gpio::FunctionPwm>()
        .unwrap_or_else(|_| panic!("Pin not valid for PWM0"));

    // Test DynamicPwmSlice. Run the test twice to warm the cache.
    test_dyn_pwm(
        &mut uart0,
        &mut systick,
        chip.clocks.system_clock.freq(),
        &mut pin_pwm0_chan_a,
        &mut pin_pwm0_chan_b,
        &mut dyn_pwm_s0,
    );
    test_dyn_pwm(
        &mut uart0,
        &mut systick,
        chip.clocks.system_clock.freq(),
        &mut pin_pwm0_chan_a,
        &mut pin_pwm0_chan_b,
        &mut dyn_pwm_s0,
    );
    uart0.write_full_blocking(b"\r\n");

    // Disconnect the pins from the channel A and B outputs, otherwise we get
    // outputs stuck high after the PWM slice is disabled.
    let _pin_pwm0_chan_a = pin_pwm0_chan_a
        .try_into_function::<gpio::FunctionNull>()
        .ok()
        .unwrap();
    let _pin_pwm0_chan_b = pin_pwm0_chan_b
        .try_into_function::<gpio::FunctionNull>()
        .ok()
        .unwrap();

    writeln!(uart0, "Done.\r").unwrap();
    loop {}
}

/// Take an unconfigured pin, configure it as a push-pull output, and return it
/// as a dynamically-typed pin.
fn into_dynamic_output_pin<I>(
    pin: gpio::Pin<I, gpio::FunctionNull, gpio::PullDown>,
) -> gpio::Pin<gpio::DynPinId, gpio::FunctionSioOutput, gpio::PullNone>
where
    I: gpio::PinId + gpio::ValidFunction<gpio::FunctionSioOutput>,
{
    pin.into_push_pull_output_in_state(gpio::PinState::Low)
        .into_dyn_pin()
        .into_pull_type::<hal::gpio::PullNone>()
}

/// Return true iff the pin reads low before the timeout period.
#[inline(always)]
fn read_till_low_or_timeout(
    pin: &mut gpio::Pin<gpio::DynPinId, gpio::FunctionPwm, gpio::PullNone>,
) -> bool {
    for _ in 0u16..=0xffff {
        if pin.as_input().is_low().unwrap() {
            return true;
        }
    }
    false
}

fn test_dyn_pwm(
    out: &mut dyn Write,
    systick: &mut SysTick,
    system_clock_freq: HertzU32,
    pin_chan_a: &mut gpio::Pin<
        gpio::DynPinId,
        gpio::FunctionPwm,
        gpio::PullNone,
    >,
    pin_chan_b: &mut gpio::Pin<
        gpio::DynPinId,
        gpio::FunctionPwm,
        gpio::PullNone,
    >,
    dyn_pwm: &mut DynamicPwmSlice,
) {
    writeln!(out, "Testing {dyn_pwm:?}\r").unwrap();
    writeln!(out, "System clock frequency: {system_clock_freq}\r").unwrap();

    // Start PWM slice in known state
    dyn_pwm.reset();
    if dyn_pwm.has_overflown() {
        writeln!(out, "has_overflown is still set after reset(), why?\r")
            .unwrap();
    }

    // Configure TOP value to set PWM frequency to 10KHz.
    // Default system_clock_freq is 125MHz so this will be a valid u16.
    let top = (system_clock_freq.to_Hz() / 10_000) as u16;
    let pwm_period = top + 1;
    dyn_pwm.set_top(top);

    // Set channel A duty cycle to 25%.
    let cc_a = top / 4;
    // Set channel B duty cycle to 75%
    let cc_b = (top * 3) / 4;
    if false {
        dyn_pwm.write_channel_a_b(cc_a, cc_b);
    } else {
        // Try writing the channel pulse widths separately to test whether
        // setting one channel duty cycle sets the other duty cycle to zero
        // (this is a bug we used to have.)
        dyn_pwm.write_channel_a(cc_a);
        dyn_pwm.write_channel_b(cc_b);
    }

    let systick_reload = SysTick::get_reload(); // this won't change

    // Measure PWM frequency
    systick.disable_counter();
    systick.clear_current();
    systick.enable_counter();
    dyn_pwm.enable();
    while !dyn_pwm.has_overflown() {}
    let measured_pwm_period = systick_reload - SysTick::get_current();
    let measured_pwm_period_err =
        (pwm_period as i32) - (measured_pwm_period as i32);
    writeln!(
        out,
        "Measured PWM period: {measured_pwm_period} ticks, \
        expected {pwm_period} \
        (err = {measured_pwm_period_err})\r"
    )
    .unwrap();

    if !dyn_pwm.has_overflown() {
        writeln!(
            out,
            "has_overflown should have remained set until explicitly cleared!\r"
        )
        .unwrap();
    }
    dyn_pwm.clear_interrupt();
    if dyn_pwm.has_overflown() {
        writeln!(
            out,
            ".clear_interrupt() did not clear the has_overflown signal!\r"
        )
        .unwrap();
    }

    // Measure channel A duty cycle
    dyn_pwm.disable();
    dyn_pwm.set_counter(0);
    systick.disable_counter();
    systick.clear_current();
    systick.enable_counter();
    dyn_pwm.enable();
    if !read_till_low_or_timeout(pin_chan_a) {
        writeln!(out, "FAIL: Channel A output never went low\r").unwrap();
        return;
    }
    let measured_cc_a = systick_reload - SysTick::get_current();
    let measured_cc_a_err = (cc_a as i32) - (measured_cc_a as i32);
    writeln!(
        out,
        "Measured channel A duty cycle: \
        {measured_cc_a} / {pwm_period}, expected {cc_a} \
        (err = {measured_cc_a_err})\r",
    )
    .unwrap();

    // Measure channel B duty cycle
    dyn_pwm.disable();
    dyn_pwm.set_counter(0);
    systick.disable_counter();
    systick.clear_current();
    systick.enable_counter();
    dyn_pwm.enable();
    if !read_till_low_or_timeout(pin_chan_b) {
        writeln!(out, "FAIL: Channel B output never went low\r").unwrap();
        return;
    }
    let measured_cc_b = systick_reload - SysTick::get_current();
    let measured_cc_b_err = (cc_b as i32) - (measured_cc_b as i32);
    writeln!(
        out,
        "Measured channel B duty cycle: \
        {measured_cc_b} / {pwm_period}, expected {cc_b} \
        (err = {measured_cc_b_err})\r",
    )
    .unwrap();

    // Reset this PWM slice to known starting state when done
    dyn_pwm.reset();
}
