//! Pico program: Compare sound from t1 vs t4 driver side-by-side
//! PWM slice 0 is used for t1 driver PWM (signals sent to speaker)
//! PWM slice 1 is used for t1 pacing PWM controlling sample timing
//! PWM slice 2 is used for t4 driver PWM (signals sent to speaker)
//! PWM slice 7 is used for t4 pacing PWM controlling sample timing
//! Pins and signals used:
//! - GPIO0 : Pin 1  UART0 tx -> USB-serial rx (white wire on my device)
//! - GPIO1 : Pin 2  UART0 rx -> USB-serial tx (green wire on my device)
//! - GND   : Pin 3  Ground   -> USB-serial ground
//! - GPIO2 : Pin 4  connected to PWM1 channel A t1 pacing
//! - GPIO3 : Pin 5  connected to PWM1 channel B t1 interrupt handler execution
//! - GPIO4 : Pin 6  connected to PWM2 channel A -> PNP 1 gate
//! - GPIO5 : Pin 7  connected to PWM2 channel B -> NPN 1 gate
//! - GPIO12: Pin 16 connected to PWM6 channel A -> PNP 2 gate
//! - GPIO13: Pin 17 connected to PWM6 channel B -> NPN 2 gate
//! - GPIO14: Pin 19 connected to PWM7 channel A t4 -> trigger on sample start
//! - GPIO15: Pin 20 connected to PWM7 channel B t4 -> trace interrupt handler
//! - GPIO16: Pin 21 connected to PWM0 channel A t1 driving speaker 1
//! - GPIO17: Pin 22 connected to PWM0 channel B t1 driving speaker 2
//! - GND   : Pin 23 ground connection on breadboard / circuitboard
//! - GPIO20: Pin 26 connected to PWM2 channel A t4 (inverted) -> NPN 2 gate
//! - GPIO21: Pin 27 connected to PWM2 channel B t4 (inverted) -> PNP 2 gate
//! - GPIO22: Pin 29 connected to application pushbutton
//! - RUN   : Pin 30 connected to reset pushbutton
//! - GPIO25: connected to internal LED to signal when button is pressed
//!
//! The collectors of PNP 1 and NPN 1 are connected to speaker 3 input 1;
//! the collectors of PNP 2 and NPN 2 are connected to speaker 3 input 2.
//!
//! This code is careful to ensure that PNP 1 and NPN 1 are never turned on at
//! the same time, and PNP 2 and NPN 2 are never turned on at the same time.
//! Also there is a "dead zone" of 225 nS between when the PNP transistor is
//! turned off and the corresponding NPN transitor is turned on and vise versa.
//!
//! While the application button is pressed and held a 1kHz sine wave is
//! generated. The first time the button is pressed the t1 driver is activated,
//! the second time the t4 driver, the third time back to the t1 driver, etc.
#![no_std]
#![no_main]

use core::fmt::Write; // for write!(), writeln!()
use cortex_m_rt::entry;
use embedded_hal::digital::OutputPin;
use handy_rp2040_common::{
    buttons::{wait_for_button_press, wait_for_button_release},
    chip, pwmutil,
    sonic::{self, Channel, WaveGen, Waveform},
    uart,
};
use panic_halt as _;
use rp_pico::hal::{self, gpio, pac::interrupt, timer::Timer, Clock};

#[entry]
fn main() -> ! {
    // Standard setup
    let mut chip = chip::Chip::new(rp_pico::XOSC_CRYSTAL_FREQ);

    let mut uart0 = uart::init_uart0(
        chip.UART0,
        chip.pins.gpio0,
        chip.pins.gpio1,
        &mut chip.RESETS,
        chip.clocks.peripheral_clock.freq(),
    );

    writeln!(uart0, "\r").unwrap();
    writeln!(uart0, "Hello from pico-driver-compare.rs!\r").unwrap();

    let mut timer = Timer::new(chip.TIMER, &mut chip.RESETS, &chip.clocks);

    // Init PWM slices
    let pwm_slices = hal::pwm::Slices::new(chip.PWM, &mut chip.RESETS);

    // Split out the t1 driver PWM slice 0 and connect channels A and B pins
    let driver_pwm = pwm_slices.pwm0;
    let driver_a_pin = chip
        .pins
        .gpio16
        .into_function::<gpio::FunctionNull>()
        .into_pull_type::<gpio::PullNone>();
    let driver_b_pin = chip
        .pins
        .gpio17
        .into_function::<gpio::FunctionNull>()
        .into_pull_type::<gpio::PullNone>();

    // Split out the t1 pacing PWM slice 1 and connect GPIO2 pin 4 to channel A
    // Use this as the oscilloscope trigger on each new audio sample.
    let pacing_pwm = pwm_slices.pwm1;
    let _pacing_a_pin = chip
        .pins
        .gpio2
        .into_function::<gpio::FunctionPwm>()
        .into_pull_type::<gpio::PullNone>();

    // Configure GPIO3 pin 5 to be used as an output to trace t1 interrupt
    // handler execution.
    let handler_trace_pin = chip
        .pins
        .gpio3
        .into_function::<gpio::FunctionSioOutput>()
        .into_pull_type::<gpio::PullNone>();

    let sonic_t1_client = sonic::init_t1driver(
        driver_pwm,
        pacing_pwm,
        Some(driver_a_pin),
        Some(driver_b_pin),
        Some(handler_trace_pin),
    );

    // PWM2 is the H-bridge side 1 driver PWM slice
    let driver1_pwm = pwm_slices.pwm2;
    let driver1_channel_a_pin = chip.pins.gpio4.into_pull_type();
    let driver1_channel_b_pin = chip.pins.gpio5.into_pull_type();

    // PWM6 is the H-bridge side 2 driver PWM slice
    let driver2_pwm = pwm_slices.pwm6;
    let driver2_channel_a_pin = chip.pins.gpio12.into_pull_type();
    let driver2_channel_b_pin = chip.pins.gpio13.into_pull_type();

    // PWM7 is the t4 pacing PWM slice
    let pacing_pwm = pwm_slices.pwm7;

    // Connect to the t4 pacing PWM slice channel A
    let _pacing_a_pin = chip
        .pins
        .gpio14
        .into_function::<gpio::FunctionPwm>()
        .into_pull_type::<gpio::PullNone>();

    // Configure this output to be used as an output to trace t4 interrupt
    // handler execution.
    let handler_trace_pin = chip
        .pins
        .gpio15
        .into_function::<gpio::FunctionSioOutput>()
        .into_pull_type::<gpio::PullNone>();

    let sonic_t4_client = sonic::init_t4driver(
        pacing_pwm,
        driver1_pwm,
        driver1_channel_a_pin,
        driver1_channel_b_pin,
        driver2_pwm,
        driver2_channel_a_pin,
        driver2_channel_b_pin,
        Some(handler_trace_pin),
    );

    // LED to give feedback when button is pressed
    let mut led = chip.pins.gpio25.into_push_pull_output();
    // Connect pin 29 (GPIO22) to start/stop button
    let mut button = chip.pins.gpio22.into_pull_up_input();

    // Set up channels A and B of each driver to generate 1kHz sine wave
    let waveform = Waveform::SineWave;
    let freq_x16: u16 = 1000 * 16;
    for sonic_client in [&sonic_t1_client, &sonic_t4_client] {
        for channel in [Channel::ChannelA, Channel::ChannelB] {
            sonic_client.set_waveform(channel, waveform);
            sonic_client.set_freq(channel, freq_x16);
            sonic_client.set_volume(channel, WaveGen::MAX_AMPLITUDE / 2);
        }
    }

    uart0
        .write_str(
            "\
Press and hold the button to generate 1kHz sine wave\r
Each time you press the button it will switch between the t1 and t4 drivers\r
",
        )
        .unwrap();

    let mut loop_count: u8 = 0;
    loop {
        let (sonic_client, driver_label) = if loop_count & 0x1 == 0 {
            (&sonic_t4_client, "t4")
        } else {
            (&sonic_t1_client, "t1")
        };

        wait_for_button_press(&mut button);

        sonic_client.set_sound_output(true);
        led.set_high().unwrap();
        write!(
            uart0,
            "Generating 1kHz sine wave using {driver_label} driver..."
        )
        .unwrap();

        wait_for_button_release(&mut button, &mut timer);

        sonic_client.set_sound_output(false);
        led.set_low().unwrap();
        writeln!(uart0, "done.\r").unwrap();

        loop_count = loop_count.wrapping_add(1);
    }
}

#[allow(non_snake_case)]
#[interrupt]
fn PWM_IRQ_WRAP() {
    pwmutil::pwm_irq_wrap_handler();
}
