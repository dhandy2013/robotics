//! Pico program: complementary PWM output of square wave at maximum frequency
//! PWM slice 2 is used for the "driver" PWM controlling H-bridge side 1
//! PWM slice 6 is used for the "driver" PWM controlling H-bridge side 2
//! PWM slice 7 is used for the "pacing" PWM controlling sample timing
//! Pins and signals used:
//! - GPIO0 : Pin 1  UART0 tx -> USB-serial rx (white wire on my device)
//! - GPIO1 : Pin 2  UART0 rx -> USB-serial tx (green wire on my device)
//! - GND   : Pin 3  Ground   -> USB-serial ground
//! - GPIO4 : Pin 6  connected to PWM2 channel A -> PNP 1 gate
//! - GPIO5 : Pin 7  connected to PWM2 channel B -> NPN 1 gate
//! - GPIO12: Pin 16 connected to PWM6 channel A -> NPN 2 gate
//! - GPIO13: Pin 17 connected to PWM6 channel B -> PNP 2 gate
//! - GPIO14: Pin 19 connected to PWM7 channel A -> trigger on sample start
//! - GPIO15: Pin 20 connected to PWM7 channel B -> trace interrupt handler
//! - GND   : Pin 23 ground connection on breadboard / circuitboard
//! - GPIO22: Pin 29 connected to on/off pushbutton
//! - RUN   : Pin 30 connected to reset pushbutton
//! - GPIO25: connected to internal LED to signal when button is pressed
//!
//! This code is meant to be run without the PWM outputs going to transistor
//! gates, but rather to oscilloscope probes to verify that dead zone timing is
//! correct and at least 225 nS elapse between turning off one transistor and
//! turning on another one driving the same node.
#![no_std]
#![no_main]

use core::fmt::Write; // for write!(), writeln!()
use cortex_m_rt::entry;
use embedded_hal::digital::OutputPin;
use handy_rp2040_common::{
    buttons::{wait_for_button_press, wait_for_button_release},
    chip, pwmutil,
    sonic::{self, Channel, WaveGen, Waveform},
    uart,
};
use panic_halt as _;
use rp_pico::hal::{self, gpio, pac::interrupt, timer::Timer, Clock};

#[entry]
fn main() -> ! {
    // Standard chip setup
    let mut chip = chip::Chip::new(rp_pico::XOSC_CRYSTAL_FREQ);
    let mut uart0 = uart::init_uart0(
        chip.UART0,
        chip.pins.gpio0,
        chip.pins.gpio1,
        &mut chip.RESETS,
        chip.clocks.peripheral_clock.freq(),
    );

    let mut timer = Timer::new(chip.TIMER, &mut chip.RESETS, &chip.clocks);

    writeln!(uart0, "\r").unwrap();
    writeln!(uart0, "Hello from pico-square-comp-max.rs!\r").unwrap();

    // Init PWM slices
    let pwm_slices = hal::pwm::Slices::new(chip.PWM, &mut chip.RESETS);

    // PWM2 is the H-bridge side 1 driver PWM slice
    let driver1_pwm = pwm_slices.pwm2;
    let driver1_channel_a_pin = chip.pins.gpio4.into_pull_type();
    let driver1_channel_b_pin = chip.pins.gpio5.into_pull_type();

    // PWM6 is the H-bridge side 2 driver PWM slice
    let driver2_pwm = pwm_slices.pwm6;
    let driver2_channel_a_pin = chip.pins.gpio12.into_pull_type();
    let driver2_channel_b_pin = chip.pins.gpio13.into_pull_type();

    // PWM7 is the pacing PWM slice
    let pacing_pwm = pwm_slices.pwm7;

    // Connect to the pacing PWM slice channel A
    let _pacing_a_pin = chip
        .pins
        .gpio14
        .into_function::<gpio::FunctionPwm>()
        .into_pull_type::<gpio::PullNone>();

    // Configure this output to be used as an output to trace interrupt handler
    // execution.
    let handler_trace_pin = chip
        .pins
        .gpio15
        .into_function::<gpio::FunctionSioOutput>()
        .into_pull_type::<gpio::PullNone>();

    let sonic_client = sonic::init_t4driver(
        pacing_pwm,
        driver1_pwm,
        driver1_channel_a_pin,
        driver1_channel_b_pin,
        driver2_pwm,
        driver2_channel_a_pin,
        driver2_channel_b_pin,
        Some(handler_trace_pin),
    );

    // LED to give feedback when button is pressed
    let mut led = chip.pins.gpio25.into_push_pull_output();
    // Connect pin 29 (GPIO22) to start/stop button
    let mut button = chip.pins.gpio22.into_pull_up_input();

    let waveform = Waveform::HyperSquare;
    let freq_x16: u16 = 0x1001; // can be any odd number in the accepted range
    let channel = Channel::ChannelA;
    sonic_client.set_waveform(channel, waveform);
    sonic_client.set_freq(channel, freq_x16);
    sonic_client.set_volume(channel, WaveGen::MAX_AMPLITUDE);

    writeln!(
        uart0,
        "Press and hold the button for 12.5kHz square wave at max volume\r"
    )
    .unwrap();

    loop {
        wait_for_button_press(&mut button);

        sonic_client.set_sound_output(true);
        led.set_high().unwrap();
        write!(uart0, "Generating hyper square wave wave...").unwrap();

        wait_for_button_release(&mut button, &mut timer);

        sonic_client.set_sound_output(false);
        led.set_low().unwrap();
        writeln!(uart0, "done.\r").unwrap();
    }
}

#[allow(non_snake_case)]
#[interrupt]
fn PWM_IRQ_WRAP() {
    pwmutil::pwm_irq_wrap_handler();
}
