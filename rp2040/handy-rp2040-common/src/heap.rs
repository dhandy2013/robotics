//! Heap initialization and management module
//!
//! The RP2040 has 264K SRAM total. 8K is used for instruction cache.
//!
//! This module allocates 64K for heap memory, leaving 192K for
//! statically-allocated RAM and for stack. I may change these proportions
//! later.
use core::{cell::Cell, mem::MaybeUninit};
use critical_section::Mutex;
use embedded_alloc::Heap;

#[global_allocator]
static HEAP: Heap = Heap::empty();

// Update the doc comment at the top of this module when you change this number
const HEAP_SIZE: usize = 0x1_0000;
static mut HEAP_MEM: [MaybeUninit<u8>; HEAP_SIZE] =
    [MaybeUninit::uninit(); HEAP_SIZE];

static INIT_FLAG: Mutex<Cell<bool>> = Mutex::new(Cell::new(false));

/// Initialize the heap BEFORE you use Box, Vec, etc.
/// Return true iff this is the first time init was called.
pub fn init() -> bool {
    if critical_section::with(|cs| {
        let init_flag = INIT_FLAG.borrow(cs);
        if init_flag.get() {
            true
        } else {
            init_flag.set(true);
            false
        }
    }) {
        // Already initialized. Don't call HEAP.init() twice or it will panic.
        return false;
    }
    unsafe { HEAP.init(HEAP_MEM.as_ptr() as usize, HEAP_SIZE) }
    true
}

/// Return the address of the start of heap space (for diagnostic purposes only)
pub fn addr() -> usize {
    unsafe { HEAP_MEM.as_ptr() as usize }
}

/// Return the size of the entire heap space in bytes (including overhead).
pub fn size() -> usize {
    HEAP_SIZE
}

/// Returns an estimate of the amount of bytes in use.
pub fn used() -> usize {
    HEAP.used()
}

/// Returns an estimate of the amount of bytes available.
pub fn free() -> usize {
    HEAP.free()
}
