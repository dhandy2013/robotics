//! sonic audio drivers
use super::constants::{
    DEAD_ZONE_CYCLES, DRIVER_DIV_FRAC, DRIVER_DIV_INT, DRIVER_TOP,
};
use super::samplegen::SampleGen;
use crate::pwmutil::DynamicPwmSlice;
use alloc::boxed::Box;

pub(crate) struct T1AudioDriver {
    driver_pwm: DynamicPwmSlice,
    generator_a: Option<Box<dyn SampleGen>>,
    generator_b: Option<Box<dyn SampleGen>>,
    pulse_width_a: u16,
    pulse_width_b: u16,
    is_running_a: bool,
    is_running_b: bool,
}

impl T1AudioDriver {
    pub(crate) fn new(mut driver_pwm: DynamicPwmSlice) -> Self {
        // Configure driver PWM frequency
        // Leave driver PWM duty cycle set at default 0% for both channels
        // This PWM slice will be enabled later by the `start()` function.
        driver_pwm.default_config();
        driver_pwm.set_top(DRIVER_TOP);
        driver_pwm.set_div_int(DRIVER_DIV_INT);
        driver_pwm.set_div_frac(DRIVER_DIV_FRAC);

        let generator_a = None;
        let generator_b = None;
        let pulse_width_a = Self::off_pulse_width();
        let pulse_width_b = Self::off_pulse_width();

        Self {
            driver_pwm,
            generator_a,
            generator_b,
            pulse_width_a,
            pulse_width_b,
            is_running_a: false,
            is_running_b: false,
        }
    }

    /// Return bitmap corresponding to the driver PWM slice
    pub(crate) fn pwm_bitmask(&self) -> u32 {
        self.driver_pwm.bitmask()
    }

    pub(crate) fn start_a(&mut self) {
        self.is_running_a = true;
    }

    pub(crate) fn stop_a(&mut self) {
        self.is_running_a = false;
    }

    pub(crate) fn start_b(&mut self) {
        self.is_running_b = true;
    }

    pub(crate) fn stop_b(&mut self) {
        self.is_running_b = false;
    }

    pub(crate) fn swap_sample_gen_a(
        &mut self,
        maybe_sample_gen: &mut Option<Box<dyn SampleGen>>,
    ) {
        core::mem::swap(&mut self.generator_a, maybe_sample_gen);
    }

    pub(crate) fn swap_sample_gen_b(
        &mut self,
        maybe_sample_gen: &mut Option<Box<dyn SampleGen>>,
    ) {
        core::mem::swap(&mut self.generator_b, maybe_sample_gen);
    }

    /// Set the current driver PWM duty cycles based on last sample calculation.
    /// Time-critical, do this early on in pump_sound().
    pub(crate) fn set_duty_cycles(&mut self) {
        // Update the pulse width values of the two PWM channels.
        // Do this first thing so we don't have jitter in the output.
        self.driver_pwm
            .write_channel_a_b(self.pulse_width_a, self.pulse_width_b);
    }

    /// Calculate the driver PWM duty cycles for the next audio sample.
    pub(crate) fn calculate_next_duty_cycles(&mut self) {
        self.pulse_width_a = {
            if self.is_running_a {
                match self.generator_a.as_mut() {
                    Some(sample_gen_a) => {
                        Self::sample_to_pulse_width(sample_gen_a.next_sample())
                    }
                    None => Self::off_pulse_width(),
                }
            } else {
                Self::off_pulse_width()
            }
        };
        self.pulse_width_b = {
            if self.is_running_b {
                match self.generator_b.as_mut() {
                    Some(sample_gen_b) => {
                        Self::sample_to_pulse_width(sample_gen_b.next_sample())
                    }
                    None => Self::off_pulse_width(),
                }
            } else {
                Self::off_pulse_width()
            }
        };
    }

    /// Convert a signed 16-bit sample value in range -32767..=32767
    /// to an unsigned 16-bit pulse width in range 0..=DRIVER_TOP
    #[inline]
    fn sample_to_pulse_width(sample: i16) -> u16 {
        let half_range = (DRIVER_TOP / 2) as i32;
        (half_range + ((half_range * sample as i32) >> 15)) as u16
    }

    /// Return the driver-specific pulse width to use for completely turning
    /// the speaker off (as opposed to running 50% of maximum current, which
    /// is what we do for a sample value of zero.)
    #[inline]
    fn off_pulse_width() -> u16 {
        0
    }
}

pub(crate) struct T4AudioDriver {
    driver1_pwm: DynamicPwmSlice,
    driver2_pwm: DynamicPwmSlice,
    generator: Option<Box<dyn SampleGen>>,
    pulse_width_1a: u16,
    pulse_width_1b: u16,
    pulse_width_2a: u16,
    pulse_width_2b: u16,
    is_running: bool,
}

impl T4AudioDriver {
    pub(crate) fn new(
        mut driver1_pwm: DynamicPwmSlice,
        mut driver2_pwm: DynamicPwmSlice,
    ) -> Self {
        // Why do I invert the channel B PWM outputs? It's complicated. These
        // control the NPN transistors. But the way phase correct PWM works,
        // I need to invert the output and then "invert" (subtract from 100%)
        // the duty cycle in order to have the transistor off during the dead
        // zone at the beginning and end of the PWM cycle.

        // Configure driver 1 PWM frequency
        // Leave driver PWM duty cycle set at default 0% for both channels
        // This PWM slice will be enabled later by the `start()` function.
        driver1_pwm.default_config();
        driver1_pwm.set_top(DRIVER_TOP);
        driver1_pwm.set_div_int(DRIVER_DIV_INT);
        driver1_pwm.set_div_frac(DRIVER_DIV_FRAC);
        driver1_pwm.write_ph_correct(true); // use phase correct PWM
        driver1_pwm.write_inv_b(true); // invert channel B (that controls NPN 1)

        // Configure driver 2 PWM frequency
        // Leave driver PWM duty cycle set at default 0% for both channels
        // This PWM slice will be enabled later by the `start()` function.
        driver2_pwm.default_config();
        driver2_pwm.set_top(DRIVER_TOP);
        driver2_pwm.set_div_int(DRIVER_DIV_INT);
        driver2_pwm.set_div_frac(DRIVER_DIV_FRAC);
        driver2_pwm.write_ph_correct(true); // use phase correct PWM
        driver2_pwm.write_inv_b(true); // invert channel B (that controls NPN 2)

        let generator = None;
        let (pulse_width_1a, pulse_width_1b, pulse_width_2a, pulse_width_2b) =
            Self::off_pulse_widths();

        Self {
            driver1_pwm,
            driver2_pwm,
            generator,
            pulse_width_1a,
            pulse_width_1b,
            pulse_width_2a,
            pulse_width_2b,
            is_running: false,
        }
    }

    /// Return bitmap corresponding to the driver PWM slices
    pub(crate) fn pwm_bitmask(&self) -> u32 {
        self.driver1_pwm.bitmask() | self.driver2_pwm.bitmask()
    }

    pub(crate) fn start(&mut self) {
        self.is_running = true;
    }

    pub(crate) fn stop(&mut self) {
        self.is_running = false;
    }

    pub(crate) fn swap_sample_gen(
        &mut self,
        maybe_sample_gen: &mut Option<Box<dyn SampleGen>>,
    ) {
        core::mem::swap(&mut self.generator, maybe_sample_gen);
    }

    /// Set the current driver PWM duty cycles based on last sample calculation.
    /// Time-critical, do this early on in pump_sound().
    pub(crate) fn set_duty_cycles(&mut self) {
        // Update the pulse width values of the two PWM channels.
        // Do this first thing so we don't have jitter in the output.
        self.driver1_pwm
            .write_channel_a_b(self.pulse_width_1a, self.pulse_width_1b);
        self.driver2_pwm
            .write_channel_a_b(self.pulse_width_2a, self.pulse_width_2b);
    }

    /// Calculate the driver PWM duty cycles for the next audio sample.
    pub(crate) fn calculate_next_duty_cycles(&mut self) {
        (
            self.pulse_width_1a,
            self.pulse_width_1b,
            self.pulse_width_2a,
            self.pulse_width_2b,
        ) = {
            if self.is_running {
                match self.generator.as_mut() {
                    Some(sample_gen) => {
                        Self::sample_to_pulse_widths(sample_gen.next_sample())
                    }
                    None => Self::off_pulse_widths(),
                }
            } else {
                Self::off_pulse_widths()
            }
        };
    }

    const FULL_DUTY_CYCLE: u16 = DRIVER_TOP + 1;

    /// Convert a sample value in range -32767..=32767 into PWM pulse widths.
    ///
    /// The returned pulse widths, in order are:
    ///     (pulse_width_1a, pulse_width_1b, pulse_width_2a, pulse_width_2b)
    /// where:
    ///     pulse_width_1a controls the H-bridge side 1 PNP transistor
    ///     pulse_width_1b controls the H-bridge side 1 NPN transistor
    ///     pulse_width_2a controls the H-bridge side 2 PNP transistor
    ///     pulse_width_2b controls the H-bridge side 2 NPN transistor
    ///
    /// If the pulse width for a PWM channel is FULL_DUTY_CYCLE, that means the
    /// corresponding transitor is turned OFF for the full cycle. If the pulse
    /// width is 0 that means the corresponding transistor is turned ON for the
    /// full cycle. Otherwise, it is modulated during the cycle.
    #[inline]
    fn sample_to_pulse_widths(sample: i16) -> (u16, u16, u16, u16) {
        let driver_scale: u32 =
            Self::FULL_DUTY_CYCLE as u32 - DEAD_ZONE_CYCLES as u32;
        if sample == 0 {
            (
                Self::FULL_DUTY_CYCLE,
                Self::FULL_DUTY_CYCLE,
                Self::FULL_DUTY_CYCLE,
                Self::FULL_DUTY_CYCLE,
            )
        } else if sample > 0 {
            let sample = sample as u32;
            let pulse_width = ((driver_scale * sample) >> 15) as u16;
            // "Invert" the duty cycle to compensate for the fact that the PWM
            // output is inverted. It's complicated, but required because of how
            // phase-correct PWM works.
            let pulse_width = Self::FULL_DUTY_CYCLE - pulse_width;
            (Self::FULL_DUTY_CYCLE, pulse_width, 0, Self::FULL_DUTY_CYCLE)
        } else {
            let sample = (-(sample as i32)) as u32;
            let pulse_width = ((driver_scale * sample) >> 15) as u16;
            // "Invert" the duty cycle to compensate for the fact that the PWM
            // output is inverted. It's complicated, but required because of how
            // phase-correct PWM works.
            let pulse_width = Self::FULL_DUTY_CYCLE - pulse_width;
            (0, Self::FULL_DUTY_CYCLE, Self::FULL_DUTY_CYCLE, pulse_width)
        }
    }

    /// Return the pulse widths for the 4 driver PWM channels that will result
    /// in all 4 driver transistors being turned off.
    #[inline]
    fn off_pulse_widths() -> (u16, u16, u16, u16) {
        (
            Self::FULL_DUTY_CYCLE,
            Self::FULL_DUTY_CYCLE,
            Self::FULL_DUTY_CYCLE,
            Self::FULL_DUTY_CYCLE,
        )
    }
}
