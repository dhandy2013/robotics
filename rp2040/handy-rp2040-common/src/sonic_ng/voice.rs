//! Voice interface to control sound going to a single speaker
use super::{
    driver::{T1AudioDriver, T4AudioDriver},
    samplegen::SampleGen,
    SONIC_COUNTER,
};
use alloc::boxed::Box;
use core::{cell::RefCell, sync::atomic::Ordering};
use critical_section::{self, Mutex};
use handy_embedded_util::sync::{self, BusyBox};

pub trait SonicVoice {
    /// Try to replace the current sample generator with another one or None.
    /// Ok result contains the previous sample generator,
    /// Err result contains the original `sample_gen` parameter.
    /// You only get an error if the underlying sonic driver is busy.
    fn try_replace_sample_gen(
        &self,
        maybe_sample_gen: Option<Box<dyn SampleGen>>,
    ) -> Result<Option<Box<dyn SampleGen>>, Option<Box<dyn SampleGen>>>;

    /// Start/resume the sonic driver and send output to the speaker.
    /// Return an error iff the sonic driver is busy.
    fn try_start(&self) -> Result<(), ()>;

    /// Stop/pause the sonic driver and turn the speaker off.
    /// Return an error iff the sonic driver is busy.
    fn try_stop(&self) -> Result<(), ()>;

    /// Return the number of audio sample ticks since the sonic module was
    /// started.
    fn sample_count(&self) -> usize {
        SONIC_COUNTER.load(Ordering::SeqCst)
    }

    /// Replace the current sample generator with another one or None.
    /// If the sonic driver is busy (interrupt handler running) then spin in a
    /// tight loop until it is available. Return the previous sample generator
    /// or None.
    fn replace_sample_gen(
        &self,
        mut maybe_sample_gen: Option<Box<dyn SampleGen>>,
    ) -> Option<Box<dyn SampleGen>> {
        loop {
            maybe_sample_gen =
                match self.try_replace_sample_gen(maybe_sample_gen) {
                    Ok(prev_maybe_sample_gen) => return prev_maybe_sample_gen,
                    Err(maybe_sample_gen) => maybe_sample_gen,
                };
        }
    }

    /// Start/resume the sonic driver and send output to the speaker.
    /// If the sonic driver is busy then spin till it is available.
    fn start(&self) {
        loop {
            match self.try_start() {
                Ok(_) => return,
                Err(_) => {}
            }
        }
    }

    /// Stop/pause the sonic driver and turn the speaker off.
    /// If the sonic driver is busy then spin till it is available.
    fn stop(&self) {
        loop {
            match self.try_stop() {
                Ok(_) => return,
                Err(_) => {}
            }
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub(crate) enum Channel {
    A,
    B,
}

pub(crate) struct T1SonicVoice {
    driver: &'static Mutex<RefCell<BusyBox<Box<T1AudioDriver>>>>,
    channel: Channel,
}

impl T1SonicVoice {
    pub(crate) fn new(
        driver: &'static Mutex<RefCell<BusyBox<Box<T1AudioDriver>>>>,
        channel: Channel,
    ) -> Self {
        Self { driver, channel }
    }

    /// Run closure `f` on a mutable reference to the driver.
    /// If the driver is busy (checked out by another thread) return Err(()).
    /// If the driver is available and non-None return Ok(R) (closure result).
    /// PANIC if the driver is available but None.
    fn try_with_driver<F, R>(&self, f: F) -> Result<R, ()>
    where
        F: FnOnce(&mut T1AudioDriver) -> R,
    {
        critical_section::with(|cs| {
            let adapt_boxed_param =
                |boxed_driver: &mut Box<T1AudioDriver>| f(&mut *boxed_driver);
            match sync::with_bb_mut(cs, self.driver, adapt_boxed_param) {
                Ok(result) => Ok(result),
                Err(sync::Error::AlreadyBorrowed) => Err(()),
                Err(sync::Error::NotInitialized) => panic!(),
            }
        })
    }
}

impl SonicVoice for T1SonicVoice {
    fn try_replace_sample_gen(
        &self,
        mut maybe_sample_gen: Option<Box<dyn SampleGen>>,
    ) -> Result<Option<Box<dyn SampleGen>>, Option<Box<dyn SampleGen>>> {
        let result = self.try_with_driver(|driver| match self.channel {
            Channel::A => driver.swap_sample_gen_a(&mut maybe_sample_gen),
            Channel::B => driver.swap_sample_gen_b(&mut maybe_sample_gen),
        });
        match result {
            Ok(_) => Ok(maybe_sample_gen),
            Err(_) => Err(maybe_sample_gen),
        }
    }

    fn try_start(&self) -> Result<(), ()> {
        self.try_with_driver(|driver| match self.channel {
            Channel::A => driver.start_a(),
            Channel::B => driver.start_b(),
        })
    }

    fn try_stop(&self) -> Result<(), ()> {
        self.try_with_driver(|driver| match self.channel {
            Channel::A => driver.stop_a(),
            Channel::B => driver.stop_b(),
        })
    }
}

pub(crate) struct T4SonicVoice {
    driver: &'static Mutex<RefCell<BusyBox<Box<T4AudioDriver>>>>,
}

impl T4SonicVoice {
    pub(crate) fn new(
        driver: &'static Mutex<RefCell<BusyBox<Box<T4AudioDriver>>>>,
    ) -> Self {
        Self { driver }
    }

    /// Run closure `f` on a mutable reference to the driver.
    /// If the driver is busy (checked out by another thread) return Err(()).
    /// If the driver is available and non-None return Ok(R) (closure result).
    /// PANIC if the driver is available but None.
    fn try_with_driver<F, R>(&self, f: F) -> Result<R, ()>
    where
        F: FnOnce(&mut T4AudioDriver) -> R,
    {
        critical_section::with(|cs| {
            let adapt_boxed_param =
                |boxed_driver: &mut Box<T4AudioDriver>| f(&mut *boxed_driver);
            match sync::with_bb_mut(cs, self.driver, adapt_boxed_param) {
                Ok(result) => Ok(result),
                Err(sync::Error::AlreadyBorrowed) => Err(()),
                Err(sync::Error::NotInitialized) => panic!(),
            }
        })
    }
}

impl SonicVoice for T4SonicVoice {
    fn try_replace_sample_gen(
        &self,
        mut maybe_sample_gen: Option<Box<dyn SampleGen>>,
    ) -> Result<Option<Box<dyn SampleGen>>, Option<Box<dyn SampleGen>>> {
        let result = self.try_with_driver(|driver| {
            driver.swap_sample_gen(&mut maybe_sample_gen);
        });
        match result {
            Ok(_) => Ok(maybe_sample_gen),
            Err(_) => Err(maybe_sample_gen),
        }
    }

    fn try_start(&self) -> Result<(), ()> {
        self.try_with_driver(|driver| driver.start())
    }

    fn try_stop(&self) -> Result<(), ()> {
        self.try_with_driver(|driver| driver.stop())
    }
}
