//! Constants used by the sonic sound drivers
//! Calculated from basic values and parameters.
use static_assertions::const_assert;

/// Assumed system clock frequency
/// Note, this assumes clock speed has been set up as per default.
pub const FSYS: u32 = 125_000_000;

/// Target audio sample rate
pub const TICKS_PER_SECOND: u16 = 25_000;

/// PWM top value needed to get desired pacing PWM frequency
///
/// Formula for PWM frequency when not scaling and when not doing
/// phase-correct PWM:
///
///     FPWM = FSYS / (TOP + 1)
///
/// In the expression below I subtract 0.5 instead of 1.0 in order to get proper
/// rounding.
pub const PACING_TOP: u16 =
    ((FSYS as f64) / (TICKS_PER_SECOND as f64) - 0.5) as u16;

// Check that pacing PWM frequency is within 1 Hz of target
pub const ACTUAL_PACING_PWM_FREQ: f64 =
    (FSYS as f64) / (PACING_TOP as f64 + 1.0);
const _PACING_PWM_FREQ_ERR: f64 =
    ACTUAL_PACING_PWM_FREQ - TICKS_PER_SECOND as f64;
const_assert!(_PACING_PWM_FREQ_ERR > -1.0 && _PACING_PWM_FREQ_ERR < 1.0);

/// Target PWM frequency for driving the speakers as a multiple of sample rate
pub const TARGET_DRIVER_PWM_FREQ: u32 = TICKS_PER_SECOND as u32 * 8;

/// PWM parameters needed to get driver PWM freq which is 8x pacing freq.
///
///     FPWM = FSYS / ((TOP + 1) * (DIV_INT + (DIV_FRAC / 16)))
///
pub const DRIVER_TOP: u16 = 624;
pub const DRIVER_DIV_INT: u8 = 1;
pub const DRIVER_DIV_FRAC: u8 = 0;

// Check that driver PWM frequency is close to the target
// Note: If const_assert!() fails, the compiler error is unclear. It may say
// something strange about "attempt to compute `0_usize - 1_usize`."
pub const DRIVER_PWM_FREQ: f64 = (FSYS as f64)
    / ((DRIVER_TOP as f64 + 1.0)
        * (DRIVER_DIV_INT as f64 + (DRIVER_DIV_FRAC as f64 / 16.0)));
const _DRIVER_PWM_FREQ_ERR: f64 =
    DRIVER_PWM_FREQ - TARGET_DRIVER_PWM_FREQ as f64;
const _DRIVER_PWM_FREQ_TOL: f64 = 41.5; // error tolerance in Hz
const_assert!(
    _DRIVER_PWM_FREQ_ERR > -_DRIVER_PWM_FREQ_TOL
        && _DRIVER_PWM_FREQ_ERR < _DRIVER_PWM_FREQ_TOL
);

/// Amount of time that it takes a 2N4401/2N4403 BJT transistor to turn off. For
/// the complementary driver we don't want to turn on one transistor until the
/// other is fully off.
pub const DEAD_ZONE_TIME: f64 = 225.0e-9; // transistor turnoff time in seconds
/// Number of processor clock cycles it takes a BJT transitor to turn off.
pub const DEAD_ZONE_CYCLES: u16 = (DEAD_ZONE_TIME * FSYS as f64 + 0.5) as u16;
