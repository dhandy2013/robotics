//! Interface for controlling wave generators from an application
use super::WaveParams;
use core::cell::Cell;
use critical_section::{CriticalSection, Mutex};

/// For controlling the sonic parameters.
/// One instance of this struct holds the changes to the sonic parameters
/// to be picked up on the next execution interrupt handler.
#[derive(Debug)]
struct SonicControls {
    id: u8,
    refcount: Mutex<Cell<usize>>,
    wave_params: Mutex<Cell<WaveParams>>,
}

impl SonicControls {
    const fn new(id: u8) -> Self {
        Self {
            id,
            refcount: Mutex::new(Cell::new(0)),
            wave_params: Mutex::new(Cell::new(WaveParams::new())),
        }
    }

    fn wave_params(&self, cs: CriticalSection) -> WaveParams {
        self.wave_params.borrow(cs).get()
    }

    fn set_wave_params(&self, cs: CriticalSection, wave_params: &WaveParams) {
        self.wave_params.borrow(cs).set(*wave_params)
    }

    /// Increment the reference count for this SonicControls object.
    fn incr_refcount(&self, cs: CriticalSection) {
        let refcount = self.refcount.borrow(cs).get();
        self.refcount.borrow(cs).set(refcount + 1);
    }

    /// Decrement the refcount for this SonicControls object.
    /// Return true iff the refcount has gone to zero and the object should be
    /// freed.
    fn decr_refcount(&self, cs: CriticalSection) -> bool {
        let mut refcount = self.refcount.borrow(cs).get();
        refcount = refcount.saturating_sub(1);
        self.refcount.borrow(cs).set(refcount);
        refcount == 0
    }
}

/// Bitflags indicating which controls records have been allocated
static CONTROL_SCOREBOARD: Mutex<Cell<u16>> = Mutex::new(Cell::new(0));

/// Dynamically allocated sonic controls records
static SONIC_CONTROLS: [SonicControls; 32] = [
    SonicControls::new(0),
    SonicControls::new(1),
    SonicControls::new(2),
    SonicControls::new(3),
    SonicControls::new(4),
    SonicControls::new(5),
    SonicControls::new(6),
    SonicControls::new(7),
    SonicControls::new(8),
    SonicControls::new(9),
    SonicControls::new(10),
    SonicControls::new(11),
    SonicControls::new(12),
    SonicControls::new(13),
    SonicControls::new(14),
    SonicControls::new(15),
    SonicControls::new(16),
    SonicControls::new(17),
    SonicControls::new(18),
    SonicControls::new(19),
    SonicControls::new(20),
    SonicControls::new(21),
    SonicControls::new(22),
    SonicControls::new(23),
    SonicControls::new(24),
    SonicControls::new(25),
    SonicControls::new(26),
    SonicControls::new(27),
    SonicControls::new(28),
    SonicControls::new(29),
    SonicControls::new(30),
    SonicControls::new(31),
];

fn alloc_controls(cs: CriticalSection) -> Option<&'static SonicControls> {
    let mut scoreboard = CONTROL_SCOREBOARD.borrow(cs).get();
    let first_free_index = scoreboard.trailing_ones() as usize;
    if first_free_index >= SONIC_CONTROLS.len() {
        return None;
    }
    scoreboard |= 1 << first_free_index;
    CONTROL_SCOREBOARD.borrow(cs).set(scoreboard);
    let controls: &'static SonicControls = &SONIC_CONTROLS[first_free_index];
    let id = controls.id;
    assert_eq!(id as usize, first_free_index);
    controls.refcount.borrow(cs).set(1);
    controls.wave_params.borrow(cs).set(WaveParams::new());
    Some(controls)
}

fn free_controls(cs: CriticalSection, controls: &'static SonicControls) {
    let id = controls.id;
    assert!(id as usize <= SONIC_CONTROLS.len());
    let mut scoreboard = CONTROL_SCOREBOARD.borrow(cs).get();
    scoreboard &= !(1 << id);
    CONTROL_SCOREBOARD.borrow(cs).set(scoreboard);
    controls.refcount.borrow(cs).set(0);
}

#[derive(Debug)]
pub struct ControlHandle {
    controls: &'static SonicControls,
}

impl ControlHandle {
    pub(crate) fn alloc(cs: CriticalSection) -> Option<Self> {
        match alloc_controls(cs) {
            Some(controls) => Some(ControlHandle { controls }),
            None => None,
        }
    }

    pub(crate) fn wave_params_with_cs(
        &self,
        cs: CriticalSection,
    ) -> WaveParams {
        self.controls.wave_params(cs)
    }

    pub(crate) fn set_wave_params_with_cs(
        &self,
        cs: CriticalSection,
        wave_params: &WaveParams,
    ) {
        self.controls.set_wave_params(cs, wave_params);
    }

    /// Get the wave parameters for this control object
    pub fn wave_params(&self) -> WaveParams {
        critical_section::with(|cs| self.wave_params_with_cs(cs))
    }

    /// Set the wave parameters for this control object
    pub fn set_wave_params(&self, wave_params: &WaveParams) {
        critical_section::with(|cs| {
            self.set_wave_params_with_cs(cs, wave_params)
        });
    }
}

impl Clone for ControlHandle {
    fn clone(&self) -> Self {
        critical_section::with(|cs| {
            self.controls.incr_refcount(cs);
            Self {
                controls: self.controls,
            }
        })
    }
}

impl Drop for ControlHandle {
    fn drop(&mut self) {
        critical_section::with(|cs| {
            if self.controls.decr_refcount(cs) {
                free_controls(cs, self.controls);
            }
        });
    }
}
