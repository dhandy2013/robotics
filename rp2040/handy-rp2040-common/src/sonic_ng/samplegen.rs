//! Generation of audio samples
use super::{ControlHandle, WaveGen};
use crate::random;
use critical_section::{self, CriticalSection};

/// Trait for objects which generate successive audio samples
pub trait SampleGen: Send {
    fn next_sample(&mut self) -> i16;
}

pub struct ControllableWaveGen {
    wave_gen: WaveGen,
    control_handle: ControlHandle,
}

impl ControllableWaveGen {
    /// Create a new controllable wave generator.
    /// PANICS if no more controls records are available to allocate.
    pub fn new() -> Self {
        critical_section::with(|cs| Self::new_with_cs(cs))
    }

    pub(crate) fn new_with_cs(cs: CriticalSection) -> Self {
        let control_handle = ControlHandle::alloc(cs).unwrap();
        Self {
            wave_gen: WaveGen::new(),
            control_handle,
        }
    }

    pub fn clone_control_handle(&self) -> ControlHandle {
        self.control_handle.clone()
    }
}

impl SampleGen for ControllableWaveGen {
    fn next_sample(&mut self) -> i16 {
        self.wave_gen
            .set_wave_params(&(self.control_handle.wave_params()));
        self.wave_gen.next_sample()
    }
}

/// Combines two SampleGen instances by adding their sample values together.
pub struct DualSampleGen<S1, S2> {
    sample_gen_1: S1,
    sample_gen_2: S2,
}

impl<S1: SampleGen, S2: SampleGen> DualSampleGen<S1, S2> {
    pub fn new(sample_gen_1: S1, sample_gen_2: S2) -> Self {
        Self {
            sample_gen_1,
            sample_gen_2,
        }
    }
}

impl<S1: SampleGen, S2: SampleGen> SampleGen for DualSampleGen<S1, S2> {
    fn next_sample(&mut self) -> i16 {
        let sample1 = self.sample_gen_1.next_sample();
        let sample2 = self.sample_gen_2.next_sample();
        sample1.saturating_add(sample2)
    }
}

/// Generate random samples.
///
/// For this to work there has to be a background task calling random::pump()
/// in a tight loop. This is a good use for the RTIC idle task.
///
/// You can construct two of these: One with parameter NoiseChannel::One and
/// the other with the parameter NoiseChannel::Two. If you attempt to make
/// multiple instances of the random sample generator with the same noise
/// channel they will likely generate the same samples at the same time.
pub struct RandomSampleGen {
    noise_channel: NoiseChannel,
    control_handle: ControlHandle,
}

/// Random number channel for use by RandomSampleGen
#[derive(Clone, Copy, Debug)]
pub enum NoiseChannel {
    One,
    Two,
}

impl RandomSampleGen {
    /// Create a new controllable wave generator on the given noise channel.
    /// PANICS if no more controls records are available to allocate.
    pub fn new(noise_channel: NoiseChannel) -> Self {
        critical_section::with(|cs| Self::new_with_cs(cs, noise_channel))
    }

    pub(crate) fn new_with_cs(
        cs: CriticalSection,
        noise_channel: NoiseChannel,
    ) -> Self {
        let control_handle = ControlHandle::alloc(cs).unwrap();
        Self {
            noise_channel,
            control_handle,
        }
    }

    pub fn clone_control_handle(&self) -> ControlHandle {
        self.control_handle.clone()
    }
}

impl SampleGen for RandomSampleGen {
    fn next_sample(&mut self) -> i16 {
        let random_val = random::get_rand32();
        let sample = match self.noise_channel {
            NoiseChannel::One => (random_val & 0x0000ffff) as i16,
            NoiseChannel::Two => (random_val >> 16) as i16,
        };
        // Return the sample value scaled by the current amplitude.
        let wave_params = self.control_handle.wave_params();
        wave_params.scale_sample(sample)
    }
}
