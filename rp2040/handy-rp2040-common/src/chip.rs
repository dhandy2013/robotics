//! Common initialization of RP2040 on-chip resources.
//!
//! This is to avoid copy-pasting lengthy boilerplate code in every RP2040
//! application. (There will still be some copy-paste but it will be much
//! smaller.)
//!
//! Example app setup code:
//! ```rust
//! use handy_r2040_common::{chip, uart}
//!
//! let mut chip = chip::Chip::new(rp_pico::XOSC_CRYSTAL_FREQ);
//! let mut uart0 = uart::init_uart0(
//!     chip.UART0,
//!     chip.pins.gpio0,
//!     chip.pins.gpio1,
//!     &mut chip.RESETS,
//!     chip.clocks.peripheral_clock.freq(),
//! );
//! writeln!(uart0, "Hello, world!\r").unwrap();
//! ```
//!
//! Calling `Chip::new` also has a side effect of calling `heap::init` in case
//! you need to construct a Box, etc.
use hal::sio::CoreId;
use rp2040_hal::{self as hal, pac};

#[allow(non_snake_case)]
pub struct Chip {
    // Wrapped peripheral components
    pub core: hal::pac::CorePeripherals,
    pub watchdog: hal::Watchdog,
    pub clocks: hal::clocks::ClocksManager,
    /// GPIO QSPI registers
    pub gpio_qspi: hal::sio::SioGpioQspi,
    /// 8-cycle hardware divide/modulo module
    pub hwdivider: hal::sio::HwDivider,
    /// Inter-core FIFO
    pub fifo: hal::sio::SioFifo,
    /// Interpolator 0
    pub interp0: hal::sio::Interp0,
    /// Interpolator 1
    pub interp1: hal::sio::Interp1,
    pub pins: hal::gpio::Pins,
    // Raw peripheral components left over
    pub ADC: pac::ADC,
    pub BUSCTRL: pac::BUSCTRL,
    // pub CLOCKS: pac::CLOCKS, wrapped in `clocks``
    pub DMA: pac::DMA,
    pub I2C0: pac::I2C0,
    pub I2C1: pac::I2C1,
    // pub IO_BANK0: pac::IO_BANK0, wrapped in `pins`
    pub IO_QSPI: pac::IO_QSPI,
    // pub PADS_BANK0: pac::PADS_BANK0, wrapped in `pins`
    pub PADS_QSPI: pac::PADS_QSPI,
    pub PIO0: pac::PIO0,
    pub PIO1: pac::PIO1,
    // pub PLL_SYS: pac::PLL_SYS, wrapped in `clocks`
    // pub PLL_USB: pac::PLL_USB, wrapped in `clocks`
    pub PPB: pac::PPB,
    pub PSM: pac::PSM,
    pub PWM: pac::PWM,
    pub RESETS: pac::RESETS,
    pub ROSC: pac::ROSC,
    pub RTC: pac::RTC,
    // pub SIO: pac::SIO, wrapped in the `Sio` objects
    pub SPI0: pac::SPI0,
    pub SPI1: pac::SPI1,
    pub SYSCFG: pac::SYSCFG,
    pub SYSINFO: pac::SYSINFO,
    pub TBMAN: pac::TBMAN,
    pub TIMER: pac::TIMER,
    pub UART0: pac::UART0,
    pub UART1: pac::UART1,
    pub USBCTRL_DPRAM: pac::USBCTRL_DPRAM,
    pub USBCTRL_REGS: pac::USBCTRL_REGS,
    pub VREG_AND_CHIP_RESET: pac::VREG_AND_CHIP_RESET,
    // pub WATCHDOG: pac::WATCHDOG, wrapped in the `watchdog` object
    pub XIP_CTRL: pac::XIP_CTRL,
    pub XIP_SSI: pac::XIP_SSI,
    // pub XOSC: pac::XOSC, wrapped in the `clocks` object
}

impl Chip {
    /// Initialize RP2040 clocks, pins, and other resources. Return a struct
    /// containing them. Also initializes the heap in case that is needed.
    ///
    /// xosc_crystal_freq: The external oscillator frequency.
    /// On the Raspberry Pi Pico get that number by referencing:
    /// `rp_pico::XOSC_CRYSTAL_FREQ`
    pub fn new(xosc_crystal_freq: u32) -> Self {
        // Grab our peripherals
        let peripherals = hal::pac::Peripherals::take().unwrap();
        let core = hal::pac::CorePeripherals::take().unwrap();

        Self::from_core_peripherals(xosc_crystal_freq, core, peripherals)
    }

    pub fn from_core_peripherals(
        xosc_crystal_freq: u32,
        core: cortex_m::peripheral::Peripherals,
        peripherals: pac::Peripherals,
    ) -> Self {
        // Initialize the heap. This function is idempotent so no harm in
        // calling it again.
        crate::heap::init();

        // Split the Peripherals object into its 36 components.
        // Some of these components will be consumed in this method,
        // the rest will be stored in this struct separately for later use.
        #[allow(non_snake_case)]
        let (
            ADC,
            BUSCTRL,
            CLOCKS,
            DMA,
            I2C0,
            I2C1,
            IO_BANK0,
            IO_QSPI,
            PADS_BANK0,
            PADS_QSPI,
            PIO0,
            PIO1,
            PLL_SYS,
            PLL_USB,
            PPB,
            PSM,
            PWM,
            mut RESETS,
            ROSC,
            RTC,
            SIO,
            SPI0,
            SPI1,
            SYSCFG,
            SYSINFO,
            TBMAN,
            TIMER,
            UART0,
            UART1,
            USBCTRL_DPRAM,
            USBCTRL_REGS,
            VREG_AND_CHIP_RESET,
            WATCHDOG,
            XIP_CTRL,
            XIP_SSI,
            XOSC,
        ) = (
            peripherals.ADC,
            peripherals.BUSCTRL,
            peripherals.CLOCKS,
            peripherals.DMA,
            peripherals.I2C0,
            peripherals.I2C1,
            peripherals.IO_BANK0,
            peripherals.IO_QSPI,
            peripherals.PADS_BANK0,
            peripherals.PADS_QSPI,
            peripherals.PIO0,
            peripherals.PIO1,
            peripherals.PLL_SYS,
            peripherals.PLL_USB,
            peripherals.PPB,
            peripherals.PSM,
            peripherals.PWM,
            peripherals.RESETS,
            peripherals.ROSC,
            peripherals.RTC,
            peripherals.SIO,
            peripherals.SPI0,
            peripherals.SPI1,
            peripherals.SYSCFG,
            peripherals.SYSINFO,
            peripherals.TBMAN,
            peripherals.TIMER,
            peripherals.UART0,
            peripherals.UART1,
            peripherals.USBCTRL_DPRAM,
            peripherals.USBCTRL_REGS,
            peripherals.VREG_AND_CHIP_RESET,
            peripherals.WATCHDOG,
            peripherals.XIP_CTRL,
            peripherals.XIP_SSI,
            peripherals.XOSC,
        );

        // Set up the watchdog driver - needed by the clock setup code
        let mut watchdog = hal::Watchdog::new(WATCHDOG);

        // Configure the clocks
        // The default is to generate a 125 MHz system clock
        let clocks = hal::clocks::init_clocks_and_plls(
            xosc_crystal_freq,
            XOSC,
            CLOCKS,
            PLL_SYS,
            PLL_USB,
            &mut RESETS,
            &mut watchdog,
        )
        .ok()
        .unwrap();

        // Single-cycle I/O block
        let sio = hal::Sio::new(SIO);

        // Split the sio object into its 6 parts, one of which we will consume
        // in this method to create the `pins` object and the rest we will store
        // for later use.
        let (gpio_bank0, gpio_qspi, hwdivider, fifo, interp0, interp1) = (
            sio.gpio_bank0,
            sio.gpio_qspi,
            sio.hwdivider,
            sio.fifo,
            sio.interp0,
            sio.interp1,
        );

        let pins =
            hal::gpio::Pins::new(IO_BANK0, PADS_BANK0, gpio_bank0, &mut RESETS);

        Self {
            core,
            watchdog,
            clocks,
            gpio_qspi,
            hwdivider,
            fifo,
            interp0,
            interp1,
            pins,
            ADC,
            BUSCTRL,
            DMA,
            I2C0,
            I2C1,
            IO_QSPI,
            PADS_QSPI,
            PIO0,
            PIO1,
            PPB,
            PSM,
            PWM,
            RESETS,
            ROSC,
            RTC,
            SPI0,
            SPI1,
            SYSCFG,
            SYSINFO,
            TBMAN,
            TIMER,
            UART0,
            UART1,
            USBCTRL_DPRAM,
            USBCTRL_REGS,
            VREG_AND_CHIP_RESET,
            XIP_CTRL,
            XIP_SSI,
        }
    }

    // The following methods are copied from hal::sio::Sio

    /// Reads the whole bank0 GPIO inputs at once.
    pub fn read_bank0() -> u32 {
        unsafe { (*pac::SIO::PTR).gpio_in().read().bits() }
    }

    /// Returns whether we are running on Core 0 (`0`) or Core 1 (`1`).
    pub fn core() -> CoreId {
        // Safety: it is always safe to read this read-only register
        match unsafe { (*pac::SIO::ptr()).cpuid().read().bits() as u8 } {
            0 => CoreId::Core0,
            1 => CoreId::Core1,
            _ => unreachable!("This MCU only has 2 cores."),
        }
    }
}
