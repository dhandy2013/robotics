//! RP2040 audio hardware setup and manipulation
//! - Use a PWM slice for timing audio samples (pacing PWM)
//! - The audio sample rate is 25.000KHz
//! - Use one or more other PWM slices driving speakers (driver PWMs)
//! - The rate at which the driver PWM duty cycle varies is the audio
//!   wave frequency.
//! - The range over which the duty cycle varies is the audio amplitude.
//!
//! To send sound to speakers, follow this intialization sequence
//! - `init_pacer(pacer_pwm, maybe_trace_pin) -> pacer_token`
//! - `init_driver_*(&mut pacer_pwm, driver_pwm, ...) -> voice`
//! - `start(pacer_token, enable_interrupt, set_standard_interrupt_handler)`
//!
//! If you call `start()` with `enable_interrupt = true` then you also
//! need to define your own PWM_IRQ_WRAP function in your application, or you
//! will not hear any sounds from the driver. See `pump_sound()` for details.
pub mod constants;
mod controls;
mod driver;
mod samplegen;
mod voice;

pub use self::{
    constants::TICKS_PER_SECOND, controls::ControlHandle, voice::SonicVoice,
};
use self::{
    driver::{T1AudioDriver, T4AudioDriver},
    samplegen::{ControllableWaveGen, DualSampleGen, RandomSampleGen},
    voice::{Channel, T1SonicVoice, T4SonicVoice},
};
use crate::{
    heap,
    pwmutil::{self, DynamicPwmSlice},
};
use alloc::boxed::Box;
use core::{
    cell::RefCell,
    sync::atomic::{AtomicUsize, Ordering},
};
use critical_section::Mutex;
use embedded_hal::digital::OutputPin;
use handy_embedded_util::sync::{self, BusyBox};
use rp2040_hal::{gpio, pwm};
use wavegen::wavegen16::{self, WaveGenerator};

pub use samplegen::NoiseChannel;
pub use wavegen::wavegen16::Waveform;
pub type WaveGen = WaveGenerator<TICKS_PER_SECOND>;
pub type WaveParams = wavegen16::WaveParams<TICKS_PER_SECOND>;

/// Use `MaybeOutputPin::<P>::None` as a parameter when you don't want to pass
/// an optional output pin to one of the init functions.
///
/// Example:
///     sonic::MaybeOutput::None
/// can be used in place of:
///     Option::<
///         gpio::Pin<gpio::bank0::Gpio14, gpio::FunctionSioOutput, gpio::PullNone>,
///     >::None,
pub type MaybeOutput =
    Option<gpio::Pin<gpio::DynPinId, gpio::FunctionSioOutput, gpio::PullNone>>;

struct SonicPacer {
    /// PWM slice controlling sample pacing
    pacing_pwm: DynamicPwmSlice,
    maybe_trace_pin: MaybeOutput,
}

impl SonicPacer {
    fn new(
        mut pacing_pwm: DynamicPwmSlice,
        maybe_trace_pin: MaybeOutput,
    ) -> Self {
        // Configure pacing PWM frequency.
        // Pacing PWM duty cycle is set to 50% for both channels for tracing.
        // This PWM slice will be enabled later by the `start()` function.
        use constants::PACING_TOP;
        pacing_pwm.default_config();
        pacing_pwm.set_top(PACING_TOP);
        pacing_pwm.write_channel_a_b(PACING_TOP / 2, PACING_TOP / 2);
        Self {
            pacing_pwm,
            maybe_trace_pin,
        }
    }
}

static SONIC_PACER: Mutex<RefCell<Option<SonicPacer>>> =
    Mutex::new(RefCell::new(None));

/// Number of times interrupt/loop handler has been called
static SONIC_COUNTER: AtomicUsize = AtomicUsize::new(0);

pub struct SonicPacerToken;

/// Initialize the PWM slice used to set the timing of audio samples. Only call
/// this once in a program. Use the returned token as a parameter to one of the
/// `init_driver_*` functions, and finally to `start()`.
///
/// Type parameters:
///
/// S: SliceId of PWM slice for timing audio samples.
///    Can be inferred from the pacing_pwm argument.
pub fn init_pacer<S, G>(
    pacing_pwm: pwm::Slice<S, pwm::FreeRunning>,
    maybe_trace_pin: Option<
        gpio::Pin<G, gpio::FunctionSioOutput, gpio::PullNone>,
    >,
) -> SonicPacerToken
where
    S: pwm::SliceId,
    G: gpio::PinId + Send + 'static,
{
    let pacing_pwm = DynamicPwmSlice::from(pacing_pwm);
    let maybe_trace_pin = maybe_trace_pin.map(|p| p.into_dyn_pin());

    critical_section::with(move |cs| {
        let mut maybe_pacer = SONIC_PACER.borrow_ref_mut(cs);
        maybe_pacer.replace(SonicPacer::new(pacing_pwm, maybe_trace_pin));
    });
    SonicPacerToken
}

static SONIC_DRIVER_T1: Mutex<RefCell<BusyBox<Box<T1AudioDriver>>>> =
    Mutex::new(RefCell::new(BusyBox::new()));

/// Initialize simple PWM audio driver that can control two speakers, one
/// transistor per speaker (hence the "t1" part of the function name).
///
/// Initializes this module so it can execute commands to send audio output.
///
/// Type parameters:
///
/// S: SliceId of PWM slice for driving speakers.
///    Can be inferred from the driver_pwm argument.
///
/// Returns a client object that can be used to control sound via this driver.
/// Only call this function once during program execution.
///
/// See the RP2040 datasheet section 4.5.2 for which GPIO signals are valid for
/// which PWM slices and channels.
///
/// Wiring instructions: connect channel A pin to the base of a NPN BJT driving
/// one speaker, and channel B pin to the base of another NPN BJT driving a
/// different speaker.
pub fn init_driver_t1<S>(
    _pacer_token: &mut SonicPacerToken,
    driver_pwm: pwm::Slice<S, pwm::FreeRunning>,
) -> (Box<dyn SonicVoice>, Box<dyn SonicVoice>)
where
    S: pwm::SliceId,
{
    let driver_pwm = DynamicPwmSlice::from(driver_pwm);
    heap::init(); // make sure Box::new() does not panic
    critical_section::with(move |cs| {
        sync::try_replace_bb(
            cs,
            &SONIC_DRIVER_T1,
            Some(Box::new(T1AudioDriver::new(driver_pwm))),
        )
        .ok()
        .unwrap();
    });

    (
        Box::new(T1SonicVoice::new(&SONIC_DRIVER_T1, Channel::A)),
        Box::new(T1SonicVoice::new(&SONIC_DRIVER_T1, Channel::B)),
    )
}

static SONIC_DRIVER_T4_VOICE1: Mutex<RefCell<BusyBox<Box<T4AudioDriver>>>> =
    Mutex::new(RefCell::new(BusyBox::new()));

/// Initialize PWM audio driver controlling 4 transistors to drive speaker 1.
/// ("t4" part of the function name comes from the 4 transistors.) The
/// transistors are connected in an H-bridge configuration.
///
/// Type parameters:
///
/// S1, S2
///     SliceIds of PWM slices for driving side 1 and side 2 of H-bridge.
///     Can be inferred from driver1_pwm and driver2_pwm, respectively.
///
/// Returns a client object that can be used to control sound via this driver.
/// Only call this function once during program execution.
///
/// See the RP2040 datasheet section 4.5.2 for which GPIO signals are valid for
/// which PWM slices and channels.
///
/// Wiring instructions: Connect 2 PNP BJTs and 2 NPN BJTs in an H-bridge.
/// Say X1 and Y1 are the gates of the PNP and NPN transistors, respectively,
/// on side 1 of the H-bridge, and X2 and Y2 are the gates of the PNP
/// and NPN transistors, respectively, on side 2 of the H-bridge:
///
/// driver1 channel A pin -> X1 goes to gate of side 1 PNP
/// driver1 channel B pin -> Y1 goes to gate of side 1 NPN
/// driver2 channel A pin -> X2 goes to gate of side 2 PNP
/// driver2 channel B pin -> Y2 goes to gate of side 2 NPN
pub fn init_driver_t4_voice1<S1, S2>(
    _pacer_token: &mut SonicPacerToken,
    driver1_pwm: pwm::Slice<S1, pwm::FreeRunning>,
    driver2_pwm: pwm::Slice<S2, pwm::FreeRunning>,
) -> Box<dyn SonicVoice>
where
    S1: pwm::SliceId,
    S2: pwm::SliceId,
{
    let driver1_pwm = DynamicPwmSlice::from(driver1_pwm);
    let driver2_pwm = DynamicPwmSlice::from(driver2_pwm);

    heap::init(); // make sure Box::new() does not panic
    critical_section::with(move |cs| {
        sync::try_replace_bb(
            cs,
            &SONIC_DRIVER_T4_VOICE1,
            Some(Box::new(T4AudioDriver::new(driver1_pwm, driver2_pwm))),
        )
        .ok()
        .unwrap();
    });

    Box::new(T4SonicVoice::new(&SONIC_DRIVER_T4_VOICE1))
}

static SONIC_DRIVER_T4_VOICE2: Mutex<RefCell<BusyBox<Box<T4AudioDriver>>>> =
    Mutex::new(RefCell::new(BusyBox::new()));

/// Initialize PWM audio driver controlling 4 transistors to drive speaker 2.
/// ("t4" part of the function name comes from the 4 transistors.) The
/// transistors are connected in an H-bridge configuration.
///
/// Type parameters:
///
/// S1, S2
///     SliceIds of PWM slices for driving side 1 and side 2 of H-bridge.
///     Can be inferred from driver1_pwm and driver2_pwm, respectively.
///
/// Returns a client object that can be used to control sound via this driver.
/// Only call this function once during program execution.
///
/// See the RP2040 datasheet section 4.5.2 for which GPIO signals are valid for
/// which PWM slices and channels.
///
/// Wiring instructions: Connect 2 PNP BJTs and 2 NPN BJTs in an H-bridge.
/// Say X1 and Y1 are the gates of the PNP and NPN transistors, respectively,
/// on side 1 of the H-bridge, and X2 and Y2 are the gates of the PNP
/// and NPN transistors, respectively, on side 2 of the H-bridge:
///
/// driver1 channel A pin -> X1 goes to gate of side 1 PNP
/// driver1 channel B pin -> Y1 goes to gate of side 1 NPN
/// driver2 channel A pin -> X2 goes to gate of side 2 PNP
/// driver2 channel B pin -> Y2 goes to gate of side 2 NPN
pub fn init_driver_t4_voice2<S1, S2>(
    _pacer_token: &mut SonicPacerToken,
    driver1_pwm: pwm::Slice<S1, pwm::FreeRunning>,
    driver2_pwm: pwm::Slice<S2, pwm::FreeRunning>,
) -> Box<dyn SonicVoice>
where
    S1: pwm::SliceId,
    S2: pwm::SliceId,
{
    let driver1_pwm = DynamicPwmSlice::from(driver1_pwm);
    let driver2_pwm = DynamicPwmSlice::from(driver2_pwm);

    heap::init(); // make sure Box::new() does not panic
    critical_section::with(move |cs| {
        sync::try_replace_bb(
            cs,
            &SONIC_DRIVER_T4_VOICE2,
            Some(Box::new(T4AudioDriver::new(driver1_pwm, driver2_pwm))),
        )
        .ok()
        .unwrap();
    });

    Box::new(T4SonicVoice::new(&SONIC_DRIVER_T4_VOICE2))
}

/// Start all the initialized PWM slices set up by previous calls to `init_*`.
///
/// If `enable_interrupt` is true, enable interrupts on pacer PWM overflow
/// and enable NVIC PWM interrupts in general.
///
/// If `set_standard_interrupt_handler` is true, use the `pwmutil` module to
/// set up an interrupt handler to call the `pump_sound` function on pacer PWM
/// overflow.
pub fn start(
    _pacer_token: SonicPacerToken,
    enable_interrupt: bool,
    set_standard_interrupt_handler: bool,
) {
    let mut bitmask: u32 = 0;
    let pacing_slice_id =
        sync::with_shared_mut(&SONIC_PACER, |pacer: &mut SonicPacer| {
            bitmask |= pacer.pacing_pwm.bitmask();
            if enable_interrupt {
                pacer.pacing_pwm.enable_interrupt();
            }
            pacer.pacing_pwm.slice_id()
        })
        .unwrap();

    // Check out all of the driver references
    let (mut driver_t1, mut driver_t4_voice1, mut driver_t4_voice2) =
        critical_section::with(|cs| {
            let driver_t1 =
                sync::try_checkout_bb(cs, &SONIC_DRIVER_T1).ok().unwrap();
            let driver_t4_voice1 =
                sync::try_checkout_bb(cs, &SONIC_DRIVER_T4_VOICE1)
                    .ok()
                    .unwrap();
            let driver_t4_voice2 =
                sync::try_checkout_bb(cs, &SONIC_DRIVER_T4_VOICE2)
                    .ok()
                    .unwrap();
            (driver_t1, driver_t4_voice1, driver_t4_voice2)
        });

    if let Some(driver) = driver_t1.as_mut() {
        bitmask |= driver.pwm_bitmask();
    }

    if let Some(driver) = driver_t4_voice1.as_mut() {
        bitmask |= driver.pwm_bitmask();
    }

    if let Some(driver) = driver_t4_voice2.as_mut() {
        bitmask |= driver.pwm_bitmask();
    }

    // Check back in all of the driver references
    critical_section::with(move |cs| {
        sync::try_checkin_bb(cs, &SONIC_DRIVER_T1, driver_t1)
            .ok()
            .unwrap();
        sync::try_checkin_bb(cs, &SONIC_DRIVER_T4_VOICE1, driver_t4_voice1)
            .ok()
            .unwrap();
        sync::try_checkin_bb(cs, &SONIC_DRIVER_T4_VOICE2, driver_t4_voice2)
            .ok()
            .unwrap();
    });

    pwmutil::enable_pwm_simultaneous_by_bitmask(bitmask);

    if enable_interrupt {
        // Initialize the heap so Box::new() does not panic.
        crate::heap::init();

        // Tell the NVIC to start calling the PWM interrupt handler function
        pwmutil::enable_pwm_interrupts();
    }

    if set_standard_interrupt_handler {
        pwmutil::set_interrupt_handler(
            pacing_slice_id,
            Some(Box::new(move || {
                pump_sound();
            })),
        );
    }
}

/// Call this every time the pacing PWM slice overflows (i.e. once per sample).
/// One way to do this is to call this from the PWM_IRQ_WRAP interrupt handler.
pub fn pump_sound() {
    let (
        mut maybe_trace_pin,
        mut driver_t1,
        mut driver_t4_voice1,
        mut driver_t4_voice2,
    ) = critical_section::with(|cs| {
        // Get reference to pacer, clear interrupt, and extract tracing ping
        let mut maybe_pacer = SONIC_PACER.borrow(cs).borrow_mut();
        let maybe_trace_pin = match maybe_pacer.as_mut() {
            Some(pacer) => {
                // Must clear the interrupt flag or the interrupt handler gets
                // called in infinite loop. (This is redundant if using pwmutil
                // interrupt handling framework.)
                pacer.pacing_pwm.clear_interrupt();
                pacer.maybe_trace_pin.take()
            }
            None => None,
        };

        // Check out all of the driver references
        let driver_t1 =
            sync::try_checkout_bb(cs, &SONIC_DRIVER_T1).ok().unwrap();
        let driver_t4_voice1 =
            sync::try_checkout_bb(cs, &SONIC_DRIVER_T4_VOICE1)
                .ok()
                .unwrap();
        let driver_t4_voice2 =
            sync::try_checkout_bb(cs, &SONIC_DRIVER_T4_VOICE2)
                .ok()
                .unwrap();
        (
            maybe_trace_pin,
            driver_t1,
            driver_t4_voice1,
            driver_t4_voice2,
        )
    });

    // Set trace signal high to indicate start of running sound pump
    if let Some(trace_pin) = maybe_trace_pin.as_mut() {
        trace_pin.set_high().unwrap();
    }

    // Set the current duty cycles for all driver PWM slices. Time critical:
    // We need to do this for all the slices before next driver PWM overflow.
    if let Some(driver) = driver_t1.as_mut() {
        driver.set_duty_cycles();
    }
    if let Some(driver) = driver_t4_voice1.as_mut() {
        driver.set_duty_cycles();
    }
    if let Some(driver) = driver_t4_voice2.as_mut() {
        driver.set_duty_cycles();
    }

    // Calculate next duty cycle values for all drivers
    if let Some(driver) = driver_t1.as_mut() {
        driver.calculate_next_duty_cycles();
    }
    if let Some(driver) = driver_t4_voice1.as_mut() {
        driver.calculate_next_duty_cycles();
    }
    if let Some(driver) = driver_t4_voice2.as_mut() {
        driver.calculate_next_duty_cycles();
    }

    // Set trace signal low to indicate end of running sound pump
    if let Some(trace_pin) = maybe_trace_pin.as_mut() {
        trace_pin.set_low().unwrap();
    }

    critical_section::with(move |cs| {
        // Replace the trace pin
        let mut maybe_pacer = SONIC_PACER.borrow(cs).borrow_mut();
        if let Some(pacer) = maybe_pacer.as_mut() {
            pacer.maybe_trace_pin = maybe_trace_pin;
        };

        // Check back in all of the driver references
        sync::try_checkin_bb(cs, &SONIC_DRIVER_T1, driver_t1)
            .ok()
            .unwrap();
        sync::try_checkin_bb(cs, &SONIC_DRIVER_T4_VOICE1, driver_t4_voice1)
            .ok()
            .unwrap();
        sync::try_checkin_bb(cs, &SONIC_DRIVER_T4_VOICE2, driver_t4_voice2)
            .ok()
            .unwrap();
    });

    // Increment audio sample counter
    let counter = SONIC_COUNTER.load(Ordering::SeqCst);
    SONIC_COUNTER.store(counter.wrapping_add(1), Ordering::SeqCst);
}

/// Play a wave sound on a sonic voice.
/// Return a ControlHandle usable to change the waveform, pitch, and amplitude.
pub fn play_wave(
    voice: &dyn SonicVoice,
    wave_params: &WaveParams,
) -> ControlHandle {
    let sample_gen = Box::new(ControllableWaveGen::new());
    let control_handle = sample_gen.clone_control_handle();
    control_handle.set_wave_params(wave_params);
    voice.replace_sample_gen(Some(sample_gen));
    control_handle
}

/// Play two wave sounds on the same sonic voice.
/// Return a pair of ControlHandle objects to vary the waveform, pitch, and
/// amplitude of the two parts.
pub fn play_dual_wave(
    voice: &dyn SonicVoice,
    wave_params_1: &WaveParams,
    wave_params_2: &WaveParams,
) -> (ControlHandle, ControlHandle) {
    let sample_gen_1 = ControllableWaveGen::new();
    let sample_gen_2 = ControllableWaveGen::new();
    let control_handle_1 = sample_gen_1.clone_control_handle();
    let control_handle_2 = sample_gen_2.clone_control_handle();
    control_handle_1.set_wave_params(wave_params_1);
    control_handle_2.set_wave_params(wave_params_2);
    let dual_sample_gen =
        Box::new(DualSampleGen::new(sample_gen_1, sample_gen_2));
    voice.replace_sample_gen(Some(dual_sample_gen));
    (control_handle_1, control_handle_2)
}

/// Play noise on a voice using a noise channel
///
/// The `noise_channel` parameter is an enum that specifies which source of
/// random numbers to use for generating noise. If you call this function twice
/// with the same noise channel parameter, you end up playing the same samples.
///
/// `wave_params` sets the initial volume. Waveform and pitch are ignored.
/// The returned control handle can be used to vary the volume of the noise.
pub fn play_noise(
    voice: &dyn SonicVoice,
    noise_channel: NoiseChannel,
    wave_params: WaveParams,
) -> ControlHandle {
    let sample_gen = Box::new(RandomSampleGen::new(noise_channel));
    let control_handle = sample_gen.clone_control_handle();
    control_handle.set_wave_params(&wave_params);
    voice.replace_sample_gen(Some(sample_gen));
    control_handle
}
