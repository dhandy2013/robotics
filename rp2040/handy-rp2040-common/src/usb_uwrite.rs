//! Implement the uWrite interface for USB serial connection on RP2040.
//! This allows simple debug printing using ufmt.
//! Hints taken from:
//! https://github.com/rp-rs/rp-hal-boards/blob/main/boards/rp-pico/examples/pico_usb_serial.rs
//!
//! There are problems initializing the connection with the USB host.  I have to
//! wait until the device and the host are in communication with one another,
//! but I don't know for how long. Also if the USB connection is disconnected I
//! don't have a way to reconnect.  All in all this is a pain to use. I switched
//! to using the UART for debug messages.

use fugit::TimerDurationU64;
use rp2040_hal::{usb::UsbBus, Timer};
use ufmt_write::uWrite;
use usb_device::{class_prelude::*, prelude::*};
use usbd_serial::SerialPort;

pub type UsbWrite = dyn uWrite<Error = void::Void>;
pub const PACKET_SIZE: usize = 64;

pub struct UsbWriter<'a> {
    serial: SerialPort<'a, UsbBus>,
    usb_dev: UsbDevice<'a, UsbBus>,
    initialized: bool,
    rd_count: usize,
    rd_packet: [u8; PACKET_SIZE],
    wr_count: usize,
}

impl<'a> uWrite for UsbWriter<'a> {
    type Error = void::Void;

    fn write_str(&mut self, s: &str) -> Result<(), Self::Error> {
        let b = s.as_bytes();
        self.send_bytes_blocking(b);
        Ok(())
    }
}

impl<'a> UsbWriter<'a> {
    pub fn new(usb_bus: &'a UsbBusAllocator<UsbBus>) -> Self {
        // Set up the USB Communications Class Device driver
        let serial = SerialPort::new(&usb_bus);

        // Create a USB device with a fake VID and PID
        let usb_dev =
            UsbDeviceBuilder::new(&usb_bus, UsbVidPid(0x16c0, 0x27dd))
                .strings(&[StringDescriptors::new(LangID::EN)
                    .manufacturer("Handy Software and Publishing")
                    .product("Serial port")
                    .serial_number("DEBUG")])
                .expect("Failed to set strings")
                .device_class(2) // from: https://www.usb.org/defined-class-codes
                .build();

        Self {
            serial,
            usb_dev,
            initialized: false,
            rd_count: 0,
            rd_packet: [0u8; PACKET_SIZE],
            wr_count: 0,
        }
    }

    /// Return true iff the USB device has been successfully initialized.
    /// Call init() to initialize the device.
    pub fn initialized(&self) -> bool {
        self.initialized
    }

    /// Return the number of bytes read (and discarded) from the USB connection
    pub fn rd_count(&self) -> usize {
        self.rd_count
    }

    /// Return the number of bytes written to the USB connection
    pub fn wr_count(&self) -> usize {
        self.wr_count
    }

    /// Call this method before trying to read or write data.
    ///
    /// Return true if the USB device state becomes configured before
    /// the timeout microseconds expire, or if the device was already
    /// initialized.
    ///
    /// Return false if it times out waiting for the USB device to become
    /// configured. This happens for example when the Rpi Pico is running off
    /// battery power and the USB connection is not plugged into a computer.
    ///
    /// If the USB device becomes configured before the timeout, then this
    /// method waits an additional ``extra_delay_us`` microseconds before
    /// returning. This gives the host time to start listening on the USB
    /// serial device, so it doesn't miss the first message sent by the this
    /// device. If this is not needed, just set ``extra_delay_us`` to zero.
    pub fn init(
        &mut self,
        timer: &Timer,
        timeout_us: u32,
        extra_delay_us: u32,
    ) -> bool {
        if self.initialized {
            return true;
        }

        // Wait till USB device becomes configured.
        let timeout_time =
            timer.get_counter() + TimerDurationU64::micros(timeout_us as u64);
        let mut state = self.usb_dev.state();
        while !matches!(state, UsbDeviceState::Configured) {
            if timer.get_counter() > timeout_time {
                return false;
            }
            self.poll();
            state = self.usb_dev.state();
        }

        self.initialized = true;

        // Wait an additional delay after the device is configured, to give
        // the host application time to get set up.
        let extra_time = timer.get_counter()
            + TimerDurationU64::micros(extra_delay_us as u64);
        while timer.get_counter() < extra_time {
            self.poll();
        }

        true
    }

    /// Send a test write of two bytes: ASCII carriage return/linefeed.
    /// Then attempt to flush the bytes to the host computer.
    /// Return true if this succeeds.
    /// Un-initialize this object and return false if:
    /// - This object had not yet been initialized
    /// - flush() times out (timeout_us microseconds expire)
    /// - Some other error occurs
    pub fn check_or_uninit(&mut self, timer: &Timer, timeout_us: u32) -> bool {
        if !self.initialized() {
            return false;
        }
        let result = self.send_bytes(b"\r\n");
        if matches!(result, Some(0) | None) {
            // Nope. The host is not listening.
            self.initialized = false;
            return false;
        }
        if !self.flush_with_timeout(&timer, timeout_us) {
            self.initialized = false;
            return false;
        }
        true
    }

    /// Send bytes to the USB connection.
    /// Returns immediately if init() has not yet been called or the object
    /// has been un-initialized.
    /// Blocks until all bytes are accepted by the USB driver.
    /// Calls poll() if necessary to wait for buffer space to become available.
    /// Silently drops outgoing bytes on unexpected errors.
    pub fn send_bytes_blocking(&mut self, mut b: &[u8]) {
        if !self.initialized() {
            // Allow print commands to be quietly ignored if the device is
            // running off battery power and the USB cable is not plugged in to
            // a computer.
            return;
        }
        while !b.is_empty() {
            if let Some(n) = self.send_bytes(b) {
                if n == 0 {
                    // The outgoing buffer is full.
                    // Poll the USB device in hopes that more bytes get sent,
                    // freeing space in the outgoing buffer.
                    self.poll();
                } else {
                    b = &b[n..];
                }
            } else {
                // Some error ocurred that I don't know how to recover from.
                // Just drop the outgoing bytes.
                return;
            }
        }
    }

    /// Attempt to send bytes in ``b`` to the USB connection.
    /// Return the number of bytes actually sent, or None if there
    /// was some kind of USB error that prevented sending the bytes.
    pub fn send_bytes(&mut self, b: &[u8]) -> Option<usize> {
        let n = b.len();
        if n == 0 {
            return Some(0);
        }
        Some(match self.serial.write(b) {
            Ok(len) => {
                // I don't trust the USB protocol and API to always
                // return good values for len!
                let len = usize::min(len, n);
                self.wr_count = self.wr_count.saturating_add(len);
                len
            }
            Err(UsbError::WouldBlock) => {
                // The USB write buffer is full. Retry later.
                0
            }
            Err(_) => {
                // Some other error we don't understand
                return None;
            }
        })
    }

    /// Call this at least every 10ms to keep the connection alive.
    /// If the host sends data to the device, this method recieves
    /// it, counts the bytes, and throws it away.
    pub fn poll(&mut self) {
        // Check if USB device has messages from the host
        if self.usb_dev.poll(&mut [&mut self.serial]) {
            // Handle reading
            match self.serial.read(&mut self.rd_packet) {
                Err(_e) => {
                    // Do nothing
                }
                Ok(0) => {
                    // Do nothing
                }
                Ok(count) => {
                    // Count but throw away bytes read from the host
                    self.rd_count = self.rd_count.saturating_add(count);
                }
            }
        }
    }

    /// Wait until all outgoing bytes have been sent to the hardware buffers,
    /// or until we get an error from the USB driver that we don't know how to
    /// handle, whichever happens first.
    ///
    /// If the host computer is not currently listening to the USB serial port,
    /// this will block until the computer does start listening.
    ///
    /// If init() has not been called (initialized is false) return immediately.
    pub fn flush(&mut self) {
        if !self.initialized() {
            return;
        }
        loop {
            match self.serial.flush() {
                Ok(_) => break,
                Err(UsbError::WouldBlock) => {}
                Err(_err) => {
                    break;
                }
            };
        }
    }

    /// Wait until one of the following:
    /// - All outgoing bytes have been sent to the USB hardware buffers, then
    ///   return true;
    /// - We get an unexpected error from the USB driver, then return false;
    /// - The timeout microseconds expire, then return false.
    /// If init() has not been called (initialized is false) return false
    /// immediately.
    pub fn flush_with_timeout(
        &mut self,
        timer: &Timer,
        timeout_us: u32,
    ) -> bool {
        if !self.initialized() {
            return false;
        }
        let timeout_ts =
            timer.get_counter() + TimerDurationU64::micros(timeout_us as u64);
        loop {
            match self.serial.flush() {
                Ok(_) => break true,
                Err(UsbError::WouldBlock) => {}
                Err(_err) => {
                    break false;
                }
            };

            if timer.get_counter() > timeout_ts {
                break false;
            }
        }
    }
}
