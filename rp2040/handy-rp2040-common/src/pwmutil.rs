//! Utility code for working with PWM slices and signals.
use alloc::boxed::Box;
use core::{
    cell::RefCell,
    fmt::{self, Debug},
    ptr::write_volatile,
};
use critical_section::Mutex;
use rp2040_hal::{
    pac,
    pwm::{self, DynSliceId},
};

/// A dynamic PWM slice implemented at the PAC (peripheral access crate) level,
/// instead of using the HAL (hardware access layer). This unfortunately
/// requires use of unsafe code and duplicating code from the HAL.
///
/// Due to the design of the rp2040_hal crate, this is the only way to set the
/// CC (counter compare) register of both A and B channels of a PWM slice in the
/// same cycle. Otherwise I would stick with the HAL.
pub struct DynamicPwmSlice {
    slice_id: DynSliceId,
}

impl Debug for DynamicPwmSlice {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("DynamicPwmSlice")
            .field("slice_id", &self.slice_id().num)
            .finish()
    }
}

/// Construct a dynamic PWM slice from a hal::pwm::Slice, consuming the slice to
/// take ownership of it.
impl<S> From<pwm::Slice<S, pwm::FreeRunning>> for DynamicPwmSlice
where
    S: pwm::SliceId,
{
    fn from(_pwm: pwm::Slice<S, pwm::FreeRunning>) -> Self {
        Self { slice_id: S::DYN }
    }
}

impl DynamicPwmSlice {
    #[inline]
    fn ch(&self) -> &pac::pwm::CH {
        unsafe { &(*pac::PWM::ptr()).ch(self.slice_id.num as usize) }
    }

    /// Create the interrupt bitmask corresponding to this slice
    #[inline]
    pub fn bitmask(&self) -> u32 {
        1 << self.slice_id.num
    }

    /// Reset the PWM slice to a known starting condition.
    /// This will disable the PWM slice and disable and clear interrupts,
    /// then call .default_config().
    pub fn reset(&mut self) {
        self.disable();
        self.disable_interrupt();
        self.clear_interrupt();
        self.default_config();
    }

    #[inline]
    pub fn slice_id(&self) -> DynSliceId {
        self.slice_id
    }

    /// Set a default config for the slice.
    pub fn default_config(&mut self) {
        self.write_ph_correct(false);
        self.set_div_int(1); // No PWM clock divisor
        self.set_div_frac(0); // No PWM clock divisor
        self.write_inv_a(false); // Don't invert channel A
        self.write_inv_b(false); // Don't invert channel B
        self.set_top(0xfffe); // Wrap at 0xfffe, so cc = 0xffff can indicate
                              // 100% duty cycle
        self.set_counter(0x0000); // Reset the counter
        self.write_channel_a(0); // Channel A default duty cycle 0%
        self.write_channel_b(0); // Channel B default duty cycle 0%
    }

    /// Set flag indicating whether PWM slice is in phase correct mode
    pub fn write_ph_correct(&mut self, value: bool) {
        self.ch().csr().modify(|_, w| w.ph_correct().bit(value));
    }

    /// Set flag indicating whether to invert PWM channel A
    pub fn write_inv_a(&mut self, value: bool) {
        self.ch().csr().modify(|_, w| w.a_inv().bit(value));
    }

    /// Set flag indicating whether to invert PWM channel B
    pub fn write_inv_b(&mut self, value: bool) {
        self.ch().csr().modify(|_, w| w.b_inv().bit(value));
    }

    /// Set PWM TOP register that determines PWM frequency.
    pub fn set_top(&mut self, value: u16) {
        self.ch().top().write(|w| unsafe { w.top().bits(value) });
    }

    /// Get PWM TOP register that determines PWM frequency.
    pub fn get_top(&self) -> u16 {
        self.ch().top().read().top().bits()
    }

    /// Set PWM integer portion of clock divider.
    ///
    /// From the RP2040 datasheet section 4.5.2.6:
    /// The period of the PWM slice output in free-running mode in system clock
    /// cycles is:
    ///
    /// period = (TOP + 1) * (CSR_PH_CORRECT + 1) * (DIV_INT + (DIV_FRAC / 16))
    ///
    /// where TOP is the value set by .set_top() (maximum 65535),
    /// CSR_PH_CORRECT is 1 if .write_ph_correct(true), otherwise 0,
    /// DIV_INT is the value set by .set_div_int() (maximum 255),
    /// DIV_FRAC is the value set by .set_div_frac() (maximum 15).
    pub fn set_div_int(&mut self, value: u8) {
        self.ch()
            .div()
            .modify(|_, w| unsafe { w.int().bits(value) });
    }

    /// Set PWM fractional part of clock divider.
    ///
    /// See the doc for .set_div_int() for the effect this has on PWM period.
    pub fn set_div_frac(&mut self, value: u8) {
        self.ch()
            .div()
            .modify(|_, w| unsafe { w.frac().bits(value) });
    }

    /// Set the PWM counter register
    pub fn set_counter(&mut self, value: u16) {
        self.ch().ctr().write(|w| unsafe { w.ctr().bits(value) });
    }

    /// Get the PWM counter register
    pub fn get_counter(&self) -> u16 {
        self.ch().ctr().read().ctr().bits()
    }

    /// Enable the PWM slice, starting the counter.
    pub fn enable(&mut self) {
        self.ch().csr().modify(|_, w| w.en().bit(true));
    }

    /// Disable the PWM slice, stopping the counter.
    pub fn disable(&mut self) {
        self.ch().csr().modify(|_, w| w.en().bit(false));
    }

    /// Clear PWM interrupt signal for this slice, marking it as handled.
    /// This also makes `has_overflown()` return false.
    pub fn clear_interrupt(&mut self) {
        unsafe { (*pac::PWM::ptr()).intr().write(|w| w.bits(self.bitmask())) };
    }

    /// Did this slice trigger an overflow interrupt?
    ///
    /// This reports the raw interrupt flag, without considering masking or
    /// forcing bits. It may return true even if the interrupt is disabled
    /// or false even if the interrupt is forced.
    pub fn has_overflown(&self) -> bool {
        let mask = self.bitmask();
        unsafe { (*pac::PWM::ptr()).intr().read().bits() & mask == mask }
    }

    /// Force the interrupt. This bit is not cleared by hardware and must be
    /// manually cleared (e.g. by calling self.clear_interrupt()) to stop the
    /// interrupt from continuing to be asserted.
    pub fn force_interrupt(&mut self) {
        unsafe {
            let pwm = &(*pac::PWM::ptr());
            let reg = pwm.intf().as_ptr();
            write_bitmask_set(reg, self.bitmask());
        }
    }

    /// Enable the PWM_IRQ_WRAP interrupt when this slice overflows.
    /// You must also call `pwmutil::enable_pwm_interrupts` for this to take
    /// effect.
    pub fn enable_interrupt(&mut self) {
        unsafe {
            let pwm = &(*pac::PWM::ptr());
            let reg = pwm.inte().as_ptr();
            write_bitmask_set(reg, self.bitmask());
        }
    }

    /// Disable the PWM_IRQ_WRAP interrupt for this slice.
    pub fn disable_interrupt(&mut self) {
        unsafe {
            let pwm = &(*pac::PWM::ptr());
            let reg = pwm.inte().as_ptr();
            write_bitmask_clear(reg, self.bitmask());
        };
    }

    /// Set PWM channel A duty cycle to the new channel A pulse width.
    ///
    /// This sets the CC register for channel A of this PWM slice.
    /// The duty cycle is 0% if CC is 0, and 100% when CC = TOP + 1.
    pub fn write_channel_a(&mut self, pulse_width: u16) {
        self.ch()
            .cc()
            .modify(|_, w| unsafe { w.a().bits(pulse_width) });
    }

    /// Set PWM channel B duty cycle to the new channel B pulse width.
    ///
    /// This sets the CC register for channel B of this PWM slice.
    /// The duty cycle is 0% if CC is 0, and 100% when CC = TOP + 1.
    pub fn write_channel_b(&mut self, pulse_width: u16) {
        self.ch()
            .cc()
            .modify(|_, w| unsafe { w.b().bits(pulse_width) });
    }

    /// Set the pulse width (AKA duty cycle) for both channel A and channel B of
    /// this PWM slice simultaneously in the same cycle.
    pub fn write_channel_a_b(
        &mut self,
        pulse_width_a: u16,
        pulse_width_b: u16,
    ) {
        self.ch().cc().write(|w| unsafe {
            w.bits((pulse_width_b as u32) << 16 | pulse_width_a as u32)
        });
    }
}

/// Enable multiple PWM slices at the same time to make their counters sync up.
pub fn enable_pwm_simultaneous(slice_ids: &[DynSliceId]) {
    let mut bits: u32 = 0;
    for slice_id in slice_ids.iter() {
        let slice = DynamicPwmSlice {
            slice_id: *slice_id,
        };
        bits |= slice.bitmask();
    }
    unsafe {
        let pwm = &(*pac::PWM::ptr());
        let reg = pwm.en().as_ptr();
        write_bitmask_set(reg, bits);
    }
}

/// Enable multiple PWM slices at the same time, you supply the bitmask
pub fn enable_pwm_simultaneous_by_bitmask(bits: u32) {
    unsafe {
        let pwm = &(*pac::PWM::ptr());
        let reg = pwm.en().as_ptr();
        write_bitmask_set(reg, bits);
    }
}

/// Copied from rp2040_hal::atomic_register_access
///
/// Perform atomic bitmask set operation on register
///
/// See [section 2.1.2 of the RP2040 datasheet][section_2_1_2] for details.
///
/// [section_2_1_2]: https://datasheets.raspberrypi.com/rp2040/rp2040-datasheet.pdf#atomic-rwtype
///
/// # Safety
///
/// In addition to the requirements of [core::ptr::write_volatile],
/// `register` must point to a register providing atomic aliases.
#[inline]
pub(crate) unsafe fn write_bitmask_set(register: *mut u32, bits: u32) {
    let alias = (register as usize + 0x2000) as *mut u32;
    write_volatile(alias, bits);
}

/// Copied from rp2040_hal::atomic_register_access
///
/// Perform atomic bitmask clear operation on register
///
/// See [section 2.1.2 of the RP2040 datasheet][section_2_1_2] for details.
///
/// [section_2_1_2]: https://datasheets.raspberrypi.com/rp2040/rp2040-datasheet.pdf#atomic-rwtype
///
/// # Safety
///
/// In addition to the requirements of [core::ptr::write_volatile],
/// `register` must point to a register providing atomic aliases.
#[inline]
pub(crate) unsafe fn write_bitmask_clear(register: *mut u32, bits: u32) {
    let alias = (register as usize + 0x3000) as *mut u32;
    write_volatile(alias, bits);
}

pub type InterruptHandler = Box<dyn FnMut() + Send + 'static>;

// The RP2040 has 8 slices. Interrupts can be enabled separately for each one.
const NUM_PWM_SLICES: u8 = 8;
const NUM_INTERRUPT_HANDLERS: usize = NUM_PWM_SLICES as usize;
static INTERRUPT_HANDLERS: Mutex<
    RefCell<[Option<InterruptHandler>; NUM_INTERRUPT_HANDLERS]>,
> = Mutex::new(RefCell::new([
    None, None, None, None, None, None, None, None,
]));

/// Set (Some) or clear (None) a global interrupt handler.
/// slice_id: ID of PWM slice for which this handler will handle interrupts.
/// Return the old handler, or None if there wasn't any at the moment.
///
/// This function also returns None if a handler was previously set but is busy
/// executing on the other processor core. In that case when the old handler
/// finishes executing the new handler will take effect and the old handler will
/// be returned from call_interrupt_handler() (and most likely dropped).
pub fn set_interrupt_handler(
    slice_id: DynSliceId,
    maybe_handler: Option<InterruptHandler>,
) -> Option<InterruptHandler> {
    let i = slice_id.num as usize;
    critical_section::with(move |cs| {
        let ih_ref = INTERRUPT_HANDLERS.borrow(cs);
        let mut ih_mut = ih_ref.borrow_mut();
        match maybe_handler {
            Some(handler) => ih_mut[i].replace(handler),
            None => ih_mut[i].take(),
        }
    })
}

/// Call a global interrupt handler if it is set for this PWM slice.
/// slice_id: ID of PWM slice of interest.
/// Return Some(handler) if while this function is executing the handler, the
/// other core called set_interrupt_handler() with a different non-None handler.
/// This gives us a chance to somehow recover the handler, but usually it is Ok
/// to just drop it. If this edge case does not occur, then return None.
pub fn call_interrupt_handler(
    slice_id: DynSliceId,
) -> Option<InterruptHandler> {
    let i = slice_id.num as usize;

    // Grab the interrupt handler if any, else return None.
    let mut handler = critical_section::with(move |cs| {
        let ih_ref = INTERRUPT_HANDLERS.borrow(cs);
        let mut ih_mut = ih_ref.borrow_mut();
        ih_mut[i].take()
    })?;

    // The handler is a closure. It is expected to have side effects.
    handler();

    // Replace the handler back into the INTERRUPT_HANDLERS array,
    // if a new handler was not set while the old one was running.
    critical_section::with(move |cs| {
        let ih_ref = INTERRUPT_HANDLERS.borrow(cs);
        let mut ih_mut = ih_ref.borrow_mut();
        match ih_mut[i] {
            Some(_) => Some(handler), // returns the old handler
            None => ih_mut[i].replace(handler), // returns None
        }
    })
}

/// We can't define the PWM_IRQ_WRAP interrupt handler function directly in a
/// library module like this, because it would cause linker errors in
/// applications that use this crate and also use a framework like RTIC that
/// defines PWM_IRQ_WRAP.
///
/// So we do the next best thing: Provide this function for each application to
/// call from its own PWM_IRQ_WRAP function, in order to get the benefit of this
/// module's PWM interrupt handling logic. Use it like this:
/// ```
/// use handy_rp2040_common::pwmutil;
/// use rp2040_hal::pac::interrupt;
///
/// #[allow(non_snake_case)]
/// #[interrupt]
/// fn PWM_IRQ_WRAP() {
///     pwmutil::pwm_irq_wrap_handler();
/// }
/// ```
#[inline]
pub fn pwm_irq_wrap_handler() {
    // Figure out exactly which PWM slices have overflown.
    // Clear the interrupt flags for those slices, but only for slices for
    // which the interrupt was enabled or the interrupt was forced. We don't
    // want to mess up code that might be polling the intr status without
    // enabling the interrupt for a particular slice.
    //
    // Fun fact: There is a different set of interrupt flags _per core_, but
    // only one set of interrupt handlers registered in shared memory.  So if
    // both cores enable a PWM slice interrupt, then the handler for that slice
    // will be called twice. If I understand the datasheet correctly.
    let pwm_ptr = pac::PWM::ptr();
    let mut triggered_slices = unsafe {
        // intr_bits is a bitmask of slices that have overflown
        let mut intr_bits = (*pwm_ptr).intr().read().bits();
        // en_bits are the slices for which interrupts are enabled
        let en_bits = (*pwm_ptr).en().read().bits();
        // intf_bits are the slices for which interrupts were forced
        let intf_bits = (*pwm_ptr).intf().read().bits();
        // Remove any bits from intr_bits that don't involve interrupts
        intr_bits &= en_bits | intf_bits;
        // Now clear those bits from the `intr` PWM register so they don't
        // re-trigger this interrupt.
        (*pwm_ptr).intr().write(|w| w.bits(intr_bits));
        // Return the bitmask of slices of interest
        intr_bits
    };

    // Call the registered handler (if any) for each triggered PWM slice.
    loop {
        let slice_num = triggered_slices.trailing_zeros();
        if slice_num >= NUM_PWM_SLICES as u32 {
            break;
        }
        triggered_slices ^= 1 << slice_num;
        let slice_id = DynSliceId {
            num: slice_num as u8,
        };
        call_interrupt_handler(slice_id);
    }
}

/// Enable PWM interrupts generally.
/// This does not enable interrupts for any particular PWM slice. To do that,
/// in addition you must call `DynamicPwmSlice::enable_interrupt`.
///
/// IMPORTANT NOTE: Whenever you call this function and enable PWM interrupts on
/// a slice, you *must* also define a PWM_IRQ_WRAP function in your program.
/// Otherwise, a default PWM_IRQ_WRAP handler is used that does nothing.
/// "Nothing" includes not clearing the interrupt flag, resulting in the
/// do-nothing interrupt handler being called in an infinite loop, crashing your
/// program.
///
/// See the doc comment for `pwm_irq_wrap_handler` for a simple PWM_IRQ_WRAP
/// function you can use in your programs.
pub fn enable_pwm_interrupts() {
    // Tell the NVIC to start calling the PWM interrupt handler function
    unsafe {
        pac::NVIC::unmask(pac::Interrupt::PWM_IRQ_WRAP);
    }
}
