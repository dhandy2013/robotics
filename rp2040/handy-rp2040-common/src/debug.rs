//! Debugging utilities
use rp2040_hal::pac;

/// Set GPIO25 output high.
/// On the Raspberry Pi Pico this turns on the built-in LED.
/// This only works if GPIO25 has previously been set up as a push-pull output.
#[inline]
pub fn led_on() {
    unsafe {
        let sio = &(*pac::SIO::ptr());
        let set = sio.gpio_out_set();
        set.write(|w| {
            w.bits(1 << 25);
            w
        });
    }
}

/// Set GPIO25 output low.
/// On the Raspberry Pi Pico this turns off the built-in LED.
/// This only works if GPIO25 has previously been set up as a push-pull output.
#[inline]
pub fn led_off() {
    unsafe {
        let sio = &(*pac::SIO::ptr());
        let clr = sio.gpio_out_clr();
        clr.write(|w| {
            w.bits(1 << 25);
            w
        });
    }
}
