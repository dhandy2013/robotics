//! RP2040 audio hardware setup and manipulation
//! - Use a PWM slice for generating ave. voltage on 2 channels (driver PWM)
//! - Use another PWM slice for timing audio samples (pacing PWM)
//! - We're shooting for 22.050KHz sample rate = 1/2 audio CD quality
//! - The rate at which the driver PWM duty cycle varies is the audio
//!   frequency.
//! - The range over which the duty cycle varies is the audio amplitude.
//! - The driver PWM channel A is the audio left channel, channel B is the right
//!   channel.
//!
//! If you call one of the driver init modules in this function that sets up an
//! interrupt handler, you also need to define your own PWM_IRQ_WRAP function in
//! your application, or you will not hear any sounds from the driver. See the
//! doc comment for pwmutil::pwm_irq_wrap_handler for details.

use crate::pwmutil::{self, enable_pwm_simultaneous, DynamicPwmSlice};
use alloc::boxed::Box;
use core::{
    cell::RefCell,
    sync::atomic::{AtomicUsize, Ordering},
};
use critical_section::Mutex;
use embedded_hal::digital::OutputPin;
use rp2040_hal::{gpio, gpio::DynPinId, pwm};
use static_assertions::const_assert;
use wavegen::wavegen16::WaveGenerator;

pub use wavegen::wavegen16::Waveform;

pub type WaveGen = WaveGenerator<TICKS_PER_SECOND>;

/// Use `MaybeFloatingNull::<P>::None` as a parameter when you don't want to
/// pass a PWM pin to one of the init functions. Type P must be filled in with a
/// pin ID from the gpio::bank0 module. It doesn't matter which static pin ID
/// you use as long as it is compatible with the driver PWM slice.
///
/// Example:
///     sonic::MaybeFloatingNull::<gpio::bank0::Gpio14>::None
/// can be used in place of:
//      Option::<
//          gpio::Pin<gpio::bank0::Gpio14, gpio::FunctionNull, gpio::PullNone>,
//      >::None,
pub type MaybeFloatingNull<P> =
    Option<gpio::Pin<P, gpio::FunctionNull, gpio::PullNone>>;

/// Use `MaybeOutputPin::<P>::None` as a parameter when you don't want to pass
/// an optional output pin to one of the init functions.
///
/// Example:
///     sonic::MaybeOutput::None
/// can be used in place of:
//      Option::<
//          gpio::Pin<gpio::bank0::Gpio14, gpio::FunctionSioOutput, gpio::PullNone>,
//      >::None,
pub type MaybeOutput =
    Option<gpio::Pin<gpio::DynPinId, gpio::FunctionSioOutput, gpio::PullNone>>;

/// Assumed system clock frequency
/// TODO: Get this dynamically, since clock speed can be changed?
pub const FSYS: u32 = 125_000_000;

/// Target audio sample rate
pub const TICKS_PER_SECOND: u16 = 25_000;

/// PWM top value needed to get desired pacing PWM frequency
///
/// Formula for PWM frequency when not scaling and when not doing
/// phase-correct PWM:
///
///     FPWM = FSYS / (TOP + 1)
///
/// In the expression below I subtract 0.5 instead of 1.0 in order to get proper
/// rounding.
pub const PACING_TOP: u16 =
    ((FSYS as f64) / (TICKS_PER_SECOND as f64) - 0.5) as u16;

// Check that pacing PWM frequency is within 1 Hz of target
pub const ACTUAL_PACING_PWM_FREQ: f64 =
    (FSYS as f64) / (PACING_TOP as f64 + 1.0);
const _PACING_PWM_FREQ_ERR: f64 =
    ACTUAL_PACING_PWM_FREQ - TICKS_PER_SECOND as f64;
const_assert!(_PACING_PWM_FREQ_ERR > -1.0 && _PACING_PWM_FREQ_ERR < 1.0);

/// Target PWM frequency for driving the speakers as a multiple of sample rate
pub const TARGET_DRIVER_PWM_FREQ: u32 = TICKS_PER_SECOND as u32 * 8;

/// PWM parameters needed to get driver PWM freq which is 8x pacing freq.
///
///     FPWM = FSYS / ((TOP + 1) * (DIV_INT + (DIV_FRAC / 16)))
///
pub const DRIVER_TOP: u16 = 624;
pub const DRIVER_DIV_INT: u8 = 1;
pub const DRIVER_DIV_FRAC: u8 = 0;

// Check that driver PWM frequency is close to the target
// Note: If const_assert!() fails, the compiler error is unclear. It may say
// something strange about "attempt to compute `0_usize - 1_usize`."
pub const DRIVER_PWM_FREQ: f64 = (FSYS as f64)
    / ((DRIVER_TOP as f64 + 1.0)
        * (DRIVER_DIV_INT as f64 + (DRIVER_DIV_FRAC as f64 / 16.0)));
const _DRIVER_PWM_FREQ_ERR: f64 =
    DRIVER_PWM_FREQ - TARGET_DRIVER_PWM_FREQ as f64;
const _DRIVER_PWM_FREQ_TOL: f64 = 41.5; // error tolerance in Hz
const_assert!(
    _DRIVER_PWM_FREQ_ERR > -_DRIVER_PWM_FREQ_TOL
        && _DRIVER_PWM_FREQ_ERR < _DRIVER_PWM_FREQ_TOL
);

/// Amount of time that it takes a BJT transistor to turn off. For the
/// complementary driver we don't want to turn on one transistor until the
/// other is fully off.
const DEAD_ZONE_TIME: f64 = 225.0e-9; // transistor turnoff time in seconds
/// Number of processor clock cycles it takes a BJT transitor to turn off.
const DEAD_ZONE_CYCLES: u16 = (DEAD_ZONE_TIME * FSYS as f64 + 0.5) as u16;

/// This struct owns the driver PWM and pacing PWM slices and has methods to
/// control it for generating audio signals using the t1 driver strategy.
///
struct SonicPwmT1 {
    driver_pwm: DynamicPwmSlice,
    pacing_pwm: DynamicPwmSlice,
    // The two channel pin function types are DynFunction instead of PwmFunction
    // because we switch the function between PWM and Null (disabled).
    // We aren't losing that much in type checking anyway, because the RP2040 can
    // do PWM on *every* GPIO pin.
    channel_a_pin:
        Option<gpio::Pin<DynPinId, gpio::DynFunction, gpio::PullNone>>,
    channel_b_pin:
        Option<gpio::Pin<DynPinId, gpio::DynFunction, gpio::PullNone>>,
    channel_a_pin_inv:
        Option<gpio::Pin<DynPinId, gpio::DynFunction, gpio::PullNone>>,
    channel_b_pin_inv:
        Option<gpio::Pin<DynPinId, gpio::DynFunction, gpio::PullNone>>,
    clock_running: bool,
}

impl SonicPwmT1 {
    /// Configure two PWM slices and up to 4 GPIO pins for sound generation
    /// Return the configured Sonic object.
    ///
    /// The pacing PWM frequency will be set to TICKS_PER_SECOND given
    /// the assumed FSYS system clock frequency.
    pub fn new(
        mut driver_pwm: DynamicPwmSlice,
        mut pacing_pwm: DynamicPwmSlice,
        channel_a_pin: Option<
            gpio::Pin<DynPinId, gpio::DynFunction, gpio::PullNone>,
        >,
        channel_b_pin: Option<
            gpio::Pin<DynPinId, gpio::DynFunction, gpio::PullNone>,
        >,
        channel_a_pin_inv: Option<
            gpio::Pin<DynPinId, gpio::DynFunction, gpio::PullNone>,
        >,
        channel_b_pin_inv: Option<
            gpio::Pin<DynPinId, gpio::DynFunction, gpio::PullNone>,
        >,
    ) -> Self {
        // Configure driver PWM frequency
        // Leave driver PWM duty cycle set at default 0% for both channels
        // This PWM slice will be enabled later by the `start_clock()` method.
        driver_pwm.default_config();
        driver_pwm.set_top(DRIVER_TOP);
        driver_pwm.set_div_int(DRIVER_DIV_INT);
        driver_pwm.set_div_frac(DRIVER_DIV_FRAC);

        // Configure pacing PWM frequency.
        // Pacing PWM duty cycle is set to 50% for both channels for tracing.
        // This PWM slice will be enabled later by the `start_clock()` method.
        pacing_pwm.default_config();
        pacing_pwm.set_top(PACING_TOP);
        pacing_pwm.write_channel_a_b(PACING_TOP / 2, PACING_TOP / 2);

        // Return our configured sonic PWM object
        Self {
            driver_pwm,
            pacing_pwm,
            channel_a_pin,
            channel_b_pin,
            channel_a_pin_inv,
            channel_b_pin_inv,
            clock_running: false,
        }
    }

    /// Wait for the next time the pacing PWM timer reaches TOP.
    /// Return true if there was some spinning time (we had some timeslice left),
    /// false if the pacing timer had already wrapped or if it is not running.
    ///
    /// This method does _not_ clear the interrupt flag. Call
    /// `.clear_interrupt()` in your polling routine if the interrupt handler
    /// will not do it for you.
    #[allow(dead_code)]
    pub fn wait_for_timer_tick(&self) -> bool {
        if !self.clock_running {
            // Avoid infinite loop waiting for a tick that will never happen
            return false;
        }

        let result = if self.pacing_pwm.has_overflown() {
            // We are too late, the timer period ran out
            false
        } else {
            // Wait for the pacing timer interrupt
            while !self.pacing_pwm.has_overflown() {}
            true
        };

        result
    }

    /// Start the pacing PWM clock running (interrupts start happening)
    fn start_clock(&mut self) {
        // Clear any previous interrupt flag
        self.pacing_pwm.clear_interrupt();

        // Start the driver and pacing PWM clocks running simultaneously
        enable_pwm_simultaneous(&[
            self.driver_pwm.slice_id(),
            self.pacing_pwm.slice_id(),
        ]);

        // Enable interrupts for the pacing PWM slice
        self.pacing_pwm.enable_interrupt();
        self.clock_running = true;
    }

    /// Stop the pacing PWM clock (to stop interrupts from being fired)
    #[allow(dead_code)]
    fn stop_clock(&mut self) {
        self.clock_running = false;
        self.pacing_pwm.disable();
    }

    /// Start sending sound to the configured pins
    fn start_sound(&mut self) {
        if let Some(ref mut channel_a_pin) = self.channel_a_pin {
            channel_a_pin
                .try_set_function(gpio::DynFunction::Pwm)
                .ok()
                .unwrap();
        }
        if let Some(ref mut channel_b_pin) = self.channel_b_pin {
            channel_b_pin
                .try_set_function(gpio::DynFunction::Pwm)
                .ok()
                .unwrap();
        }
        if let Some(ref mut channel_a_pin_inv) = self.channel_a_pin_inv {
            channel_a_pin_inv.set_output_override(gpio::OutputOverride::Invert);
            channel_a_pin_inv
                .try_set_function(gpio::DynFunction::Pwm)
                .ok()
                .unwrap();
        }
        if let Some(ref mut channel_b_pin_inv) = self.channel_b_pin_inv {
            channel_b_pin_inv.set_output_override(gpio::OutputOverride::Invert);
            channel_b_pin_inv
                .try_set_function(gpio::DynFunction::Pwm)
                .ok()
                .unwrap();
        }
    }

    /// Stop sending sound to the configured pins
    fn stop_sound(&mut self) {
        if let Some(ref mut channel_a_pin) = self.channel_a_pin {
            channel_a_pin
                .try_set_function(gpio::DynFunction::Null)
                .ok()
                .unwrap();
        }
        if let Some(ref mut channel_b_pin) = self.channel_b_pin {
            channel_b_pin
                .try_set_function(gpio::DynFunction::Null)
                .ok()
                .unwrap();
        }
        if let Some(ref mut channel_a_pin_inv) = self.channel_a_pin_inv {
            channel_a_pin_inv
                .try_set_function(gpio::DynFunction::Null)
                .ok()
                .unwrap();
            channel_a_pin_inv
                .set_output_override(gpio::OutputOverride::DontInvert);
        }
        if let Some(ref mut channel_b_pin_inv) = self.channel_b_pin_inv {
            channel_b_pin_inv
                .try_set_function(gpio::DynFunction::Null)
                .ok()
                .unwrap();
            channel_b_pin_inv
                .set_output_override(gpio::OutputOverride::DontInvert);
        }
    }

    #[allow(dead_code)]
    fn is_clock_running(&self) -> bool {
        self.clock_running
    }

    /// Clear the interrupt flag.
    /// Do this inside the PWM interrupt handler or you get an infinite loop.
    #[allow(dead_code)]
    fn clear_interrupt(&mut self) {
        self.pacing_pwm.clear_interrupt();
    }

    /// Set driver PWM channel A duty cycle to the new channel A pulse width.
    /// Valid pulse_width values are: 0..=DRIVER_TOP
    #[allow(dead_code)]
    fn write_channel_a(&mut self, pulse_width: u16) {
        self.driver_pwm.write_channel_a(pulse_width);
    }

    /// Set driver PWM channel B duty cycle to the new channel B pulse width.
    /// Valid pulse_width values are: 0..=DRIVER_TOP
    #[allow(dead_code)]
    fn write_channel_b(&mut self, pulse_width: u16) {
        self.driver_pwm.write_channel_b(pulse_width);
    }

    fn write_channel_a_b(&mut self, pulse_width_a: u16, pulse_width_b: u16) {
        self.driver_pwm
            .write_channel_a_b(pulse_width_a, pulse_width_b);
    }
}

struct T1AudioDriver {
    sonic_pwm: SonicPwmT1,
    controls: &'static Mutex<RefCell<SonicControls>>,
    counter: &'static AtomicUsize,
    generator_a: WaveGen,
    generator_b: WaveGen,
    pulse_width_a: u16,
    pulse_width_b: u16,
    sound_on: bool,
}

impl T1AudioDriver {
    fn start(
        mut sonic_pwm: SonicPwmT1,
        controls: &'static Mutex<RefCell<SonicControls>>,
        counter: &'static AtomicUsize,
    ) -> Self {
        // Set up desired initial conditions for interrupt handler.
        // Just in case the system is being re-initialized.
        sonic_pwm.stop_sound();
        counter.store(0, Ordering::SeqCst);
        sonic_pwm.start_clock();

        let mut generator_a = WaveGen::new();
        let mut generator_b = WaveGen::new();
        let sample_a = generator_a.next_sample();
        let sample_b = generator_b.next_sample();
        let pulse_width_a = Self::sample_to_pulse_width(sample_a);
        let pulse_width_b = Self::sample_to_pulse_width(sample_b);

        Self {
            sonic_pwm,
            controls,
            counter,
            generator_a,
            generator_b,
            pulse_width_a,
            pulse_width_b,
            sound_on: false,
        }
    }

    /// Call this *only* from the main PWM interrupt handler function!
    fn handle_interrupt(&mut self) {
        // Update the pulse width values of the two PWM channels.
        // Do this first thing so we don't have jitter in the output.
        self.sonic_pwm
            .write_channel_a_b(self.pulse_width_a, self.pulse_width_b);

        // Copy info from the global client data used to communicate with this
        // interrupt handler.
        let new_sound_on = critical_section::with(|cs| {
            let client_refcell = self.controls.borrow(cs);
            let client = client_refcell.borrow();

            // Update the waveform generators based on current client parameters
            client
                .channel_a_params
                .copy_to_wavegen(&mut self.generator_a);
            client
                .channel_b_params
                .copy_to_wavegen(&mut self.generator_b);

            // Return whether we should turn output pins on or off.
            client.sound_on
        });

        if new_sound_on != self.sound_on {
            self.sound_on = new_sound_on;
            if new_sound_on {
                self.sonic_pwm.start_sound();
            } else {
                self.sonic_pwm.stop_sound();
            }
        }

        // Calculate the next pulse widths for each PWM channel
        let sample_a = self.generator_a.next_sample();
        let sample_b = self.generator_b.next_sample();
        self.pulse_width_a = Self::sample_to_pulse_width(sample_a);
        self.pulse_width_b = Self::sample_to_pulse_width(sample_b);

        // Increment the audio sample count (used as a clock by clients)
        //
        // According to the docs, the ARM thumbv6m does not support
        // fetch_add().  It can only do load() and store(). But this is Ok
        // because this interrupt routine and init() are the only places
        // where TIMER_TICKS.store() is called.
        let timer_ticks = self.counter.load(Ordering::SeqCst);
        self.counter
            .store(timer_ticks.wrapping_add(1), Ordering::SeqCst);
    }

    /// Convert a signed 16-bit sample value in range -32767..=32767
    /// to an unsigned 16-bit pulse width in range 0..=DRIVER_TOP
    #[inline]
    fn sample_to_pulse_width(sample: i16) -> u16 {
        let half_range = (DRIVER_TOP / 2) as i32;
        (half_range + ((half_range * sample as i32) >> 15)) as u16
    }
}

/// This struct owns the driver PWM and pacing PWM slices and has methods to
/// control it for generating audio signals using the t4 driver strategy.
///
/// The driver pin function types are DynFunction instead of PwmFunction because
/// we switch the function between PWM and Null (disabled).  We aren't losing
/// that much in type checking anyway, because the RP2040 can do PWM on *every*
/// GPIO pin.
struct SonicPwmT4 {
    pacing_pwm: DynamicPwmSlice,

    driver1_pwm: DynamicPwmSlice,
    driver1_channel_a_pin:
        gpio::Pin<DynPinId, gpio::DynFunction, gpio::PullNone>,
    driver1_channel_b_pin:
        gpio::Pin<DynPinId, gpio::DynFunction, gpio::PullNone>,

    driver2_pwm: DynamicPwmSlice,
    driver2_channel_a_pin:
        gpio::Pin<DynPinId, gpio::DynFunction, gpio::PullNone>,
    driver2_channel_b_pin:
        gpio::Pin<DynPinId, gpio::DynFunction, gpio::PullNone>,

    clock_running: bool,
}

impl SonicPwmT4 {
    /// Configure two PWM slices and 4 GPIO pins for sound generation. Return
    /// the configured Sonic object.
    ///
    /// The pacing PWM frequency will be set to TICKS_PER_SECOND given
    /// the assumed FSYS system clock frequency.
    pub fn new(
        mut pacing_pwm: DynamicPwmSlice,
        mut driver1_pwm: DynamicPwmSlice,
        driver1_channel_a_pin: gpio::Pin<
            DynPinId,
            gpio::DynFunction,
            gpio::PullNone,
        >,
        driver1_channel_b_pin: gpio::Pin<
            DynPinId,
            gpio::DynFunction,
            gpio::PullNone,
        >,
        mut driver2_pwm: DynamicPwmSlice,
        driver2_channel_a_pin: gpio::Pin<
            DynPinId,
            gpio::DynFunction,
            gpio::PullNone,
        >,
        driver2_channel_b_pin: gpio::Pin<
            DynPinId,
            gpio::DynFunction,
            gpio::PullNone,
        >,
    ) -> Self {
        // Configure pacing PWM frequency.
        // Pacing PWM duty cycle is set to 50% for both channels for tracing.
        // This PWM slice will be enabled later by the `start_clock()` method.
        pacing_pwm.default_config();
        pacing_pwm.set_top(PACING_TOP);
        pacing_pwm.write_channel_a_b(PACING_TOP / 2, PACING_TOP / 2);

        // Why do I invert the channel B PWM outputs? It's complicated. These
        // control the NPN transistors. But the way phase correct PWM works,
        // I need to invert the output and then "invert" (subtract from 100%)
        // the duty cycle in order to have the transistor off during the dead
        // zone at the beginning and end of the PWM cycle.

        // Configure driver 1 PWM frequency
        // Leave driver PWM duty cycle set at default 0% for both channels
        // This PWM slice will be enabled later by the `start_clock()` method.
        driver1_pwm.default_config();
        driver1_pwm.set_top(DRIVER_TOP);
        driver1_pwm.set_div_int(DRIVER_DIV_INT);
        driver1_pwm.set_div_frac(DRIVER_DIV_FRAC);
        driver1_pwm.write_ph_correct(true); // use phase correct PWM
        driver1_pwm.write_inv_b(true); // invert channel B (that controls NPN 1)

        // Configure driver 2 PWM frequency
        // Leave driver PWM duty cycle set at default 0% for both channels
        // This PWM slice will be enabled later by the `start_clock()` method.
        driver2_pwm.default_config();
        driver2_pwm.set_top(DRIVER_TOP);
        driver2_pwm.set_div_int(DRIVER_DIV_INT);
        driver2_pwm.set_div_frac(DRIVER_DIV_FRAC);
        driver2_pwm.write_ph_correct(true); // use phase correct PWM
        driver2_pwm.write_inv_b(true); // invert channel B (that controls NPN 2)

        // Return our configured sonic PWM object
        Self {
            pacing_pwm,
            driver1_pwm,
            driver1_channel_a_pin,
            driver1_channel_b_pin,
            driver2_pwm,
            driver2_channel_a_pin,
            driver2_channel_b_pin,
            clock_running: false,
        }
    }

    /// Wait for the next time the pacing PWM timer reaches TOP.
    /// Return true if there was some spinning time (we had some timeslice left),
    /// false if the pacing timer had already wrapped or if it is not running.
    ///
    /// This method does _not_ clear the interrupt flag. Call
    /// `.clear_interrupt()` in your polling routine if the interrupt handler
    /// will not do it for you.
    #[allow(dead_code)]
    pub fn wait_for_timer_tick(&self) -> bool {
        if !self.clock_running {
            // Avoid infinite loop waiting for a tick that will never happen
            return false;
        }

        let result = if self.pacing_pwm.has_overflown() {
            // We are too late, the timer period ran out
            false
        } else {
            // Wait for the pacing timer interrupt
            while !self.pacing_pwm.has_overflown() {}
            true
        };

        result
    }

    /// Start the pacing PWM clock running (interrupts start happening)
    fn start_clock(&mut self) {
        // Clear any previous interrupt flag
        self.pacing_pwm.clear_interrupt();

        // Start the driver and pacing PWM clocks running simultaneously
        enable_pwm_simultaneous(&[
            self.pacing_pwm.slice_id(),
            self.driver1_pwm.slice_id(),
            self.driver2_pwm.slice_id(),
        ]);

        // Enable interrupts for the pacing PWM slice
        self.pacing_pwm.enable_interrupt();
        self.clock_running = true;
    }

    /// Stop the pacing PWM clock (to stop interrupts from being fired)
    #[allow(dead_code)]
    fn stop_clock(&mut self) {
        self.clock_running = false;
        self.pacing_pwm.disable();
    }

    /// Start sending sound to the configured pins
    fn start_sound(&mut self) {
        self.driver1_channel_a_pin
            .try_set_function(gpio::DynFunction::Pwm)
            .ok()
            .unwrap();
        self.driver1_channel_b_pin
            .try_set_function(gpio::DynFunction::Pwm)
            .ok()
            .unwrap();
        self.driver2_channel_a_pin
            .try_set_function(gpio::DynFunction::Pwm)
            .ok()
            .unwrap();
        self.driver2_channel_b_pin
            .try_set_function(gpio::DynFunction::Pwm)
            .ok()
            .unwrap();
    }

    /// Stop sending sound to the configured pins
    fn stop_sound(&mut self) {
        self.driver1_channel_a_pin
            .try_set_function(gpio::DynFunction::Null)
            .ok()
            .unwrap();
        self.driver1_channel_b_pin
            .try_set_function(gpio::DynFunction::Null)
            .ok()
            .unwrap();
        self.driver2_channel_a_pin
            .try_set_function(gpio::DynFunction::Null)
            .ok()
            .unwrap();
        self.driver2_channel_b_pin
            .try_set_function(gpio::DynFunction::Null)
            .ok()
            .unwrap();
    }

    #[allow(dead_code)]
    fn is_clock_running(&self) -> bool {
        self.clock_running
    }

    /// Clear the interrupt flag.
    /// The flag must be cleared within the PWM interrupt handler somehow, or
    /// else it will be re-entered immediately on exiting it.
    #[allow(dead_code)]
    fn clear_interrupt(&mut self) {
        self.pacing_pwm.clear_interrupt();
    }

    fn write_driver_1_2_channel_a_b(
        &mut self,
        pulse_width_1a: u16,
        pulse_width_1b: u16,
        pulse_width_2a: u16,
        pulse_width_2b: u16,
    ) {
        self.driver1_pwm
            .write_channel_a_b(pulse_width_1a, pulse_width_1b);
        self.driver2_pwm
            .write_channel_a_b(pulse_width_2a, pulse_width_2b);
    }
}

struct T4AudioDriver {
    sonic_pwm: SonicPwmT4,
    controls: &'static Mutex<RefCell<SonicControls>>,
    counter: &'static AtomicUsize,
    generator: WaveGen,
    pulse_width_1a: u16,
    pulse_width_1b: u16,
    pulse_width_2a: u16,
    pulse_width_2b: u16,
    sound_on: bool,
}

impl T4AudioDriver {
    fn start(
        mut sonic_pwm: SonicPwmT4,
        controls: &'static Mutex<RefCell<SonicControls>>,
        counter: &'static AtomicUsize,
    ) -> Self {
        // Set up desired initial conditions for interrupt handler.
        // Just in case the system is being re-initialized.
        sonic_pwm.stop_sound();
        counter.store(0, Ordering::SeqCst);
        sonic_pwm.start_clock();

        let mut generator = WaveGen::new();
        let sample = generator.next_sample();
        let (pulse_width_1a, pulse_width_1b, pulse_width_2a, pulse_width_2b) =
            Self::sample_to_pulse_widths(sample);

        Self {
            sonic_pwm,
            controls,
            counter,
            generator,
            pulse_width_1a,
            pulse_width_1b,
            pulse_width_2a,
            pulse_width_2b,
            sound_on: false,
        }
    }

    /// Call this *only* from the main PWM interrupt handler function!
    /// (Or an equivalent event loop.)
    fn handle_interrupt(&mut self) {
        // Update the pulse width values of the two PWM channels.
        // Do this first thing so we don't have jitter in the output.
        self.sonic_pwm.write_driver_1_2_channel_a_b(
            self.pulse_width_1a,
            self.pulse_width_1b,
            self.pulse_width_2a,
            self.pulse_width_2b,
        );

        // Copy info from the global client data used to communicate with this
        // interrupt handler.
        let new_sound_on = critical_section::with(|cs| {
            let client_refcell = self.controls.borrow(cs);
            let client = client_refcell.borrow();

            // Update the waveform generator based on current client parameters
            client.channel_a_params.copy_to_wavegen(&mut self.generator);

            // Return whether we should turn output pins on or off.
            client.sound_on
        });

        // Determine whether we should turn output pins on or off.
        if new_sound_on != self.sound_on {
            self.sound_on = new_sound_on;
            if new_sound_on {
                self.sonic_pwm.start_sound();
            } else {
                self.sonic_pwm.stop_sound();
            }
        }

        // Calculate the next PWM pulse widths
        let sample = self.generator.next_sample();
        (
            self.pulse_width_1a,
            self.pulse_width_1b,
            self.pulse_width_2a,
            self.pulse_width_2b,
        ) = Self::sample_to_pulse_widths(sample);

        // Increment the audio sample count (used as a clock by clients)
        //
        // According to the docs, the ARM thumbv6m does not support
        // fetch_add().  It can only do load() and store(). But this is Ok
        // because this interrupt routine and init() are the only places
        // where TIMER_TICKS.store() is called.
        let timer_ticks = self.counter.load(Ordering::SeqCst);
        self.counter
            .store(timer_ticks.wrapping_add(1), Ordering::SeqCst);
    }

    /// Convert a sample value in range -32767..=32767 into PWM pulse widths.
    ///
    /// The returned pulse widths, in order are:
    ///     (pulse_width_1a, pulse_width_1b, pulse_width_2a, pulse_width_2b)
    /// where:
    ///     pulse_width_1a controls the H-bridge side 1 PNP transistor
    ///     pulse_width_1b controls the H-bridge side 1 NPN transistor
    ///     pulse_width_2a controls the H-bridge side 2 PNP transistor
    ///     pulse_width_2b controls the H-bridge side 2 NPN transistor
    ///
    /// If the pulse width for a PWM channel is FULL_DUTY_CYCLE, that means the
    /// corresponding transitor is turned OFF for the full cycle. If the pulse
    /// width is 0 that means the corresponding transistor is turned ON for the
    /// full cycle. Otherwise, it is modulated during the cycle.
    #[inline]
    fn sample_to_pulse_widths(sample: i16) -> (u16, u16, u16, u16) {
        const FULL_DUTY_CYCLE: u16 = DRIVER_TOP + 1;
        const DRIVER_SCALE: u32 =
            FULL_DUTY_CYCLE as u32 - DEAD_ZONE_CYCLES as u32;
        if sample == 0 {
            (
                FULL_DUTY_CYCLE,
                FULL_DUTY_CYCLE,
                FULL_DUTY_CYCLE,
                FULL_DUTY_CYCLE,
            )
        } else if sample > 0 {
            let sample = sample as u32;
            let pulse_width = ((DRIVER_SCALE * sample) >> 15) as u16;
            // "Invert" the duty cycle to compensate for the fact that the PWM
            // output is inverted. It's complicated, but required because of how
            // phase-correct PWM works.
            let pulse_width = FULL_DUTY_CYCLE - pulse_width;
            (FULL_DUTY_CYCLE, pulse_width, 0, FULL_DUTY_CYCLE)
        } else {
            let sample = (-(sample as i32)) as u32;
            let pulse_width = ((DRIVER_SCALE * sample) >> 15) as u16;
            // "Invert" the duty cycle to compensate for the fact that the PWM
            // output is inverted. It's complicated, but required because of how
            // phase-correct PWM works.
            let pulse_width = FULL_DUTY_CYCLE - pulse_width;
            (0, FULL_DUTY_CYCLE, FULL_DUTY_CYCLE, pulse_width)
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub enum Channel {
    ChannelA,
    ChannelB,
}

#[derive(Clone, Debug)]
pub struct SonicParams {
    waveform: Waveform,
    phase_vel: u16,
    volume: u16,
}

impl SonicParams {
    const fn new() -> Self {
        Self {
            waveform: Waveform::Off,
            phase_vel: WaveGen::MIN_PHASE_VEL,
            volume: 0,
        }
    }

    fn copy_to_wavegen(&self, wavegen: &mut WaveGen) {
        wavegen.set_waveform(self.waveform);
        wavegen.set_phase_vel(self.phase_vel);
        wavegen.set_amp(self.volume);
    }
}

/// For controlling the sonic parameters.
/// One instance of this struct holds the changes to the sonic parameters
/// to be picked up on the next execution interrupt handler.
struct SonicControls {
    channel_a_params: SonicParams,
    channel_b_params: SonicParams,
    sound_on: bool,
}

impl SonicControls {
    const fn new() -> Self {
        Self {
            channel_a_params: SonicParams::new(),
            channel_b_params: SonicParams::new(),
            sound_on: false,
        }
    }

    fn set_channel_waveform(&mut self, channel: Channel, waveform: Waveform) {
        let channel_params = match channel {
            Channel::ChannelA => &mut self.channel_a_params,
            Channel::ChannelB => &mut self.channel_b_params,
        };
        channel_params.waveform = waveform;
    }

    fn set_channel_volume(&mut self, channel: Channel, volume: u16) {
        let channel_params = match channel {
            Channel::ChannelA => &mut self.channel_a_params,
            Channel::ChannelB => &mut self.channel_b_params,
        };
        channel_params.volume = volume;
    }

    fn set_channel_phase_vel(&mut self, channel: Channel, phase_vel: u16) {
        let channel_params = match channel {
            Channel::ChannelA => &mut self.channel_a_params,
            Channel::ChannelB => &mut self.channel_b_params,
        };
        channel_params.phase_vel = phase_vel;
    }

    /// Set waveform, phase_velocity, and volume for both channels
    fn set_sound_params(
        &mut self,
        waveform: Waveform,
        phase_vel: u16,
        volume: u16,
    ) {
        self.channel_a_params.waveform = waveform;
        self.channel_a_params.phase_vel = phase_vel;
        self.channel_a_params.volume = volume;
        self.channel_b_params.waveform = waveform;
        self.channel_b_params.phase_vel = phase_vel;
        self.channel_b_params.volume = volume;
    }

    /// Controls whether the sound being generated is sent via output pins.
    /// true = turn output pins on and send the sound,
    /// false = turn output pins off.
    fn set_sound_output(&mut self, sound_on: bool) {
        self.sound_on = sound_on;
    }
}

/// Initialize simple PWM audio driver that can control two speakers, one
/// transistor per speaker (hence the "t1" part of the function name).
///
/// Initializes this module so it can execute commands to send audio output.
/// This initializes two PWM slices and an interrupt handler.
///
/// Type parameters:
///
/// S1
///     SliceId of PWM slice for driving speakers.
///     Can be inferred from the driver_pwm argument.
///
/// S2
///     SliceId of PWM slice for pacing samples.
///     Can be inferred from the pacing_pwm argument.
///
/// G1:
///     Channel A pin ID. Must be valid for channel A for PWM slice S1.
///
///     Can be inferred from the channel_a_pin argument if it is not None,
///     otherwise must be specified even if channel A will not be connected
///     (specifying a GPIO that will not be used does no harm, this is to
///     satisfy the type system.)
///
///     Example valid for PWM0: rp2040_hal::gpio::bank0::Gpio16
///
/// G2:
///     Channel B pin ID. Must be valid for channel B for PWM slice S1.
///
///     Can be inferred from the channel_b_pin argument if it is not None,
///     otherwise must be specified even if channel B will not be connected
///     (specifying a GPIO that will not be used does no harm, this is to
///     satisfy the type system.)
///
///     Example valid for PWM0: rp2040_hal::gpio::bank0::Gpio17
///
/// G3: Any valid pin ID (used for optional interrupt handler tracing)
///
/// Returns a client object that can be used to control sound via this driver.
/// Only call this function once during program execution.
///
/// See the RP2040 datasheet section 4.5.2 for which GPIO signals are valid for
/// which PWM slices and channels.
///
/// Wiring instructions: connect channel_a_pin to the base of a NPN BJT driving
/// one speaker, and channel_b_pin to the base of another NPN BJT driving a
/// different speaker.
pub fn init_t1driver<S1, S2, G1, G2, G3>(
    driver_pwm: pwm::Slice<S1, pwm::FreeRunning>,
    pacing_pwm: pwm::Slice<S2, pwm::FreeRunning>,
    channel_a_pin: Option<gpio::Pin<G1, gpio::FunctionNull, gpio::PullNone>>,
    channel_b_pin: Option<gpio::Pin<G2, gpio::FunctionNull, gpio::PullNone>>,
    mut handler_trace_pin: Option<
        gpio::Pin<G3, gpio::FunctionSioOutput, gpio::PullNone>,
    >,
) -> SonicClient
where
    S1: pwm::SliceId,
    S2: pwm::SliceId,
    G1: gpio::PinId + pwm::ValidPwmOutputPin<S1, pwm::A>,
    G2: gpio::PinId + pwm::ValidPwmOutputPin<S1, pwm::B>,
    G3: gpio::PinId + Send + 'static,
{
    // Initialize the heap so Box::new() does not panic.
    crate::heap::init();

    let driver_pwm = DynamicPwmSlice::from(driver_pwm);
    let pacing_pwm = DynamicPwmSlice::from(pacing_pwm);
    let pacing_slice_id = pacing_pwm.slice_id();

    let channel_a_pin = make_maybe_dynamic_pin(channel_a_pin);
    let channel_b_pin = make_maybe_dynamic_pin(channel_b_pin);

    let sonic_pwm = SonicPwmT1::new(
        driver_pwm,
        pacing_pwm,
        channel_a_pin,
        channel_b_pin,
        None,
        None,
    );

    /// The state used by the sonic client that controls the t1 driver.
    /// Changes are made to this structure, then picked up by the audio driver
    /// structure the next time the interrupt handler is called.
    static SONIC_CLIENT_T1: Mutex<RefCell<SonicControls>> =
        Mutex::new(RefCell::new(SonicControls::new()));

    /// Count of PWM timer ticks since the t1 driver was initialized.
    /// This will roll over after about 2.25 days.
    static TIMER_TICKS_T1: AtomicUsize = AtomicUsize::new(0);

    // Create and activate the audio driver interrupt handler
    let mut audio_driver =
        T1AudioDriver::start(sonic_pwm, &SONIC_CLIENT_T1, &TIMER_TICKS_T1);

    pwmutil::set_interrupt_handler(
        pacing_slice_id,
        Some(Box::new(move || {
            match handler_trace_pin.as_mut() {
                Some(p) => p.set_high().unwrap(),
                None => {}
            }

            audio_driver.handle_interrupt();

            match handler_trace_pin.as_mut() {
                Some(p) => p.set_low().unwrap(),
                None => {}
            }
        })),
    );

    // Tell the NVIC to start calling the PWM interrupt handler function
    pwmutil::enable_pwm_interrupts();

    SonicClient {
        controls: &SONIC_CLIENT_T1,
        counter: &TIMER_TICKS_T1,
    }
}

/// Initialize PWM audio driver controlling 4 transistors to drive 1 speaker.
/// (Hence the "t4" part of the function name.) The transistors are connected
/// in an H-bridge configuration.
///
/// Type parameters:
///
/// S1
///     SliceId of PWM slice for pacing samples.
///     Can be inferred from the pacing_pwm argument.
///
/// S2, S3
///     SliceIds of PWM slices for driving side 1 and side 2 of H-bridge.
///     Can be inferred from driver1_pwm and driver2_pwm, respectively.
///
/// G1, G2:
///     IDs of the two pins connected to driver1 PWM channels A and B.
///     Must be valid for channels A and B respectively for PWM slice S2.
///     Can be inferred from driver1_channel_a_pin and driver2_channel_b_pin.
///
/// G3, G4:
///     IDs of the two pins connected to driver2 PWM channels A and B.
///     Must be valid for channels A and B respectively for PWM slice S3.
///     Can be inferred from driver2_channel_a_pin and driver2_channel_b_pin.
///
/// G5:
///     Any valid pin ID. Used for optional interrupt handler tracing.
///     Can be inferred from handler_trace_pin.
///
/// Returns a client object that can be used to control sound via this driver.
/// Only call this function once during program execution.
///
/// See the RP2040 datasheet section 4.5.2 for which GPIO signals are valid for
/// which PWM slices and channels.
///
/// Wiring instructions: Connect 2 PNP BJTs and 2 NPN BJTs in an H-bridge.
/// Say X1 and Y1 are the gates of the PNP and NPN transistors, respectively,
/// on side 1 of the H-bridge, and X2 and Y2 are the gates of the PNP
/// and NPN transistors, respectively, on side 2 of the H-bridge:
///
/// driver1_channel_a_pin -> X1 goes to gate of side 1 PNP
/// driver1_channel_b_pin -> Y1 goes to gate of side 1 NPN
/// driver2_channel_a_pin -> X2 goes to gate of side 2 PNP
/// driver2_channel_b_pin -> Y2 goes to gate of side 2 NPN
pub fn init_t4driver<S1, S2, S3, G1, G2, G3, G4, G5>(
    pacing_pwm: pwm::Slice<S1, pwm::FreeRunning>,
    driver1_pwm: pwm::Slice<S2, pwm::FreeRunning>,
    driver1_channel_a_pin: gpio::Pin<G1, gpio::FunctionNull, gpio::PullNone>,
    driver1_channel_b_pin: gpio::Pin<G2, gpio::FunctionNull, gpio::PullNone>,
    driver2_pwm: pwm::Slice<S3, pwm::FreeRunning>,
    driver2_channel_a_pin: gpio::Pin<G3, gpio::FunctionNull, gpio::PullNone>,
    driver2_channel_b_pin: gpio::Pin<G4, gpio::FunctionNull, gpio::PullNone>,
    mut handler_trace_pin: Option<
        gpio::Pin<G5, gpio::FunctionSioOutput, gpio::PullNone>,
    >,
) -> SonicClient
where
    S1: pwm::SliceId,
    S2: pwm::SliceId,
    S3: pwm::SliceId,
    G1: gpio::PinId + pwm::ValidPwmOutputPin<S2, pwm::A>,
    G2: gpio::PinId + pwm::ValidPwmOutputPin<S2, pwm::B>,
    G3: gpio::PinId + pwm::ValidPwmOutputPin<S3, pwm::A>,
    G4: gpio::PinId + pwm::ValidPwmOutputPin<S3, pwm::B>,
    G5: gpio::PinId + Send + 'static,
{
    // Initialize the heap so Box::new() does not panic.
    crate::heap::init();

    let pacing_pwm = DynamicPwmSlice::from(pacing_pwm);
    let pacing_slice_id = pacing_pwm.slice_id();

    let driver1_pwm = DynamicPwmSlice::from(driver1_pwm);
    let driver1_channel_a_pin = make_dynamic_pin(driver1_channel_a_pin);
    let driver1_channel_b_pin = make_dynamic_pin(driver1_channel_b_pin);

    let driver2_pwm = DynamicPwmSlice::from(driver2_pwm);
    let driver2_channel_a_pin = make_dynamic_pin(driver2_channel_a_pin);
    let driver2_channel_b_pin = make_dynamic_pin(driver2_channel_b_pin);

    let sonic_pwm = SonicPwmT4::new(
        pacing_pwm,
        driver1_pwm,
        driver1_channel_a_pin,
        driver1_channel_b_pin,
        driver2_pwm,
        driver2_channel_a_pin,
        driver2_channel_b_pin,
    );

    /// The state used by the sonic client that controls the t4 driver.
    /// Changes are made to this structure, then picked up by the audio driver
    /// structure the next time the interrupt handler is called.
    static SONIC_CLIENT_T4: Mutex<RefCell<SonicControls>> =
        Mutex::new(RefCell::new(SonicControls::new()));

    /// Count of PWM timer ticks since the t4 driver was initialized.
    /// This will roll over after about 2.25 days.
    static TIMER_TICKS_T4: AtomicUsize = AtomicUsize::new(0);

    // Create and activate the audio driver interrupt handler
    let mut audio_driver =
        T4AudioDriver::start(sonic_pwm, &SONIC_CLIENT_T4, &TIMER_TICKS_T4);

    pwmutil::set_interrupt_handler(
        pacing_slice_id,
        Some(Box::new(move || {
            match handler_trace_pin.as_mut() {
                Some(p) => p.set_high().unwrap(),
                None => {}
            }

            audio_driver.handle_interrupt();

            match handler_trace_pin.as_mut() {
                Some(p) => p.set_low().unwrap(),
                None => {}
            }
        })),
    );

    // Tell the NVIC to start calling the PWM interrupt handler function
    pwmutil::enable_pwm_interrupts();

    SonicClient {
        controls: &SONIC_CLIENT_T4,
        counter: &TIMER_TICKS_T4,
    }
}

/// Turn an (optional) PWM pin into a pin whose function can be dynamically
/// changed from PWM to none (disabled). This is necessary because when you stop
/// the PWM clock you want to change the pin function from PWM to something else
/// so that the output isn't randomly stuck high or low.
fn make_maybe_dynamic_pin<G>(
    pwm_pin: Option<gpio::Pin<G, gpio::FunctionNull, gpio::PullNone>>,
) -> Option<gpio::Pin<DynPinId, gpio::DynFunction, gpio::PullNone>>
where
    G: gpio::PinId,
{
    pwm_pin.map(|p| make_dynamic_pin(p))
}

/// Turn an PWM pin into a pin whose function can be dynamically changed from
/// PWM to none (disabled). This is necessary because when you stop the PWM
/// clock you want to change the pin function from PWM to something else so that
/// the output isn't randomly stuck high or low.
fn make_dynamic_pin<G>(
    pwm_pin: gpio::Pin<G, gpio::FunctionNull, gpio::PullNone>,
) -> gpio::Pin<DynPinId, gpio::DynFunction, gpio::PullNone>
where
    G: gpio::PinId,
{
    let p = pwm_pin.into_dyn_pin();
    // SAFETY: Yes, I know I am making the pin function dynamic.
    unsafe { p.into_unchecked::<gpio::DynFunction, gpio::PullNone>() }
}

pub struct SonicClient {
    controls: &'static Mutex<RefCell<SonicControls>>,
    counter: &'static AtomicUsize,
}

/// SonicClient private interface
impl SonicClient {
    /// Change the sonic client parameters.
    /// callback: Closure that takes as a parameter a mutable reference to a
    /// sonic client object. The reference must only be used inside the callback
    /// closure. This function returns whatever the callback returns.
    fn change_params<F, R>(&self, callback: F) -> R
    where
        F: FnOnce(&mut SonicControls) -> R,
    {
        critical_section::with(|cs| {
            let ref_cell = self.controls.borrow(cs);
            let mut mut_cell = ref_cell.borrow_mut();
            callback(&mut *mut_cell)
        })
    }
}

/// SonicClient public interface
impl SonicClient {
    /// Wait for the next time the pacing PWM timer interrupt is fired.
    /// If the init function has not been called (and therefore the pacing PWM
    /// interrupt has not yet been enabled) then this will wait forever.
    pub fn wait_for_timer_tick(&self) -> bool {
        let start_ticks = self.counter.load(Ordering::SeqCst);
        while self.counter.load(Ordering::SeqCst) == start_ticks {}
        true
    }

    /// Return the number of times that the PWM interrupt handler has been called
    /// since the init function was called.
    pub fn get_timer_ticks(&self) -> usize {
        self.counter.load(Ordering::SeqCst)
    }

    /// The type of waveform to generate
    pub fn set_waveform(&self, channel: Channel, waveform: Waveform) {
        self.change_params(|client| {
            client.set_channel_waveform(channel, waveform);
        });
    }

    /// Volume scaler - in range 0..=16384
    pub fn set_volume(&self, channel: Channel, volume: u16) {
        self.change_params(|client| {
            client.set_channel_volume(channel, volume);
        });
    }

    /// Phase velocity - change in phase per sample
    /// The phase is in units such that 65536 units makes one cycle.
    pub fn set_phase_vel(&self, channel: Channel, phase_vel: u16) {
        self.change_params(|client| {
            client.set_channel_phase_vel(channel, phase_vel);
        });
    }

    /// Set frequency in units of 1/16 Hz.
    /// In other words, `freq_x16` is the frequency in Hz times 16.
    /// The highest frequency that can be set via this method is 4095.9375 Hz.
    /// For higher frequencies, or to avoid runtime overhead of converting from
    /// frequency to phase velocity, use the `set_phase_vel` method.
    pub fn set_freq(&self, channel: Channel, freq_x16: u16) {
        let phase_vel = WaveGen::freq_x16_to_phase_vel(freq_x16);
        self.set_phase_vel(channel, phase_vel);
    }

    /// Set waveform, phase_velocity, and volume for both channels
    pub fn set_sound_params(
        &self,
        waveform: Waveform,
        phase_vel: u16,
        volume: u16,
    ) {
        self.change_params(|client| {
            client.set_sound_params(waveform, phase_vel, volume);
        });
    }

    /// Control whether the sound being generated is sent over the
    /// configured pins to (presumable) speakers or amplifiers.
    /// sound_on: true to turn sound on, false to turn sound off.
    pub fn set_sound_output(&self, sound_on: bool) {
        self.change_params(|client| {
            client.set_sound_output(sound_on);
        });
    }
}
