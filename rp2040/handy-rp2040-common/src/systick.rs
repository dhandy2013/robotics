//! Set up, read, and reset the SysTick timer.
//!
//! Why create this module when cortex_m::delay::Delay exists? Because sometimes
//! I really want to get the low level tick count directly! And sometimes I want
//! to delay a number of processor clock cycles instead of wall-clock times.
use embedded_hal::delay::DelayNs;
use fugit::HertzU32;
use rp2040_hal::{self as hal};

/// Timer using the SYST clock to get maximum resolution.
/// This implements the DelayNs trait, but only the delay_ms() method attempts
/// accurate delays.
pub struct SysTick {
    syst: cortex_m::peripheral::SYST,
    frequency: HertzU32,
}

impl SysTick {
    /// Set up high resolution timing, using recommended SYST reset sequence.
    ///
    /// Set SysTick to run at full processor clock speed, otherwise by default
    /// it will run at the default 1MHz. SysTick is a 24-bit decrementing
    /// counter. The reload value is set to the maximum (0xff_ffff). When the
    /// processor is running at the default 125MHz, this gives you 134.2
    /// milliseconds before SysTick wraps.
    ///
    /// Takes ownership of the SYST peripheral because it will be configuring
    /// and resetting it.  Use `free()` to get SYST back.
    ///
    /// Takes a `frequency` parameter for use in converting from real-time units
    /// (ms) to system tick counts.
    ///
    /// Example:
    /// ```rust
    /// use handy_rp2040_common::{chip::Chip, systick::SysTick};
    ///
    /// let mut chip = Chip::new();
    /// let mut systick = SysTick::new(
    ///     chip.core.SYST,
    ///     chip.clocks.system_clock.freq()
    /// );
    /// systick.clear_current();
    /// ```
    pub fn new(syst: cortex_m::peripheral::SYST, frequency: HertzU32) -> Self {
        let mut this = Self { syst, frequency };
        this.init();
        this
    }

    fn init(&mut self) {
        self.syst.disable_counter();
        self.syst.set_reload(0x00ff_ffff);
        self.syst.clear_current();
        self.syst
            .set_clock_source(cortex_m::peripheral::syst::SystClkSource::Core);
        self.syst.enable_counter();
    }

    /// Return the reload value that the SysTick counter is set to after it
    /// reaches zero. The default is 0x00ff_ffff.
    pub fn get_reload() -> u32 {
        hal::pac::SYST::get_reload()
    }

    /// Set the reload valie that the SysTick counter is set to after it
    /// reaches zero. The maximum is 0x00ff_ffff.
    pub fn set_reload(&mut self, value: u32) {
        self.syst.set_reload(value);
    }

    /// Return the system clock frequency that this SysTick object was given.
    pub fn frequency(&self) -> HertzU32 {
        self.frequency
    }

    /// Consume this SysTick object and return the captured SYST resource.
    pub fn free(self) -> cortex_m::peripheral::SYST {
        self.syst
    }

    /// Enable the SysTick counter. Note: the new() method starts it out
    /// enabled.  You only need to call this if you called disable_counter().
    pub fn enable_counter(&mut self) {
        self.syst.enable_counter();
    }

    /// Disable the SysTick counter.
    pub fn disable_counter(&mut self) {
        self.syst.disable_counter();
    }

    /// Return true iff the SysTick counter is enabled. This takes a &mut self
    /// parameter because it has side effects and can clear the bit that
    /// indicates the timer has wrapped.
    pub fn is_counter_enabled(&mut self) -> bool {
        self.syst.is_counter_enabled()
    }

    /// Get the current SysTick timer count. The SysTick timer counts _down_.
    /// This function takes two clock cycles minimum.
    #[inline(always)]
    pub fn get_current() -> u32 {
        hal::pac::SYST::get_current()
    }

    /// Reset the the SysTick timer count to zero, so that it wraps to the reload
    /// value (0xff_ffff) on the next cycle (because this timer counts _down_).
    #[inline(always)]
    pub fn clear_current(&mut self) {
        self.syst.clear_current();
    }

    /// Return true iff the SysTick timer has wrapped around since the last reset.
    /// *NOTE* This takes `&mut self` because the read operation is side effectful
    /// and will clear the bit of the read register.
    #[inline(always)]
    pub fn has_wrapped(&mut self) -> bool {
        self.syst.has_wrapped()
    }

    /// Return the maximum number of milliseconds in 0xffff_ffff SysTick counts.
    /// When the system clock is 125MHz this value is 34_359 ms.
    pub fn max_ms_delay(&self) -> u16 {
        let max_ms = (1000u64 * 0xffff_ffffu64) / self.frequency.to_Hz() as u64;
        max_ms.min(0xffff) as u16
    }

    /// Convert a number of milliseconds to a number of SysTick counts.
    /// The return value is silently limited to 0xffff_ffff.
    pub fn ms_to_ticks(&self, ms: u32) -> u32 {
        let ticks = (ms as u64 * self.frequency.to_Hz() as u64) / 1000u64;
        ticks.min(0xffff_ffff) as u32
    }

    /// Delay the given number of SysTick counts. This resets the counter.
    pub fn delay_ticks(&mut self, ticks: u32) {
        let full_cycles = ticks >> 24;
        if full_cycles > 0 {
            self.syst.clear_current();

            for _ in 0..full_cycles {
                while !self.syst.has_wrapped() {}
            }
        }

        let ticks = ticks & 0x00ff_ffff;
        if ticks > 1 {
            self.syst.set_reload(ticks - 1);
            self.syst.clear_current();

            while !self.syst.has_wrapped() {}

            self.syst.set_reload(0x00ff_ffff);
            self.syst.clear_current();
        }
    }
}

impl DelayNs for SysTick {
    fn delay_ns(&mut self, ns: u32) {
        self.delay_ms(ns / 1000_000)
    }

    fn delay_us(&mut self, us: u32) {
        self.delay_ms(us / 1000)
    }

    #[inline]
    fn delay_ms(&mut self, ms: u32) {
        let ticks = self.ms_to_ticks(ms);
        self.delay_ticks(ticks);
    }
}
