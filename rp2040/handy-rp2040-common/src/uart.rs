//! RP2040 UART helper module
//! This contains code that I use in most RP2040 programs for debug output
//! and control.
use cortex_m::peripheral::NVIC;
use fugit::HertzU32;
use hal::uart::{ValidPinIdRx, ValidPinIdTx};
use rp2040_hal::{
    self as hal,
    gpio::{
        bank0::{Gpio0, Gpio1, Gpio4, Gpio5},
        DefaultTypeState, FunctionUart, Pin, PullDown,
    },
    pac,
    uart::{Reader, UartPeripheral, Writer},
};

/// The size of each UART input and output FIFO.
/// RP2040 datasheet section 4.2: "Separate 32x8 Tx and 32x12 Rx FIFOs"
pub const UART_BUFFER_SIZE: usize = 32;

/// UART0 is the serial interface I use for debug messages from core 0
/// and these are the pins I usually assign to it.
pub type Uart0Type = UartPeripheral<
    hal::uart::Enabled,
    hal::pac::UART0,
    (
        Pin<Gpio0, FunctionUart, PullDown>,
        Pin<Gpio1, FunctionUart, PullDown>,
    ),
>;

pub type Uart0Reader = Reader<
    hal::pac::UART0,
    (
        Pin<Gpio0, FunctionUart, PullDown>,
        Pin<Gpio1, FunctionUart, PullDown>,
    ),
>;

pub type Uart0Writer = Writer<
    hal::pac::UART0,
    (
        Pin<Gpio0, FunctionUart, PullDown>,
        Pin<Gpio1, FunctionUart, PullDown>,
    ),
>;

/// UART1 is the serial interface I use for debug messages from core 1
/// and these are the pins I usually assign to it.
pub type Uart1Type = UartPeripheral<
    hal::uart::Enabled,
    hal::pac::UART1,
    (
        hal::gpio::Pin<Gpio4, FunctionUart, PullDown>,
        hal::gpio::Pin<Gpio5, FunctionUart, PullDown>,
    ),
>;

/// Initialize UART0 the way I usually do for debug output, connected
/// to GPIO0 (RP2040 tx/host rx) and GPIO1 (RP2040 rx/host tx). The baud rate
/// is 115200.
///
/// Typical use:
///
/// ```rust
/// let mut uart0 = init_uart0(
///     peripherals.UART0,
///     pins.gpio0,
///     pins.gpio1,
///     &mut peripherals.RESETS,
///     clocks.peripheral_clock.freq(),
/// );
/// writeln!(uart0, "Hello, world!\r").unwrap();
/// ```
pub fn init_uart0(
    uart0_raw: pac::UART0,
    gpio0: hal::gpio::Pin<
        Gpio0,
        <Gpio0 as DefaultTypeState>::Function,
        <Gpio0 as DefaultTypeState>::PullType,
    >,
    gpio1: hal::gpio::Pin<
        Gpio1,
        <Gpio1 as DefaultTypeState>::Function,
        <Gpio1 as DefaultTypeState>::PullType,
    >,
    resets: &mut pac::RESETS,
    peripheral_clock_freq: HertzU32,
) -> Uart0Type {
    let uart0_pins = (
        // UART TX (characters sent from RP2040) on pin 1 (GPIO0)
        gpio0.into_function::<hal::gpio::FunctionUart>(),
        // UART RX (characters received by RP2040) on pin 2 (GPIO1)
        gpio1.into_function::<hal::gpio::FunctionUart>(),
    );
    let uart0: Uart0Type =
        hal::uart::UartPeripheral::new(uart0_raw, uart0_pins, resets)
            .enable(
                // 115200 baud, 8 data bits, 1 stop bit, no parity
                hal::uart::UartConfig::default(),
                peripheral_clock_freq,
            )
            .unwrap();
    uart0
}

/// Enable UART0 interrupts generally.
///
/// This does not enable interrupts for any particular UART0 event. To do that,
/// in addition you must call methods like:
/// - `UartPeripheral::enable_rx_interrupt`
/// - `UartPeripheral::enable_tx_interrupt`.
pub fn enable_uart0_interrupts() {
    unsafe {
        NVIC::unmask(pac::Interrupt::UART0_IRQ);
    }
}

/// Cause the UART0_IRQ interrupt handler to be called
pub fn pend_uart0_intr() {
    NVIC::pend(pac::Interrupt::UART0_IRQ);
}

/// Clear UART0 transmit interrupt flag.
///
/// If you have called `enable_tx_interrupt()` on the UART0 device, then you
/// need to call this function inside the UART0_IRQ interrupt handler or else
/// you will get an infinite loop where the interrupt handler is immediately
/// re-invoked every time it exits.
pub fn clear_uart0_tx_intr() {
    // Get an (unsafe) duplicate instance of pac::uart0::RegisterBlock so I can
    // access low-level registers not surfaced by UartPeripheral. See RP2040
    // Datasheet section 4.2.8 List of Registers (UART) for details on these
    // registers.
    let uart0 = unsafe { pac::UART0::steal() };
    // "Write to clear" register - write a 1 to a bit to clear that interrupt
    let uarticr = uart0.uarticr(); // Interrupt Clear Register, UARTICR
    uarticr.modify(|_r, w| {
        // Transmit interrupt clear. Clears the UARTTXINTR interrupt
        w.txic().clear_bit_by_one();
        w
    });
}

/// Return true iff the UART0 transmit FIFO is not empty or the transmit shift
/// register is not empty or the last stop bit has not yet been sent.
pub fn uart0_is_busy() -> bool {
    // Get an (unsafe) duplicate instance of pac::uart0::RegisterBlock so I can
    // access low-level registers not surfaced by UartPeripheral. See RP2040
    // Datasheet section 4.2.8 List of Registers (UART) for details on these
    // registers.
    //
    // SAFETY: It is Ok for multiple threads/cores to read this at the same
    // time.
    let uart0 = unsafe { pac::UART0::steal() };
    uart0.uartfr().read().busy().bit_is_set()
}

pub type UnconfiguredUart1TxPin<I> =
    hal::gpio::Pin<I, hal::gpio::FunctionNull, hal::gpio::PullDown>;
pub type UnconfiguredUart1RxPin<I> =
    hal::gpio::Pin<I, hal::gpio::FunctionNull, hal::gpio::PullDown>;

pub type ConfiguredUart1TxPin<I> =
    hal::gpio::Pin<I, hal::gpio::FunctionUart, hal::gpio::PullDown>;
pub type ConfiguredUart1RxPin<I> =
    hal::gpio::Pin<I, hal::gpio::FunctionUart, hal::gpio::PullDown>;

/// Initialize UART1 connected to any compatiable RP2040 tx and rx pins. The
/// baud rate is 115200.
///
/// Typical use:
///
/// ```rust
/// let mut uart1 = init_uart1(
///     peripherals.UART1,
///     pins.gpio4,
///     pins.gpio5,
///     &mut peripherals.RESETS,
///     clocks.peripheral_clock.freq(),
/// );
/// writeln!(uart1, "Hello, world!\r").unwrap();
/// ```
pub fn init_uart1<I1, I2>(
    uart1_raw: pac::UART1,
    uart1_tx: UnconfiguredUart1TxPin<I1>,
    uart1_rx: UnconfiguredUart1RxPin<I2>,
    resets: &mut pac::RESETS,
    peripheral_clock_freq: HertzU32,
) -> UartPeripheral<
    hal::uart::Enabled,
    pac::UART1,
    (ConfiguredUart1TxPin<I1>, ConfiguredUart1RxPin<I2>),
>
where
    I1: hal::gpio::PinId
        + hal::gpio::ValidFunction<FunctionUart>
        + ValidPinIdTx<pac::UART1>,
    I2: hal::gpio::PinId
        + hal::gpio::ValidFunction<FunctionUart>
        + ValidPinIdRx<pac::UART1>,
{
    let uart1_pins = (
        // UART TX (characters sent from RP2040)
        uart1_tx.into_function::<hal::gpio::FunctionUart>(),
        // UART RX (characters received by RP2040)
        uart1_rx.into_function::<hal::gpio::FunctionUart>(),
    );
    let uart1 = hal::uart::UartPeripheral::new(uart1_raw, uart1_pins, resets)
        .enable(
            // 115200 baud, 8 data bits, 1 stop bit, no parity
            hal::uart::UartConfig::default(),
            peripheral_clock_freq,
        )
        .unwrap();
    uart1
}
