//! Button-related utility code
use embedded_hal::{delay::DelayNs, digital::InputPin};
use rp2040_hal::{self as hal, gpio};

/// GPIO22 is what I use for the application button, for example to wait for
/// the user to press a button before running a demo.
pub type ButtonType = hal::gpio::Pin<
    hal::gpio::bank0::Gpio22,
    hal::gpio::FunctionSioInput,
    gpio::PullUp,
>;

/// Return true iff the pullup input pin has been pulled low,
/// presumably by a pushbutton.
#[inline]
pub fn is_button_pressed<G: gpio::PinId>(
    button_pin: &mut gpio::Pin<G, gpio::FunctionSioInput, gpio::PullUp>,
) -> bool {
    button_pin.is_low().unwrap()
}

/// Wait until pullup input pin goes low, presumably because
/// a pushbutton connected to it has been pressed.
pub fn wait_for_button_press<G: gpio::PinId>(
    button_pin: &mut gpio::Pin<G, gpio::FunctionSioInput, gpio::PullUp>,
) {
    wait_for_button_press_with_callback(button_pin, &mut || true);
}

/// Wait until pullup input pin goes low  (presumably because
/// a pushbutton connected to it has been pressed) or until
/// ``callback`` returns ``false``, whichever happens first.
/// Return the last return value from ``callback``, which will
/// be called at least once.
pub fn wait_for_button_press_with_callback<G, CB>(
    button_pin: &mut gpio::Pin<G, gpio::FunctionSioInput, gpio::PullUp>,
    callback: &mut CB,
) -> bool
where
    G: gpio::PinId,
    CB: FnMut() -> bool,
{
    loop {
        if !callback() {
            break false;
        }
        if is_button_pressed(button_pin) {
            break true;
        }
    }
}

/// Wait until pullup input pin goes high for at least 10ms,
/// presumably because the connected pushbutton has been released.
pub fn wait_for_button_release<G: gpio::PinId, D: DelayNs>(
    button_pin: &mut gpio::Pin<G, gpio::FunctionSioInput, gpio::PullUp>,
    delay: &mut D,
) {
    wait_for_button_release_with_callback(button_pin, delay, &mut || true);
}

/// Wait until pullup input pin goes high for at least 10ms
/// (presumably because the connected pushbutton has been released)
/// or until ``callback`` returns ``false``, whichever happens first.
/// Return the last return value from ``callback``. ``callback`` will
/// be called at approximately 1ms intervals and is guaranteed to be
/// called at least once.
pub fn wait_for_button_release_with_callback<G, D, CB>(
    button_pin: &mut gpio::Pin<G, gpio::FunctionSioInput, gpio::PullUp>,
    delay: &mut D,
    callback: &mut CB,
) -> bool
where
    G: gpio::PinId,
    D: DelayNs,
    CB: FnMut() -> bool,
{
    const DEBOUNCE_THRESHOLD_MS: u8 = 10;
    let mut debounce_ms_count: u8 = 0;
    loop {
        if !callback() {
            break false;
        }

        debounce_ms_count = if is_button_pressed(button_pin) {
            0
        } else {
            debounce_ms_count + 1
        };
        if debounce_ms_count >= DEBOUNCE_THRESHOLD_MS {
            break true;
        }

        delay.delay_ms(1);
    }
}
