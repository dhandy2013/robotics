#!/usr/bin/python3
"""
Read and display values from an MCP3202 12-bit A/D converter.

Pin connections:
    ------- --- -----------
    MCP3202 RPi Description
    pin     pin
    ------- --- -----------
    1       24  #CS/SHDN (CE0)
    2       -   CH0 (analog input 0)
    3       -   CH1 (analog input 1)
    4       6   VSS (0V Ground)
    5       19  DIN (MOSI - data from RPi to ADC)
    6       21  DOUT (MISO - data from ADC to RPi)
    7       23  CLK (SCLK)
    8       2   VDD/VREF (5V)

Run this program as user root.
For loopback testing, connect RPi pin 19 (MOSI) to pin 21 (MISO).

This program uses libbcm2835:
    https://github.com/mubeta06/py-libbcm2835
"""

import collections
import ctypes
import os
import sys
import time

dt = 0.100 # time step in seconds
sample_window_size = 10

from libbcm2835._bcm2835 import (
        bcm2835_init, 
        bcm2835_spi_begin,
        bcm2835_spi_setBitOrder, BCM2835_SPI_BIT_ORDER_MSBFIRST,
        bcm2835_spi_setDataMode, BCM2835_SPI_MODE0,
        bcm2835_spi_setClockDivider, BCM2835_SPI_CLOCK_DIVIDER_65536,
        bcm2835_spi_chipSelect, BCM2835_SPI_CS0, BCM2835_SPI_CS1,
        bcm2835_spi_setChipSelectPolarity, BCM2835_SPI_CS0, LOW,
        bcm2835_spi_transfer,
        bcm2835_spi_transfernb,
        bcm2835_spi_end,
        bcm2835_close,
        )
from libbcm2835 import _bcm2835


def setup(clock_divider=1024):
    """
    Set up Raspberry Pi for SPI communication

    clock_divider: Number used to control SPI clock speed
        1024 = 4.096us = 244.140625kHz 

    """
    if not bcm2835_init():
        raise Exception("Error initializing bcm2835 module")
    bcm2835_spi_begin()
    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST) # The default
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE0) # The default
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW) # the default
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS1, LOW) # the default
    bcm2835_spi_setClockDivider(clock_divider)


def select_chip(chip_num):
    """
    Call setup before calling this function.
    Selects which SPI-controlled chip to access.
    chip_num: 0 for CS0, 1 for CS1
    """
    if not chip_num:
        bcm2835_spi_chipSelect(BCM2835_SPI_CS0)
    else:
        bcm2835_spi_chipSelect(BCM2835_SPI_CS1)


def cleanup():
    bcm2835_spi_end()
    bcm2835_close()


def read_sample(channel):
    channel = channel & 0x01
    control = 0b10100000 | (channel << 6)
    # Send/receive a 3-byte chunk to the SPI port
    write_data = bytes([0b00000001, control, 0b00000000])
    read_data = ctypes.create_string_buffer(len(write_data))
    bcm2835_spi_transfernb(write_data, read_data, len(write_data))
    sample = ((read_data.raw[1] & 0b00001111) << 8) + read_data.raw[2]
    return sample


def main():
    setup()
    select_chip(0)
    num_samples = 0
    ave0 = 0
    ave1 = 0
    sample0_window = collections.deque()
    sample1_window = collections.deque()
    print(" ch0    ave  ch1    ave")
    t0 = time.time()
    try:
        try:
            while True:
                sample0 = read_sample(0)
                sample1 = read_sample(1)
                sample0_window.append(sample0)
                sample1_window.append(sample1)
                num_samples += 1
                if num_samples > sample_window_size:
                    sample0_window.popleft()
                    sample1_window.popleft()
                if (num_samples % sample_window_size) == 0:
                    ave0 = sum(sample0_window) / sample_window_size
                    ave1 = sum(sample1_window) / sample_window_size
                print('\r{0:4d} {1:6.1f} {2:4d} {3:6.1f}'.format(
                        sample0, ave0, sample1, ave1), end='')
                t = t0 + (num_samples * dt)
                while time.time() < t:
                    pass
        except KeyboardInterrupt:
            print("\nGoodbye.")
    finally:
        cleanup()


if __name__ == '__main__':
    sys.exit(main())

# end-of-file
