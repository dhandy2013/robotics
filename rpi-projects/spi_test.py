#!/usr/bin/python3
"""
Test the Raspberry Pi SPI interface

Run this program as user root.
For loopback testing, connect RPi pin 19 (MOSI) to pin 21 (MISO).

This program uses libbcm2835:
    https://github.com/mubeta06/py-libbcm2835
"""

import ctypes
import os
import sys

from libbcm2835._bcm2835 import (
        bcm2835_init, 
        bcm2835_spi_begin,
        bcm2835_spi_setBitOrder, BCM2835_SPI_BIT_ORDER_MSBFIRST,
        bcm2835_spi_setDataMode, BCM2835_SPI_MODE0,
        bcm2835_spi_setClockDivider, BCM2835_SPI_CLOCK_DIVIDER_65536,
        bcm2835_spi_chipSelect, BCM2835_SPI_CS0,
        bcm2835_spi_setChipSelectPolarity, BCM2835_SPI_CS0, LOW,
        bcm2835_spi_transfer,
        bcm2835_spi_transfernb,
        bcm2835_spi_end,
        bcm2835_close,
        )
from libbcm2835 import _bcm2835


def setup():
    if not bcm2835_init():
        raise Exception("Error initializing bcm2835 module")

    # 1024 = 4.096us = 244.140625kHz 
    clock_divider = _bcm2835.BCM2835_SPI_CLOCK_DIVIDER_1024

    bcm2835_spi_begin()
    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST) # The default
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE0) # The default
    bcm2835_spi_setClockDivider(clock_divider)
    bcm2835_spi_chipSelect(BCM2835_SPI_CS0) # The default
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW) # the default


def cleanup():
    bcm2835_spi_end()
    bcm2835_close()


def main():
    setup()
    try:
        # Send a byte to the slave and simultaneously read a byte back from the
        # slave -- if you tie MISO to MOSI, you should read back what was sent.
        write_data = 0x23
        print("Sending to SPI: {0:02X}h".format(write_data))
        read_data = bcm2835_spi_transfer(write_data);
        print("Read from SPI : {0:02X}h".format(read_data))

        # Send/receive a 3-byte chunk to the SPI port
        write_data = b'abc'
        print("Sending to SPI:", write_data)
        read_data = ctypes.create_string_buffer(len(write_data))
        bcm2835_spi_transfernb(write_data, read_data, len(write_data))
        print("Received from SPI:", read_data.raw)
    finally:
        cleanup()

    print("Goodbye.")


if __name__ == '__main__':
    sys.exit(main())

# end-of-file
