#!/usr/bin/python3
"""
Read accelerometer values from an A/D converter and send them over a TCP
connection.

The A/D converter reports 12-bit values. The accelerometer measures +/- 2g
acceleration. This program converts the raw binary values to decimal
acceleration values measured in "g" units.

Pin connections MCP3202 12-bit A/D converter::

    ------- --- -----------
    MCP3202 RPi Description
    pin     pin
    ------- --- -----------
    1       24  #CS/SHDN (CE0)
    2       -   CH0 (analog input 0)
    3       -   CH1 (analog input 1)
    4       6   VSS (0V Ground)
    5       19  DIN (MOSI - data from RPi to ADC)
    6       21  DOUT (MISO - data from ADC to RPi)
    7       23  CLK (SCLK)
    8       2   VDD/VREF (5V)

Run this program as user root.
For loopback testing, connect RPi pin 19 (MOSI) to pin 21 (MISO).

This program uses libbcm2835:
    https://github.com/mubeta06/py-libbcm2835
"""

import collections
import configparser
import ctypes
import os
import signal
import socket
import sys
import threading
import time

from libbcm2835._bcm2835 import (
        bcm2835_init, 
        bcm2835_spi_begin,
        bcm2835_spi_setBitOrder, BCM2835_SPI_BIT_ORDER_MSBFIRST,
        bcm2835_spi_setDataMode, BCM2835_SPI_MODE0,
        bcm2835_spi_setClockDivider, BCM2835_SPI_CLOCK_DIVIDER_65536,
        bcm2835_spi_chipSelect, BCM2835_SPI_CS0, BCM2835_SPI_CS1,
        bcm2835_spi_setChipSelectPolarity, BCM2835_SPI_CS0, LOW,
        bcm2835_spi_transfer,
        bcm2835_spi_transfernb,
        bcm2835_spi_end,
        bcm2835_close,
        )
from libbcm2835 import _bcm2835

#
# Network config
#

tcp_port = 9000

#
# Timing control
#

sample_interval = 0.100 # seconds
sample_window_size = 10

#
# ADC range
#

num_adc_bits = 12
raw_range = (2**num_adc_bits) - 1


# Hardware interface

def setup(clock_divider=1024):
    """
    Set up Raspberry Pi for SPI communication

    clock_divider: Number used to control SPI clock speed
        1024 = 4.096us = 244.140625kHz 

    """
    if not bcm2835_init():
        raise Exception("Error initializing bcm2835 module")
    bcm2835_spi_begin()
    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST) # The default
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE0) # The default
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW) # the default
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS1, LOW) # the default
    bcm2835_spi_setClockDivider(clock_divider)


def select_chip(chip_num):
    """
    Call setup before calling this function.
    Selects which SPI-controlled chip to access.
    chip_num: 0 for CS0, 1 for CS1
    """
    if not chip_num:
        bcm2835_spi_chipSelect(BCM2835_SPI_CS0)
    else:
        bcm2835_spi_chipSelect(BCM2835_SPI_CS1)


def hardware_cleanup():
    bcm2835_spi_end()
    bcm2835_close()


def read_sample(channel):
    channel = channel & 0x01
    control = 0b10100000 | (channel << 6)
    # Send/receive a 3-byte chunk to the SPI port
    write_data = bytes([0b00000001, control, 0b00000000])
    read_data = ctypes.create_string_buffer(len(write_data))
    bcm2835_spi_transfernb(write_data, read_data, len(write_data))
    sample = ((read_data.raw[1] & 0b00001111) << 8) + read_data.raw[2]
    return sample


server_sock = None
client_connections = []

def listen_for_clients():
    server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_sock.bind(('', tcp_port))
    server_sock.listen(2)
    print("\nListening on TCP port", tcp_port)
    while server_sock:
        conn, addr = server_sock.accept()
        print("\nConnection from", addr)
        client_connections.append((conn, addr))
    print('\nDone listening on TCP port', tcp_port)


def send_accelerations_to_clients(a0, a1):
    msg = ('{0:+.3f} {1:+.3f}\n'.format(a0, a1)).encode('ascii')
    # Make a working copy of the list of connections so we can modify the
    # original list inside the "for" loop.
    connections = client_connections[:]
    for conn_info in connections:
        conn, addr = conn_info
        try:
            conn.sendall(msg)
        except socket.error:
            print("Disconnected", addr)
            client_connections.remove(conn_info)


def network_cleanup():
    global server_sock
    sock = server_sock
    server_sock = None
    connections = client_connections[:]
    client_connections[:] = []
    for conn in client_connections:
        conn.close()
    if sock:
        try:
            sock.close()
        except socket.error:
            pass # ignore errors


class SampleConverter:

    def __init__(self):
        self.reload()

    def reload(self):
        self.channel_converters = []
        self.config = config = configparser.ConfigParser()
        for channel_num in range(2):
            channel_name = 'chan{0}'.format(channel_num)
            config.add_section(channel_name)
            config.set(channel_name, 'cal_p1g_raw', str(raw_range * 0.750))
            config.set(channel_name, 'cal_zrg_raw', str(raw_range * 0.500))
            config.set(channel_name, 'cal_m1g_raw', str(raw_range * 0.250))
        config_filename = 'accel_server.ini'
        if os.path.isfile(config_filename):
            config.read(config_filename)
        for channel_num in range(2):
            channel_name = 'chan{0}'.format(channel_num)
            cal_p1g_raw = config.getfloat(channel_name, 'cal_p1g_raw')
            cal_zrg_raw = config.getfloat(channel_name, 'cal_zrg_raw')
            cal_m1g_raw = config.getfloat(channel_name, 'cal_m1g_raw')
            self.channel_converters.append(self._make_converter_func(
                                        cal_p1g_raw, cal_zrg_raw, cal_m1g_raw))

    def _make_converter_func(self, cal_p1g_raw, cal_zrg_raw, cal_m1g_raw):
        def raw_sample_to_g(raw):
            if raw >= cal_zrg_raw:
                return (raw - cal_zrg_raw) / (cal_p1g_raw - cal_zrg_raw)
            else:
                return -((raw - cal_zrg_raw) / (cal_m1g_raw - cal_zrg_raw))
        return raw_sample_to_g

    def raw_sample_to_g(self, channel_num, raw):
        return self.channel_converters[channel_num](raw)

    def dump_config(self, fp=None):
        if fp is None:
            fp = sys.stdout
        self.config.write(fp)


def reload_config(signum, stackframe):
    """Reload calibration.
    To cause this function to be called:
        ps -ef | grep python # find the process id
        kill -HUP <process-id>
    """
    print("\nReloading calibration")
    converter.reload()
    converter.dump_config()
    

def main():
    global converter
    converter = SampleConverter()
    converter.dump_config()
    signal.signal(signal.SIGHUP, reload_config)
    #####
    setup()
    select_chip(0)
    thread = threading.Thread(target=listen_for_clients)
    thread.daemon = True
    thread.start()
    #####
    num_samples = 0
    ave0 = 0
    ave1 = 0
    sample0_window = collections.deque()
    sample1_window = collections.deque()
    print(" ch0    ave  ch1    ave")
    t0 = time.time()
    try:
        try:
            while True:
                sample0 = read_sample(0)
                sample1 = read_sample(1)
                sample0_window.append(sample0)
                sample1_window.append(sample1)
                num_samples += 1
                if num_samples > sample_window_size:
                    sample0_window.popleft()
                    sample1_window.popleft()
                if (num_samples % sample_window_size) == 0:
                    ave0 = sum(sample0_window) / sample_window_size
                    ave1 = sum(sample1_window) / sample_window_size
                    print('\r{0:4d} {1:6.1f} {2:4d} {3:6.1f}'.format(
                            sample0, ave0, sample1, ave1), end='')
                a0 = converter.raw_sample_to_g(0, sample0)
                a1 = converter.raw_sample_to_g(1, sample1)
                send_accelerations_to_clients(a0, a1)
                t = t0 + (num_samples * sample_interval)
                while time.time() < t:
                    pass
        except KeyboardInterrupt:
            print("\nCtrl-C pressed")
    finally:
        hardware_cleanup()
        network_cleanup()
    time.sleep(1.0) # wait a little longer for network cleanup
    print("Goodbye.")


if __name__ == '__main__':
    sys.exit(main())

# end-of-file
