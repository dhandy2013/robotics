#!/usr/bin/python3
"""
Pulse an LED on and off gradually using hardware PWM

Note:
This program was translated from the demo pwm.c source file at:

    http://www.airspayce.com/mikem/bcm2835/pwm_8c-example.html

It did not behave as I expected. It played a loud tone on the headphones
plugged in to the Raspberry Pi's audio jack.
"""

import sys
import time

from libbcm2835._bcm2835 import *

output_pin = RPI_GPIO_P1_12 # GPIO 18
pwm_channel = 0
pwm_range = 1024

# Timing
frequency = 20.0
duty_cycle = 50.0


def setup():
    if (not bcm2835_init()):
        raise Exception("Could not initialize bcm2835")

    # Set the output pin to Alt Fun 5, to allow PWM channel 0 to be output
    # there.
    bcm2835_gpio_fsel(output_pin, BCM2835_GPIO_FSEL_ALT5)

    # Clock divider is set to 16.
    # With a devider of 16 and pwm_range of 1024, in MARKSPACE mode,
    # the pulse repetition frequency will be
    # 1.2MHz/1024 = 1171.875Hz, suitable for driveing a DC motor with PWM
    bcm2835_pwm_set_clock(BCM2835_PWM_CLOCK_DIVIDER_16)
    bcm2835_pwm_set_mode(pwm_channel, 1, 1)
    bcm2835_pwm_set_range(pwm_channel, pwm_range)


def cleanup():
    bcm2835_close()


def loop():
    print("Blinking LED connected to GPIO", output_pin)
    try:
        # Vary the PWM m/s ratio between 1/RANGE and (RANGE-1)/RANGE
        direction = 1
        data = 1
        while True:
            if data == 1:
                direction = 1
            elif data == (pwm_range - 1):
                direction = -1
            data += direction
            bcm2835_pwm_set_data(pwm_channel, data)
            bcm2835_delay(50)
    except KeyboardInterrupt:
        print("\nStopped.")


def main():
    setup()
    try:
        loop()
    finally:
        cleanup()


if __name__ == '__main__':
    sys.exit(main())

# end-of-file
