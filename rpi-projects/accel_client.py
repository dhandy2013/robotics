#!/usr/bin/python3
"""
Accelerometer test client program
"""

import socket
import sys

tcp_port = 9000


def main():
    if len(sys.argv) < 2:
        print("You must supply an IP address on the command line.")
        return 1
    server_ip_addr = sys.argv[1]
    print("Connecting to TCP port", tcp_port, "at IP/hostname",
            server_ip_addr)
    conn = socket.create_connection((server_ip_addr, tcp_port))
    f = conn.makefile()
    try:
        for msg in f:
            print(msg.strip())
    except KeyboardInterrupt:
        print("\nCtrl-C pressed")


if __name__ == '__main__':
    sys.exit(main())

# end-of-file
