#!/usr/bin/python3
"""
Blink an LED on and off rapidly
"""

import sys
import time

import RPi.GPIO as GPIO

# Pin numbers from RPi point of view
output_pin = 12 # GPIO 18

# Timing
frequency = 2.0
duty_cycle = 50.0

def setup():
    # Set mode to use RPi board's pin numbers
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(output_pin, GPIO.OUT)

def cleanup():
    GPIO.output(output_pin, 0)
    GPIO.cleanup()

def loop():
    print("Blinking LED connected to RPi pin", output_pin)
    pwm = GPIO.PWM(output_pin, frequency)
    pwm.start(duty_cycle)
    try:
        while True:
            time.sleep(0.1)
    except KeyboardInterrupt:
        pwm.stop()
        print("\nStopped.")

def main():
    setup()
    try:
        loop()
    finally:
        cleanup()

if __name__ == '__main__':
    sys.exit(main())

# end-of-file
