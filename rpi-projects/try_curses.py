"""
Try using the curses module in Python 3
"""

import curses
import locale
import sys
import time

LANG = 'en_US.UTF-8'


def run(stdscr):
    curses.curs_set(False)
    curses.init_pair(1, curses.COLOR_YELLOW, curses.COLOR_BLUE)
    stdscr.attron(curses.color_pair(1))
    stdscr.clear()
    height, width = stdscr.getmaxyx()
    for y in range(height):
        for x in range(width):
            try:
                stdscr.addch(y, x, ord(' '))
            except curses.error:
                # Exception when writing to bottom right cell
                pass
    stdscr.addstr(0, 0, 'Hello curses')
    stdscr.nodelay(True)
    y, x = height // 2, width // 2
    stdscr.addch(y, x, '*')
    stdscr.refresh()
    while True:
        ch = stdscr.getch()
        if ch == -1:
            stdscr.addstr(1, 0, time.asctime())
            time.sleep(0.1)
            continue
        stdscr.addch(y, x, ' ')
        if ch == 0x1b: # Esc
            break
        elif ch == curses.KEY_UP:
            y = y - 1
        elif ch == curses.KEY_DOWN:
            y = y + 1
        elif ch == curses.KEY_LEFT:
            x = x - 1
        elif ch == curses.KEY_RIGHT:
            x += 1
        stdscr.addch(y, x, '*')
        stdscr.refresh()


def main():
    locale.setlocale(locale.LC_ALL, LANG)
    curses.wrapper(run)


if __name__ == '__main__':
    sys.exit(main())

# end-of-file
