#!/usr/bin/python3
"""
Robot Driver Program
"""

import sys
import time
import random
import RPi.GPIO as GPIO


# Pin numbers from RPi point of view
GPIO17 = 11
GPIO27 = 13
GPIO22 = 15
output_pins = [GPIO17, GPIO27, GPIO22]

# Output pin indices
LEFT = 0
MIDDLE = 1
RIGHT = 2


def setup():
    # Set mode to use RPi board's pin numbers
    GPIO.setmode(GPIO.BOARD)
    for output_pin in output_pins:
        GPIO.setup(output_pin, GPIO.OUT)
        GPIO.output(output_pin, 0)


def cleanup():
    print('cleanup')
    for output_pin in output_pins:
        print('Setting RPi pin', output_pin, 'to 0v')
        GPIO.output(output_pin, 0)
    if False:
        GPIO.cleanup()
    else:
        print('Leaving output pins set to ground')

def celebrate():
    for i in range(50):
        t = random.randint(0,3)
        if t == 0:
            turn_left()
        elif t == 1:
            turn_right()
        elif t == 2:
            go_forward()
        elif t == 3:
            go_backward()
        time.sleep(.5)
        stop()


def test():
    print('Starting test mode. Press Ctrl-C to go back to command mode.')
    try:
        while True:
            for i, output_pin in enumerate(output_pins):
                print('{0:03b}'.format(4>>i))
                GPIO.output(output_pin, 1)
                time.sleep(1.0)
                GPIO.output(output_pin, 0)
    except KeyboardInterrupt:
        print("\nTest stopped.")


def turn_on_pin(pin_index):
    turn_on_pins([pin_index])


def turn_on_pins(pin_indices):
    for i, output_pin in enumerate(output_pins):
        if i in pin_indices:
            GPIO.output(output_pin, 1)
        else:
            GPIO.output(output_pin, 0)


def turn_left():
    turn_on_pin(LEFT)

def turn_right():
    turn_on_pin(RIGHT)

def go_forward():
    turn_on_pins((LEFT, RIGHT))

def go_backward():
    turn_on_pin(MIDDLE)

def stop():
    turn_on_pin(-1)


def loop():
    print('Robot driver program. Enter l, r, f, b, or stop.')
    print('Press Ctrl-C to quit the program.')
    try:
        while True:
            command = input('> ').strip().lower()
            if command == 'l':
                turn_left()
            elif command == 'r':
                turn_right()
            elif command =='f':
                go_forward()
            elif command == 'b':
                go_backward()
            else:
                stop()
                if command == 'quit':
                    break
                if command == 'test':
                    test()
                if command == 'celebrate':
                    celebrate()
    except KeyboardInterrupt:
        print("\nQuitting.")


def main():
    setup()
    try:
        loop()
    finally:
        cleanup()


if __name__ == '__main__':
    sys.exit(main())

# end-of-file
