#!/usr/bin/python
"""
Poll a pin connected to a switch.
Set another pin value connected to an LED
based on the switch setting.
"""

import sys
import time

import RPi.GPIO as GPIO

# Pin numbers from RPi point of view
input_pin = 3
output_pin = 7

def setup():
    # Set mode to use RPi board's pin numbers
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(input_pin, GPIO.IN)
    GPIO.setup(output_pin, GPIO.OUT)

def cleanup():
    GPIO.cleanup()

def read_pin():
    return GPIO.input(input_pin)

def loop():
    prev_state = read_pin()
    try:
        while True:
            next_state = read_pin()
            GPIO.output(output_pin, next_state)
            if next_state != prev_state:
                print "Switch:", next_state
                prev_state = next_state
            time.sleep(0.1)
    except KeyboardInterrupt:
        print "\nStopped."

def main():
    setup()
    try:
        loop()
    finally:
        cleanup()

if __name__ == '__main__':
    sys.exit(main())

# end-of-file
