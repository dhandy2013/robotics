//! Embedded Graphics implemented on top of Linux DRM graphics
use crate::{
    drmdriver::{self, Card},
    embedded_drawer::{EmbeddedDrawer, PixelIndexer},
};
use drm::{
    buffer::DrmFourcc,
    control::{
        dumbbuffer::{DumbBuffer, DumbMapping},
        Device,
    },
};
use std::{io, path::Path};

/// Graphics drawn on a Linux DRM graphics device
/// We assume that mode and size of frame buffer do not change for the lifetime
/// of this struct.
pub struct DrmGraphics {
    card: Card,
    conn: drm::control::connector::Handle,
    crtc_info: drm::control::crtc::Info,
    db: DumbBuffer,
    fb: drm::control::framebuffer::Handle,
    pixel_indexer: PixelIndexer,
}

impl DrmGraphics {
    const BYTES_PER_PIXEL: usize = PixelIndexer::BYTES_PER_PIXEL;

    pub fn open(drm_device_path: &Path) -> Result<Self, crate::Error> {
        let card = Card::open(drm_device_path)?;
        Ok(Self::new(card)?)
    }

    /// Create a DrmGraphics object from a DRM graphics Card.
    /// Requirements:
    /// - bytes per pixel must be 4
    /// - pixel layout must be RGBX (LSB to MSB)
    pub fn new(card: Card) -> Result<Self, crate::Error> {
        let connector_info = drmdriver::get_first_connector(&card)?;
        let crtc_info =
            drmdriver::get_crtc_for_connector(&card, &connector_info)?;
        let mode = crtc_info.mode().ok_or_else(|| {
            crate::Error(String::from("No mode for graphics card connector"))
        })?;
        let (width, height) = mode.size();
        let orig_fb = drmdriver::get_framebuffer_for_crtc(&card, &crtc_info)?;

        // Validate pixel format assumptions: 4 bytes in BGRA order
        let expected_bpp = Self::BYTES_PER_PIXEL * 8;
        assert_eq!(orig_fb.bpp() as usize, expected_bpp);

        let bytes_per_line = orig_fb.pitch() as usize;

        // Select the pixel format
        let bits_per_pixel = orig_fb.bpp();
        let depth = orig_fb.depth();
        let fmt = DrmFourcc::Xrgb8888;

        // Create a "dumb buffer" and connect it to a new framebuffer.
        // If buffer resolution is above display resolution,
        // an ENOSPC (not enough GPU memory) error may occur.
        let db = card.create_dumb_buffer(
            (width as u32, height as u32),
            fmt,
            bits_per_pixel,
        )?;
        let fb = card.add_framebuffer(&db, depth, bits_per_pixel).map_err(
            |err| {
                // Try not to leak resources
                card.destroy_dumb_buffer(db)
                    .expect("destroying dumb buffer should succeed");
                crate::Error(format!("Could not create framebuffer: {err:?}"))
            },
        )?;

        let conn = connector_info.handle();
        if let Err(err) = drmdriver::set_mode(&card, conn, &crtc_info, mode, fb)
        {
            // Try not to leak resources
            card.destroy_framebuffer(fb)
                .expect("destroying framebuffer should succeed");
            card.destroy_dumb_buffer(db)
                .expect("destroying dumb buffer should succeed");
            Err(crate::Error(format!(
                "DrmGraphics::new: drmdriver::set_mode failed: {err:?}"
            )))
        } else {
            Ok(Self {
                card,
                conn,
                crtc_info,
                db,
                fb,
                pixel_indexer: PixelIndexer::new(
                    width as i32,
                    height as i32,
                    bytes_per_line,
                ),
            })
        }
    }

    pub fn print_device_info(&self) -> Result<(), crate::Error> {
        drmdriver::print_verbose_card_info(&self.card)
    }

    /// Demo: temporarily create a new buffer, switch modes, "do stuff",
    /// then release the buffer and restore the original mode.
    pub fn get_buffer_and_do_stuff(
        drm_device_path: &Path,
    ) -> Result<(), crate::Error> {
        let card = Card::open(drm_device_path)?;
        drmdriver::get_drm_framebuffer_and_do_stuff(&card)
    }

    fn map_buffer(&mut self) -> Result<DumbMapping<'_>, io::Error> {
        let map = self.card.map_dumb_buffer(&mut self.db);
        map
    }

    pub fn as_drawer(
        &mut self,
    ) -> Result<EmbeddedDrawer<DumbMapping<'_>>, crate::Error> {
        let indexer = self.pixel_indexer;
        let map = self.map_buffer()?;
        let drawer = EmbeddedDrawer::new(map, indexer);
        Ok(drawer)
    }
}

impl Drop for DrmGraphics {
    fn drop(&mut self) {
        if let Err(err) = self.card.destroy_framebuffer(self.fb) {
            log::error!("DrmGraphics::drop: destroying framebuffer: {err}");
        }
        if let Err(err) = self.card.destroy_dumb_buffer(self.db) {
            log::error!("DrmGraphics::drop: destroying dumb buffer: {err}");
        }
        if let Err(err) = drmdriver::set_connector_mode_from_crtc(
            &self.card,
            self.conn,
            &self.crtc_info,
        ) {
            log::error!("DrmGraphics::drop: setting original mode: {err:?}");
        }
    }
}
