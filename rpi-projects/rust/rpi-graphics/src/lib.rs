//! Library code go to with Raspberry Pi graphics

pub mod drmdriver;
pub mod drmgraphics;
pub mod embedded_drawer;
pub mod fbdriver;
pub mod fbgraphics;

#[derive(Debug)]
#[allow(unused)]
pub struct Error(pub String);

impl From<std::io::Error> for Error {
    fn from(value: std::io::Error) -> Self {
        let msg = format!("{value:?}");
        Self(msg)
    }
}
