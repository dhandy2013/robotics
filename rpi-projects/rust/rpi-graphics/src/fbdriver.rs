//! Code specific to the Linux framebuffer device (/dev/fb*)
use linuxfb::{Error as FbError, Framebuffer};
use std::path::PathBuf;

impl From<FbError> for crate::Error {
    fn from(value: FbError) -> Self {
        let msg = format!("{value:?}");
        Self(msg)
    }
}

pub fn list_framebuffer_device_paths() -> std::io::Result<Vec<PathBuf>> {
    Framebuffer::list()
}

pub fn print_fb_device_info(fb: &Framebuffer) -> Result<(), crate::Error> {
    println!("Size in pixels: {:?}", fb.get_size());
    println!("Bytes per pixel: {:?}", fb.get_bytes_per_pixel());
    println!("Physical size in mm: {:?}", fb.get_physical_size());

    Ok(())
}
