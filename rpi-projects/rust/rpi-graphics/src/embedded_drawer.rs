//! Draw with embedded_graphics API on any memory-mappable framebuffer
use embedded_graphics::{
    pixelcolor::Rgb888,
    prelude::{
        Dimensions, DrawTarget, OriginDimensions, Pixel, Point, RgbColor, Size,
    },
    primitives::Rectangle,
};

struct PerformanceCounters {
    num_draw_iter_calls: usize,
    num_draw_iter_pixels: usize,
    num_fill_solid_calls: usize,
}

impl Default for PerformanceCounters {
    fn default() -> Self {
        Self {
            num_draw_iter_calls: 0,
            num_draw_iter_pixels: 0,
            num_fill_solid_calls: 0,
        }
    }
}

impl PerformanceCounters {
    #[inline]
    pub fn inc_draw_iter_calls(&mut self) {
        self.num_draw_iter_calls += 1;
    }

    #[inline]
    pub fn inc_draw_iter_pixels(&mut self, inc_by: usize) {
        self.num_draw_iter_pixels += inc_by;
    }

    #[inline]
    pub fn inc_fill_solid_calls(&mut self) {
        self.num_fill_solid_calls += 1;
    }

    pub fn log_stats(&self) {
        log::info!("num_draw_iter_calls: {}", self.num_draw_iter_calls);
        log::info!("num_draw_iter_pixels: {}", self.num_draw_iter_pixels);
        log::info!("num_fill_solid_calls: {}", self.num_fill_solid_calls);
    }
}

#[derive(Clone, Copy)]
pub(crate) struct PixelIndexer {
    width: i32,
    height: i32,
    bytes_per_line: usize,
}

impl PixelIndexer {
    pub(crate) const BYTES_PER_PIXEL: usize = 4;

    pub(crate) fn new(width: i32, height: i32, bytes_per_line: usize) -> Self {
        Self {
            width,
            height,
            bytes_per_line,
        }
    }

    #[inline]
    pub(crate) fn width(&self) -> i32 {
        self.width
    }

    #[inline]
    pub(crate) fn height(&self) -> i32 {
        self.height
    }

    #[inline]
    pub(crate) fn bytes_per_line(&self) -> usize {
        self.bytes_per_line
    }

    #[inline]
    pub(crate) fn pixel_index(&self, coord: Point) -> Option<usize> {
        let (x, y) = coord.into();
        if x < 0 || x >= self.width || y < 0 || y >= self.height {
            return None;
        }
        let index = (self.bytes_per_line * y as usize)
            + (Self::BYTES_PER_PIXEL * x as usize);
        Some(index)
    }
}
pub struct EmbeddedDrawer<M: AsMut<[u8]>> {
    map: M,
    pixel_indexer: PixelIndexer,
    performance_counters: PerformanceCounters,
}

impl<M: AsMut<[u8]>> AsMut<[u8]> for EmbeddedDrawer<M> {
    fn as_mut(&mut self) -> &mut [u8] {
        self.map.as_mut()
    }
}

impl<M: AsMut<[u8]>> EmbeddedDrawer<M> {
    pub(crate) fn new(map: M, pixel_indexer: PixelIndexer) -> Self {
        Self {
            map,
            pixel_indexer,
            performance_counters: PerformanceCounters::default(),
        }
    }

    pub fn log_stats(&self) {
        self.performance_counters.log_stats();
    }
}

impl<M: AsMut<[u8]>> DrawTarget for EmbeddedDrawer<M> {
    type Color = Rgb888;
    type Error = crate::Error;

    fn draw_iter<I>(&mut self, pixels: I) -> Result<(), Self::Error>
    where
        I: IntoIterator<Item = embedded_graphics::Pixel<Self::Color>>,
    {
        self.performance_counters.inc_draw_iter_calls();
        let indexer = self.pixel_indexer;
        let color_alpha: u8 = 255;
        let mut num_pixels: usize = 0;
        for Pixel(coord, color) in pixels.into_iter() {
            num_pixels += 1;
            let data = self.as_mut();
            if let Some(index) = indexer.pixel_index(coord) {
                data[index] = color.b();
                data[index + 1] = color.g();
                data[index + 2] = color.r();
                data[index + 3] = color_alpha;
            }
        }
        self.performance_counters.inc_draw_iter_pixels(num_pixels);
        Ok(())
    }

    fn fill_solid(
        &mut self,
        area: &Rectangle,
        color: Self::Color,
    ) -> Result<(), Self::Error> {
        self.performance_counters.inc_fill_solid_calls();
        // TODO: What is the effect of clipping on area.top_left coordinates?
        let area = area.intersection(&self.bounding_box());

        if area.bottom_right().is_none() {
            // zero-sized area
            return Ok(());
        }

        let color_alpha: u8 = 255;
        let pixel_bytes: [u8; PixelIndexer::BYTES_PER_PIXEL] =
            [color.b(), color.g(), color.r(), color_alpha];

        let indexer = self.pixel_indexer;
        let data = self.as_mut();

        let mut line_start_index = indexer.pixel_index(area.top_left).unwrap();
        for _row in area.rows() {
            let mut pixel_index = line_start_index;
            for _col in area.columns() {
                data[pixel_index..(pixel_index + pixel_bytes.len())]
                    .copy_from_slice(&pixel_bytes[..]);
                pixel_index += pixel_bytes.len();
            }
            line_start_index += indexer.bytes_per_line();
        }

        Ok(())
    }
}

impl<M: AsMut<[u8]>> OriginDimensions for EmbeddedDrawer<M> {
    fn size(&self) -> Size {
        Size::new(
            self.pixel_indexer.width() as u32,
            self.pixel_indexer.height() as u32,
        )
    }
}
