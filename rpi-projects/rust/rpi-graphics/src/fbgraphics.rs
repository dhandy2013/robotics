//! Embedded Graphics implemented on top of a Linux framebuffer
use crate::{
    embedded_drawer::{EmbeddedDrawer, PixelIndexer},
    fbdriver,
};
use linuxfb::{Error as FbError, Framebuffer};
use std::path::{Path, PathBuf};

/// Graphics drawn on a Linux Framebuffer
/// We assume that mode and size of frame buffer do not change for the lifetime
/// of this struct.
pub struct FbGraphics {
    fb: Framebuffer,
    width: i32,
    height: i32,
    bytes_per_line: usize,
}

impl FbGraphics {
    const BYTES_PER_PIXEL: usize = PixelIndexer::BYTES_PER_PIXEL;

    pub fn list() -> std::io::Result<Vec<PathBuf>> {
        fbdriver::list_framebuffer_device_paths()
    }

    pub fn open(fb_device_path: &Path) -> Result<Self, crate::Error> {
        let fb = Framebuffer::new(fb_device_path).map_err(|err: FbError| {
            crate::Error(format!(
                "Error opening frame buffer {fb_device_path:?}: {err:?}"
            ))
        })?;
        Ok(Self::new(fb))
    }

    /// Create an FbGraphics object from a Framebuffer.
    /// Requirements:
    /// - bytes per pixel must be 4
    /// - pixel layout must be RGBX (LSB to MSB)
    pub fn new(fb: Framebuffer) -> Self {
        let (width, height) = fb.get_size();
        assert!(width <= i32::MAX as u32);
        assert!(height <= i32::MAX as u32);

        // Validate pixel format assumptions: 4 bytes in BGRA order
        let bytes_per_pixel = fb.get_bytes_per_pixel() as usize;
        assert_eq!(bytes_per_pixel, Self::BYTES_PER_PIXEL);
        let layout = fb.get_pixel_layout();
        assert_eq!(layout.red.offset, 16);
        assert_eq!(layout.red.length, 8);
        assert_eq!(layout.red.msb_right, false);
        assert_eq!(layout.green.offset, 8);
        assert_eq!(layout.green.length, 8);
        assert_eq!(layout.blue.offset, 0);
        assert_eq!(layout.blue.length, 8);
        assert!(layout.alpha.length == 0 || layout.alpha.length == 8);

        let bytes_per_line: usize = width as usize * Self::BYTES_PER_PIXEL;
        Self {
            fb,
            width: width as i32,
            height: height as i32,
            bytes_per_line,
        }
    }

    pub fn print_device_info(&self) -> Result<(), crate::Error> {
        fbdriver::print_fb_device_info(&self.fb)
    }

    pub fn as_drawer(
        &mut self,
    ) -> Result<EmbeddedDrawer<memmap::MmapMut>, crate::Error> {
        let map = self.fb.map()?;
        let drawer = EmbeddedDrawer::new(
            map,
            PixelIndexer::new(self.width, self.height, self.bytes_per_line),
        );
        Ok(drawer)
    }
}
