//! Linux DRM graphics support
use crate::Error;
use drm::{
    buffer::DrmFourcc,
    control::{
        self, atomic, connector, crtc, dumbbuffer::DumbBuffer, property,
        AtomicCommitFlags, Device as ControlDevice,
    },
    Device,
};
use std::{
    os::fd::{AsFd, BorrowedFd},
    path::Path,
    time::Duration,
};

/// A wrapper for a DRM device node, e.g. /dev/dri/card0
pub struct Card(std::fs::File);

/// Implementing [`AsFd`] is a prerequisite to implementing the traits found in
/// the drm crate. Here, we are just calling [`File::as_fd()`] on the inner
/// [`File`].
impl AsFd for Card {
    fn as_fd(&self) -> BorrowedFd<'_> {
        self.0.as_fd()
    }
}

/// Helper methods for opening a `Card`.
impl Card {
    pub fn open(path: &Path) -> std::io::Result<Self> {
        let mut options = std::fs::OpenOptions::new();
        options.read(true);
        options.write(true);
        let file = options.open(path)?;
        Ok(Card(file))
    }

    pub fn open_global() -> std::io::Result<Self> {
        Self::open(Path::new("/dev/dri/card0"))
    }
}

/// This makes Card usable as a DRM Device
impl Device for Card {}
impl ControlDevice for Card {}

/// Return info about the first connected display on the graphics card
pub fn get_first_connector(
    card: &Card,
) -> Result<drm::control::connector::Info, Error> {
    let resources = card.resource_handles()?;
    let mut maybe_connector_info: Option<connector::Info> = None;
    for &connector_handle in resources.connectors().iter() {
        let force_probe = false;
        let connector_info =
            card.get_connector(connector_handle, force_probe)?;
        if connector_info.state() == drm::control::connector::State::Connected {
            maybe_connector_info = Some(connector_info);
            break;
        }
    }
    match maybe_connector_info {
        Some(connector_info) => Ok(connector_info),
        None => Err(Error(String::from(
            "No connected connector on graphics card",
        ))),
    }
}

pub fn get_crtc_for_connector(
    card: &Card,
    connector_info: &drm::control::connector::Info,
) -> Result<drm::control::crtc::Info, Error> {
    let current_encoder =
        connector_info.current_encoder().ok_or_else(|| {
            Error(String::from("No encoder on graphics card connector"))
        })?;
    let encoder_info = card.get_encoder(current_encoder)?;
    let current_crtc = encoder_info.crtc().ok_or_else(|| {
        Error(String::from("No crtc on graphics card connector"))
    })?;
    Ok(card.get_crtc(current_crtc)?)
}

pub fn get_connector_handles_for_crtc(
    card: &Card,
    crtc_info: &drm::control::crtc::Info,
) -> Result<Vec<drm::control::connector::Handle>, Error> {
    let resources = card.resource_handles()?;
    let mut result = Vec::new();
    for &connector_handle in resources.connectors().iter() {
        let force_probe = false;
        let connector_info =
            card.get_connector(connector_handle, force_probe)?;
        let connector_crtc = get_crtc_for_connector(card, &connector_info)?;
        if connector_crtc.handle() == crtc_info.handle() {
            let connector_handle = connector_info.handle();
            result.push(connector_handle);
        }
    }
    Ok(result)
}

/// Set the mode of a connector based on CRTC settings
pub fn set_connector_mode_from_crtc(
    card: &Card,
    conn: drm::control::connector::Handle,
    crtc_info: &drm::control::crtc::Info,
) -> Result<(), Error> {
    let handle = crtc_info.handle();
    let maybe_framebuffer = crtc_info.framebuffer();
    let pos = crtc_info.position();
    let maybe_mode = crtc_info.mode();
    card.set_crtc(handle, maybe_framebuffer, pos, &[conn][..], maybe_mode)?;
    Ok(())
}

pub fn get_framebuffer_for_crtc(
    card: &Card,
    crtc_info: &drm::control::crtc::Info,
) -> Result<drm::control::framebuffer::Info, Error> {
    let current_fb = crtc_info.framebuffer().ok_or_else(|| {
        Error(String::from("No framebuffer on graphics card connector"))
    })?;
    Ok(card.get_framebuffer(current_fb)?)
}

pub fn print_verbose_card_info(card: &Card) -> Result<(), Error> {
    let driver = card.get_driver()?;
    println!("DRM driver name       : {:?}", driver.name());
    println!("DRM driver date       : {:?}", driver.date());
    println!("DRM driver description: {:?}", driver.description());

    let resources = card.resource_handles()?;
    let show_connected_modes = false;
    for connector in resources.connectors().iter() {
        let connector_info = card.get_connector(*connector, false)?;
        println!(
            "Connector {:?}: {:?}",
            connector_info.interface(),
            connector_info.state()
        );
        if connector_info.state() != drm::control::connector::State::Connected {
            continue;
        }
        if show_connected_modes {
            println!("    Modes:\n{:#?}", connector_info.modes());
        }
        if let Some(current_encoder) = connector_info.current_encoder() {
            let encoder_info = card.get_encoder(current_encoder)?;
            if let Some(current_crtc) = encoder_info.crtc() {
                let crtc_info = card.get_crtc(current_crtc)?;
                if let Some(current_fb) = crtc_info.framebuffer() {
                    let fb_info = card.get_framebuffer(current_fb)?;
                    let bpp = fb_info.bpp();
                    let pitch = fb_info.pitch();
                    println!("    bpp={bpp} pitch={pitch}");
                }
                if let Some(current_mode) = crtc_info.mode() {
                    let (width, height) = current_mode.size();
                    println!("    Size: {width}x{height}");
                }
            }
        }
    }

    // Enable all possible client capabilities
    println!("Setting client capabilities");
    for &cap in capabilities::CLIENT_CAP_ENUMS {
        println!("    {:?}: {:?}", cap, card.set_client_capability(cap, true));
    }

    // Get driver capabilities
    println!("Getting driver capabilities");
    for &cap in capabilities::DRIVER_CAP_ENUMS {
        println!("    {:?}: {:?}", cap, card.get_driver_capability(cap));
    }

    Ok(())
}

pub fn get_drm_framebuffer_and_do_stuff(card: &Card) -> Result<(), Error> {
    card.set_client_capability(drm::ClientCapability::UniversalPlanes, true)
        .expect("Unable to request UniversalPlanes capability");
    card.set_client_capability(drm::ClientCapability::Atomic, true)
        .expect("Unable to request Atomic capability");

    // Load the information.
    let res = card
        .resource_handles()
        .expect("Could not load normal resource ids.");
    let coninfo: Vec<connector::Info> = res
        .connectors()
        .iter()
        .flat_map(|con| card.get_connector(*con, true))
        .collect();

    // Filter each connector until we find one that's connected.
    let conn = coninfo
        .iter()
        .find(|&i| i.state() == connector::State::Connected)
        .expect("No connected connectors");

    let saved_crtc = get_crtc_for_connector(card, conn)?;

    // Save current graphics mode
    let (mut db, fb) = set_best_mode(card, conn)?;

    // Pause 2.5 seconds with gray screen
    ::std::thread::sleep(Duration::from_millis(2500));

    // Make the screen bright white
    {
        let mut map = card
            .map_dumb_buffer(&mut db)
            .expect("Could not map dumbbuffer");
        for b in map.as_mut() {
            *b = 255;
        }
    }

    // Pause 2.5 seconds with white screen
    ::std::thread::sleep(Duration::from_millis(2500));

    // Clean up resources
    card.destroy_framebuffer(fb).unwrap();
    card.destroy_dumb_buffer(db).unwrap();

    // Restore previous graphics mode
    set_connector_mode_from_crtc(card, conn.handle(), &saved_crtc)?;

    Ok(())
}

pub fn set_best_mode(
    card: &Card,
    conn: &drm::control::connector::Info,
) -> Result<(DumbBuffer, drm::control::framebuffer::Handle), Error> {
    card.set_client_capability(drm::ClientCapability::UniversalPlanes, true)
        .map_err(|err| {
            Error(format!(
                "Unable to request UniversalPlanes capability: {err:?}"
            ))
        })?;
    card.set_client_capability(drm::ClientCapability::Atomic, true)
        .map_err(|err| {
            Error(format!("Unable to request Atomic capability: {err:?}"))
        })?;

    // Get the first (usually best) mode
    let &mode = conn.modes().first().expect("No modes found on connector");
    let (disp_width, disp_height) = mode.size();

    // Find a crtc and FB
    let res = card.resource_handles().map_err(|err| {
        Error(format!("Could not load normal resource ids: {err:?}"))
    })?;
    let crtcinfo: Vec<crtc::Info> = res
        .crtcs()
        .iter()
        .flat_map(|crtc| card.get_crtc(*crtc))
        .collect();
    let crtc = crtcinfo
        .first()
        .ok_or_else(|| Error(String::from("No crtcs found")))?;

    // Select the pixel format
    let fmt = DrmFourcc::Xrgb8888;
    let depth = 24;
    let bits_per_pixel = 32;

    // Create a "dumb buffer".
    // If buffer resolution is above display resolution, a ENOSPC (not enough
    // GPU memory) error may occur.
    let mut db = card
        .create_dumb_buffer(
            (disp_width.into(), disp_height.into()),
            fmt,
            bits_per_pixel,
        )
        .map_err(|err| {
            Error(format!("Could not create dumb buffer: {err:?}"))
        })?;

    // Map it and grey it out.
    {
        let mut map = card.map_dumb_buffer(&mut db).map_err(|err| {
            Error(format!("Could not map dumb buffer: {err:?}"))
        })?;
        for b in map.as_mut() {
            *b = 128;
        }
    }

    // Create a framebuffer from the dumb buffer
    let fb =
        card.add_framebuffer(&db, depth, bits_per_pixel)
            .map_err(|err| {
                Error(format!("Could not create framebuffer: {err:?}"))
            })?;

    let planes = card
        .plane_handles()
        .map_err(|err| Error(format!("Could not list planes: {err:?}")))?;
    let (better_planes, compatible_planes): (
        Vec<control::plane::Handle>,
        Vec<control::plane::Handle>,
    ) = planes
        .iter()
        .filter(|&&plane| {
            card.get_plane(plane)
                .map(|plane_info| {
                    let compatible_crtcs =
                        res.filter_crtcs(plane_info.possible_crtcs());
                    compatible_crtcs.contains(&crtc.handle())
                })
                .unwrap_or(false)
        })
        .partition(|&&plane| {
            if let Ok(props) = card.get_properties(plane) {
                for (&id, &val) in props.iter() {
                    if let Ok(info) = card.get_property(id) {
                        if info
                            .name()
                            .to_str()
                            .map(|x| x == "type")
                            .unwrap_or(false)
                        {
                            return val
                                == (drm::control::PlaneType::Primary as u32)
                                    .into();
                        }
                    }
                }
            }
            false
        });
    let plane = *better_planes.first().unwrap_or(&compatible_planes[0]);

    log::trace!("{:#?}", mode);
    log::trace!("{:#?}", fb);
    log::trace!("{:#?}", db);
    log::trace!("{:#?}", plane);

    let con_props = card
        .get_properties(conn.handle())
        .expect("Could not get props of connector")
        .as_hashmap(card)
        .expect("Could not get a prop from connector");
    let crtc_props = card
        .get_properties(crtc.handle())
        .expect("Could not get props of crtc")
        .as_hashmap(card)
        .expect("Could not get a prop from crtc");
    let plane_props = card
        .get_properties(plane)
        .expect("Could not get props of plane")
        .as_hashmap(card)
        .expect("Could not get a prop from plane");

    let mut atomic_req = atomic::AtomicModeReq::new();
    atomic_req.add_property(
        conn.handle(),
        con_props["CRTC_ID"].handle(),
        property::Value::CRTC(Some(crtc.handle())),
    );
    let blob = card
        .create_property_blob(&mode)
        .expect("Failed to create blob");
    atomic_req.add_property(
        crtc.handle(),
        crtc_props["MODE_ID"].handle(),
        blob,
    );
    atomic_req.add_property(
        crtc.handle(),
        crtc_props["ACTIVE"].handle(),
        property::Value::Boolean(true),
    );
    atomic_req.add_property(
        plane,
        plane_props["FB_ID"].handle(),
        property::Value::Framebuffer(Some(fb)),
    );
    atomic_req.add_property(
        plane,
        plane_props["CRTC_ID"].handle(),
        property::Value::CRTC(Some(crtc.handle())),
    );
    atomic_req.add_property(
        plane,
        plane_props["SRC_X"].handle(),
        property::Value::UnsignedRange(0),
    );
    atomic_req.add_property(
        plane,
        plane_props["SRC_Y"].handle(),
        property::Value::UnsignedRange(0),
    );
    atomic_req.add_property(
        plane,
        plane_props["SRC_W"].handle(),
        property::Value::UnsignedRange((mode.size().0 as u64) << 16),
    );
    atomic_req.add_property(
        plane,
        plane_props["SRC_H"].handle(),
        property::Value::UnsignedRange((mode.size().1 as u64) << 16),
    );
    atomic_req.add_property(
        plane,
        plane_props["CRTC_X"].handle(),
        property::Value::SignedRange(0),
    );
    atomic_req.add_property(
        plane,
        plane_props["CRTC_Y"].handle(),
        property::Value::SignedRange(0),
    );
    atomic_req.add_property(
        plane,
        plane_props["CRTC_W"].handle(),
        property::Value::UnsignedRange(mode.size().0 as u64),
    );
    atomic_req.add_property(
        plane,
        plane_props["CRTC_H"].handle(),
        property::Value::UnsignedRange(mode.size().1 as u64),
    );

    // Set the crtc
    // On many setups, this requires root access.
    if let Err(err) =
        card.atomic_commit(AtomicCommitFlags::ALLOW_MODESET, atomic_req)
    {
        // Clean up resources on error
        card.destroy_framebuffer(fb)
            .expect("destroying framebuffer should succeed");
        card.destroy_dumb_buffer(db)
            .expect("destroying dumb buffer should succeed");
        Err(Error(format!("Failed to set best mode: {err:?}")))
    } else {
        // Return the framebuffer and the dumb buffer.
        Ok((db, fb))
    }
}

pub fn set_mode(
    card: &Card,
    conn: drm::control::connector::Handle,
    crtc: &drm::control::crtc::Info,
    mode: drm::control::Mode,
    fb: drm::control::framebuffer::Handle,
) -> Result<(), Error> {
    let res = card.resource_handles().map_err(|err| {
        Error(format!("Could not load normal resource ids: {err:?}"))
    })?;

    card.set_client_capability(drm::ClientCapability::UniversalPlanes, true)
        .map_err(|err| {
            Error(format!(
                "Unable to request UniversalPlanes capability: {err:?}"
            ))
        })?;
    card.set_client_capability(drm::ClientCapability::Atomic, true)
        .map_err(|err| {
            Error(format!("Unable to request Atomic capability: {err:?}"))
        })?;

    let planes = card
        .plane_handles()
        .map_err(|err| Error(format!("Could not list planes: {err:?}")))?;
    let (better_planes, compatible_planes): (
        Vec<control::plane::Handle>,
        Vec<control::plane::Handle>,
    ) = planes
        .iter()
        .filter(|&&plane| {
            card.get_plane(plane)
                .map(|plane_info| {
                    let compatible_crtcs =
                        res.filter_crtcs(plane_info.possible_crtcs());
                    compatible_crtcs.contains(&crtc.handle())
                })
                .unwrap_or(false)
        })
        .partition(|&&plane| {
            if let Ok(props) = card.get_properties(plane) {
                for (&id, &val) in props.iter() {
                    if let Ok(info) = card.get_property(id) {
                        if info
                            .name()
                            .to_str()
                            .map(|x| x == "type")
                            .unwrap_or(false)
                        {
                            return val
                                == (drm::control::PlaneType::Primary as u32)
                                    .into();
                        }
                    }
                }
            }
            false
        });
    let plane = *better_planes.first().unwrap_or(&compatible_planes[0]);

    let con_props = card
        .get_properties(conn)
        .expect("Could not get props of connector")
        .as_hashmap(card)
        .expect("Could not get a prop from connector");
    let crtc_props = card
        .get_properties(crtc.handle())
        .expect("Could not get props of crtc")
        .as_hashmap(card)
        .expect("Could not get a prop from crtc");
    let plane_props = card
        .get_properties(plane)
        .expect("Could not get props of plane")
        .as_hashmap(card)
        .expect("Could not get a prop from plane");

    let mut atomic_req = atomic::AtomicModeReq::new();
    atomic_req.add_property(
        conn,
        con_props["CRTC_ID"].handle(),
        property::Value::CRTC(Some(crtc.handle())),
    );
    let blob = card
        .create_property_blob(&mode)
        .expect("Failed to create blob");
    atomic_req.add_property(
        crtc.handle(),
        crtc_props["MODE_ID"].handle(),
        blob,
    );
    atomic_req.add_property(
        crtc.handle(),
        crtc_props["ACTIVE"].handle(),
        property::Value::Boolean(true),
    );
    atomic_req.add_property(
        plane,
        plane_props["FB_ID"].handle(),
        property::Value::Framebuffer(Some(fb)),
    );
    atomic_req.add_property(
        plane,
        plane_props["CRTC_ID"].handle(),
        property::Value::CRTC(Some(crtc.handle())),
    );
    atomic_req.add_property(
        plane,
        plane_props["SRC_X"].handle(),
        property::Value::UnsignedRange(0),
    );
    atomic_req.add_property(
        plane,
        plane_props["SRC_Y"].handle(),
        property::Value::UnsignedRange(0),
    );
    atomic_req.add_property(
        plane,
        plane_props["SRC_W"].handle(),
        property::Value::UnsignedRange((mode.size().0 as u64) << 16),
    );
    atomic_req.add_property(
        plane,
        plane_props["SRC_H"].handle(),
        property::Value::UnsignedRange((mode.size().1 as u64) << 16),
    );
    atomic_req.add_property(
        plane,
        plane_props["CRTC_X"].handle(),
        property::Value::SignedRange(0),
    );
    atomic_req.add_property(
        plane,
        plane_props["CRTC_Y"].handle(),
        property::Value::SignedRange(0),
    );
    atomic_req.add_property(
        plane,
        plane_props["CRTC_W"].handle(),
        property::Value::UnsignedRange(mode.size().0 as u64),
    );
    atomic_req.add_property(
        plane,
        plane_props["CRTC_H"].handle(),
        property::Value::UnsignedRange(mode.size().1 as u64),
    );

    // Set the crtc
    // On many setups, this requires root access.
    card.atomic_commit(AtomicCommitFlags::ALLOW_MODESET, atomic_req)
        .map_err(|err| {
            Error(format!("Failed to set mode {mode:?}: {err:?}"))
        })?;

    Ok(())
}

pub mod capabilities {
    use drm::ClientCapability as CC;
    pub const CLIENT_CAP_ENUMS: &[CC] =
        &[CC::Stereo3D, CC::UniversalPlanes, CC::Atomic];

    use drm::DriverCapability as DC;
    pub const DRIVER_CAP_ENUMS: &[DC] = &[
        DC::DumbBuffer,
        DC::VBlankHighCRTC,
        DC::DumbPreferredDepth,
        DC::DumbPreferShadow,
        DC::Prime,
        DC::MonotonicTimestamp,
        DC::ASyncPageFlip,
        DC::CursorWidth,
        DC::CursorHeight,
        DC::AddFB2Modifiers,
        DC::PageFlipTarget,
        DC::CRTCInVBlankEvent,
        DC::SyncObj,
        DC::TimelineSyncObj,
    ];
}
