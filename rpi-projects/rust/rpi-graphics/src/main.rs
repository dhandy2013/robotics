//! Draw graphics directly to a Linux framebuffer in a console
//!
//! The idea is to be able to run a single graphical app at a time on a
//! Raspberry Pi with limited resources without starting X or any graphical
//! display manager. This code is written to be portable on any Linux machine
//! but was designed with the Raspberry Pi in mind.
//!
//! To run this code on a Linux system that is running X or Wayland and a
//! graphcal display manager, use Ctrl-Alt-F3 to switch to a linux console
//! first.
//!
//! There are two ways to do direct graphics on Linux:
//! - The classic framebuffer device (/dev/fb0)
//! - The newer DRM device (/dev/dri/card0)
//!
//! If this program can't use the classic framebuffer (because it is Fedora 36+
//! and there is no /dev/df0) it will try to use DRM instead.
use clap::{Parser, ValueEnum};
use embedded_graphics::{
    mono_font::{ascii::FONT_10X20, MonoTextStyle},
    pixelcolor::Rgb888,
    prelude::{DrawTarget, Point, Primitive, RgbColor},
    primitives::{Circle, PrimitiveStyleBuilder},
    text::{Baseline, Text, TextStyleBuilder},
    Drawable,
};
use rpi_graphics::{drmgraphics::DrmGraphics, fbgraphics::FbGraphics, Error};
use rppal::system::DeviceInfo;
use std::{
    path::{Path, PathBuf},
    thread,
    time::{Duration, Instant},
};

#[derive(Parser)]
struct Cli {
    #[arg(short, long, default_value_t = GraphicsType::Auto)]
    graphics_type: GraphicsType,

    #[arg(short, long)]
    verbose: bool,

    #[arg(
        short,
        long,
        value_name = "SECONDS",
        help = "seconds to pause after drawing"
    )]
    pause: Option<f32>,
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum, Debug)]
enum GraphicsType {
    /// Draw on Linux framebuffer device
    Fb,
    /// Draw using DRM/KMS Linux graphics
    Drm,
    /// Pick type automatically, prefer fb if available
    Auto,
}

impl ToString for GraphicsType {
    fn to_string(&self) -> String {
        use GraphicsType::*;
        match self {
            Fb => String::from("fb"),
            Drm => String::from("drm"),
            Auto => String::from("auto"),
        }
    }
}

impl Cli {
    fn run(&self) -> Result<(), Error> {
        println!("rpi-graphics");
        match DeviceInfo::new() {
            Ok(device_info) => {
                log::info!("Running on a {}", device_info.model());
            }
            Err(_) => {
                log::info!("Running on a non-Raspberry Pi system");
            }
        }
        let fb_list = FbGraphics::list()?;
        match self.graphics_type {
            GraphicsType::Auto => {
                if fb_list.len() > 0 {
                    self.do_fb_graphics(&fb_list)
                } else {
                    self.do_drm_graphics()
                }
            }
            GraphicsType::Fb => self.do_fb_graphics(&fb_list),
            GraphicsType::Drm => self.do_drm_graphics(),
        }
    }

    fn do_fb_graphics(&self, fb_list: &[PathBuf]) -> Result<(), Error> {
        log::debug!(
            "Found {} Linux framebuffer device{}",
            fb_list.len(),
            if fb_list.len() == 1 { "" } else { "s" },
        );
        let fb_path = fb_list.get(0).ok_or_else(|| {
            Error(String::from("No Linux framebuffer devices"))
        })?;
        log::info!("Opening framebuffer device: {fb_path:?}");
        let mut graphics = FbGraphics::open(&fb_path)?;
        if self.verbose {
            graphics.print_device_info()?;
        }
        let mut drawer = graphics.as_drawer()?;
        draw_stuff_with_embedded_graphics(&mut drawer)?;
        drawer.log_stats();
        self.maybe_pause();
        Ok(())
    }

    fn do_drm_graphics(&self) -> Result<(), Error> {
        let card_path = Path::new("/dev/dri/card0");
        log::info!("Opening DRM graphics device: {card_path:?}");
        if false {
            DrmGraphics::get_buffer_and_do_stuff(&card_path)?;
            self.maybe_pause();
        } else {
            let mut graphics = DrmGraphics::open(card_path)?;
            if self.verbose {
                graphics.print_device_info()?;
            }
            let mut drawer = graphics.as_drawer()?;
            draw_stuff_with_embedded_graphics(&mut drawer)?;
            drawer.log_stats();
            self.maybe_pause();
        }
        Ok(())
    }

    fn maybe_pause(&self) {
        if let Some(pause_sec) = self.pause {
            if pause_sec <= 0.0 {
                return;
            }
            let d = Duration::from_secs_f32(pause_sec);
            thread::sleep(d);
        }
    }
}

fn main() -> Result<(), Error> {
    env_logger::init();
    let cli = Cli::parse();
    cli.run()
}

fn draw_stuff_with_embedded_graphics<D, E>(display: &mut D) -> Result<(), E>
where
    D: DrawTarget<Color = Rgb888, Error = E>,
{
    let t_start = Instant::now();

    let character_style_white = MonoTextStyle::new(&FONT_10X20, Rgb888::WHITE);
    let character_style_red = MonoTextStyle::new(&FONT_10X20, Rgb888::RED);
    let character_style_green = MonoTextStyle::new(&FONT_10X20, Rgb888::GREEN);
    let character_style_blue = MonoTextStyle::new(&FONT_10X20, Rgb888::BLUE);
    let text_style = TextStyleBuilder::new().baseline(Baseline::Top).build();

    let next_point = Text::with_text_style(
        "Hello framebuffer! (white)\n",
        Point::new(0, 0),
        character_style_white,
        text_style,
    )
    .draw(display)?;

    let next_point = Text::with_text_style(
        "Hello framebuffer! (red)\n",
        next_point,
        character_style_red,
        text_style,
    )
    .draw(display)?;

    let next_point = Text::with_text_style(
        "Hello framebuffer! (green)\n",
        next_point,
        character_style_green,
        text_style,
    )
    .draw(display)?;

    let _next_point = Text::with_text_style(
        "Hello framebuffer! (blue)\n",
        next_point,
        character_style_blue,
        text_style,
    )
    .draw(display)?;

    let bb = display.bounding_box();
    Circle::with_center(bb.center(), 100)
        .into_styled(
            PrimitiveStyleBuilder::new()
                .stroke_color(Rgb888::WHITE)
                .fill_color(Rgb888::RED)
                .stroke_width(1)
                .build(),
        )
        .draw(display)?;

    let t_end = Instant::now();
    log::info!("Drawing took {:?}", t_end - t_start);

    Ok(())
}
