//! Raspberry Pi app: clock control program
//! Assumptions: 32.768kHz signal is attached to GPIO4
use rppal::gpio::Gpio;
use rppal::system::DeviceInfo;
use std::error::Error;
use std::time::{Duration, Instant};

const NUM_TESTS: usize = 100;

fn measure_32khz_clock(gpio: &Gpio) -> Result<(), Box<dyn Error>> {
    println!("Checking resolution of monotonic clock");
    let t0 = Instant::now();
    let (t1, t2) = (t0.elapsed(), t0.elapsed());
    println!("Minimum difference in elapsed times: {} nanoseconds",
             t2.as_nanos() - t1.as_nanos());

    println!("Making {} measurements of period of 32.768KHz clock signal",
             NUM_TESTS);

    // Gather the data
    let mut timestamps = [Duration::new(0, 0); NUM_TESTS + 1];
    let extclk = gpio.get(4)?.into_input_pullup();
    // Get clock into known state
    while extclk.is_high() { }
    while extclk.is_low() { }
    // Loop and measure clock period durations
    for i in 0..NUM_TESTS + 1 {
        timestamps[i] = t0.elapsed();
        while extclk.is_high() { }
        while extclk.is_low() { }
    }

    // Report the raw results
    let mut min = 0xffff_ffffu128;
    let mut max = 0u128;
    println!("Clock period durations in nanoseconds:");
    for k in 0..((NUM_TESTS  / 8) + 1) {
        for j in 0..8 {
            let i = (k * 8) + j;
            if i >= NUM_TESTS { break; }
            let d = timestamps[i + 1].as_nanos() - timestamps[i].as_nanos();
            if d < min {
                min = d;
            }
            if d > max {
                max = d;
            }
            print!(" {:8}", d);
        }
        println!("");
    }

    // Report statistics
    let total = timestamps[NUM_TESTS].as_nanos() - timestamps[0].as_nanos();
    let ave = total as f64 / 1_000.0 / NUM_TESTS as f64;
    let min = min as f64 / 1_000.0;
    let max = max as f64 / 1_000.0;
    println!("Microseconds per 32.768kHz cycle:");
    println!("Min     : {:10.6}", min);
    println!("Ave     : {:10.6}", ave);
    println!("Max     : {:10.6}", max);
    println!("Expected: {:10.6}", 1_000_000.0 / 32768.0);
    Ok(())
}

fn main() -> Result<(), Box<dyn Error>> {
    println!("Hello, I see I'm running on a {}", DeviceInfo::new()?.model());
    let gpio = Gpio::new()?;

    measure_32khz_clock(&gpio)?;

    println!("Done.");
    Ok(())
}
