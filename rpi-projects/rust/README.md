Programming the Raspberry Pi in Rust
====================================

See: https://stackoverflow.com/a/48087233

Setup steps for cross-compiling on Fedora Linux:
```
# Do this once per machine
rustup target add arm-unknown-linux-gnueabihf
mkdir -p ~/src
cd ~/src
git clone --depth=1 https://github.com/raspberrypi/tools raspberrypi-tools
mkdir -p ~/bin
# Make sure ~/bin is in PATH
cd ~/bin
ln -s ~/src/raspberrypi-tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin/arm-linux-gnueabihf-gcc

# cd to workspace dir
cat >> .cargo/config <<EOF
[target.arm-unknown-linux-gnueabihf]
linker = "arm-linux-gnueabihf-gcc"
EOF

# Create "hello world" Rust app
vim Cargo.toml  # add "rpi-test" to the members list in the workspace
cargo new --bin rpi-test

# Build the app
cargo build --target=arm-unknown-linux-gnueabihf -p rpi-test

# Deploy it to a Raspberry Pi named "berry2"
scp target/arm-unknown-linux-gnueabihf/debug/rpi-test pi@berry2:~/

# Run it on "berry2"
ssh pi@berry2 ./rpi-test
```
