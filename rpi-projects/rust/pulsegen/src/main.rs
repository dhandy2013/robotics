//! Raspberry Pi pulse signal generator
//! The pulse is generated on the GPIO18 pin.
//!
//! For the "pwm" subcommand to work, you need to configure hardware PWM
//! in /boot/config.txt. See /boot/overlays/README, search for "pwm".
//!
//! Examples:
//!
//! Generate 2 pulses 100 milliseconds wide, with 100 milliseconds betwen them.
//! Use std::thread::sleep() to control the timing.
//! ```
//! pulsegen sleep 100ms -n 2
//! ```
//!
//! Generate one pulse the width of 1250 spin loops, which is approximately 4.2
//! microseconds on a Raspberry Pi 4B
//! ```
//! pulsegen spin 1250
//! ```
//!
//! Generate a hardware PWM signal with a period of 100 microseconds. Each cycle
//! is high for 25 microseconds and low for 75 microseconds. The signal is
//! turned off after a 1 millisecond sleep (actual sleep time wil vary.)
//! ```
//! pulsegen pwm 100us 25us 1ms
//! ```
use rppal::gpio::Gpio;
use rppal::pwm::{Channel, Polarity, Pwm};
use rppal::system::DeviceInfo;
use simple_signal::{self, Signal};
use std::error::Error;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(
    name = "pulsegen",
    about = "Pulse Generator for Raspberry PI, uses GPIO18 for output"
)]
enum Commands {
    Sleep(SleepOptions),
    Spin(SpinOptions),
    Pwm(PwmOptions),
}

#[derive(Debug, StructOpt)]
struct SleepOptions {
    pulse_width: DurationWithUnits,
    #[structopt(short = "n", long = "num-pulses", default_value = "1")]
    num_pulses: usize,
}

#[derive(Debug, StructOpt)]
struct SpinOptions {
    num_loops: u64,
    #[structopt(short = "n", long = "num-pulses", default_value = "1")]
    num_pulses: usize,
}

#[derive(Debug, StructOpt)]
struct PwmOptions {
    period: DurationWithUnits,
    pulse_width: DurationWithUnits,
    signal_duration: DurationWithUnits,
}

#[derive(Debug)]
enum DurationWithUnits {
    Seconds(u64),
    Millseconds(u64),
    Microseconds(u64),
    Nanoseconds(u64),
}

impl DurationWithUnits {
    /// Convert to a regular time Duration.
    fn as_duration(&self) -> Duration {
        match self {
            Self::Seconds(raw_duration) => Duration::from_secs(*raw_duration),
            Self::Millseconds(raw_duration) => {
                Duration::from_millis(*raw_duration)
            }
            Self::Microseconds(raw_duration) => {
                Duration::from_micros(*raw_duration)
            }
            Self::Nanoseconds(raw_duration) => {
                Duration::from_nanos(*raw_duration)
            }
        }
    }
}

impl std::str::FromStr for DurationWithUnits {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut end_of_digits_index: usize = 0;
        for c in s.chars() {
            match c {
                '0'..='9' => {
                    end_of_digits_index += 1;
                    continue;
                }
                _ => {
                    break;
                }
            }
        }

        if let Ok(raw_duration) = s[..end_of_digits_index].parse::<u64>() {
            match &s[end_of_digits_index..] {
                "s" => Ok(DurationWithUnits::Seconds(raw_duration)),
                "ms" => Ok(DurationWithUnits::Millseconds(raw_duration)),
                "us" => Ok(DurationWithUnits::Microseconds(raw_duration)),
                "ns" => Ok(DurationWithUnits::Nanoseconds(raw_duration)),
                _ => Err("For units use one of: s, ms, us, ns"),
            }
        } else {
            Err("Not a valid integer")
        }
    }
}

fn gen_pulse_with_sleep(
    pulse_width: Duration,
    num_pulses: usize,
) -> Result<(), Box<dyn Error>> {
    let gpio = Gpio::new()?;
    let mut out = gpio.get(18)?.into_output();
    // Experiments show that the output is set low by default,
    // but let's make sure.
    out.set_low();

    for _ in 0..num_pulses {
        out.set_high();
        thread::sleep(pulse_width);
        out.set_low();
        thread::sleep(pulse_width);
    }

    Ok(())
}

fn gen_pulse_with_spin(
    num_loops: u64,
    num_pulses: usize,
) -> Result<(), Box<dyn Error>> {
    let gpio = Gpio::new()?;
    let mut out = gpio.get(18)?.into_output();
    // Experiments show that the output is set low by default,
    // but let's make sure.
    out.set_low();

    for _ in 0..num_pulses {
        out.set_high();
        for _ in 0..num_loops {
            std::hint::spin_loop();
        }
        out.set_low();
        for _ in 0..num_loops {
            std::hint::spin_loop();
        }
    }

    Ok(())
}

fn gen_pwm(
    period: Duration,
    pulse_width: Duration,
    signal_duration: Duration,
) -> Result<(), Box<dyn Error>> {
    let pwm = Pwm::with_period(
        Channel::Pwm0,
        period,
        pulse_width,
        Polarity::Normal,
        false, // not yet enabled
    )?;

    // Save the Pwm object where it can be used from multiple threads
    let pwm = Arc::new(Mutex::new(pwm));

    // Set up signal handler so that we turn PWM off even if program is killed
    // early with Ctrl-C.
    // Note: Every time simple_signal::set_handler() is called it launches a new
    // background thread. And it can never be deactivated.  This is only
    // acceptable because the process exits right after this function returns
    // anyway. If this program ever becomes more complicated (like, goes on to
    // do anything else after this function returns) then we need to switch to
    // the signal-hook crate:
    // https://docs.rs/signal-hook/0.3.9/signal_hook/
    simple_signal::set_handler(&[Signal::Int, Signal::Term], {
        let pwm = pwm.clone();
        move |signals| {
            println!("\nCaught signals: {:?}", signals);
            if let Ok(pwm) = pwm.lock() {
                println!("Turning off PWM signal");
                let result = pwm.disable();
                if result.is_err() {
                    eprintln!("Error turning off PWM signal");
                    std::process::exit(1);
                }
            } else {
                eprintln!("Error getting lock on Pwm object");
                std::process::exit(1);
            }
            println!("Quitting.");
            std::process::exit(0);
        }
    });

    // Turn on the PWM signal
    if let Ok(pwm) = pwm.lock() {
        pwm.enable()?;
    } else {
        return Err(Box::new(std::io::Error::new(
            std::io::ErrorKind::Other,
            "Could not enable PWM: Could not get lock on Pwm object",
        )));
    }

    // Let the PWM signal run a while
    thread::sleep(signal_duration);

    // Turn off the PWM signal
    if let Ok(pwm) = pwm.lock() {
        pwm.disable()?;
    } else {
        return Err(Box::new(std::io::Error::new(
            std::io::ErrorKind::Other,
            "Could not disable PWM: Could not get lock on Pwm object",
        )));
    }

    Ok(())
}

fn main() -> Result<(), Box<dyn Error>> {
    println!(
        "Pulse Generator - running on a {}",
        DeviceInfo::new()?.model()
    );

    match Commands::from_args() {
        Commands::Sleep(args) => gen_pulse_with_sleep(
            args.pulse_width.as_duration(),
            args.num_pulses,
        )?,
        Commands::Spin(args) => {
            gen_pulse_with_spin(args.num_loops, args.num_pulses)?
        }
        Commands::Pwm(args) => gen_pwm(
            args.period.as_duration(),
            args.pulse_width.as_duration(),
            args.signal_duration.as_duration(),
        )?,
    }

    println!("Done.");
    Ok(())
}
