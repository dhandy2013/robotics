use rppal::system::DeviceInfo;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    println!(
        "Hello, I see I'm running on a {}",
        DeviceInfo::new()?.model()
    );
    Ok(())
}
