#!/usr/bin/python3
# serpipe.py
"""
Send to/from a file or standard input/output over a serial connection.
Handles flow control and ensures the data is not corrupted in transit.

Usage:

Transfer file from local machine to remote machine:
    1. On remote machine: serpipe.py get-tty OUTPUT-FILENAME
    2. On local machine:  serpipe.py put INPUT-FILENAME SERIAL-PORT

Transfer file from remote machine to local machine:
    1. On remote machine: serpipe.py put-tty INPUT-FILENAME
    2. On local machine:  serpipe.py get OUTPUT-FILENAME SERIAL-PORT

OUTPUT-FILENAME/INPUT-FILENAME:
    The name of a disk file to write or read, or "-" for standard
    output/input.

SERIAL-PORT:
    The name of a configured serial device.
    Typical value on Linux: /dev/ttyUSB0
    Typical value on Windows: COM4

Examples:

Transfer a file named README.txt from a laptop computer running Linux to a
Raspberry Pi computer connected via a USB-to-TTL-Serial Cable::

    # Connect to the Raspberry Pi and run receive command
    $ picocom --baud 115200 /dev/ttyUSB0
    $ python serpipe.py get-tty README.txt

    # Disconnect from Raspberry Pi and run put command
    Ctrl-A Ctrl-Q # quit picocom, leaving serial port configured
    $ python serpipe.py put README.txt /dev/ttyUSB0

Transfer file output.log from the Raspberry Pi to the laptop computer::

    # Connect to the Raspberry Pi and run send command
    $ picocom --baud 115200 /dev/ttyUSB0
    $ python serpipe.py put-tty output.log

    # Disconnect from Raspberry Pi and run get command
    Ctrl-A Ctrl-Q # quit picocom, leaving serial port configured
    # Note: file name doesn't have to be the same as the original
    $ python serpipe.py get output-2.log /dev/ttyUSB0

Both the local and remote instances of serpipe.py will exit when the file
has been transferred.
"""

import ast
import binascii
import collections
import inspect
import logging
import os
import socket
import sys
try:
    # Only on Linux
    import termios
except ImportError:
    termios = None

try:
    # Required on Windows, optional on Linux
    import serial
except ImportError:
    serial = None


def open_serial_port(serial_port, **kwparams):
    """
    Open a serial port, or make a TCP port connection to simulate a serial
    port.

    -------------------     ------------------
    serial_port pattern     Type of connection
    -------------------     ------------------
    /dev/tty*               TTYWrapper
    tty*                    serial.Serial
    COM*                    serial.Serial
    host:port               TCPWrapper
    :port                   TCPWrapper
    -------------------     ------------------

    -------------------     ------------------
    Type of connection      kwparams options
    -------------------     ------------------
    serial.Serial           baudrate=int_baud, timeout=float_seconds
    TCPWrapper              listen=False, timeout=float_seconds
    """
    if serial_port.startswith('/dev/tty'):
        return TTYWrapper(serial_port)
    if ':' in serial_port:
        host, port = serial_port.rsplit(':', 1)
        port = int(port)
        listen = kwparams.get('listen', False)
        timeout = float(kwparams.get('timeout', 5.0))
        if isinstance(listen, str):
            listen = ast.literal_eval(listen)
        return TCPWrapper((host, port), listen=listen, timeout=timeout)
    # At this point assume it really is a serial port name
    if serial is None:
        raise RuntimeError("pyserial is not installed, cannot open {}"
                           .format(serial_port))
    if serial_port.startswith('tty'):
        # Convert linux tty port name to device name
        serial_port = '/dev/' + serial_port
    baudrate = int(kwparams.get('baudrate', 115200))
    timeout = float(kwparams.get('timeout', 1.0))
    return serial.Serial(serial_port, baudrate=baudrate, timeout=timeout)


class IOWrapper:
    """
    A base class for file-like objects.
    I would use io.IOBase but the docs don't give good enough guidance how
    to extend it, and the source code is in C, not Python.
    """

    # Maximum number of bytes to read that can be passed to os.read() without
    # getting an OverflowError exception. Found by trial-and-error on an
    # x86_64 system.
    maxsize = (2**31) - 1

    # This is a limit to the size of bytes we will attempt to pass to
    # os.read(). We're not using maxsize because os.read() might be trying
    # to allocate a buffer of this size each time it is called.
    max_blocksize = 1024 * 8

    def __init__(self):
        self.input_buf = collections.deque()

    def _get_from_buf(self, n):
        # Remove and return n byte from the input buffer.
        # Remove and return all of the bytes in the input buffer if n is
        # greater than the number of bytes in the buffer.
        result = []
        nbytes = 0
        while (n > nbytes) and self.input_buf:
            data = self.input_buf.popleft()
            if len(data) > (n - nbytes):
                chunk = data[:(n - nbytes)]
                result.append(chunk)
                nbytes += len(chunk)
                self.input_buf.appendleft(data[len(chunk):])
            else:
                result.append(data)
                nbytes += len(data)
        return b''.join(result)

    def _add_to_buf(self, data):
        self.input_buf.append(data)

    def _put_back_in_buf(self, data):
        self.input_buf.appendleft(data)

    def _read(self, n):
        """
        Perform a single blocking read of up to n bytes.
        """
        raise NotImplementedError()

    def _write(self, data):
        """
        Perform a single blocking write operation.
        Return the number of bytes of data that were actually written, which
        may be less than len(data).
        """
        raise NotImplementedError()

    def close(self):
        """
        Calling this method closes the connection. No more I/O operations
        can be performed after that.
        """
        raise NotImplementedError()

    @property
    def closed(self):
        """Return True iff close() has been called."""
        raise NotImplementedError()

    def fileno(self):
        """
        Return the underlying connection's file number if it is available.
        """
        raise NotImplementedError()

    def flush(self):
        # Output is not buffered, so this method does nothing
        pass

    def isatty(self):
        return True

    def read(self, n=-1):
        """
        Read n bytes and return them as a bytes object.
        Keep on trying until all n bytes are read, or until EOF, whichever
        comes first. If n is < 0, read all bytes until EOF.
        """
        if n < 0:
            n = self.maxsize
        blocksize = min(n, self.max_blocksize)
        nbytes = 0
        result = []
        while nbytes < n:
            data_read_size = min(blocksize, n - nbytes)
            data = self.read1(data_read_size)
            if not data:
                break
            result.append(data)
            nbytes += len(data)
        result_bytes = b''.join(result)
        assert len(result_bytes) <= n
        return result_bytes

    def read1(self, n=-1):
        """
        Read at most n bytes, returning them as a bytes object.
        If n bytes are already available in the input buffer, remove them
        from the input buffer. Otherwise, perform at most 1 read on the
        underlying device. If n is not given or less than zero, then
        TTYWrapper.max_blocksize is used.
        """
        if n < 0:
            n = self.max_blocksize
        nbytes = 0
        result = []
        data = self._get_from_buf(n)
        if data:
            result.append(data)
            nbytes += len(data)
        if n > nbytes:
            data = self._read(n - nbytes)
            # It seems like sometimes we get back more bytes than requested
            extra = len(data) - (n - nbytes)
            if extra > 0:
                self._put_back_in_buf(data[extra:])
                data = data[:extra]
            result.append(data)
        result_bytes = b''.join(result)
        assert len(result_bytes) <= n
        return result_bytes

    def readable(self):
        return not self.closed()

    def readline(self, limit=-1):
        """
        Read bytes and buffer them until the newline byte (0x0A) is read or
        the limit is reached (if limit is non-negative), or EOF, whichever
        happens first. Then return all the bytes read up to and including
        the newline byte as a bytes object, retaining any extra bytes in the
        input buffer.
        """
        if limit < 0:
            limit = self.maxsize
        blocksize = min(limit, self.max_blocksize)
        result = []
        nbytes = 0
        while nbytes < limit:
            data_read_size = min(blocksize, limit - nbytes)
            data = self.read1(data_read_size)
            if not data:
                break
            i = data.find(b'\n')
            if i < 0:
                result.append(data)
                nbytes += len(data)
                continue
            result.append(data[:i+1])
            if i < (len(data) - 1):
                self._put_back_in_buf(data[i+1:])
            break
        return b''.join(result)

    def seekable(self):
        return False

    def writable(self):
        return not self.closed()

    def write(self, data):
        """
        Write the bytes in data to the device.
        Keep trying till all the bytes are written.
        """
        nbytes = 0
        while nbytes < len(data):
            n = self._write(data[nbytes:])
            nbytes += n
        return nbytes


class TTYWrapper(IOWrapper):
    """
    A file-like object wrapper around a "TTY device" like /dev/tty.
    This works on Linux, but not on Windows.

    The TTYWrapper class is needed because the open() function won't let me
    open a TTY device for simultaneous read/write access, and the
    os.fdopen() function won't let me do it either. In both cases I get this
    error: io.UnsupportedOperation: File or stream is not seekable.

    See:
        http://bugs.python.org/issue20074
        http://bugs.python.org/issue18116
    """

    def __init__(self, tty_name):
        super(TTYWrapper, self).__init__()
        if not termios:
            raise OSError('Cannot open TTY device "{0}": no termios module'
                            .format(tty_name))
        if isinstance(tty_name, int):
            self.name = "<file descriptior {0}>".format(tty_name)
            self.fd = tty_name
        else:
            self.name = tty_name
            flags = os.O_RDWR
            if hasattr(os, 'O_NOCTTY'):
                flags |= os.O_NOCTTY
            self.fd = os.open(tty_name, flags)
        # Save previous TTY state
        self.prev_tcattr = termios.tcgetattr(self.fd)
        # Turn off echo
        new = self.prev_tcattr[:]
        new[3] &= ~termios.ECHO  # 3 == 'lflags'
        self.tcsetattr_flags = termios.TCSAFLUSH
        if hasattr(termios, 'TCSASOFT'):
            self.tcsetattr_flags |= termios.TCSASOFT
        try:
            termios.tcsetattr(self.fd, self.tcsetattr_flags, new)
        except termios.error as e:
            self.close()
            raise e

    def _read(self, n):
        """
        Perform a single blocking read of up to n bytes.
        """
        return os.read(self.fd, n)

    def _write(self, data):
        """
        Perform a single blocking write operation.
        Return the number of bytes of data that were actually written, which
        may be less than len(data).
        """
        return os.write(self.fd, data)

    def close(self):
        if self.fd is None:
            return
        try:
            try:
                termios.tcsetattr(self.fd, self.tcsetattr_flags,
                                  self.prev_tcattr)
                self.flush()  # issue7208
            except termios.error:
                pass
            os.close(self.fd)
        finally:
            self.fd = None

    @property
    def closed(self):
        return (self.fd is None)

    def fileno(self):
        return self.fd


class TCPWrapper(IOWrapper):
    """
    A file-like object wrapper around a TCP connection.
    Mostly for testing purposes.
    """

    def __init__(self, addr, listen=False, timeout=5.0):
        super(TCPWrapper, self).__init__()
        if listen:
            server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            server_sock.bind(addr)
            server_sock.listen(1)
            self.sock, client_addr = server_sock.accept()
            server_sock.close() # Not accepting any more connections
        else:
            self.sock = socket.create_connection(addr, timeout)

    def _read(self, n):
        """
        Perform a single blocking read of up to n bytes.
        """
        try:
            return self.sock.recv(n)
        except socket.timeout:
            return b''

    def _write(self, data):
        """
        Perform a single blocking write operation.
        Return the number of bytes of data that were actually written, which
        may be less than len(data).
        """
        return self.sock.send(data)

    def close(self):
        if self.sock is None:
            return
        try:
            self.sock.close()
        finally:
            self.sock = None

    @property
    def closed(self):
        return (self.sock is None)

    def fileno(self):
        return self.sock.fileno()


#
# Data transfer protocol
#

# A message consists of 79 or less non-newline bytes from the ASCII
# character set, followed by a newline character (0x0A).
# The sender sends at most one message, then waits for an acknowledgement
# message before sending another message.

# Messages:
# "OK" - acknowledgement message
# "DATA <64-bytes-of-base64-encoded-data> <8-hex-bytes-cumulative-CRC>"
# "EOF <8-hex-bytes-total-CRC>"

# Send only 48 binary bytes at a time in a DATA message.
# After conversion to base64, and adding command, CRC and whitespace, one
# line == 79 bytes. We need to keep the maximum line size at 80 bytes or
# less.
blocksize = 48

def read_msg(tty):
    return tty.readline(80).split()

def send_msg(tty, msg):
    first_chunk = True
    for chunk in msg:
        if first_chunk:
            first_chunk = False
        else:
            tty.write(b' ')
        tty.write(chunk)
    tty.write(b'\n')
    tty.flush()

def expect_msg(tty, expected_msg_type):
    while True:
        msg = read_msg(tty)
        if msg:
            break
    if msg[0] != expected_msg_type:
        raise IOError("Bad message: "
                      "Expected message type {0!r}, got message: {1!r}"
                      .format(expected_msg_type, msg))
    return msg

def put_data_stream(infile, tty):
    def _format_crc(crc):
        return bytes('%08X' % crc, 'ascii')
    crc = 0
    while True:
        data = infile.read(blocksize)
        if not data:
            break
        crc = binascii.crc32(data, crc)
        b64_data = binascii.b2a_base64(data).rstrip()
        msg = [b'DATA', b64_data, _format_crc(crc)]
        send_msg(tty, msg)
        expect_msg(tty, b'OK')
    send_msg(tty, [b'EOF', _format_crc(crc)])
    expect_msg(tty, b'OK')

def get_data_stream(tty, outfile):
    crc = 0
    while True:
        msg = read_msg(tty)
        if not msg:
            # Could have extra newline or whitespace at start of stream
            continue
        if msg[0] == b'DATA':
            if len(msg) < 3:
                raise IOError("Bad DATA message format: {0!r}".format(msg))
            data = msg[1]
            data = binascii.a2b_base64(data)
            crc = binascii.crc32(data, crc)
            received_crc = int(msg[2], 16)
            if received_crc != crc:
                raise IOError("Bad DATA CRC")
            outfile.write(data)
            send_msg(tty, [b'OK'])
            continue
        if msg[0] == b'EOF':
            if len(msg) < 2:
                raise IOError("Bad EOF message format: {0!r}".format(msg))
            received_crc = int(msg[1], 16)
            if received_crc != crc:
                raise IOError("Bad EOF CRC")
            outfile.flush()
            send_msg(tty, [b'OK'])
            break
        raise IOError("Bad message: {0!r}".format(msg))


def kwparams_from_params(params):
    kwparams = {}
    for param in params:
        name, _, value = param.partition('=')
        kwparams[name] = value
    return kwparams


class SerpipeApp:

    def __init__(self):
        pass

    def cmd_get_tty(self, output_filename, tty_name='/dev/tty'):
        logging.info('Opening tty: %s', tty_name)
        tty = open_serial_port(tty_name, listen=True)
        outfile = None
        using_stdout = False
        try:
            logging.info('Waiting for RTS')
            expect_msg(tty, b'RTS')
            logging.info('Sending CTS')
            send_msg(tty, [b'CTS'])
            if output_filename == '-':
                using_stdout = True
                logging.info('Using standard output')
                outfile = sys.stdout.buffer
            else:
                logging.info('Opening output file: %s', output_filename)
                outfile = open(output_filename, 'wb')
            logging.info('Beginning data streaming')
            get_data_stream(tty, outfile)
        finally:
            logging.info('Closing tty: %s', tty_name)
            tty.close()
            if outfile and not using_stdout:
                logging.info('Closing output file: %s', output_filename)
                outfile.close()
        return 0

    def cmd_put_tty(self, input_filename, tty_name='/dev/tty'):
        logging.info('Opening tty: %s', tty_name)
        tty = open_serial_port(tty_name, listen=True)
        infile = None
        using_stdin = False
        try:
            logging.info('Waiting for RTR')
            expect_msg(tty, b'RTR')
            logging.info('Sending CTR')
            send_msg(tty, [b'CTR'])
            if input_filename == '-':
                using_stdin = True
                logging.info('Using standard input')
                infile = sys.stdin.buffer
            else:
                logging.info('Opening input file: %s', input_filename)
                infile = open(input_filename, 'rb')
            logging.info('Beginning data streaming')
            put_data_stream(infile, tty)
        finally:
            logging.info('Closing tty: %s', tty_name)
            tty.close()
            if infile and not using_stdin:
                logging.info('Closing input file: %s', input_filename)
                infile.close()
        return 0

    def cmd_get(self, output_filename, serial_port, *params):
        print('Opening serial port:', serial_port, file=sys.stderr)
        kwparams = kwparams_from_params(params)
        tty = open_serial_port(serial_port, **kwparams)
        outfile = None
        using_stdout = False
        try:
            print('Sending RTR', file=sys.stderr)
            send_msg(tty, [b'RTR'])
            print('Waiting for CTR', file=sys.stderr)
            expect_msg(tty, b'CTR')
            if output_filename == '-':
                using_stdout = True
                outfile = sys.stdout.buffer
            else:
                outfile = open(output_filename, 'wb')
            print('Initiating data streaming', file=sys.stderr)
            get_data_stream(tty, outfile)
        finally:
            print('Closing serial port:', serial_port, file=sys.stderr)
            tty.close()
            if outfile and not using_stdout:
                print('Closing output file:', output_filename, file=sys.stderr)
                outfile.close()
        return 0

    def cmd_put(self, input_filename, serial_port, *params):
        print('Opening serial port:', serial_port, file=sys.stderr)
        kwparams = kwparams_from_params(params)
        tty = open_serial_port(serial_port, **kwparams)
        infile = None
        using_stdin = False
        try:
            print('Sending RTS', file=sys.stderr)
            send_msg(tty, [b'RTS'])
            print('Waiting for CTS', file=sys.stderr)
            expect_msg(tty, b'CTS')
            if input_filename == '-':
                using_stdin = True
                infile = sys.stdin.buffer
            else:
                infile = open(input_filename, 'rb')
            print('Initiating data streaming', file=sys.stderr)
            put_data_stream(infile, tty)
        finally:
            print('Closing serial port:', serial_port, file=sys.stderr)
            tty.close()
            if infile and not using_stdin:
                print('Closing input file:', input_filename, file=sys.stderr)
                infile.close()
        return 0


def get_min_max_nargs(method_or_function):
    """
    method_or_function: A method or function object
    Return (min_nargs, max_nargs)
    min_nargs: minimum number of positional arguments
               (parameters) required by method or function.
    max_nargs: maximum number of positional arguments that can be passed to
               the method.
   The numbers returned do not include the leading 'self' argument
   implicitly passed to methods when they are called.
    """
    argspec = inspect.getfullargspec(method_or_function)
    max_nargs = len(argspec.args)
    if argspec.defaults:
        ndefaults = len(argspec.defaults)
    else:
        # argspec.defaults could be None
        ndefaults = 0
    min_nargs = max_nargs - ndefaults
    if inspect.ismethod(method_or_function):
        min_nargs -= 1
        max_nargs -= 1
    return (min_nargs, max_nargs)


def main():
    if len(sys.argv) <= 1:
        print(__doc__)
        return 1
    #logfile = '/tmp/serpipe.log'
    logfile = os.devnull
    logging.basicConfig(filename=logfile, level=logging.INFO)
    command = sys.argv[1]
    methodname = 'cmd_' + command.lower().replace('-', '_')
    args = sys.argv[2:]
    app = SerpipeApp()
    method = getattr(app, methodname, None)
    if not method:
        print("Unknown command:", command, file=sys.stderr)
        return 1
    min_nargs, max_nargs = get_min_max_nargs(method)
    if len(args) < min_nargs:
        print("Missing required parameters", file=sys.stderr)
        return 1
    if len(args) > max_nargs:
        print("Too many parameters", file=sys.stderr)
        return 1
    return method(*args)


if __name__ == '__main__':
    sys.exit(main())

# end-of-file
