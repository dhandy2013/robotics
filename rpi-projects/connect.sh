#!/bin/bash -x
# Connect to the Raspberry Pi over the serial port with the correct speed.

# Quit picocom      : Ctrl-A Ctrl-Q
# Send a file       : Ctrl-A Ctrl-S
# Receive a file    : Ctrl-A Ctrl-R
# Interrupt transfer: Ctrl-C

# Note: Sending/receiving files doesn't work for me.

exec picocom \
    --baud 115200 \
    --send-cmd "lsz -vv" \
    --receive-cmd "lrz -vv" \
    /dev/ttyUSB0
