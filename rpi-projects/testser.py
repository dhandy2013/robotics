#!/usr/bin/python3
# testser.py
"""
Test serial port communication.
"""

import ast
import collections
import os
import socket
import sys

import serial

this_module = sys.modules[__name__]


def open_serial_port(serial_port, **kwparams):
    """
    Open a serial port, or make a TCP port connection to simulate a serial
    port.

    -------------------     ------------------
    serial_port pattern     Type of connection
    -------------------     ------------------
    /dev/tty*               TTYWrapper
    tty*                    serial.Serial
    COM*                    serial.Serial
    host:port               TCPWrapper
    :port                   TCPWrapper
    -------------------     ------------------
    
    -------------------     ------------------
    Type of connection      kwparams options
    -------------------     ------------------
    serial.Serial           baudrate=int_baud, timeout=float_seconds
    TCPWrapper              listen=False, timeout=float_seconds
    """
    if serial_port.startswith('/dev/tty'):
        return TTYWrapper(serial_port)
    if ':' in serial_port:
        host, port = serial_port.rsplit(':', 1)
        port = int(port)
        listen = kwparams.get('listen', False)
        timeout = float(kwparams.get('timeout', 5.0))
        if isinstance(listen, str):
            listen = ast.literal_eval(listen)
        return TCPWrapper((host, port), listen=listen, timeout=timeout)
    # At this point assume it really is a serial port name
    if serial_port.startswith('tty'):
        # Convert linux tty port name to device name
        serial_port = '/dev/' + serial_port
    baudrate = int(kwparams.get('baudrate', 115200))
    timeout = float(kwparams.get('timeout', 1.0))
    return serial.Serial(serial_port, baudrate=baudrate, timeout=timeout)


class IOWrapper:
    """
    A base class for file-like objects.
    I would use io.IOBase but the docs don't give good enough guidance how
    to extend it, and the source code is in C, not Python.
    """

    # Maximum number of bytes to read that can be passed to os.read() without
    # getting an OverflowError exception. Found by trial-and-error on an
    # x86_64 system.
    maxsize = (2**31) - 1

    # This is a limit to the size of bytes we will attempt to pass to
    # os.read(). We're not using maxsize because os.read() might be trying
    # to allocate a buffer of this size each time it is called.
    max_blocksize = 1024 * 8

    def __init__(self):
        self.input_buf = collections.deque()

    def _get_from_buf(self, n):
        # Remove and return n byte from the input buffer.
        # Remove and return all of the bytes in the input buffer if n is
        # greater than the number of bytes in the buffer.
        result = []
        nbytes = 0
        while (n > nbytes) and self.input_buf:
            data = self.input_buf.popleft()
            if len(data) > (n - nbytes):
                chunk = data[:(n - nbytes)]
                result.append(chunk)
                nbytes += len(chunk)
                self.input_buf.appendleft(data[len(chunk):])
            else:
                result.append(data)
                nbytes += len(data)
        return b''.join(result)

    def _add_to_buf(self, data):
        self.input_buf.append(data)

    def _put_back_in_buf(self, data):
        self.input_buf.appendleft(data)

    def _read(self, n):
        """
        Perform a single blocking read of up to n bytes.
        """
        raise NotImplementedError()

    def _write(self, data):
        """
        Perform a single blocking write operation.
        Return the number of bytes of data that were actually written, which
        may be less than len(data).
        """
        raise NotImplementedError()

    def close(self):
        """
        Calling this method closes the connection. No more I/O operations
        can be performed after that.
        """
        raise NotImplementedError()

    @property
    def closed(self):
        """Return True iff close() has been called."""
        raise NotImplementedError()

    def fileno(self):
        """
        Return the underlying connection's file number if it is available.
        """
        raise NotImplementedError()

    def flush(self):
        # Output is not buffered, so this method does nothing
        pass

    def isatty(self):
        return True

    def read(self, n=-1):
        """
        Read n bytes and return them as a bytes object.
        Keep on trying until all n bytes are read, or until EOF, whichever
        comes first. If n is < 0, read all bytes until EOF.
        """
        if n < 0:
            n = self.maxsize
        blocksize = min(n, self.max_blocksize)
        nbytes = 0
        result = []
        while nbytes < n:
            data_read_size = min(blocksize, n - nbytes)
            data = self.read1(data_read_size)
            if not data:
                break
            result.append(data)
            nbytes += len(data)
        result_bytes = b''.join(result)
        assert len(result_bytes) <= n
        return result_bytes

    def read1(self, n=-1):
        """
        Read at most n bytes, returning them as a bytes object.
        If n bytes are already available in the input buffer, remove them
        from the input buffer. Otherwise, perform at most 1 read on the
        underlying device. If n is not given or less than zero, then
        TTYWrapper.max_blocksize is used.
        """
        if n < 0:
            n = self.max_blocksize
        nbytes = 0
        result = []
        data = self._get_from_buf(n)
        if data:
            result.append(data)
            nbytes += len(data)
        if n > nbytes:
            data = self._read(n - nbytes)
            # It seems like sometimes we get back more bytes than requested
            extra = len(data) - (n - nbytes)
            if extra > 0:
                self._put_back_in_buf(data[extra:])
                data = data[:extra]
            result.append(data)
        result_bytes = b''.join(result)
        assert len(result_bytes) <= n
        return result_bytes

    def readable(self):
        return not self.closed()

    def readline(self, limit=-1):
        """
        Read bytes and buffer them until the newline byte (0x0A) is read or
        the limit is reached (if limit is non-negative), or EOF, whichever
        happens first. Then return all the bytes read up to and including
        the newline byte as a bytes object, retaining any extra bytes in the
        input buffer.
        """
        if limit < 0:
            limit = self.maxsize
        blocksize = min(limit, self.max_blocksize)
        result = []
        nbytes = 0
        while nbytes < limit:
            data_read_size = min(blocksize, limit - nbytes)
            data = self.read1(data_read_size)
            if not data:
                break
            i = data.find(b'\n')
            if i < 0:
                result.append(data)
                nbytes += len(data)
                continue
            result.append(data[:i+1])
            if i < (len(data) - 1):
                self._put_back_in_buf(data[i+1:])
            break
        return b''.join(result)

    def seekable(self):
        return False

    def writable(self):
        return not self.closed()

    def write(self, data):
        """
        Write the bytes in data to the device.
        Keep trying till all the bytes are written.
        """
        nbytes = 0
        while nbytes < len(data):
            n = self._write(data[nbytes:])
            nbytes += n
        return nbytes


class TTYWrapper(IOWrapper):
    """
    A file-like object wrapper around a "TTY device" like /dev/tty.
    This works on Linux, but not on Windows.

    The TTYWrapper class is needed because the open() function won't let me
    open a TTY device for simultaneous read/write access, and the
    os.fdopen() function won't let me do it either. In both cases I get this
    error: io.UnsupportedOperation: File or stream is not seekable.

    See:
        http://bugs.python.org/issue20074
        http://bugs.python.org/issue18116
    """

    def __init__(self, tty_name):
        super(TTYWrapper, self).__init__()
        if isinstance(tty_name, int):
            self.name = "<file descriptior {0}>".format(tty_name)
            self.fd = tty_name
        else:
            self.name = tty_name
            flags = os.O_RDWR
            if hasattr(os, 'O_NOCTTY'):
                flags |= os.O_NOCTTY
            self.fd = os.open(tty_name, flags)

    def _read(self, n):
        """
        Perform a single blocking read of up to n bytes.
        """
        return os.read(self.fd, n)

    def _write(self, data):
        """
        Perform a single blocking write operation.
        Return the number of bytes of data that were actually written, which
        may be less than len(data).
        """
        return os.write(self.fd, data)

    def close(self):
        if self.fd is None:
            return
        try:
            os.close(self.fd)
        finally:
            self.fd = None

    @property
    def closed(self):
        return (self.fd is None)

    def fileno(self):
        return self.fd


class TCPWrapper(IOWrapper):
    """
    A file-like object wrapper around a TCP connection.
    Mostly for testing purposes.
    """

    def __init__(self, addr, listen=False, timeout=5.0):
        super(TCPWrapper, self).__init__()
        if listen:
            server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            server_sock.bind(addr)
            server_sock.listen(1)
            self.sock, client_addr = server_sock.accept()
            server_sock.close() # Not accepting any more connections
        else:
            self.sock = socket.create_connection(addr, timeout)

    def _read(self, n):
        """
        Perform a single blocking read of up to n bytes.
        """
        try:
            return self.sock.recv(n)
        except socket.timeout:
            return b''

    def _write(self, data):
        """
        Perform a single blocking write operation.
        Return the number of bytes of data that were actually written, which
        may be less than len(data).
        """
        return self.sock.send(data)

    def close(self):
        if self.sock is None:
            return
        try:
            self.sock.close()
        finally:
            self.sock = None

    @property
    def closed(self):
        return (self.sock is None)

    def fileno(self):
        return self.sock.fileno()


def test_port(tty):
    try:
        tty.write(b'echo "blah"\n')
        try:
            while True:
                data = tty.readline(80)
                if not data:
                    break
                print(data)
        except KeyboardInterrupt:
            print("\nQuit.")
    finally:
        tty.close()


def cmd_test_tty(tty_name='/dev/ttyUSB0'):
    print("Opening", tty_name)
    tty = TTYWrapper(tty_name)
    test_port(tty)


def cmd_test_serial_port(serial_port='COM4'):
    speed = 115200
    timeout = 1.0
    print("Opening", serial_port)
    tty = serial.Serial(serial_port, speed, timeout=timeout)
    test_port(tty)


def cmd_test_port(serial_port, *params):
    kwparams = {}
    for param in params:
        name, _, value = param.partition('=')
        kwparams[name] = value
    print("Opening", serial_port, "with kwparams={0!r}".format(kwparams))
    tty = open_serial_port(serial_port, **kwparams)
    test_port(tty)


def main():
    if len(sys.argv) <= 1:
        print('Missing command.')
        return 1
    command = sys.argv[1]
    function_name = 'cmd_' + command.lower().replace('-', '_')
    func = getattr(this_module, function_name, None)
    if not func:
        print("Unknown command:", command)
        return 1
    return func(*(sys.argv[2:]))


if __name__ == '__main__':
    sys.exit(main())

# end-of-file
