#!/usr/bin/python3
"""
Turn an LED on and off gradually using software PWM
"""

import math
import random
import sys
import time

import RPi.GPIO as GPIO


# Pin numbers from RPi point of view
output_pin = 12 # GPIO 18

# Timing
pulse_frequency = 2.0
update_interval = (1/pulse_frequency) / 50.0


def setup():
    # Set mode to use RPi board's pin numbers
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(output_pin, GPIO.OUT)


def cleanup():
    GPIO.output(output_pin, 0)
    GPIO.cleanup()


def sinusoidal_loop():
    duty_cycle = 0.0
    pwm = GPIO.PWM(output_pin, 2/update_interval)
    pwm.start(duty_cycle)
    t0 = time.time()
    pi2 = 2.0 * math.pi
    try:
        while True:
            t = time.time() - t0
            a = math.sin((t * pulse_frequency) * pi2)
            duty_cycle = 100.0 * abs((a + 1.0) / 2.0)
            pwm.ChangeDutyCycle(duty_cycle)
            time.sleep(update_interval)
    except KeyboardInterrupt:
        pwm.stop()
        print("\nStopped.")


def for_loop():
    pulse_period = 1 / pulse_frequency
    delta_duty_cycle = 100.0 * ((2 * update_interval) / pulse_period)
    pwm = GPIO.PWM(output_pin, 2/update_interval)
    duty_cycle = 0.0
    pwm.start(duty_cycle)
    t0 = time.time()
    try:
        while True:
            t = time.time() - t0
            pwm.ChangeDutyCycle(duty_cycle)
            duty_cycle += delta_duty_cycle
            if duty_cycle > 100.0:
                delta_duty_cycle *= -1.0
                duty_cycle = 100.0 + delta_duty_cycle
            elif duty_cycle < 0.0:
                delta_duty_cycle *= -1.0
                duty_cycle = delta_duty_cycle
            time.sleep(update_interval)
    except KeyboardInterrupt:
        pwm.stop()
        print("\nStopped.")


def main():
    setup()
    try:
        print("Pulsing LED connected to RPi pin", output_pin)
        #for_loop()
        sinusoidal_loop()
    finally:
        cleanup()

if __name__ == '__main__':
    sys.exit(main())

# end-of-file
