#!/bin/bash -x
# Copy a file from a laptop computer to a Raspberry Pi computer over a USB
# serial port.
#
# Run this command on the Raspberry Pi:
#   serpipe.py get-tty <filename>
#
# Run this command on the laptop computer:
#   put.sh <filename>

python3=$(which python3)
serial_port=ttyUSB0
$python3 serpipe.py put $1 $serial_port
